﻿Imports Common
Public Class Form1

   
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim sw As Stopwatch
        sw = Stopwatch.StartNew
        Dim s As New Service.Service

        Dim ListaMoviles = s.ObternerMovilesActualizacion
        ''''''''''''''''''''''''''''

        Dim idVehiculo As Integer : Dim idPago As Integer
        Try
            Dim d As New DataClasses1DataContext(getSettings("dbConnection"))
            For Each movil In ListaMoviles
                'actualizar administradores
                Try
                    Dim ListaAdmin As DataTable = Funciones.proc_AdministradoresLoadByPrimaryKey(movil.cedula).Tables(0)
                    If ListaAdmin.Rows.Count = 0 Then
                        d.proc_AdministradoresInsert(movil.nombre, movil.cedula, movil.telefono, (movil.fecha), movil.idUsuario)
                    Else
                        'update admin
                        d.proc_AdministradoresUpdate(movil.nombre, movil.cedula, movil.telefono, (movil.fecha), movil.idUsuario)
                    End If
                Catch ex As Exception

                End Try

                Try
                    'actualizar vehiculos
                    Dim listaVehiculos As DataTable = Funciones.proc_ObtenerVehiculoPorNroMovil(movil.NroMovil).Tables(0)

                    If listaVehiculos.Rows.Count = 0 Then
                        'crear movil
                        d.proc_VehiculosInsert(idVehiculo, movil.NroMovil, movil.Habilitado, movil.Modelo, movil.Placa, Nothing, Nothing, movil.EstadoSoat, movil.idUsuario, Nothing, _
                                             movil.fechaFinSancion, movil.TipoVehiculo, movil.AdminTemp, movil.AdminTemp2, movil.cedula, movil.fechaAfiliacion, _
                                                Nothing, Nothing, Nothing, Nothing)
                    Else
                        'update movil
                        d.proc_VehiculosUpdate(listaVehiculos.Rows(0)("idVehiculo"), movil.NroMovil, movil.Habilitado, movil.Modelo, movil.Placa, Nothing, Nothing, movil.EstadoSoat, movil.idUsuario, Nothing, _
                                               movil.fechaFinSancion, movil.TipoVehiculo, movil.AdminTemp, movil.AdminTemp2, movil.cedula, movil.fechaAfiliacion, _
                                                Nothing, Nothing, Nothing, Nothing)
                        idVehiculo = listaVehiculos.Rows(0)("idVehiculo")
                    End If
                Catch ex As Exception

                End Try


                Try
                    'actualizar pagosRF
                    If (movil.FechaPago) IsNot Nothing Then
                        Dim ListaPagos As DataTable = Funciones.proc_ObtenerPago(idVehiculo, movil.FechaPago).Tables(0)

                        If ListaPagos.Rows.Count = 0 Then
                            d.proc_PagosRFInsert(idPago, idVehiculo, movil.FechaPago, movil.FechaInhabilitacion, movil.Importe, movil.Usuario, Nothing, Nothing, Nothing)
                        Else
                            d.proc_PagosRFUpdate(ListaPagos.Rows(0)("idPago"), idVehiculo, movil.FechaPago, movil.FechaInhabilitacion, movil.Importe, movil.Usuario, Nothing, Nothing, Nothing)
                            idPago = ListaPagos.Rows(0)("idPago")
                        End If
                    End If
                Catch ex As Exception

                End Try

            Next
            MsgBox("listo")
        Catch ex As Exception
            
        Finally
            sw.Stop()
            MsgBox(sw.Elapsed.ToString)
        End Try

    End Sub
End Class
