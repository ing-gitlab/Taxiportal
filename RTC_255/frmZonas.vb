﻿Imports Common

Public Class frmZonas

#Region "Declaraciones"

    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))

#End Region



#Region "Eventos"

    Private Sub frmZonas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ObtenerZonas()
    End Sub

    Private Sub GridEX1_AddingRecord(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GridEX1.AddingRecord
        Dim id As Integer
        d.proc_ZonasInsert(id, GridEX1.GetValue("Descripcion"))
        Dim listaBarrios = d.proc_BarriosLoadAll
        For Each barrio In listaBarrios
            d.proc_TarifasInsert(id, barrio.idBarrio, NOW_GLOBAL, 0, USER_GLOBAL.idUsuario)
        Next

        ObtenerZonas()
    End Sub

    Private Sub GridEX1_DeletingRecords(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GridEX1.DeletingRecords
        Try
            If MsgBox("Está seguro que desea eliminar la zona?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                e.Cancel = True
                Exit Sub
            End If
            d.proc_ZonasDelete(GridEX1.GetValue("idZona"))
        Catch ex As Exception
            MsgBox(ex.Message)
            e.Cancel = True
        End Try
    End Sub

    Private Sub GridEX1_UpdatingRecord(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GridEX1.UpdatingRecord
        Try
            d.proc_ZonasUpdate(GridEX1.GetValue("idZona"), GridEX1.GetValue("Descripcion"))

        Catch ex As Exception
            MsgBox(ex.Message)
            e.Cancel = True
        End Try
    End Sub

#End Region


#Region "Metodos"

    Private Sub ObtenerZonas()
        GridEX1.SetDataBinding(d.proc_ZonasLoadAll.ToList, String.Empty)
        GridEX1.RetrieveStructure()
        GridEX1.RootTable.Columns("idZona").Visible = False

    End Sub

#End Region


 
End Class