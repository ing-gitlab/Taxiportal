﻿Imports Common
Imports System.IO
Imports System.Data
Imports System.Drawing.Imaging
Imports SQLImages

Public Class frmPersonas

#Region "Declaraciones"
    Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")
    Private esNuevo As Boolean = False
    Private numeroOriginal As String
    Private _idPersona As Integer
    Private Estado As String
    Private Filtro As Integer = 1
    Private Modelo As Integer
    Private placaOriginal As String
    Private _Seleccionar As Boolean = False
    Private _ObtenerPersona As Boolean = True
    Private _ModifiquePersona As Boolean = False
    Private _Persona As proc_PersonasLoadByPrimaryKeyResult
    Dim bmp As Bitmap                             'Guardar las imagenes en disco
    Private _ModoEdicion As Boolean = False
    Private _haAceptado As Boolean = False
    Private _Bandera As Integer
    Private _Operador As Boolean
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim modificar As Boolean = False
#End Region


#Region "EVENTOS"

    Private Sub frmPersonas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
        GridEX1.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic

        'Etiquetas para cada btn
        Me.ToolTip1.SetToolTip(BtnHistorial, "Historial")
        Me.ToolTip1.SetToolTip(BtnEliminar, "Eliminar Persona")
        Me.ToolTip1.SetToolTip(btnAhorro, "Pago Ahorros")
        Me.ToolTip1.SetToolTip(btnPrestamo, "Prestamos")
        Me.ToolTip1.SetToolTip(btnRecaudacion, "Recaudación")
        Me.ToolTip1.SetToolTip(btnMovimientos, "Movimientos")

        If _ObtenerPersona Then
            ObtenerEstados()
            ObtenerPersonas()
        End If

        Dim Usuario = d.proc_UsuariosLoadByPrimaryKey(USER_GLOBAL.idUsuario).ToList.First()
        If Usuario.idTipoUsuario <> 1 Then
            BnNuevo.Visible = False
            BtnEliminar.Visible = False
            BnCancelar.Visible = True
            BnGuardar.Visible = False
            _Operador = True
        End If


        ' obtenerMoviles()
    End Sub

    Private Sub GridEX1_RowDoubleClick(ByVal sender As Object, ByVal e As Janus.Windows.GridEX.RowActionEventArgs) Handles GridEX1.RowDoubleClick
        SeleccionarPersona()
    End Sub


    Private Sub GridEX1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.SelectionChanged
        Try
            If Me.GridEX1.GetRow.RowType = Janus.Windows.GridEX.RowType.FilterRow OrElse Me.GridEX1.RowCount = 0 Then Exit Sub

            Me.TxtNombre.Text = GridEX1.GetValue("Nombre")
            Me.TxtCedula.Text = GridEX1.GetValue("Cedula")
            'Me.chkSi.Checked = GridEX1.GetValue("Habilitado")
            Me.TxtDireccion.Text = GridEX1.GetValue("Direccion")
            Me.TxtTelefono.Text = GridEX1.GetValue("Telefono")
            Me._idPersona = GridEX1.GetValue("idPersona")
            Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            'cambiar el store para q no devuelva todos los campos
            Dim Cadete = d.proc_PersonasLoadByPrimaryKey(GridEX1.GetValue("idPersona")).ToList.First
            ObtenerCuentaAhorro()

            Dim img As New SQLImages.Main
            Dim aux() As Byte

            If Cadete.Imagen Is Nothing Then
                PbCadete.Image = Nothing
            Else
                aux = Cadete.Imagen.ToArray
                If aux.Length = 0 Then
                    PbCadete.Image = Nothing
                Else
                    PbCadete.Image = img.ByteToBitmap(aux)
                End If
            End If
            CboEstado_Tipo.SelectedValue = GridEX1.GetValue("idEstadoPersona")
        Catch ex As System.Exception
            'MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub CgFoto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CgFoto.Click
        Dim sqlimages As New SQLImages.Main
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then

            PbCadete.Image = Image.FromFile(OpenFileDialog1.FileName)
        End If
    End Sub

    Private Sub BnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BnCancelar.Click
        Me.txtAhorroObligatorio.Value = 0
        Me.txtAhorroAdicional.Value = 0
        Me.txtDeuda.Value = 0
        Me.iniciarEdicion(False)
        ObtenerPersonas()
        RbConductores.Enabled = True
        RbPropietarios.Enabled = True
        RbTodos.Enabled = True
        CboEstado_Tipo.Enabled = False
        'Me.obtenerMoviles()
        If _ModoEdicion Then
            Me.Close()
        End If
    End Sub

    Private Sub BnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BnNuevo.Click
        Me.txtAhorroObligatorio.Value = 0
        Me.txtAhorroAdicional.Value = 0
        Me.txtDeuda.Value = 0
        esNuevo = True
        iniciarEdicion(True)

        If RbTodos.Checked Then
            CboEstado_Tipo.Enabled = True
        End If

        TxtNombre.Text = ""
        TxtCedula.Text = ""
        TxtDireccion.Text = ""
        TxtTelefono.Text = ""

        Me.PbCadete.Image = My.Resources.nodisponible 'My.Resources.NoDisp.GetThumbnailImage(pbSoat.Width, pbSoat.Height, Nothing, Nothing)

    End Sub

    Private Sub BnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BnModificar.Click
        esNuevo = False
        iniciarEdicion(True)
        Me.TxtNombre.Focus()
        placaOriginal = TxtCedula.Text
        Me.numeroOriginal = Me.TxtDireccion.Text
        Me.AcceptButton = BnGuardar
     
        RbConductores.Enabled = False
        RbPropietarios.Enabled = False
        RbTodos.Enabled = False


    End Sub

    Private Sub BnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
        Try

            If MsgBox("¿Está seguro de eliminar esta Persona?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Confirmación de eliminación") = MsgBoxResult.Yes Then
                Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
                'd.proc_PersonasDelete(_idPersona)
                Dim prestamos = d.proc_obtenerPrestamosPorPersona(_idPersona).ToList

                For Each pre In prestamos
                    If Not pre.Finalizado Then

                        MsgBox("No se puede eliminar. Esta persona tiene prestamos pendientes")
                        Exit Sub
                    End If
                Next

                d.proc_EliminarPersona(_idPersona, 1)
                ObtenerPersonas()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub BnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BnGuardar.Click
        Try

            RbConductores.Enabled = True
            RbPropietarios.Enabled = True
            RbTodos.Enabled = True
            CboEstado_Tipo.Enabled = False

            'On Error Resume Next
            Dim imagenCadeteReducida(-1) As Byte
            Dim imagenCadete(-1) As Byte
            Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            'Dim ImagenAux As Image
            Dim img As New SQLImages.Main
            Dim idCadete As Integer
            Dim Posicion As Integer
            ' Dim Aux As System.Data.Linq.Binary

            If validar() Then
                If esNuevo Then

                    Dim nuevo = d.proc_PersonasInsert(idCadete, TxtNombre.Text, TxtCedula.Text, TxtDireccion.Text, TxtTelefono.Text, img.ImageToByte(PbCadete.Image.GetThumbnailImage(120, 160, Nothing, Nothing), PbCadete.Image.RawFormat), CboEstado_Tipo.SelectedValue, USER_GLOBAL.idUsuario, 0)
                    d.proc_CuentaAhorroInsert(idCadete, NOW_GLOBAL, Nothing, 0, 0, 0)


                Else
                    'modificar
                    Posicion = Me.GridEX1.GetRow().Position
                    ' MsgBox(CboEstado_Tipo.SelectedValue.ToString)
                    d.proc_PersonasUpdate(GridEX1.GetValue("idPersona"), TxtNombre.Text, TxtCedula.Text, TxtDireccion.Text, TxtTelefono.Text, img.ImageToByte(PbCadete.Image.GetThumbnailImage(120, 160, Nothing, Nothing), PbCadete.Image.RawFormat), CInt(CboEstado_Tipo.SelectedValue), USER_GLOBAL.idUsuario, 0)
                    'd.proc_PersonasUpdate(GridEX1.GetValue("idPersona"),
                    _ModifiquePersona = True
                    If _ModoEdicion Then
                        StartBroadcast(PORT_Principal, "RefrescarCadetes")
                        Me.Close()
                    End If

                End If

                iniciarEdicion(False)

                ObtenerPersonas()
                StartBroadcast(PORT_Principal, "RefrescarCadetes")
                If esNuevo Then
                    Posicion = Me.GridEX1.RowCount - 1
                End If
                Me.GridEX1.MoveTo(Posicion)
                If Me._ModoEdicion Then
                    _haAceptado = True
                    Me.Close()
                End If
            End If
            Me.AcceptButton = Nothing
            Me.GridEX1.Focus()
        Catch ex As Exception

        End Try



    End Sub

    Private Sub PbCadete_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PbCadete.DoubleClick
        If PbCadete.Image Is Nothing Then
            MsgBox("Imagen No Disponible", MsgBoxStyle.Information, "Aviso")

        Else
            Me.AbrirImagen(Me.PbCadete.Image)
        End If
    End Sub

    Private Sub BnHistorial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnHistorial.Click
        '_Persona = AbrirFormParaSeleccionar()

        Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
        Dim f As New frmHistorial
        f.abrirFormulario("Historial de Cadetes", d.proc_Historial_PersonasLoadByIdPersona(GridEX1.GetValue("idPersona")).ToList)
        f.GridEX1.RootTable.Columns("idHistorial_Personas").Visible = False
        f.GridEX1.RootTable.Columns("idPersona").Visible = False
        f.GridEX1.RootTable.Columns("tipo").Visible = False
        f.GridEX1.RootTable.Columns("nombre").Caption = "Nombre"
        f.GridEX1.RootTable.Columns("cedula").Caption = "Cedula"
        f.GridEX1.RootTable.Columns("direccion").Caption = "Dirección"
        f.GridEX1.RootTable.Columns("telefono").Caption = "Teléfono"
        f.GridEX1.RootTable.Columns("idUsuario").Caption = "Usuario"
        'f.GridEX1.RootTable.Columns("Habilitado").ColumnType = Janus.Windows.GridEX.ColumnType.CheckBox
        f.GridEX1.ColumnAutoResize = True


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAhorro.Click
        Dim f As New frmPagoAhorros
        f.AbrirFormConPersona(GridEX1.GetValue("idPersona"))
        ObtenerCuentaAhorro()
    End Sub

    Private Sub btnPrestamo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrestamo.Click
        Dim f As New frmPrestamo
        f.AbrirFormulario(GridEX1.GetValue("idPersona"), PbCadete.Image)
        ObtenerCuentaAhorro()
    End Sub

    Private Sub btnMovimientos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMovimientos.Click
        Dim f As New frmMovimientos
        f.AbrirForm(GridEX1.GetValue("idPersona"))
    End Sub

    Private Sub btnRecaudacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecaudacion.Click

        Try
            Dim f As New frmRecaudacion
            Dim vehiculo = d.proc_obtenerVehiculosPorConductor(GridEX1.GetValue("idPersona")).ToList.First
            ' f.ObtenerIdVehiculo(vehiculo.idVehiculo)
            Dim persona = d.proc_PersonasLoadByPrimaryKey(GridEX1.GetValue("idPersona")).ToList.First
            f.AbrirFrmRecaudacion(persona)
            f.ShowDialog()
        Catch ex As Exception
            MsgBox("Esta persona no es conductor de ningun vehiculo." & vbCrLf & "Por favor primero asígnelo como conductor en la ventana de Vehiculos y luego intente nuevamente.")
            'MsgBox("Esta Persona no tiene Recaudos")
        End Try

    End Sub

    Private Sub RbConductores_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbConductores.CheckedChanged
        Filtro = 1
        ObtenerPersonas()
       
    End Sub

    Private Sub RbPropietarios_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbPropietarios.CheckedChanged
        Filtro = 2
        ObtenerPersonas()

    End Sub

    Private Sub RbTodos_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbTodos.CheckedChanged
        Filtro = 3
        ObtenerPersonas()
    End Sub

#End Region

#Region "METODOS"

    Private Sub ObtenerEstados()
        CboEstado_Tipo.DataSource = d.proc_Estado_PersonaLoadAll()
        CboEstado_Tipo.DisplayMember = "Descripcion"
        CboEstado_Tipo.ValueMember = "idTipo"
    End Sub

    Private Sub ObtenerPersonas()


        Using d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            If Filtro = 3 Then
                Me.GridEX1.DataSource = d.proc_PersonasLoadAll()
            Else
                Me.GridEX1.DataSource = d.proc_ObtenerPersonasFiltradas(Filtro).ToArray
            End If


            Me.GridEX1.ColumnAutoResize = True
            Me.GridEX1.RetrieveStructure()
            Me.GridEX1.RootTable.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.False
            Me.GridEX1.RootTable.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False
            Me.GridEX1.RootTable.Columns("idPersona").Visible = False
            Me.GridEX1.RootTable.Columns("imagen").Visible = False
            Me.GridEX1.RootTable.Columns("idUsuario").Visible = False
            Me.GridEX1.RootTable.Columns("idEstadoPersona").Visible = False
            Me.GridEX1.RootTable.Columns("estado").Visible = False
            Me.GridEX1.RootTable.Columns("nombre").Caption = "Nombre"
            Me.GridEX1.RootTable.Columns("cedula").Caption = "Cédula"
            Me.GridEX1.RootTable.Columns("direccion").Caption = "Dirección"
            Me.GridEX1.RootTable.Columns("telefono").Caption = "Teléfono"
            Me.GridEX1.RootTable.Columns("idEstadoPersona").Caption = "Tipo Persona"
            'Dim cc As New Janus.Windows.GridEX.GridEXGroup(GridEX1.Tables(0).Columns("idEstadoPersona"))
            'Me.GridEX1.Tables(0).Columns("idEstadoPersona").Table.Groups.Add(cc)
            Me.lbCantidad.Text = "Cantidad: " & GridEX1.RowCount

            If GridEX1.RowCount = 0 Then
                If RbConductores.Checked Then
                    CboEstado_Tipo.SelectedValue = 1
                ElseIf RbPropietarios.Checked Then
                    CboEstado_Tipo.SelectedValue = 2
                End If
            End If

        End Using

    End Sub

    Private Sub iniciarEdicion(ByVal esInicio As Boolean)
        GridEX1.Enabled = Not esInicio
        If Not _Operador Then
            BnNuevo.Visible = Not esInicio

            TxtNombre.ReadOnly = Not esInicio
            TxtCedula.ReadOnly = Not esInicio
            TxtDireccion.ReadOnly = Not esInicio
            TxtTelefono.ReadOnly = Not esInicio
            CgFoto.Enabled = esInicio
            BnGuardar.Visible = esInicio
        End If
        BnModificar.Visible = Not esInicio
        BnCancelar.Visible = esInicio
        PbCadete.Enabled = esInicio
        btnAhorro.Enabled = Not esInicio
        BtnHistorial.Enabled = Not esInicio
        BtnEliminar.Enabled = Not esInicio
        btnPrestamo.Enabled = Not esInicio
        btnMovimientos.Enabled = Not esInicio
        btnRecaudacion.Enabled = Not esInicio
    End Sub

    Private Sub InicioSeleccionar(ByVal NO As Boolean)
        GridEX1.Enabled = Not NO
        BnNuevo.Visible = NO
        BtnEliminar.Visible = NO
        CgFoto.Enabled = NO
        BnGuardar.Visible = NO
        BnModificar.Visible = NO
        BnCancelar.Visible = NO
        PbCadete.Enabled = NO

    End Sub

    Private Sub AbrirImagen(ByVal Imagen As Image)

        bmp = Imagen
        bmp.Save(My.Application.Info.DirectoryPath & "\imagen.jpg")
        Shell("rundll32.exe " & Environment.GetEnvironmentVariable("WINDIR") & "\system32\shimgvw.dll,ImageView_Fullscreen " & My.Application.Info.DirectoryPath & "\imagen.jpg")
    End Sub

    Private Function ValidarNombre()
        If TxtNombre.Text = "" Then
            MsgBox("Debe ingresar el nombre", MsgBoxStyle.Critical, "Error")
            TxtNombre.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidarCedula()
        Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
        If TxtCedula.Text = "" Then
            MsgBox("Debe ingresar la cédula", MsgBoxStyle.Critical, "Error")
            TxtCedula.Focus()
            Return False
        End If
        Dim i = d.proc_ObtenerPersonasPorCedula(TxtCedula.Text).ToList
        If i.Count > 0 And TxtCedula.Text <> GridEX1.GetValue("Cedula") Then
            MsgBox("No se puede tener dos Cédulas Idénticas")
            Return False
        End If

        Return True
    End Function

    Private Function ValidarDireccion()
        If TxtDireccion.Text = "" Then
            MsgBox("Debe ingresar la dirección", MsgBoxStyle.Critical, "Error")
            TxtDireccion.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidarTelefono()
        If TxtTelefono.Text = "" Then
            MsgBox("Debe ingresar el Teléfono", MsgBoxStyle.Critical, "Error")
            TxtTelefono.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function validar()
        If Not ValidarNombre() Then
            Return False
        End If
        If Not ValidarCedula() Then
            Return False
        End If
        If Not ValidarDireccion() Then
            Return False
        End If

        If Not ValidarTelefono() Then
            Return False
        End If

        Return True
    End Function

    Public Function AbrirFormParaEdicion(ByVal idPersona As Integer, ByVal Seleccion As Boolean) As Boolean
        ObtenerPersonas()
        GridEX1.Find(GridEX1.RootTable.Columns("idPersona"), Janus.Windows.GridEX.ConditionOperator.Equal, idPersona, 0, 1)
        iniciarEdicion(True)
        _ObtenerPersona = False
        _ModoEdicion = True
        'Me.ShowDialog()

        Return _ModifiquePersona
    End Function

    Public Function AbrirFormParaSeleccionar() As proc_PersonasLoadByPrimaryKeyResult
        ObtenerPersonas()
        _Seleccionar = True
        _ObtenerPersona = False
        _ModoEdicion = False
        InicioSeleccionar(False)
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False

        'Me.ShowDialog()

        Return _Persona
    End Function

    Private Sub SeleccionarPersona()
        Try
            If _Seleccionar Then
                _Persona = d.proc_PersonasLoadByPrimaryKey(GridEX1.GetValue("IdPersona")).ToList.First
                Me.Close()
            End If
        Catch ex As Exception

        End Try


    End Sub

    Private Sub ObtenerCuentaAhorro()
        Try
            Dim CTA = d.proc_ObtenerCuentaAhorro(Int32.Parse(GridEX1.GetValue("idPersona"))).ToList.First


            Me.txtAhorroObligatorio.Value = CTA.importeAhorro
            Me.txtAhorroAdicional.Value = CTA.importeAhorroAdicional
            Me.txtDeuda.Value = CTA.importeDeuda

        Catch ex As Exception
            Me.txtAhorroObligatorio.Value = 0
            Me.txtAhorroAdicional.Value = 0
            Me.txtDeuda.Value = 0

        End Try

    End Sub

#End Region


End Class