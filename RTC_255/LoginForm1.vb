﻿Imports Common
Public Class LoginForm1

#Region "Declaraciones"

    Dim _Cerrar As Boolean
    Dim _CantidadIntentos As Int16 = 0
    Dim d As DataClasses1DataContext

#End Region

#Region "Eventos"

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        '  Clipboard.SetText(Environment.CurrentDirectory)

        If srv2.Checked Then
            CNNSTR_GLOBAL = "Redundante"

        Else
            CNNSTR_GLOBAL = ""
        End If
        d = New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
        Try
            Dim u = d.proc_UsuariosLoadByPrimaryKey(UsernameTextBox.Text).ToList
            If u.Count > 0 Then
                If u(0).password.Equals(PasswordTextBox.Text) Then
                    _Cerrar = True
                    USER_GLOBAL = u.First
                    d.proc_LogInsert(NOW_GLOBAL, USER_GLOBAL.idUsuario, String.Empty, "Login")
                    CONFIG_GLOBAL = d.proc_ConfiguracionesLoadByPrimaryKey(USER_GLOBAL.idUsuario).ToList.First
                    ' NOW_GLOBAL = d.proc_GetDate.ToList(0).Now

                    Dim s As String = getSettings("cad")
                    If s <> "0" Then
                        Dim d As Date = Date.Parse(s)
                        If NOW_GLOBAL() >= d Then
                            MsgBox("Caducó la licencia. Póngase en Contacto con el Administrador del Sistema")
                            End
                        End If
                    End If

                    Close()

                Else
                    MsgBox("Password incorrecto")

                    d.proc_LogInsert(USER_GLOBAL.idUsuario, "intento: " & _CantidadIntentos, Now, "Password incorrecto.")
                    PasswordTextBox.Focus()
                    PasswordTextBox.SelectAll()
                    _CantidadIntentos += 1
                End If
            Else
                MsgBox("Usuario inexistente")
                UsernameTextBox.Focus()
                UsernameTextBox.SelectAll()
                _CantidadIntentos += 1
            End If
            If _CantidadIntentos > 2 Then
                MsgBox("Cantidad de intentos de ingreso superada. El sistema se cerrará")
                End

            End If
        Catch ex As Exception
            MsgBox("Error de Conexión", MsgBoxStyle.Critical)
            ' d.proc_LogInsert("System", ex.Message, Now, "Error en Login")
        End Try

    End Sub

    Private Sub LoginForm1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not _Cerrar Then
            MsgBox("Debe ingresar con un usuario para utilizar el sistema.")
            e.Cancel = True
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Salir() Then End
    End Sub

    Private Sub LoginForm1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Alt AndAlso e.Control AndAlso e.KeyCode = Keys.F Then
            Clipboard.SetText(Environment.CurrentDirectory)
        End If
    End Sub

    Private Sub LoginForm1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
#If DEBUG Then

        Me.UsernameTextBox.Text = "lpana"
        Me.PasswordTextBox.Text = "1234"
        OK_Click(Me, Nothing)


#End If
    End Sub

#End Region

#Region "Metodos"

    Public Function Abrir() As Boolean
        ShowDialog()
        Return 1
    End Function

#End Region





End Class
