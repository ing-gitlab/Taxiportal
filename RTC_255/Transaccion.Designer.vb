﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransaccion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridEXLayout3 As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransaccion))
        Dim GridEXLayout4 As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Me.gbPersona = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblTotalRecaudacionDiaria = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnRecaudacion = New System.Windows.Forms.Button()
        Me.GridEX1 = New Janus.Windows.GridEX.GridEX()
        Me.txtMovil = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtTotalPagar = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.lblTotalVale = New System.Windows.Forms.Label()
        Me.GridEX2 = New Janus.Windows.GridEX.GridEX()
        Me.ChImprimir = New System.Windows.Forms.CheckBox()
        Me.btnVale = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblExcedente = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbltotalAhorro = New System.Windows.Forms.Label()
        Me.lblTotalAhorroAdicional = New System.Windows.Forms.Label()
        Me.lblTotalRecDiaria = New System.Windows.Forms.Label()
        Me.gbPersona.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.GridEX2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbPersona
        '
        Me.gbPersona.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbPersona.Controls.Add(Me.lblTotalAhorroAdicional)
        Me.gbPersona.Controls.Add(Me.lbltotalAhorro)
        Me.gbPersona.Controls.Add(Me.Label1)
        Me.gbPersona.Controls.Add(Me.Label4)
        Me.gbPersona.Controls.Add(Me.lblTotalRecaudacionDiaria)
        Me.gbPersona.Controls.Add(Me.PictureBox1)
        Me.gbPersona.Controls.Add(Me.btnRecaudacion)
        Me.gbPersona.Controls.Add(Me.GridEX1)
        Me.gbPersona.Controls.Add(Me.txtMovil)
        Me.gbPersona.Controls.Add(Me.Label11)
        Me.gbPersona.Location = New System.Drawing.Point(8, -1)
        Me.gbPersona.Name = "gbPersona"
        Me.gbPersona.Size = New System.Drawing.Size(961, 300)
        Me.gbPersona.TabIndex = 0
        Me.gbPersona.TabStop = False
        Me.gbPersona.Text = "Ingresos"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(866, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 13)
        Me.Label4.TabIndex = 117
        Me.Label4.Text = "Recaudaciones"
        '
        'lblTotalRecaudacionDiaria
        '
        Me.lblTotalRecaudacionDiaria.AutoSize = True
        Me.lblTotalRecaudacionDiaria.Location = New System.Drawing.Point(150, 272)
        Me.lblTotalRecaudacionDiaria.Name = "lblTotalRecaudacionDiaria"
        Me.lblTotalRecaudacionDiaria.Size = New System.Drawing.Size(134, 13)
        Me.lblTotalRecaudacionDiaria.TabIndex = 115
        Me.lblTotalRecaudacionDiaria.Text = "Total Recaudacion Diaria :"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Location = New System.Drawing.Point(488, 22)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(15, 15)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 114
        Me.PictureBox1.TabStop = False
        '
        'btnRecaudacion
        '
        Me.btnRecaudacion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRecaudacion.BackColor = System.Drawing.Color.Transparent
        Me.btnRecaudacion.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.recauda2
        Me.btnRecaudacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnRecaudacion.Location = New System.Drawing.Point(898, 73)
        Me.btnRecaudacion.Name = "btnRecaudacion"
        Me.btnRecaudacion.Size = New System.Drawing.Size(30, 30)
        Me.btnRecaudacion.TabIndex = 3
        Me.btnRecaudacion.UseVisualStyleBackColor = False
        '
        'GridEX1
        '
        Me.GridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.[True]
        Me.GridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridEX1.ColumnAutoResize = True
        GridEXLayout3.LayoutString = resources.GetString("GridEXLayout3.LayoutString")
        Me.GridEX1.DesignTimeLayout = GridEXLayout3
        Me.GridEX1.Location = New System.Drawing.Point(21, 56)
        Me.GridEX1.Name = "GridEX1"
        Me.GridEX1.Size = New System.Drawing.Size(839, 191)
        Me.GridEX1.TabIndex = 2
        Me.GridEX1.TabKeyBehavior = Janus.Windows.GridEX.TabKeyBehavior.ControlNavigation
        '
        'txtMovil
        '
        Me.txtMovil.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtMovil.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtMovil.Location = New System.Drawing.Point(420, 19)
        Me.txtMovil.Name = "txtMovil"
        Me.txtMovil.Size = New System.Drawing.Size(62, 20)
        Me.txtMovil.TabIndex = 1
        Me.txtMovil.Text = "0"
        Me.txtMovil.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtMovil.Value = CType(0, Short)
        Me.txtMovil.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(369, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "&Móvil:"
        '
        'btnImprimir
        '
        Me.btnImprimir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImprimir.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.printer
        Me.btnImprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnImprimir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnImprimir.Enabled = False
        Me.btnImprimir.Location = New System.Drawing.Point(924, 396)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(30, 30)
        Me.btnImprimir.TabIndex = 4
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.lblExcedente)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtTotalPagar)
        Me.GroupBox1.Controls.Add(Me.lblTotalVale)
        Me.GroupBox1.Controls.Add(Me.GridEX2)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 299)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(871, 242)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Egresos"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(387, 210)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(95, 13)
        Me.Label6.TabIndex = 118
        Me.Label6.Text = "TOTAL A PAGAR:"
        '
        'txtTotalPagar
        '
        Me.txtTotalPagar.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtTotalPagar.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtTotalPagar.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtTotalPagar.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtTotalPagar.Location = New System.Drawing.Point(518, 203)
        Me.txtTotalPagar.Name = "txtTotalPagar"
        Me.txtTotalPagar.ReadOnly = True
        Me.txtTotalPagar.Size = New System.Drawing.Size(193, 20)
        Me.txtTotalPagar.TabIndex = 118
        Me.txtTotalPagar.Text = "$ 0"
        Me.txtTotalPagar.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtTotalPagar.Value = 0
        Me.txtTotalPagar.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'lblTotalVale
        '
        Me.lblTotalVale.AutoSize = True
        Me.lblTotalVale.Location = New System.Drawing.Point(7, 223)
        Me.lblTotalVale.Name = "lblTotalVale"
        Me.lblTotalVale.Size = New System.Drawing.Size(94, 13)
        Me.lblTotalVale.TabIndex = 117
        Me.lblTotalVale.Text = "Total Pago Vales :"
        '
        'GridEX2
        '
        Me.GridEX2.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.[True]
        Me.GridEX2.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridEX2.ColumnAutoResize = True
        GridEXLayout4.LayoutString = resources.GetString("GridEXLayout4.LayoutString")
        Me.GridEX2.DesignTimeLayout = GridEXLayout4
        Me.GridEX2.Location = New System.Drawing.Point(10, 22)
        Me.GridEX2.Name = "GridEX2"
        Me.GridEX2.Size = New System.Drawing.Size(850, 169)
        Me.GridEX2.TabIndex = 3
        Me.GridEX2.TabKeyBehavior = Janus.Windows.GridEX.TabKeyBehavior.ControlNavigation
        '
        'ChImprimir
        '
        Me.ChImprimir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ChImprimir.AutoSize = True
        Me.ChImprimir.Checked = True
        Me.ChImprimir.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChImprimir.Enabled = False
        Me.ChImprimir.Location = New System.Drawing.Point(908, 502)
        Me.ChImprimir.Name = "ChImprimir"
        Me.ChImprimir.Size = New System.Drawing.Size(61, 17)
        Me.ChImprimir.TabIndex = 93
        Me.ChImprimir.Text = "Imprimir"
        Me.ChImprimir.UseVisualStyleBackColor = True
        '
        'btnVale
        '
        Me.btnVale.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVale.BackColor = System.Drawing.Color.Transparent
        Me.btnVale.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.Vales
        Me.btnVale.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVale.Enabled = False
        Me.btnVale.Location = New System.Drawing.Point(924, 337)
        Me.btnVale.Name = "btnVale"
        Me.btnVale.Size = New System.Drawing.Size(30, 30)
        Me.btnVale.TabIndex = 2
        Me.btnVale.UseVisualStyleBackColor = False
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.Location = New System.Drawing.Point(817, 559)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGuardar.Location = New System.Drawing.Point(736, 559)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(905, 321)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 118
        Me.Label5.Text = "Abrir Vale"
        '
        'lblExcedente
        '
        Me.lblExcedente.AutoSize = True
        Me.lblExcedente.Location = New System.Drawing.Point(399, 210)
        Me.lblExcedente.Name = "lblExcedente"
        Me.lblExcedente.Size = New System.Drawing.Size(75, 13)
        Me.lblExcedente.TabIndex = 119
        Me.lblExcedente.Text = "EXCEDENTE:"
        Me.lblExcedente.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(896, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 13)
        Me.Label1.TabIndex = 118
        Me.Label1.Text = "Abrir"
        '
        'lbltotalAhorro
        '
        Me.lbltotalAhorro.AutoSize = True
        Me.lbltotalAhorro.Location = New System.Drawing.Point(417, 272)
        Me.lbltotalAhorro.Name = "lbltotalAhorro"
        Me.lbltotalAhorro.Size = New System.Drawing.Size(71, 13)
        Me.lbltotalAhorro.TabIndex = 119
        Me.lbltotalAhorro.Text = "Total Ahorro :"
        '
        'lblTotalAhorroAdicional
        '
        Me.lblTotalAhorroAdicional.AutoSize = True
        Me.lblTotalAhorroAdicional.Location = New System.Drawing.Point(627, 272)
        Me.lblTotalAhorroAdicional.Name = "lblTotalAhorroAdicional"
        Me.lblTotalAhorroAdicional.Size = New System.Drawing.Size(123, 13)
        Me.lblTotalAhorroAdicional.TabIndex = 120
        Me.lblTotalAhorroAdicional.Text = "Total Ahorro Adicioanal :"
        '
        'lblTotalRecDiaria
        '
        Me.lblTotalRecDiaria.AutoSize = True
        Me.lblTotalRecDiaria.Location = New System.Drawing.Point(15, 559)
        Me.lblTotalRecDiaria.Name = "lblTotalRecDiaria"
        Me.lblTotalRecDiaria.Size = New System.Drawing.Size(132, 13)
        Me.lblTotalRecDiaria.TabIndex = 120
        Me.lblTotalRecDiaria.Text = "Total Recaudación diaria :"
        '
        'frmTransaccion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(981, 594)
        Me.Controls.Add(Me.lblTotalRecDiaria)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ChImprimir)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnVale)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gbPersona)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmTransaccion"
        Me.Text = "Transaccion"
        Me.gbPersona.ResumeLayout(False)
        Me.gbPersona.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.GridEX2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbPersona As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GridEX1 As Janus.Windows.GridEX.GridEX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRecaudacion As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnVale As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtMovil As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ChImprimir As System.Windows.Forms.CheckBox
    Friend WithEvents GridEX2 As Janus.Windows.GridEX.GridEX
    Friend WithEvents lblTotalRecaudacionDiaria As System.Windows.Forms.Label
    Friend WithEvents lblTotalVale As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtTotalPagar As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents lblExcedente As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTotalAhorroAdicional As System.Windows.Forms.Label
    Friend WithEvents lbltotalAhorro As System.Windows.Forms.Label
    Friend WithEvents lblTotalRecDiaria As System.Windows.Forms.Label
End Class
