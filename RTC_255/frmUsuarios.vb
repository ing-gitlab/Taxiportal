﻿Imports Common

Public Class frmUsuarios

#Region "Declaraciones"
    Private esNuevo As Boolean = False
    Private idUsuarioOriginal As String
    Private ContraseñaOriginal As String
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))

#End Region


#Region "Eventos"

    Private Sub btnEliminarUsuario_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarUsuario.Click
        Try
            If MsgBox("¿Está Seguro De Eliminar El Usuario?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Confirmación de eliminación") = MsgBoxResult.Yes Then

                Dim eliminar = d.proc_UsuariosDelete(GridEX1.GetValue("idUsuario"))
                d.proc_ConfiguracionesDeleteUsuario(GridEX1.GetValue("idUsuario"))
                LlenarGrilla()
            End If
        Catch ex As Exception

        End Try

    End Sub


    Private Sub btnAgregarUsuario_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregarUsuario.Click
        esNuevo = True
        iniciarEdicion(True)
        Me.txtNombre.Text = ""
        Me.txtContraseña.Text = ""
        Me.txtIdUsuario.Text = ""
        Me.cboTipoUsuario.SelectedIndex = -1
        Me.txtNombre.Focus()
    End Sub



    Private Sub frmUsuarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
        LlenarGrilla()
        ObtenerTipoUsuario()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        Dim Posicion As Integer
        'Me.ContraseñaOriginal = Me.txtContraseña.Text
        'idUsuarioOriginal = Me.txtIdUsuario.Text
        If validar() Then


            If esNuevo Then
                'Ingresar Usuario

                Dim nuevoUsuario = d.proc_UsuariosInsert(txtIdUsuario.Text, txtNombre.Text, txtContraseña.Text, Me.cboTipoUsuario.SelectedValue)
                d.proc_ConfiguracionesInsert(txtIdUsuario.Text, NOW_GLOBAL, 1, 1, 1, 1, 0, 0, 3, CONFIG_GLOBAL.zonaDefault, CONFIG_GLOBAL.tipoServicioDefault, 1, 5, getSettings("PorcentajeVales"), CONFIG_GLOBAL.horaInicioRecargoNocturno, CONFIG_GLOBAL.horaFinRecargoNocturno, CONFIG_GLOBAL.ImporteRecargoNocturno, 0)


            Else
                'modificar Usuario

                Posicion = Me.GridEX1.GetRow().Position
                d.proc_UsuariosUpdate(GridEX1.GetValue("idUsuario"), txtNombre.Text, txtContraseña.Text, Me.cboTipoUsuario.SelectedValue)
            End If

            iniciarEdicion(False)
            LlenarGrilla()

            If esNuevo Then
                Posicion = Me.GridEX1.RowCount - 1
            End If

            Me.GridEX1.MoveTo(Posicion)
            Me.GridEX1.Focus()
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.iniciarEdicion(False)
        LlenarGrilla()
        Me.GridEX1.Focus()
    End Sub

    Private Sub GridEX1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.SelectionChanged
        Try
            If Me.GridEX1.GetRow.RowType = Janus.Windows.GridEX.RowType.FilterRow Or Me.GridEX1.RowCount = 0 Then Exit Sub
            Me.txtNombre.Text = GridEX1.GetValue("Nombre")
            Me.cboTipoUsuario.SelectedValue = GridEX1.GetValue("idTipoUsuario")
            Me.txtIdUsuario.Text = GridEX1.GetValue("idUsuario")

        Catch ex As System.Exception
        End Try
    End Sub


    Private Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        esNuevo = False
        iniciarEdicion(True)
        Me.txtNombre.Focus()
        Me.idUsuarioOriginal = Me.txtIdUsuario.Text
        Me.ContraseñaOriginal = Me.txtContraseña.Text
        Me.txtContraseña.Text = GridEX1.GetValue("password")
    End Sub

#End Region


#Region "Metodos"
    Private Sub LlenarGrilla()

        GridEX1.SetDataBinding(d.proc_obtenerUsuario, "")
        GridEX1.RetrieveStructure()

        GridEX1.Tables(0).Columns("idUsuario").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("nombre").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("descripcion").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("password").Visible = False
        GridEX1.Tables(0).Columns("nombre").Caption = "Nombre"
        GridEX1.Tables(0).Columns("nombre").AutoSize()
        GridEX1.Tables(0).Columns("nombre").Caption = "Nombre"
        GridEX1.Tables(0).Columns("descripcion").Caption = "Descripción"
        GridEX1.Tables(0).Columns("idTipoUsuario").Visible = False

        For Each centrar As Janus.Windows.GridEX.GridEXColumn In GridEX1.Tables(0).Columns
            centrar.HeaderAlignment = Janus.Windows.GridEX.TextAlignment.Center
        Next

        Me.btnModificar.Enabled = (Me.GridEX1.RowCount > 0)
        Me.btnEliminarUsuario.Enabled = (Me.GridEX1.RowCount > 0)
    End Sub


    Private Function ValidarTipoUsuario() As Boolean
        If Me.cboTipoUsuario.SelectedIndex < 0 Then
            MsgBox("Debe Seleccionar El Tipo De Usuario", MsgBoxStyle.Critical, "Error")
            Me.cboTipoUsuario.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidarNombre()
        If Me.txtNombre.Text = "" Then
            MsgBox("Debe Ingresar Nombre Del Usuario", MsgBoxStyle.Critical, "Error")
            Me.txtNombre.Focus()
            Return False
        End If
        Return True
    End Function
    Private Function ValidarContrasena()
        If Me.txtContraseña.Text = "" Then
            MsgBox("Debe Ingresar La Contraseña", MsgBoxStyle.Critical, "Error")
            Me.txtContraseña.Focus()
            Return False
        End If

        Return True
    End Function
    Private Function ValidarIdUsuario()

        If Me.txtIdUsuario.Text = "" Then
            MsgBox("Debe Ingrar El Id Usuario", MsgBoxStyle.Critical, "Error")
            Me.txtIdUsuario.Focus()
            Return False
        End If

        If Me.esNuevo Or txtIdUsuario.Text <> idUsuarioOriginal Then

            Dim l = d.proc_ValidarIdUsuario(txtIdUsuario.Text).ToList()
            If l.Count > 0 Then
                MsgBox("Este id Usuario Se Encuentra Registrado ", MsgBoxStyle.Critical, "ERROR")
                Me.txtIdUsuario.Focus()
                Return False
            End If
        End If


        Return True
    End Function

    Private Function validar()
        If Not ValidarNombre() Then
            Return False
        End If
        If Not ValidarContrasena() Then
            Return False
        End If
        If Not ValidarTipoUsuario() Then
            Return False
        End If
        If Not ValidarIdUsuario() Then
            Return False
        End If


        Return True
    End Function
    Private Sub iniciarEdicion(ByVal esInicio As Boolean)
        Me.GridEX1.Enabled = Not esInicio

        Me.btnAgregarUsuario.Visible = Not esInicio
        Me.btnModificar.Visible = Not esInicio
        Me.btnEliminarUsuario.Visible = Not esInicio
        Me.txtNombre.ReadOnly = Not esInicio
        Me.txtContraseña.ReadOnly = Not esInicio
        Me.txtIdUsuario.ReadOnly = Not esInicio
        Me.cboTipoUsuario.Enabled = esInicio
        Me.btnAceptar.Visible = esInicio
        Me.btnCancelar.Visible = esInicio
        Me.txtContraseña.Text = ""

    End Sub

    Private Sub ObtenerTipoUsuario()
        Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
        Dim ListaUsuario = d.proc_Tipo_UsuarioLoadAll.ToList()
        Me.cboTipoUsuario.DataSource = ListaUsuario
    End Sub

#End Region


 
End Class