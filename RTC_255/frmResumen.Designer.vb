﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmResumen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmResumen))
        Me.GridEX1 = New Janus.Windows.GridEX.GridEX()
        Me.terminado = New Janus.Windows.UI.CommandBars.UICommand()
        Me.toolBarEstadosServicios = New Janus.Windows.UI.CommandBars.UICommandManager(Me.components)
        Me.BottomRebar1 = New Janus.Windows.UI.CommandBars.UIRebar()
        Me.UiCommandBar1 = New Janus.Windows.UI.CommandBars.UICommandBar()
        Me.enCurso1 = New Janus.Windows.UI.CommandBars.UICommand()
        Me.cancelado1 = New Janus.Windows.UI.CommandBars.UICommand()
        Me.terminado1 = New Janus.Windows.UI.CommandBars.UICommand()
        Me.enCamino1 = New Janus.Windows.UI.CommandBars.UICommand()
        Me.Separator1 = New Janus.Windows.UI.CommandBars.UICommand()
        Me.nuevo1 = New Janus.Windows.UI.CommandBars.UICommand()
        Me.editar1 = New Janus.Windows.UI.CommandBars.UICommand()
        Me.servicioRapido1 = New Janus.Windows.UI.CommandBars.UICommand()
        Me.imprimir1 = New Janus.Windows.UI.CommandBars.UICommand()
        Me.Separator2 = New Janus.Windows.UI.CommandBars.UICommand()
        Me.enCurso = New Janus.Windows.UI.CommandBars.UICommand()
        Me.cancelado = New Janus.Windows.UI.CommandBars.UICommand()
        Me.enCamino = New Janus.Windows.UI.CommandBars.UICommand()
        Me.nuevo = New Janus.Windows.UI.CommandBars.UICommand()
        Me.editar = New Janus.Windows.UI.CommandBars.UICommand()
        Me.eliminar = New Janus.Windows.UI.CommandBars.UICommand()
        Me.servicioRapido = New Janus.Windows.UI.CommandBars.UICommand()
        Me.imprimir = New Janus.Windows.UI.CommandBars.UICommand()
        Me.Buscar = New Janus.Windows.UI.CommandBars.UICommand()
        Me.btnBuscar = New Janus.Windows.UI.CommandBars.UICommand()
        Me.RF = New Janus.Windows.UI.CommandBars.UICommand()
        Me.LeftRebar1 = New Janus.Windows.UI.CommandBars.UIRebar()
        Me.RightRebar1 = New Janus.Windows.UI.CommandBars.UIRebar()
        Me.TopRebar1 = New Janus.Windows.UI.CommandBars.UIRebar()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.chkCancelado = New System.Windows.Forms.CheckBox()
        Me.chkTerminado = New System.Windows.Forms.CheckBox()
        Me.chkEnCurso = New System.Windows.Forms.CheckBox()
        Me.chkEnCamino = New System.Windows.Forms.CheckBox()
        Me.cmdActualizar = New Janus.Windows.EditControls.UIButton()
        Me.lblHoras2 = New System.Windows.Forms.Label()
        Me.txtHoras = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.lblHoras = New System.Windows.Forms.Label()
        Me.dtpDia = New System.Windows.Forms.DateTimePicker()
        Me.lblDia = New System.Windows.Forms.Label()
        Me.filtroHora = New Janus.Windows.EditControls.UIRadioButton()
        Me.filtroFecha = New Janus.Windows.EditControls.UIRadioButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.terminado2 = New Janus.Windows.UI.CommandBars.UICommand()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.toolBarEstadosServicios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BottomRebar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UiCommandBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LeftRebar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RightRebar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TopRebar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TopRebar1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GridEX1
        '
        Me.GridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridEX1.ColumnAutoResize = True
        Me.GridEX1.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular
        Me.GridEX1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridEX1.Location = New System.Drawing.Point(12, 32)
        Me.GridEX1.Name = "GridEX1"
        Me.GridEX1.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection
        Me.GridEX1.Size = New System.Drawing.Size(973, 342)
        Me.GridEX1.TabIndex = 0
        Me.GridEX1.TabKeyBehavior = Janus.Windows.GridEX.TabKeyBehavior.ControlNavigation
        Me.GridEX1.WatermarkImage.Image = CType(resources.GetObject("GridEX1.WatermarkImage.Image"), System.Drawing.Image)
        Me.GridEX1.WatermarkImage.Size = New System.Drawing.Size(130, 140)
        Me.GridEX1.WatermarkImage.WashColor = System.Drawing.Color.AliceBlue
        '
        'terminado
        '
        Me.terminado.Key = "terminado"
        Me.terminado.Name = "terminado"
        Me.terminado.Shortcut = System.Windows.Forms.Shortcut.ShiftF2
        Me.terminado.Text = "Terminado"
        '
        'toolBarEstadosServicios
        '
        Me.toolBarEstadosServicios.BottomRebar = Me.BottomRebar1
        Me.toolBarEstadosServicios.CommandBars.AddRange(New Janus.Windows.UI.CommandBars.UICommandBar() {Me.UiCommandBar1})
        Me.toolBarEstadosServicios.Commands.AddRange(New Janus.Windows.UI.CommandBars.UICommand() {Me.terminado, Me.enCurso, Me.cancelado, Me.enCamino, Me.nuevo, Me.editar, Me.eliminar, Me.servicioRapido, Me.imprimir, Me.Buscar, Me.btnBuscar, Me.RF})
        Me.toolBarEstadosServicios.ContainerControl = Me
        Me.toolBarEstadosServicios.Id = New System.Guid("44c32be8-9c96-49f3-ad56-45d94219c0a6")
        Me.toolBarEstadosServicios.LeftRebar = Me.LeftRebar1
        Me.toolBarEstadosServicios.RightRebar = Me.RightRebar1
        Me.toolBarEstadosServicios.TopRebar = Me.TopRebar1
        '
        'BottomRebar1
        '
        Me.BottomRebar1.CommandManager = Me.toolBarEstadosServicios
        Me.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomRebar1.Location = New System.Drawing.Point(0, 0)
        Me.BottomRebar1.Name = "BottomRebar1"
        Me.BottomRebar1.Size = New System.Drawing.Size(0, 0)
        Me.BottomRebar1.TabIndex = 0
        '
        'UiCommandBar1
        '
        Me.UiCommandBar1.CommandManager = Me.toolBarEstadosServicios
        Me.UiCommandBar1.Commands.AddRange(New Janus.Windows.UI.CommandBars.UICommand() {Me.enCurso1, Me.cancelado1, Me.terminado1, Me.enCamino1, Me.Separator1, Me.nuevo1, Me.editar1, Me.servicioRapido1, Me.imprimir1, Me.Separator2})
        Me.UiCommandBar1.Key = "CommandBar1"
        Me.UiCommandBar1.Location = New System.Drawing.Point(0, 0)
        Me.UiCommandBar1.Name = "UiCommandBar1"
        Me.UiCommandBar1.RowIndex = 0
        Me.UiCommandBar1.ShowCustomizeButton = Janus.Windows.UI.InheritableBoolean.[False]
        Me.UiCommandBar1.ShowToolTips = Janus.Windows.UI.InheritableBoolean.[True]
        Me.UiCommandBar1.Size = New System.Drawing.Size(182, 26)
        Me.UiCommandBar1.TabIndex = 0
        Me.UiCommandBar1.Text = "CommandBar1"
        '
        'enCurso1
        '
        Me.enCurso1.Key = "enCurso"
        Me.enCurso1.Name = "enCurso1"
        Me.enCurso1.ToolTipText = "Shift+F1"
        '
        'cancelado1
        '
        Me.cancelado1.Key = "cancelado"
        Me.cancelado1.Name = "cancelado1"
        Me.cancelado1.ToolTipText = "Shift+F2"
        '
        'terminado1
        '
        Me.terminado1.Key = "terminado"
        Me.terminado1.Name = "terminado1"
        Me.terminado1.Shortcut = System.Windows.Forms.Shortcut.ShiftF3
        Me.terminado1.ToolTipText = "Shift+F3"
        '
        'enCamino1
        '
        Me.enCamino1.Key = "enCamino"
        Me.enCamino1.Name = "enCamino1"
        Me.enCamino1.ToolTipText = "Shift+F4"
        '
        'Separator1
        '
        Me.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator
        Me.Separator1.Key = "Separator"
        Me.Separator1.Name = "Separator1"
        '
        'nuevo1
        '
        Me.nuevo1.Key = "nuevo"
        Me.nuevo1.Name = "nuevo1"
        '
        'editar1
        '
        Me.editar1.Key = "editar"
        Me.editar1.Name = "editar1"
        '
        'servicioRapido1
        '
        Me.servicioRapido1.Key = "servicioRapido"
        Me.servicioRapido1.Name = "servicioRapido1"
        '
        'imprimir1
        '
        Me.imprimir1.Key = "imprimir"
        Me.imprimir1.Name = "imprimir1"
        '
        'Separator2
        '
        Me.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator
        Me.Separator2.Key = "Separator"
        Me.Separator2.Name = "Separator2"
        '
        'enCurso
        '
        Me.enCurso.Key = "enCurso"
        Me.enCurso.Name = "enCurso"
        Me.enCurso.Shortcut = System.Windows.Forms.Shortcut.ShiftF1
        Me.enCurso.Text = "En Curso"
        Me.enCurso.Visible = Janus.Windows.UI.InheritableBoolean.[False]
        '
        'cancelado
        '
        Me.cancelado.Key = "cancelado"
        Me.cancelado.Name = "cancelado"
        Me.cancelado.Shortcut = System.Windows.Forms.Shortcut.ShiftF2
        Me.cancelado.Text = "Cancelado"
        '
        'enCamino
        '
        Me.enCamino.Key = "enCamino"
        Me.enCamino.Name = "enCamino"
        Me.enCamino.Shortcut = System.Windows.Forms.Shortcut.ShiftF4
        Me.enCamino.Text = "Móvil en Camino"
        Me.enCamino.Visible = Janus.Windows.UI.InheritableBoolean.[False]
        '
        'nuevo
        '
        Me.nuevo.Key = "nuevo"
        Me.nuevo.Name = "nuevo"
        Me.nuevo.Shortcut = System.Windows.Forms.Shortcut.CtrlN
        Me.nuevo.Text = "Nuevo"
        Me.nuevo.ToolTipText = "Ctrl+N"
        '
        'editar
        '
        Me.editar.Key = "editar"
        Me.editar.Name = "editar"
        Me.editar.Shortcut = System.Windows.Forms.Shortcut.CtrlE
        Me.editar.Text = "Editar"
        Me.editar.ToolTipText = "Ctrl+E"
        '
        'eliminar
        '
        Me.eliminar.Key = "eliminar"
        Me.eliminar.Name = "eliminar"
        Me.eliminar.Shortcut = System.Windows.Forms.Shortcut.CtrlF3
        Me.eliminar.Text = "Eliminar"
        '
        'servicioRapido
        '
        Me.servicioRapido.Key = "servicioRapido"
        Me.servicioRapido.Name = "servicioRapido"
        Me.servicioRapido.Shortcut = System.Windows.Forms.Shortcut.CtrlS
        Me.servicioRapido.Text = "Servicio Rapido"
        Me.servicioRapido.ToolTipText = "Ctrl+S"
        '
        'imprimir
        '
        Me.imprimir.Key = "imprimir"
        Me.imprimir.Name = "imprimir"
        Me.imprimir.Shortcut = System.Windows.Forms.Shortcut.CtrlI
        Me.imprimir.Text = "Imprimir"
        Me.imprimir.ToolTipText = "Ctrl+I"
        '
        'Buscar
        '
        Me.Buscar.CommandType = Janus.Windows.UI.CommandBars.CommandType.TextBoxCommand
        Me.Buscar.ControlValue = ""
        Me.Buscar.IsEditableControl = Janus.Windows.UI.InheritableBoolean.[True]
        Me.Buscar.Key = "Buscar"
        Me.Buscar.Name = "Buscar"
        Me.Buscar.Text = "Buscar"
        '
        'btnBuscar
        '
        Me.btnBuscar.Key = "btnBuscar"
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Shortcut = System.Windows.Forms.Shortcut.F3
        Me.btnBuscar.Text = "Buscar"
        '
        'RF
        '
        Me.RF.Key = "RF"
        Me.RF.Name = "RF"
        Me.RF.Text = "Enviar a RF"
        '
        'LeftRebar1
        '
        Me.LeftRebar1.CommandManager = Me.toolBarEstadosServicios
        Me.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left
        Me.LeftRebar1.Location = New System.Drawing.Point(0, 0)
        Me.LeftRebar1.Name = "LeftRebar1"
        Me.LeftRebar1.Size = New System.Drawing.Size(0, 0)
        Me.LeftRebar1.TabIndex = 0
        '
        'RightRebar1
        '
        Me.RightRebar1.CommandManager = Me.toolBarEstadosServicios
        Me.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right
        Me.RightRebar1.Location = New System.Drawing.Point(0, 0)
        Me.RightRebar1.Name = "RightRebar1"
        Me.RightRebar1.Size = New System.Drawing.Size(0, 0)
        Me.RightRebar1.TabIndex = 0
        '
        'TopRebar1
        '
        Me.TopRebar1.CommandBars.AddRange(New Janus.Windows.UI.CommandBars.UICommandBar() {Me.UiCommandBar1})
        Me.TopRebar1.CommandManager = Me.toolBarEstadosServicios
        Me.TopRebar1.Controls.Add(Me.UiCommandBar1)
        Me.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopRebar1.Location = New System.Drawing.Point(0, 0)
        Me.TopRebar1.Name = "TopRebar1"
        Me.TopRebar1.Size = New System.Drawing.Size(997, 26)
        Me.TopRebar1.TabIndex = 29
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.CheckBox1)
        Me.GroupBox1.Controls.Add(Me.chkCancelado)
        Me.GroupBox1.Controls.Add(Me.chkTerminado)
        Me.GroupBox1.Controls.Add(Me.chkEnCurso)
        Me.GroupBox1.Controls.Add(Me.chkEnCamino)
        Me.GroupBox1.Controls.Add(Me.cmdActualizar)
        Me.GroupBox1.Controls.Add(Me.lblHoras2)
        Me.GroupBox1.Controls.Add(Me.txtHoras)
        Me.GroupBox1.Controls.Add(Me.lblHoras)
        Me.GroupBox1.Controls.Add(Me.dtpDia)
        Me.GroupBox1.Controls.Add(Me.lblDia)
        Me.GroupBox1.Controls.Add(Me.filtroHora)
        Me.GroupBox1.Controls.Add(Me.filtroFecha)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 392)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(955, 70)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filtros"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(769, 19)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(86, 17)
        Me.CheckBox1.TabIndex = 11
        Me.CheckBox1.Text = "Mostrar Filtro"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'chkCancelado
        '
        Me.chkCancelado.AutoSize = True
        Me.chkCancelado.Checked = True
        Me.chkCancelado.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCancelado.Location = New System.Drawing.Point(678, 47)
        Me.chkCancelado.Name = "chkCancelado"
        Me.chkCancelado.Size = New System.Drawing.Size(77, 17)
        Me.chkCancelado.TabIndex = 10
        Me.chkCancelado.Text = "&Cancelado"
        Me.chkCancelado.UseVisualStyleBackColor = True
        '
        'chkTerminado
        '
        Me.chkTerminado.AutoSize = True
        Me.chkTerminado.Checked = True
        Me.chkTerminado.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTerminado.Location = New System.Drawing.Point(678, 19)
        Me.chkTerminado.Name = "chkTerminado"
        Me.chkTerminado.Size = New System.Drawing.Size(76, 17)
        Me.chkTerminado.TabIndex = 9
        Me.chkTerminado.Text = "&Terminado"
        Me.chkTerminado.UseVisualStyleBackColor = True
        '
        'chkEnCurso
        '
        Me.chkEnCurso.AutoSize = True
        Me.chkEnCurso.Checked = True
        Me.chkEnCurso.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEnCurso.Location = New System.Drawing.Point(556, 47)
        Me.chkEnCurso.Name = "chkEnCurso"
        Me.chkEnCurso.Size = New System.Drawing.Size(69, 17)
        Me.chkEnCurso.TabIndex = 8
        Me.chkEnCurso.Text = "E&n Curso"
        Me.chkEnCurso.UseVisualStyleBackColor = True
        '
        'chkEnCamino
        '
        Me.chkEnCamino.AutoSize = True
        Me.chkEnCamino.Checked = True
        Me.chkEnCamino.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEnCamino.Location = New System.Drawing.Point(556, 19)
        Me.chkEnCamino.Name = "chkEnCamino"
        Me.chkEnCamino.Size = New System.Drawing.Size(77, 17)
        Me.chkEnCamino.TabIndex = 7
        Me.chkEnCamino.Text = "&En Camino"
        Me.chkEnCamino.UseVisualStyleBackColor = True
        '
        'cmdActualizar
        '
        Me.cmdActualizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdActualizar.Icon = CType(resources.GetObject("cmdActualizar.Icon"), System.Drawing.Icon)
        Me.cmdActualizar.Location = New System.Drawing.Point(869, 33)
        Me.cmdActualizar.Name = "cmdActualizar"
        Me.cmdActualizar.Size = New System.Drawing.Size(80, 24)
        Me.cmdActualizar.TabIndex = 12
        Me.cmdActualizar.Text = "Actualizar"
        Me.cmdActualizar.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003
        '
        'lblHoras2
        '
        Me.lblHoras2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblHoras2.Location = New System.Drawing.Point(418, 39)
        Me.lblHoras2.Name = "lblHoras2"
        Me.lblHoras2.Size = New System.Drawing.Size(40, 16)
        Me.lblHoras2.TabIndex = 6
        Me.lblHoras2.Text = "Horas."
        '
        'txtHoras
        '
        Me.txtHoras.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtHoras.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtHoras.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtHoras.DecimalDigits = 0
        Me.txtHoras.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General
        Me.txtHoras.Location = New System.Drawing.Point(382, 37)
        Me.txtHoras.Name = "txtHoras"
        Me.txtHoras.Size = New System.Drawing.Size(32, 20)
        Me.txtHoras.TabIndex = 5
        Me.txtHoras.Text = "8"
        Me.txtHoras.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtHoras.Value = 8
        Me.txtHoras.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'lblHoras
        '
        Me.lblHoras.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblHoras.Location = New System.Drawing.Point(205, 38)
        Me.lblHoras.Name = "lblHoras"
        Me.lblHoras.Size = New System.Drawing.Size(171, 16)
        Me.lblHoras.TabIndex = 3
        Me.lblHoras.Text = "Mostrar los servicios en las últimas"
        '
        'dtpDia
        '
        Me.dtpDia.CustomFormat = "dd/MM/yyyy"
        Me.dtpDia.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDia.Location = New System.Drawing.Point(340, 36)
        Me.dtpDia.Name = "dtpDia"
        Me.dtpDia.Size = New System.Drawing.Size(84, 20)
        Me.dtpDia.TabIndex = 4
        Me.dtpDia.Visible = False
        '
        'lblDia
        '
        Me.lblDia.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblDia.AutoSize = True
        Me.lblDia.Location = New System.Drawing.Point(195, 36)
        Me.lblDia.Name = "lblDia"
        Me.lblDia.Size = New System.Drawing.Size(139, 13)
        Me.lblDia.TabIndex = 2
        Me.lblDia.Text = "Mostrar los servicios del dia:"
        Me.lblDia.Visible = False
        '
        'filtroHora
        '
        Me.filtroHora.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.filtroHora.Checked = True
        Me.filtroHora.Location = New System.Drawing.Point(46, 20)
        Me.filtroHora.Name = "filtroHora"
        Me.filtroHora.Size = New System.Drawing.Size(112, 16)
        Me.filtroHora.TabIndex = 0
        Me.filtroHora.Text = "Filtrar por Horas"
        '
        'filtroFecha
        '
        Me.filtroFecha.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.filtroFecha.Location = New System.Drawing.Point(46, 41)
        Me.filtroFecha.Name = "filtroFecha"
        Me.filtroFecha.Size = New System.Drawing.Size(112, 16)
        Me.filtroFecha.TabIndex = 1
        Me.filtroFecha.Text = "Filtrar por Fecha"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 10000
        '
        'lblCantidad
        '
        Me.lblCantidad.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(860, 377)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(39, 13)
        Me.lblCantidad.TabIndex = 2
        Me.lblCantidad.Text = "Label1"
        '
        'terminado2
        '
        Me.terminado2.Key = "terminado"
        Me.terminado2.Name = "terminado2"
        '
        'frmResumen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(997, 488)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GridEX1)
        Me.Controls.Add(Me.TopRebar1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmResumen"
        Me.Text = "Servicios"
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.toolBarEstadosServicios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BottomRebar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UiCommandBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LeftRebar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RightRebar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TopRebar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TopRebar1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GridEX1 As Janus.Windows.GridEX.GridEX
    Friend WithEvents terminado As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents toolBarEstadosServicios As Janus.Windows.UI.CommandBars.UICommandManager
    Friend WithEvents BottomRebar1 As Janus.Windows.UI.CommandBars.UIRebar
    Friend WithEvents LeftRebar1 As Janus.Windows.UI.CommandBars.UIRebar
    Friend WithEvents RightRebar1 As Janus.Windows.UI.CommandBars.UIRebar
    Friend WithEvents TopRebar1 As Janus.Windows.UI.CommandBars.UIRebar
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdActualizar As Janus.Windows.EditControls.UIButton
    Friend WithEvents lblHoras2 As System.Windows.Forms.Label
    Friend WithEvents txtHoras As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents lblHoras As System.Windows.Forms.Label
    Friend WithEvents dtpDia As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDia As System.Windows.Forms.Label
    Friend WithEvents filtroHora As Janus.Windows.EditControls.UIRadioButton
    Friend WithEvents filtroFecha As Janus.Windows.EditControls.UIRadioButton
    Friend WithEvents UiCommandBar1 As Janus.Windows.UI.CommandBars.UICommandBar
    Friend WithEvents enCurso1 As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents cancelado1 As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents terminado1 As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents enCurso As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents cancelado As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents enCamino1 As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents enCamino As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents nuevo As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents Separator1 As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents nuevo1 As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents editar1 As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents editar As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents eliminar As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents servicioRapido1 As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents servicioRapido As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents imprimir1 As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents imprimir As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents chkCancelado As System.Windows.Forms.CheckBox
    Friend WithEvents chkTerminado As System.Windows.Forms.CheckBox
    Friend WithEvents chkEnCurso As System.Windows.Forms.CheckBox
    Friend WithEvents chkEnCamino As System.Windows.Forms.CheckBox
    Friend WithEvents Separator2 As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents Buscar As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents btnBuscar As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents RF As Janus.Windows.UI.CommandBars.UICommand
    Friend WithEvents terminado2 As Janus.Windows.UI.CommandBars.UICommand
End Class
