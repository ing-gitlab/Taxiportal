﻿Imports Common

Public Class frmServicios

#Region "Declaraciones"
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim RefrescarGrid As Boolean = True
    Dim mEsNuevo As Boolean = True
    Dim mVehiculo As proc_VehiculosLoadByPrimaryKeyResult
    Dim _idServicio As Integer
    Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")
    Dim PORT_Resumen As Integer = getSettings("frmResumen_PORT")
    Dim mCliente
    Dim servicio As New proc_ServiciosLoadByPrimaryKeyResult
#End Region


    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTelefono.GotFocus
        AcceptButton = btnCliente

    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTelefono.LostFocus
        AcceptButton = btnServicio
    End Sub

    Private Sub LimpiarCampos()

        txtNombre.Text = String.Empty
        txtObs.Text = String.Empty
        txtDireccion.Text = String.Empty
        txtRef.Text = String.Empty
        cboBarrio.SelectedValue = 1
        txtMovil1.Text = String.Empty
        txtMovil2.Text = String.Empty
        dtpFecha.Value = NOW_GLOBAL
    End Sub

    Public Function AbrirForm(ByVal pEsNuevo As Boolean)
        mEsNuevo = pEsNuevo
        ObtenerBarrios()
        Me.dtpFecha.Value = NOW_GLOBAL
        cboBarrio.Focus()
        ShowDialog()
        Return RefrescarGrid
    End Function

    Public Function AbrirConVehiculo(ByVal idVehiculo As Integer) As Boolean
        ObtenerBarrios()
        mEsNuevo = True
        mVehiculo = d.proc_VehiculosLoadByPrimaryKey(idVehiculo).ToList.First
        txtMovil1.Value = 0
        txtMovil2.Value = 0
        Show()

        Return True
    End Function

    Private Sub ObtenerCliente()
        Dim ListaCliente = d.proc_ClientesLoadByTelefono(txtTelefono.Text).ToList
        If ListaCliente.Count = 0 Then
            ' no existe cliente
            mCliente = Nothing
            LimpiarCampos()
            txtNombre.Focus()
        Else
            Dim Cliente = ListaCliente(0)
            mCliente = Cliente
            With Cliente
                txtNombre.Text = .nombre
                txtDireccion.Text = .direccion
                txtRef.Text = .referencia
                txtObs.Text = .observaciones
                cboBarrio.SelectedValue = .idBarrio
            End With
            txtMovil1.Focus()
        End If

    End Sub

    Private Sub btnCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCliente.Click
        ObtenerCliente()
    End Sub

    Private Sub ObtenerBarrios()
        Dim ListaBarrios = d.proc_ObtenerBarriosTodos.ToList
        cboBarrio.DisplayMember = "Barrio"
        cboBarrio.ValueMember = "idBarrio"
        cboBarrio.DataSource = ListaBarrios

    End Sub

    Private Function Validar() As Boolean
        If txtMovil1.Text = "" Then txtMovil1.Text = 0
        If txtMovil2.Text = "" Then txtMovil2.Text = 0
        Dim listaMovil = d.proc_ObtenerVehiculoPorNroMovil(Convert.ToInt16(txtMovil1.Value)).ToList
        If listaMovil.Count = 0 And Convert.ToInt16(txtMovil1.Value) <> 0 Then
            MsgBox("Número de móvil inexistente. Corríjalo e intente nuevamente.")
            txtMovil1.Focus()
            txtMovil1.SelectAll()
            Exit Function
        End If
        If txtMovil2.Text <> 0 AndAlso txtMovil2.Text <> String.Empty Then
            listaMovil = d.proc_ObtenerVehiculoPorNroMovil(Convert.ToInt16(txtMovil2.Value)).ToList
            If listaMovil.Count = 0 Then
                MsgBox("Número de móvil inexistente. Corríjalo e intente nuevamente.")
                txtMovil1.Focus()
                txtMovil1.SelectAll()
                Exit Function
            End If
        End If


        If mCliente Is Nothing Then
                Dim idCliente As Integer
            d.proc_ClientesInsert(idCliente, txtNombre.Text, txtDireccion.Text, txtRef.Text, txtTelefono.Text, cboBarrio.SelectedValue, True, txtObs.Text, False, USER_GLOBAL)
            mCliente = d.proc_ClientesLoadByTelefono(txtTelefono.Text).ToList.First
        Else
            Dim C = d.proc_ClientesLoadByTelefono(txtTelefono.Text).ToList.First
            d.proc_ClientesUpdate(C.idCliente, txtNombre.Text, txtDireccion.Text, txtRef.Text, txtTelefono.Text, cboBarrio.SelectedValue, C.estado, txtObs.Text, C.usaVales, USER_GLOBAL)
        End If

        Return True
    End Function

    Private Sub btnServicio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnServicio.Click
        Try
            If Not Validar() Then Exit Sub
            Dim barrio As Integer
            If cboBarrio.SelectedIndex < 0 Then
                d.proc_BarriosInsert(barrio, cboBarrio.Text, 0)
            Else
                barrio = cboBarrio.SelectedValue
            End If
           
            Dim idServicio As Integer
            Dim movil2 As Integer
            Dim movil1 As Integer
            Dim listaMovil = d.proc_ObtenerVehiculoPorNroMovil(Integer.Parse(txtMovil1.Value)).ToList
            Dim ListaMovil2 = d.proc_ObtenerVehiculoPorNroMovil(Integer.Parse(txtMovil2.Value)).ToList

            If listaMovil.Count Then
                movil1 = listaMovil(0).idVehiculo
            End If

            If ListaMovil2.Count Then
                movil2 = ListaMovil2(0).idVehiculo
            End If
            ' verificar si los moviles estan habilitados

            Dim listaHabilitados = d.proc_ObtenerEstadoVehiculos(False, True).ToList
            For Each MovilHab In listaHabilitados
                If MovilHab.NroMovil = txtMovil1.Value Then
                    If CONFIG_GLOBAL.permitirAsignacionMovilInhabilitado Then
                        If CONFIG_GLOBAL.notificarAsignacionMovilInhab Then
                            MsgBox("Se le asignará la carrera al móvil " & txtMovil1.Value & " que está inhabilitado para asignarle carreras")
                        End If
                    Else
                        MsgBox("El móvil " & txtMovil1.Value & " está inhabilitado para asignarle carreras")
                        txtMovil1.Focus()
                        Exit Sub

                    End If
                End If
                If MovilHab.NroMovil = txtMovil2.Value Then
                    If CONFIG_GLOBAL.permitirAsignacionMovilInhabilitado Then
                        If CONFIG_GLOBAL.notificarAsignacionMovilInhab Then
                            MsgBox("Se le asignará la carrera al móvil " & txtMovil2.Value & " que está inhabilitado para asignarle carreras")
                        End If
                    Else
                        MsgBox("El móvil " & txtMovil2.Value & " está inhabilitado para asignarle carreras")
                        txtMovil2.Focus()
                        Exit Sub

                    End If

                End If
            Next
            ' fin de la verificacion

            ' verificar si los moviles estan sancionados

            'Dim listaSancion = d.proc_ObtenerSancionesMovil(movil1).ToList
            'If listaSancion.Count > 0 Then
            '    Try
            '        If listaSancion(0).FechaHasta > NOW_GLOBAL() Then

            '            If CONFIG_GLOBAL.permitirAsignacionMovilInhabilitado Then
            '                If CONFIG_GLOBAL.notificarAsignacionMovilInhab Then
            '                    MsgBox("Se le asignará la carrera al móvil " & txtMovil1.Value & " que está sancionado")
            '                End If
            '            Else
            '                MsgBox("El móvil " & txtMovil1.Value & " está sancionado")
            '                txtMovil1.Focus()
            '                Exit Sub

            '            End If

            '        End If
            '    Catch ex As Exception

            '    End Try

            'End If

            'listaSancion = d.proc_ObtenerSancionesMovil(movil2).ToList
            'If listaSancion.Count > 0 Then
            '    If listaSancion(0).FechaHasta > NOW_GLOBAL Then

            '        If CONFIG_GLOBAL.permitirAsignacionMovilInhabilitado Then
            '            If CONFIG_GLOBAL.notificarAsignacionMovilInhab Then
            '                MsgBox("Se le asignará la carrera al móvil " & txtMovil2.Value & " que está sancionado")
            '            End If
            '        Else
            '            MsgBox("El móvil " & txtMovil2.Value & " está sancionado")
            '            txtMovil2.Focus()
            '            Exit Sub

            '        End If

            '    End If
            'End If

            ' fin de la verificacion
            
            'verificar si el movil está ocupado en otro servicio
            If txtMovil1.Text <> 0 Then
                Dim listaServicios = d.proc_ObtenerServiciosActivosPorMovil(listaMovil(0).idVehiculo())

                If listaServicios.Count > 0 Then
                    Dim servicioActivo = d.proc_ObtenerServiciosActivosPorMovil(listaMovil(0).idVehiculo())(0).idServicio
                    If _idServicio <> servicioActivo Then
                        If MsgBox("El móvil " & txtMovil1.Text & " está ocupado en un servicio." & vbCrLf & "Desea dar por terminado el servicio anterior y asignarlo nuevamente?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                            Dim s = d.proc_ServiciosLoadByPrimaryKey(servicioActivo).ToList.First
                            d.proc_ServiciosUpdate(s.idServicio, s.idVehiculo1, s.idVehiculo2, s.idCliente, s.fechaCreacion, s.fechaServicio, _
                                    NOW_GLOBAL, s.fechaFinalizacion, s.idBarrioOrigen, s.direccionOrigen, s.idBarrioDestino, _
                                      s.direccionDestino, s.tarifa, s.ObservacionesConductor, "", EstadosServicios.Terminado, USER_GLOBAL, s.tipoServicio, s.radioFrecuencia)


                        Else

                            txtMovil1.Focus()
                            Exit Sub
                        End If


                    End If



                End If
            End If

            If txtMovil2.Text <> "0" Then
                Dim listaServicios2 = d.proc_ObtenerServiciosActivosPorMovil(ListaMovil2(0).idVehiculo())

                If listaServicios2.Count > 0 Then

                    If _idServicio <> d.proc_ObtenerServiciosActivosPorMovil(ListaMovil2(0).idVehiculo())(0).idServicio Then
                        MsgBox("El móvil " & txtMovil2.Text & " está ocupado en un servicio " & vbCrLf & "Debe terminar o cancelar este servicio antes de asignar nuevamente el móvil.")
                        txtMovil2.Focus()
                        Exit Sub
                    End If

                End If
            End If
            'fin de la verificacion del movil ocupado

            If mEsNuevo Then
                Try
                    d.proc_ServiciosInsert(idServicio, movil1, _
                                             movil2, _
                                             Integer.Parse(mCliente.idCliente), NOW_GLOBAL, IIf(dtpFecha.Value < NOW_GLOBAL(), NOW_GLOBAL, CDate(dtpFecha.Value)), Nothing, _
                                             Nothing, Nothing, Nothing, barrio, txtDireccion.Text, _
                                             Decimal.Parse(txtTarifa.Value), Nothing, txtObs.Text, _
                                             EstadosServicios.Solicitado, USER_GLOBAL, TipoServicios.Normal, IIf(d.proc_ObtenerUltimoServicio().ToList(0).radioFrecuencia = 1, 2, 1))

                Catch ex As Exception
                    MsgBox("Error insertando servicio")

                End Try
                'd.proc_LogInsert(USER_GLOBAL, String.Format("Cliente: {0} Barrio: {1} - {2}", mCliente.idCliente, _
                '                                       cboBarrio.SelectedValue, cboBarrio.Text), NOW_GLOBAL, _
                '                                      "Inserta servicio normal")
            Else
                Try
                    Dim ser = d.proc_ServiciosLoadByPrimaryKey(_idServicio).ToList.First
                    d.proc_ServiciosUpdate(_idServicio, movil1, _
                                                  movil2, _
                                                  Integer.Parse(mCliente.idCliente), ser.fechaCreacion, CDate(dtpFecha.Value), ser.fechaHR, _
                                                  ser.fechaFinalizacion, Nothing, Nothing, barrio, txtDireccion.Text, _
                                                  Decimal.Parse(txtTarifa.Value), Nothing, txtObs.Text, ser.idEstado, USER_GLOBAL, ser.tipoServicio, ser.radioFrecuencia)

                Catch ex As Exception
                    MsgBox("Error actualizando servicio")
                End Try

                'd.proc_LogInsert(USER_GLOBAL, String.Format("Cliente: {0} Barrio: {1} - {2}", _
                '                                           mCliente.idCliente, cboBarrio.SelectedValue, _
                '                                          cboBarrio.Text), NOW_GLOBAL, "Modifica servicio normal")
            End If
            Try
                d.proc_ColaDelete(1, movil1)
            Catch ex As Exception
                MsgBox("Error eliminando de cola")
                d.proc_LogInsert(USER_GLOBAL, "movil: " & movil1, NOW_GLOBAL, "Error borrando móvil de cola")
            End Try
            StartBroadcast(PORT_Principal, "RefrescarMoviles")
            StartBroadcast(PORT_Resumen, "1")

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
        Close()
    End Sub

    Private Sub btnBarrio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBarrio.Click
        Dim f As New frmBarrios
        Dim barrio = f.AbrirForm()
        If barrio > 0 Then
            ObtenerBarrios()
            cboBarrio.SelectedValue = barrio
        End If
    End Sub

    Public Function EditarServicio(ByVal idServicio As Integer) As Boolean
        mEsNuevo = False
        servicio = d.proc_ServiciosLoadByPrimaryKey(idServicio).ToList.First
        Dim movil1 As Integer
        If servicio.idVehiculo1 <> 0 Then
            movil1 = d.proc_VehiculosLoadByPrimaryKey(servicio.idVehiculo1).ToList.First.NroMovil
        Else
            movil1 = 0
        End If
        Dim movil2 As Integer
        If servicio.idVehiculo2 <> 0 Then
            movil2 = d.proc_VehiculosLoadByPrimaryKey(servicio.idVehiculo2).ToList.First.NroMovil
        Else
            movil2 = 0
        End If

        mCliente = d.proc_ClientesLoadByPrimaryKey(servicio.idCliente).ToList.First
        ObtenerBarrios()
        cboBarrio.Focus()
        _idServicio = servicio.idServicio

        cboBarrio.SelectedValue = servicio.idBarrioDestino
        txtTarifa.Value = servicio.tarifa
        txtTelefono.Text = mCliente.telefono
        txtDireccion.Text = mCliente.direccion
        txtNombre.Text = mCliente.nombre
        txtRef.Text = mCliente.referencia
        txtMovil1.Value = movil1
        txtMovil2.Text = movil2
        txtObs.Text = servicio.ObservacionesCliente
        dtpFecha.Value = servicio.fechaServicio
        Me.btnRF.Enabled = servicio.idEstado = EstadosServicios.Solicitado
        If servicio.radioFrecuencia = 1 Then
            btnRF.Text = "Enviar a RF 2"
        Else
            btnRF.Text = "Enviar a RF 1"
        End If
        If servicio.idEstado = EstadosServicios.Terminado Then
            BloquearControlesParaEdicion()
        End If
        txtMovil1.Focus()
        Show()
        Return True
    End Function

    Private Sub BloquearControlesParaEdicion()
        btnServicio.Enabled = False
        btnRF.Enabled = False


    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Close()
    End Sub

    Private Sub cboBarrio_DropDownClosed(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBarrio.DropDownClosed
        AcceptButton = btnServicio
    End Sub

    Private Sub btnRF_Click(sender As System.Object, e As System.EventArgs) Handles btnRF.Click

        d.proc_ServicioUpdateRF(servicio.idServicio, IIf(servicio.radioFrecuencia = 1, 2, 1))
        StartBroadcast(PORT_Resumen, "1")
        Close()


    End Sub

    Private Sub frmServicios_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class