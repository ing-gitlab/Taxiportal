﻿Imports Common
Public Class frmConfig

#Region "Declaraciones"

    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))


#End Region


#Region "Eventos"

    Private Sub frmConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargarConfig()
    End Sub


    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Try
            Dim rf As Integer
            If CHRF1.Checked And CHRF2.Checked Then
                rf = 0
            ElseIf CHRF1.Checked Then
                rf = 1
            Else
                rf = 2
            End If
            d.proc_ConfiguracionesUpdate(USER_GLOBAL.idUsuario, NOW_GLOBAL, chkMostrarEnCamino.Checked, _
                                         chkMostrarEnCurso.Checked, chkMostrarTerminado.Checked, _
                                         chkMostrarCancelado.Checked, chkAsignarInhab.Checked, chkNotifInhab.Checked, _
                                          txtDias.Value, cboOrigen.SelectedValue, rbNormal.Checked, chkImprimir.Checked, txtMinuto.Value, Decimal.Parse(getSettings("PorcentajeVales")), txtInicio.Value, txtFin.Value, Decimal.Parse(txtImporte.Value), rf)
            CONFIG_GLOBAL = d.proc_ConfiguracionesLoadByPrimaryKey(USER_GLOBAL.idUsuario).ToList(0)

            'd.proc_ConfigGlobalUpdate(0, TextBoxPrompter.Text)
            Me.Close()
        Catch ex As Exception
            Me.Close()
        End Try

    End Sub



#End Region


#Region "Metodos"
    Private Sub CargarConfig()
        On Error Resume Next
        With CONFIG_GLOBAL
            chkAsignarInhab.Checked = .permitirAsignacionMovilInhabilitado
            chkAsignarInhab.Enabled = (d.proc_UsuariosLoadByPrimaryKey(.idUsuario).ToList(0).idTipoUsuario = 1)
            chkMostrarCancelado.Checked = .mostrarCancelado
            chkMostrarEnCamino.Checked = .mostrarEnCamino
            chkMostrarEnCurso.Checked = .mostrarEnCurso
            chkMostrarTerminado.Checked = .mostrarTerminado
            chkNotifInhab.Checked = .notificarAsignacionMovilInhab
            txtDias.Value = .diasAlertaInhab
            chkImprimir.Checked = .imprimirTicketeAutomatico
            txtMinuto.Value = .minutosNotificacionDemora
            Dim ListaZonas = d.proc_ZonasLoadAll.ToList
            cboOrigen.DisplayMember = "descripcion"
            cboOrigen.ValueMember = "idZona"
            cboOrigen.DataSource = ListaZonas
            cboOrigen.SelectedValue = CInt(.zonaDefault)
            rbNormal.Checked = .tipoServicioDefault = 1
            rbRapido.Checked = .tipoServicioDefault = 0
            GroupBox1.Text = "Usuario: " & USER_GLOBAL.idUsuario
            txtInicio.Text = .horaInicioRecargoNocturno
            txtFin.Text = .horaFinRecargoNocturno
            txtImporte.Value = .ImporteRecargoNocturno
            CHRF1.Checked = (.radioFrecuencia = 0 Or .radioFrecuencia = 1)
            CHRF2.Checked = (.radioFrecuencia = 0 Or .radioFrecuencia = 2)



        End With
        'Carga de texto del prompter de la configuración global
        Dim ConfiGlobal = d.proc_ConfigGlobalLoadByPrimaryKey(0).ToList()
        TextBoxPrompter.Text = ConfiGlobal(0).TextoPrompter


    End Sub
#End Region






End Class