Imports Common

Public Class frmRecaudacion
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents monthSelect As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents LbTotal As System.Windows.Forms.Label
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents CboImpTiquete As System.Windows.Forms.CheckBox
    Friend WithEvents BtnImprimir As System.Windows.Forms.Button
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtObservaciones As System.Windows.Forms.RichTextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents BtnGuardar As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents txtA�o As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtAhorroAdicional As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents txtAhorro As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents txtRecaudacion As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents gbPersona As System.Windows.Forms.GroupBox
    Friend WithEvents txtMovil As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents PbPersona As System.Windows.Forms.PictureBox
    Friend WithEvents txtDeuda As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtTotalAdicional As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents txtAhorroObligatorio As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents BtnPerdonar As System.Windows.Forms.Button
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents PicBpico_placa As System.Windows.Forms.PictureBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents A�o2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents MothSelect2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents BtnCancelar2 As System.Windows.Forms.Button
    Friend WithEvents txtImporteCredito As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents dtDesde2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents ChImprimirTiquete As System.Windows.Forms.CheckBox
    Friend WithEvents BtnImprimir2 As System.Windows.Forms.Button
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents DtHasta2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtObservaciones2 As System.Windows.Forms.RichTextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents BtnGuardar2 As System.Windows.Forms.Button
    Friend WithEvents BtnNuevo2 As System.Windows.Forms.Button
    Friend WithEvents GridEX1 As Janus.Windows.GridEX.GridEX
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtConcepto As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtImporteRegresado As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txtCuotasPagadas As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txtRichObservaciones As System.Windows.Forms.RichTextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtValorCuota As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRecaudacion))
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.A�o2 = New System.Windows.Forms.NumericUpDown()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.MothSelect2 = New System.Windows.Forms.NumericUpDown()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtValorCuota = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txtRichObservaciones = New System.Windows.Forms.RichTextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.txtCuotasPagadas = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txtImporteRegresado = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txtConcepto = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtImporteCredito = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.dtDesde2 = New System.Windows.Forms.DateTimePicker()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.DtHasta2 = New System.Windows.Forms.DateTimePicker()
        Me.txtObservaciones2 = New System.Windows.Forms.RichTextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.BtnNuevo2 = New System.Windows.Forms.Button()
        Me.GridEX1 = New Janus.Windows.GridEX.GridEX()
        Me.ChImprimirTiquete = New System.Windows.Forms.CheckBox()
        Me.BtnCancelar2 = New System.Windows.Forms.Button()
        Me.BtnImprimir2 = New System.Windows.Forms.Button()
        Me.BtnGuardar2 = New System.Windows.Forms.Button()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtA�o = New System.Windows.Forms.NumericUpDown()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.monthSelect = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.PicBpico_placa = New System.Windows.Forms.PictureBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.BtnPerdonar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.txtAhorroAdicional = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.txtAhorro = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.txtRecaudacion = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.LbTotal = New System.Windows.Forms.Label()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.CboImpTiquete = New System.Windows.Forms.CheckBox()
        Me.BtnImprimir = New System.Windows.Forms.Button()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.txtObservaciones = New System.Windows.Forms.RichTextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.BtnGuardar = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.gbPersona = New System.Windows.Forms.GroupBox()
        Me.txtMovil = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PbPersona = New System.Windows.Forms.PictureBox()
        Me.txtDeuda = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTotalAdicional = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.txtAhorroObligatorio = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.A�o2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MothSelect2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.txtA�o, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.monthSelect, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PicBpico_placa, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.gbPersona.SuspendLayout()
        CType(Me.PbPersona, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.PictureBox2.Image = Global.TaxiPortal.My.Resources.Resources.Re_Taxi
        Me.PictureBox2.Location = New System.Drawing.Point(653, 32)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(155, 80)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 17
        Me.PictureBox2.TabStop = False
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Papyrus", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(665, 10)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(49, 18)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Label14"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.A�o2)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.Label25)
        Me.TabPage2.Controls.Add(Me.Label26)
        Me.TabPage2.Controls.Add(Me.Label27)
        Me.TabPage2.Controls.Add(Me.Button9)
        Me.TabPage2.Controls.Add(Me.Button10)
        Me.TabPage2.Controls.Add(Me.Label28)
        Me.TabPage2.Controls.Add(Me.Button11)
        Me.TabPage2.Controls.Add(Me.PictureBox3)
        Me.TabPage2.Controls.Add(Me.MothSelect2)
        Me.TabPage2.Controls.Add(Me.Label34)
        Me.TabPage2.Controls.Add(Me.Label43)
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(812, 395)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Recaudaci�n Credito"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'A�o2
        '
        Me.A�o2.Location = New System.Drawing.Point(157, 325)
        Me.A�o2.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.A�o2.Minimum = New Decimal(New Integer() {1900, 0, 0, 0})
        Me.A�o2.Name = "A�o2"
        Me.A�o2.Size = New System.Drawing.Size(64, 20)
        Me.A�o2.TabIndex = 51
        Me.A�o2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.A�o2.Value = New Decimal(New Integer() {1900, 0, 0, 0})
        '
        'Label21
        '
        Me.Label21.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(224, 34)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(32, 23)
        Me.Label21.TabIndex = 47
        Me.Label21.Text = "Sat"
        '
        'Label22
        '
        Me.Label22.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(193, 34)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(24, 23)
        Me.Label22.TabIndex = 46
        Me.Label22.Text = "Fri"
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(154, 35)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(32, 23)
        Me.Label23.TabIndex = 45
        Me.Label23.Text = "Thu"
        '
        'Label24
        '
        Me.Label24.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(121, 35)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(32, 23)
        Me.Label24.TabIndex = 44
        Me.Label24.Text = "Wed"
        '
        'Label25
        '
        Me.Label25.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(90, 35)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(32, 23)
        Me.Label25.TabIndex = 43
        Me.Label25.Text = "Tue"
        '
        'Label26
        '
        Me.Label26.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(56, 35)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(31, 23)
        Me.Label26.TabIndex = 42
        Me.Label26.Text = "Mon"
        '
        'Label27
        '
        Me.Label27.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(19, 36)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(32, 23)
        Me.Label27.TabIndex = 41
        Me.Label27.Text = "Sun"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(27, 11)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(34, 22)
        Me.Button9.TabIndex = 49
        Me.Button9.Text = "<"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(210, 10)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(34, 22)
        Me.Button10.TabIndex = 50
        Me.Button10.Text = ">"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(95, 13)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(59, 15)
        Me.Label28.TabIndex = 48
        Me.Label28.Text = "Label28"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(13, 351)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(210, 23)
        Me.Button11.TabIndex = 37
        Me.Button11.Text = "&Ir"
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.TaxiPortal.My.Resources.Resources.Re_Leyenda
        Me.PictureBox3.Location = New System.Drawing.Point(15, 193)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(120, 116)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 40
        Me.PictureBox3.TabStop = False
        '
        'MothSelect2
        '
        Me.MothSelect2.Location = New System.Drawing.Point(45, 325)
        Me.MothSelect2.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.MothSelect2.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.MothSelect2.Name = "MothSelect2"
        Me.MothSelect2.Size = New System.Drawing.Size(64, 20)
        Me.MothSelect2.TabIndex = 36
        Me.MothSelect2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.MothSelect2.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label34
        '
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(116, 325)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(39, 18)
        Me.Label34.TabIndex = 39
        Me.Label34.Text = "A�o:"
        '
        'Label43
        '
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(12, 327)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(49, 18)
        Me.Label43.TabIndex = 38
        Me.Label43.Text = "Mes:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.Label36)
        Me.GroupBox2.Controls.Add(Me.Label33)
        Me.GroupBox2.Controls.Add(Me.Label35)
        Me.GroupBox2.Controls.Add(Me.txtImporteCredito)
        Me.GroupBox2.Controls.Add(Me.dtDesde2)
        Me.GroupBox2.Controls.Add(Me.Label37)
        Me.GroupBox2.Controls.Add(Me.Label38)
        Me.GroupBox2.Controls.Add(Me.DtHasta2)
        Me.GroupBox2.Controls.Add(Me.txtObservaciones2)
        Me.GroupBox2.Controls.Add(Me.Label39)
        Me.GroupBox2.Controls.Add(Me.Label40)
        Me.GroupBox2.Controls.Add(Me.BtnNuevo2)
        Me.GroupBox2.Controls.Add(Me.GridEX1)
        Me.GroupBox2.Controls.Add(Me.ChImprimirTiquete)
        Me.GroupBox2.Controls.Add(Me.BtnCancelar2)
        Me.GroupBox2.Controls.Add(Me.BtnImprimir2)
        Me.GroupBox2.Controls.Add(Me.BtnGuardar2)
        Me.GroupBox2.Location = New System.Drawing.Point(260, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(539, 389)
        Me.GroupBox2.TabIndex = 35
        Me.GroupBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtValorCuota)
        Me.GroupBox3.Controls.Add(Me.Label46)
        Me.GroupBox3.Controls.Add(Me.txtRichObservaciones)
        Me.GroupBox3.Controls.Add(Me.Label45)
        Me.GroupBox3.Controls.Add(Me.txtCuotasPagadas)
        Me.GroupBox3.Controls.Add(Me.Label44)
        Me.GroupBox3.Controls.Add(Me.txtImporteRegresado)
        Me.GroupBox3.Controls.Add(Me.Label42)
        Me.GroupBox3.Controls.Add(Me.txtConcepto)
        Me.GroupBox3.Controls.Add(Me.Label41)
        Me.GroupBox3.Location = New System.Drawing.Point(16, 150)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(227, 224)
        Me.GroupBox3.TabIndex = 49
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Informacion del Credito"
        '
        'txtValorCuota
        '
        Me.txtValorCuota.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtValorCuota.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtValorCuota.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtValorCuota.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtValorCuota.Location = New System.Drawing.Point(73, 55)
        Me.txtValorCuota.Name = "txtValorCuota"
        Me.txtValorCuota.ReadOnly = True
        Me.txtValorCuota.Size = New System.Drawing.Size(145, 20)
        Me.txtValorCuota.TabIndex = 46
        Me.txtValorCuota.Text = "$ 0"
        Me.txtValorCuota.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtValorCuota.Value = 0
        Me.txtValorCuota.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(6, 58)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(62, 13)
        Me.Label46.TabIndex = 45
        Me.Label46.Text = "Valor Cuota"
        '
        'txtRichObservaciones
        '
        Me.txtRichObservaciones.Location = New System.Drawing.Point(11, 160)
        Me.txtRichObservaciones.Name = "txtRichObservaciones"
        Me.txtRichObservaciones.ReadOnly = True
        Me.txtRichObservaciones.Size = New System.Drawing.Size(207, 44)
        Me.txtRichObservaciones.TabIndex = 44
        Me.txtRichObservaciones.Text = ""
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(9, 144)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(78, 13)
        Me.Label45.TabIndex = 43
        Me.Label45.Text = "Observaciones"
        '
        'txtCuotasPagadas
        '
        Me.txtCuotasPagadas.Location = New System.Drawing.Point(167, 123)
        Me.txtCuotasPagadas.Name = "txtCuotasPagadas"
        Me.txtCuotasPagadas.ReadOnly = True
        Me.txtCuotasPagadas.Size = New System.Drawing.Size(50, 20)
        Me.txtCuotasPagadas.TabIndex = 42
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(70, 124)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(85, 13)
        Me.Label44.TabIndex = 41
        Me.Label44.Text = "Cuotas Pagadas"
        '
        'txtImporteRegresado
        '
        Me.txtImporteRegresado.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtImporteRegresado.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtImporteRegresado.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtImporteRegresado.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtImporteRegresado.Location = New System.Drawing.Point(73, 85)
        Me.txtImporteRegresado.Name = "txtImporteRegresado"
        Me.txtImporteRegresado.ReadOnly = True
        Me.txtImporteRegresado.Size = New System.Drawing.Size(145, 20)
        Me.txtImporteRegresado.TabIndex = 40
        Me.txtImporteRegresado.Text = "$ 0"
        Me.txtImporteRegresado.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtImporteRegresado.Value = 0
        Me.txtImporteRegresado.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(6, 88)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(59, 13)
        Me.Label42.TabIndex = 2
        Me.Label42.Text = "Regresado"
        '
        'txtConcepto
        '
        Me.txtConcepto.Location = New System.Drawing.Point(73, 23)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.ReadOnly = True
        Me.txtConcepto.Size = New System.Drawing.Size(145, 20)
        Me.txtConcepto.TabIndex = 1
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(7, 26)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(53, 13)
        Me.Label41.TabIndex = 0
        Me.Label41.Text = "Concepto"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(472, 197)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(44, 13)
        Me.Label36.TabIndex = 47
        Me.Label36.Text = "a Pagar"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(463, 185)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(59, 13)
        Me.Label33.TabIndex = 44
        Me.Label33.Text = "# Cuota(s) "
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(479, 156)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(30, 31)
        Me.Label35.TabIndex = 45
        Me.Label35.Text = "1"
        '
        'txtImporteCredito
        '
        Me.txtImporteCredito.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtImporteCredito.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtImporteCredito.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtImporteCredito.Enabled = False
        Me.txtImporteCredito.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtImporteCredito.Location = New System.Drawing.Point(344, 231)
        Me.txtImporteCredito.Name = "txtImporteCredito"
        Me.txtImporteCredito.Size = New System.Drawing.Size(179, 20)
        Me.txtImporteCredito.TabIndex = 39
        Me.txtImporteCredito.Text = "$ 0"
        Me.txtImporteCredito.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtImporteCredito.Value = 0
        Me.txtImporteCredito.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'dtDesde2
        '
        Me.dtDesde2.CustomFormat = "dd/MM/yyyy"
        Me.dtDesde2.Enabled = False
        Me.dtDesde2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtDesde2.Location = New System.Drawing.Point(344, 163)
        Me.dtDesde2.Name = "dtDesde2"
        Me.dtDesde2.Size = New System.Drawing.Size(112, 20)
        Me.dtDesde2.TabIndex = 31
        Me.dtDesde2.Value = New Date(2012, 2, 21, 0, 0, 0, 0)
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(260, 198)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(38, 13)
        Me.Label37.TabIndex = 32
        Me.Label37.Text = "&Hasta:"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(260, 165)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(72, 13)
        Me.Label38.TabIndex = 30
        Me.Label38.Text = "Pagar &Desde:"
        '
        'DtHasta2
        '
        Me.DtHasta2.CustomFormat = "dd/MM/yyyy"
        Me.DtHasta2.Enabled = False
        Me.DtHasta2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DtHasta2.Location = New System.Drawing.Point(344, 198)
        Me.DtHasta2.Name = "DtHasta2"
        Me.DtHasta2.Size = New System.Drawing.Size(112, 20)
        Me.DtHasta2.TabIndex = 33
        Me.DtHasta2.Value = New Date(2012, 2, 21, 0, 0, 0, 0)
        '
        'txtObservaciones2
        '
        Me.txtObservaciones2.Enabled = False
        Me.txtObservaciones2.Location = New System.Drawing.Point(262, 276)
        Me.txtObservaciones2.Name = "txtObservaciones2"
        Me.txtObservaciones2.Size = New System.Drawing.Size(261, 56)
        Me.txtObservaciones2.TabIndex = 10
        Me.txtObservaciones2.Text = ""
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(259, 258)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(78, 13)
        Me.Label39.TabIndex = 3
        Me.Label39.Text = "Observaciones"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(260, 234)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(78, 13)
        Me.Label40.TabIndex = 5
        Me.Label40.Text = "Importe Credito"
        '
        'BtnNuevo2
        '
        Me.BtnNuevo2.AccessibleDescription = "&Nuevo"
        Me.BtnNuevo2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnNuevo2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnNuevo2.Image = Global.TaxiPortal.My.Resources.Resources.Re_Credit_card
        Me.BtnNuevo2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.BtnNuevo2.Location = New System.Drawing.Point(409, 338)
        Me.BtnNuevo2.Name = "BtnNuevo2"
        Me.BtnNuevo2.Size = New System.Drawing.Size(55, 45)
        Me.BtnNuevo2.TabIndex = 13
        Me.BtnNuevo2.Tag = "&Nuevo"
        Me.BtnNuevo2.Text = "&Nuevo"
        Me.BtnNuevo2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnNuevo2.UseVisualStyleBackColor = True
        '
        'GridEX1
        '
        Me.GridEX1.Location = New System.Drawing.Point(16, 14)
        Me.GridEX1.Name = "GridEX1"
        Me.GridEX1.Size = New System.Drawing.Size(504, 130)
        Me.GridEX1.TabIndex = 46
        '
        'ChImprimirTiquete
        '
        Me.ChImprimirTiquete.AutoSize = True
        Me.ChImprimirTiquete.Checked = True
        Me.ChImprimirTiquete.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChImprimirTiquete.Enabled = False
        Me.ChImprimirTiquete.Location = New System.Drawing.Point(267, 341)
        Me.ChImprimirTiquete.Name = "ChImprimirTiquete"
        Me.ChImprimirTiquete.Size = New System.Drawing.Size(85, 17)
        Me.ChImprimirTiquete.TabIndex = 11
        Me.ChImprimirTiquete.Text = "Imp. Tiquete"
        Me.ChImprimirTiquete.UseVisualStyleBackColor = True
        '
        'BtnCancelar2
        '
        Me.BtnCancelar2.AccessibleDescription = "&Nuevo"
        Me.BtnCancelar2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCancelar2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCancelar2.Image = Global.TaxiPortal.My.Resources.Resources.redx
        Me.BtnCancelar2.Location = New System.Drawing.Point(409, 338)
        Me.BtnCancelar2.Name = "BtnCancelar2"
        Me.BtnCancelar2.Size = New System.Drawing.Size(55, 45)
        Me.BtnCancelar2.TabIndex = 42
        Me.BtnCancelar2.Tag = "&Cancelar"
        Me.BtnCancelar2.Text = "&Cancelar"
        Me.BtnCancelar2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnCancelar2.UseVisualStyleBackColor = True
        Me.BtnCancelar2.Visible = False
        '
        'BtnImprimir2
        '
        Me.BtnImprimir2.AccessibleDescription = "Imprimir"
        Me.BtnImprimir2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnImprimir2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnImprimir2.Image = Global.TaxiPortal.My.Resources.Resources.Re_Print
        Me.BtnImprimir2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.BtnImprimir2.Location = New System.Drawing.Point(351, 338)
        Me.BtnImprimir2.Name = "BtnImprimir2"
        Me.BtnImprimir2.Size = New System.Drawing.Size(55, 45)
        Me.BtnImprimir2.TabIndex = 12
        Me.BtnImprimir2.Tag = "Imprimir"
        Me.BtnImprimir2.Text = "Imprimir"
        Me.BtnImprimir2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnImprimir2.UseVisualStyleBackColor = True
        '
        'BtnGuardar2
        '
        Me.BtnGuardar2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnGuardar2.Enabled = False
        Me.BtnGuardar2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnGuardar2.Image = Global.TaxiPortal.My.Resources.Resources.Re_Save
        Me.BtnGuardar2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.BtnGuardar2.Location = New System.Drawing.Point(467, 338)
        Me.BtnGuardar2.Name = "BtnGuardar2"
        Me.BtnGuardar2.Size = New System.Drawing.Size(55, 45)
        Me.BtnGuardar2.TabIndex = 14
        Me.BtnGuardar2.Tag = "Guardar  Imprimir"
        Me.BtnGuardar2.Text = "Guardar"
        Me.BtnGuardar2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnGuardar2.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txtA�o)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.PictureBox1)
        Me.TabPage1.Controls.Add(Me.monthSelect)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Button2)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.Button3)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(812, 395)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Recaudaci�n Diaria"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'txtA�o
        '
        Me.txtA�o.Location = New System.Drawing.Point(157, 325)
        Me.txtA�o.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.txtA�o.Minimum = New Decimal(New Integer() {1900, 0, 0, 0})
        Me.txtA�o.Name = "txtA�o"
        Me.txtA�o.Size = New System.Drawing.Size(64, 20)
        Me.txtA�o.TabIndex = 35
        Me.txtA�o.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtA�o.Value = New Decimal(New Integer() {1900, 0, 0, 0})
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(224, 34)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(32, 23)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Sat"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(193, 34)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(24, 23)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Fri"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(154, 35)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 23)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Thu"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(121, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(32, 23)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Wed"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(13, 351)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(210, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "&Ir"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(90, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 23)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Tue"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.TaxiPortal.My.Resources.Resources.Re_Leyenda
        Me.PictureBox1.Location = New System.Drawing.Point(15, 193)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(120, 116)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 16
        Me.PictureBox1.TabStop = False
        '
        'monthSelect
        '
        Me.monthSelect.Location = New System.Drawing.Point(45, 325)
        Me.monthSelect.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.monthSelect.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.monthSelect.Name = "monthSelect"
        Me.monthSelect.Size = New System.Drawing.Size(64, 20)
        Me.monthSelect.TabIndex = 1
        Me.monthSelect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.monthSelect.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(56, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 23)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Mon"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(19, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Sun"
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(116, 325)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(39, 18)
        Me.Label9.TabIndex = 11
        Me.Label9.Text = "A�o:"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(27, 11)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(34, 22)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = "<"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 327)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 18)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Mes:"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(210, 10)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(34, 22)
        Me.Button3.TabIndex = 13
        Me.Button3.Text = ">"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(95, 13)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(59, 15)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Label10"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.Label32)
        Me.GroupBox1.Controls.Add(Me.PicBpico_placa)
        Me.GroupBox1.Controls.Add(Me.Label31)
        Me.GroupBox1.Controls.Add(Me.BtnPerdonar)
        Me.GroupBox1.Controls.Add(Me.btnCancelar)
        Me.GroupBox1.Controls.Add(Me.txtAhorroAdicional)
        Me.GroupBox1.Controls.Add(Me.txtAhorro)
        Me.GroupBox1.Controls.Add(Me.txtRecaudacion)
        Me.GroupBox1.Controls.Add(Me.LbTotal)
        Me.GroupBox1.Controls.Add(Me.dtpDesde)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.CboImpTiquete)
        Me.GroupBox1.Controls.Add(Me.BtnImprimir)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.dtpHasta)
        Me.GroupBox1.Controls.Add(Me.txtObservaciones)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.BtnGuardar)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.btnNuevo)
        Me.GroupBox1.Location = New System.Drawing.Point(260, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(539, 374)
        Me.GroupBox1.TabIndex = 34
        Me.GroupBox1.TabStop = False
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(445, 45)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(38, 13)
        Me.Label30.TabIndex = 44
        Me.Label30.Text = "# Dias"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(414, 59)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(106, 13)
        Me.Label32.TabIndex = 47
        Me.Label32.Text = "Excluye Pico y Placa"
        '
        'PicBpico_placa
        '
        Me.PicBpico_placa.Image = Global.TaxiPortal.My.Resources.Resources.Re_Pico_Placa
        Me.PicBpico_placa.Location = New System.Drawing.Point(347, 93)
        Me.PicBpico_placa.Name = "PicBpico_placa"
        Me.PicBpico_placa.Size = New System.Drawing.Size(178, 113)
        Me.PicBpico_placa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBpico_placa.TabIndex = 46
        Me.PicBpico_placa.TabStop = False
        Me.PicBpico_placa.Visible = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(449, 14)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(30, 31)
        Me.Label31.TabIndex = 45
        Me.Label31.Text = "1"
        '
        'BtnPerdonar
        '
        Me.BtnPerdonar.Location = New System.Drawing.Point(391, 183)
        Me.BtnPerdonar.Name = "BtnPerdonar"
        Me.BtnPerdonar.Size = New System.Drawing.Size(77, 23)
        Me.BtnPerdonar.TabIndex = 43
        Me.BtnPerdonar.Text = "Perdonar"
        Me.BtnPerdonar.UseVisualStyleBackColor = True
        Me.BtnPerdonar.Visible = False
        '
        'btnCancelar
        '
        Me.btnCancelar.AccessibleDescription = "&Nuevo"
        Me.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Image = Global.TaxiPortal.My.Resources.Resources.redx
        Me.btnCancelar.Location = New System.Drawing.Point(347, 315)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(55, 45)
        Me.btnCancelar.TabIndex = 42
        Me.btnCancelar.Tag = "&Cancelar"
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCancelar.UseVisualStyleBackColor = True
        Me.btnCancelar.Visible = False
        '
        'txtAhorroAdicional
        '
        Me.txtAhorroAdicional.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtAhorroAdicional.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtAhorroAdicional.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtAhorroAdicional.Enabled = False
        Me.txtAhorroAdicional.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtAhorroAdicional.Location = New System.Drawing.Point(139, 129)
        Me.txtAhorroAdicional.Name = "txtAhorroAdicional"
        Me.txtAhorroAdicional.Size = New System.Drawing.Size(193, 20)
        Me.txtAhorroAdicional.TabIndex = 41
        Me.txtAhorroAdicional.Text = "$ 0"
        Me.txtAhorroAdicional.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtAhorroAdicional.Value = 0
        Me.txtAhorroAdicional.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'txtAhorro
        '
        Me.txtAhorro.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtAhorro.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtAhorro.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtAhorro.Enabled = False
        Me.txtAhorro.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtAhorro.Location = New System.Drawing.Point(139, 97)
        Me.txtAhorro.Name = "txtAhorro"
        Me.txtAhorro.Size = New System.Drawing.Size(193, 20)
        Me.txtAhorro.TabIndex = 40
        Me.txtAhorro.Text = "$ 0"
        Me.txtAhorro.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtAhorro.Value = 0
        Me.txtAhorro.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'txtRecaudacion
        '
        Me.txtRecaudacion.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtRecaudacion.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtRecaudacion.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtRecaudacion.Enabled = False
        Me.txtRecaudacion.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtRecaudacion.Location = New System.Drawing.Point(139, 60)
        Me.txtRecaudacion.Name = "txtRecaudacion"
        Me.txtRecaudacion.Size = New System.Drawing.Size(193, 20)
        Me.txtRecaudacion.TabIndex = 39
        Me.txtRecaudacion.Text = "$ 0"
        Me.txtRecaudacion.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtRecaudacion.Value = 0
        Me.txtRecaudacion.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'LbTotal
        '
        Me.LbTotal.AutoSize = True
        Me.LbTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbTotal.Location = New System.Drawing.Point(151, 171)
        Me.LbTotal.Name = "LbTotal"
        Me.LbTotal.Size = New System.Drawing.Size(45, 13)
        Me.LbTotal.TabIndex = 34
        Me.LbTotal.Text = "TOTAL:"
        '
        'dtpDesde
        '
        Me.dtpDesde.CustomFormat = "dd/MM/yyyy"
        Me.dtpDesde.Enabled = False
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(90, 19)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(112, 20)
        Me.dtpDesde.TabIndex = 31
        Me.dtpDesde.Value = New Date(2012, 2, 21, 0, 0, 0, 0)
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(231, 21)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(38, 13)
        Me.Label15.TabIndex = 32
        Me.Label15.Text = "&Hasta:"
        '
        'CboImpTiquete
        '
        Me.CboImpTiquete.AutoSize = True
        Me.CboImpTiquete.Checked = True
        Me.CboImpTiquete.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CboImpTiquete.Enabled = False
        Me.CboImpTiquete.Location = New System.Drawing.Point(139, 315)
        Me.CboImpTiquete.Name = "CboImpTiquete"
        Me.CboImpTiquete.Size = New System.Drawing.Size(100, 17)
        Me.CboImpTiquete.TabIndex = 11
        Me.CboImpTiquete.Text = "Imprimir Tiquete"
        Me.CboImpTiquete.UseVisualStyleBackColor = True
        '
        'BtnImprimir
        '
        Me.BtnImprimir.AccessibleDescription = "Imprimir"
        Me.BtnImprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnImprimir.Image = Global.TaxiPortal.My.Resources.Resources.Re_Print
        Me.BtnImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.BtnImprimir.Location = New System.Drawing.Point(281, 316)
        Me.BtnImprimir.Name = "BtnImprimir"
        Me.BtnImprimir.Size = New System.Drawing.Size(55, 45)
        Me.BtnImprimir.TabIndex = 12
        Me.BtnImprimir.Tag = "Imprimir"
        Me.BtnImprimir.Text = "Imprimir"
        Me.BtnImprimir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnImprimir.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(12, 19)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(72, 13)
        Me.Label20.TabIndex = 30
        Me.Label20.Text = "Pagar &Desde:"
        '
        'dtpHasta
        '
        Me.dtpHasta.CustomFormat = "dd/MM/yyyy"
        Me.dtpHasta.Enabled = False
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(284, 19)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(105, 20)
        Me.dtpHasta.TabIndex = 33
        Me.dtpHasta.Value = New Date(2012, 2, 21, 0, 0, 0, 0)
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Enabled = False
        Me.txtObservaciones.Location = New System.Drawing.Point(19, 212)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(449, 81)
        Me.txtObservaciones.TabIndex = 10
        Me.txtObservaciones.Text = ""
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(16, 193)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(78, 13)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "Observaciones"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(14, 60)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(109, 13)
        Me.Label17.TabIndex = 5
        Me.Label17.Text = "Importe Recaudaci�n"
        '
        'BtnGuardar
        '
        Me.BtnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnGuardar.Enabled = False
        Me.BtnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnGuardar.Image = Global.TaxiPortal.My.Resources.Resources.Re_Save
        Me.BtnGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.BtnGuardar.Location = New System.Drawing.Point(413, 315)
        Me.BtnGuardar.Name = "BtnGuardar"
        Me.BtnGuardar.Size = New System.Drawing.Size(55, 45)
        Me.BtnGuardar.TabIndex = 14
        Me.BtnGuardar.Tag = "Guardar  Imprimir"
        Me.BtnGuardar.Text = "Guardar"
        Me.BtnGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnGuardar.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(16, 94)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(76, 13)
        Me.Label18.TabIndex = 7
        Me.Label18.Text = "Importe Ahorro"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(16, 133)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(122, 13)
        Me.Label19.TabIndex = 9
        Me.Label19.Text = "Importe Ahorro Adicional"
        '
        'btnNuevo
        '
        Me.btnNuevo.AccessibleDescription = "&Nuevo"
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.Image = Global.TaxiPortal.My.Resources.Resources.Re_Credit_card
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnNuevo.Location = New System.Drawing.Point(347, 315)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(55, 45)
        Me.btnNuevo.TabIndex = 13
        Me.btnNuevo.Tag = "&Nuevo"
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(12, 143)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(820, 421)
        Me.TabControl1.TabIndex = 6
        '
        'gbPersona
        '
        Me.gbPersona.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbPersona.Controls.Add(Me.Label14)
        Me.gbPersona.Controls.Add(Me.txtMovil)
        Me.gbPersona.Controls.Add(Me.PictureBox2)
        Me.gbPersona.Controls.Add(Me.Label11)
        Me.gbPersona.Controls.Add(Me.PbPersona)
        Me.gbPersona.Controls.Add(Me.txtDeuda)
        Me.gbPersona.Controls.Add(Me.Label12)
        Me.gbPersona.Controls.Add(Me.txtTotalAdicional)
        Me.gbPersona.Controls.Add(Me.txtAhorroObligatorio)
        Me.gbPersona.Controls.Add(Me.Label13)
        Me.gbPersona.Controls.Add(Me.Label29)
        Me.gbPersona.Location = New System.Drawing.Point(14, 12)
        Me.gbPersona.Name = "gbPersona"
        Me.gbPersona.Size = New System.Drawing.Size(814, 119)
        Me.gbPersona.TabIndex = 24
        Me.gbPersona.TabStop = False
        Me.gbPersona.Text = "Nombre de la Persona"
        '
        'txtMovil
        '
        Me.txtMovil.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtMovil.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtMovil.Enabled = False
        Me.txtMovil.Location = New System.Drawing.Point(389, 19)
        Me.txtMovil.Name = "txtMovil"
        Me.txtMovil.Size = New System.Drawing.Size(48, 20)
        Me.txtMovil.TabIndex = 7
        Me.txtMovil.Text = "0"
        Me.txtMovil.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtMovil.Value = CType(0, Short)
        Me.txtMovil.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(338, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "&M�vil:"
        '
        'PbPersona
        '
        Me.PbPersona.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PbPersona.Location = New System.Drawing.Point(18, 19)
        Me.PbPersona.Name = "PbPersona"
        Me.PbPersona.Size = New System.Drawing.Size(106, 93)
        Me.PbPersona.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PbPersona.TabIndex = 93
        Me.PbPersona.TabStop = False
        '
        'txtDeuda
        '
        Me.txtDeuda.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtDeuda.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtDeuda.DecimalDigits = 0
        Me.txtDeuda.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtDeuda.Location = New System.Drawing.Point(488, 58)
        Me.txtDeuda.Name = "txtDeuda"
        Me.txtDeuda.ReadOnly = True
        Me.txtDeuda.Size = New System.Drawing.Size(122, 20)
        Me.txtDeuda.TabIndex = 5
        Me.txtDeuda.Text = "$ 0"
        Me.txtDeuda.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtDeuda.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(437, 61)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(45, 13)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Deuda: "
        '
        'txtTotalAdicional
        '
        Me.txtTotalAdicional.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtTotalAdicional.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtTotalAdicional.DecimalDigits = 0
        Me.txtTotalAdicional.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtTotalAdicional.Location = New System.Drawing.Point(271, 87)
        Me.txtTotalAdicional.Name = "txtTotalAdicional"
        Me.txtTotalAdicional.ReadOnly = True
        Me.txtTotalAdicional.Size = New System.Drawing.Size(122, 20)
        Me.txtTotalAdicional.TabIndex = 3
        Me.txtTotalAdicional.Text = "$ 0"
        Me.txtTotalAdicional.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtTotalAdicional.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'txtAhorroObligatorio
        '
        Me.txtAhorroObligatorio.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtAhorroObligatorio.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtAhorroObligatorio.DecimalDigits = 0
        Me.txtAhorroObligatorio.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtAhorroObligatorio.Location = New System.Drawing.Point(271, 58)
        Me.txtAhorroObligatorio.Name = "txtAhorroObligatorio"
        Me.txtAhorroObligatorio.ReadOnly = True
        Me.txtAhorroObligatorio.Size = New System.Drawing.Size(122, 20)
        Me.txtAhorroObligatorio.TabIndex = 1
        Me.txtAhorroObligatorio.Text = "$ 0"
        Me.txtAhorroObligatorio.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtAhorroObligatorio.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(170, 90)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(90, 13)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "Ahorro Adicional: "
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(170, 61)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(97, 13)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "Ahorro Obligatorio: "
        '
        'frmRecaudacion
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(842, 575)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.gbPersona)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmRecaudacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "RECAUDACI�N"
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.A�o2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MothSelect2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.txtA�o, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.monthSelect, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PicBpico_placa, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.gbPersona.ResumeLayout(False)
        Me.gbPersona.PerformLayout()
        CType(Me.PbPersona, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "DECLARACIONES"
    Private dateButts() As myButt
    Dim _LocalValues(6) As String 'to store the local day names
    Dim _UltimaRecaudacion As Date
    Dim _d As New DataClasses1DataContext(getSettings("dbConnection"))
    Dim _Persona As proc_PersonasLoadByPrimaryKeyResult
    Dim _Vehiculos() As proc_obtenerVehiculosPorConductorResult
    Dim _RecaudacionesPers() As proc_RecaudacionLoadByIdPersonaResult
    Dim _RecaudacionesFech() As proc_RecaudacionLoadByFECHAResult
    Dim _Festivos() As proc_FestivosLoadByMESResult
    Dim _Pico_Placa() As proc_ObtenerDiasPicoYPlacaPorMovilyFechaResult
    Dim _img As Image
    Dim _EsNuevo As Boolean
    Dim _EsNuevo2 As Boolean
    Dim _Perdonar As Boolean = False
    Dim _Calendario As Boolean
    Dim _idRecaudacion As Integer
    Dim _DataTableEncabezado As DataTable
    Dim _DataTableDetalle As DataTable
    Dim _DiasPagar As Integer
    Dim _DiasPagar2 As Integer
    Dim EsFestivo As Boolean = False
    Dim _Credito As Boolean = False
    Dim _PagoCredito As Boolean = False
    Dim _creditoDesde As Date
    Dim _CreditoHasta As Date
    Dim _PrestamoCredito() As proc_ObtenerRecaudacionPrestamoCalendarResult
    Dim _PrestamosPer() As proc_RecaudacionPrestamoLoadByIdPersonaResult
    Dim StartingDateLabel As Integer
#End Region





#Region "RECAUDACION_DIARIA"

#Region "METODOS"
    Public Class myButt
        Inherits System.Windows.Forms.Button
    End Class
    Private Sub CheckDates1(ByVal strCurrentMonth As Integer, ByVal strCurrentYear As Integer, Exis_recaudacion As Boolean, OtroMes As Boolean)
        Dim FirstDay, MonthName As String
        Dim MyDate As Date
        Dim i, TotalDays As Integer


        'If Exis_recaudacion Then
        '    RecaudacionesPers = d.proc_RecaudacionLoadByIdPersona(3).ToArray
        'End If

        If OtroMes Then
            Dim fecha As Date = "1-" + monthSelect.Value.ToString + "-" + txtA�o.Value.ToString
            _RecaudacionesFech = _d.proc_RecaudacionLoadByFECHA(_Persona.idPersona, fecha, 1).ToArray
        End If

        'make all buttons invisible until changes are done
        For i = 0 To 41
            dateButts(i).Visible = False
        Next

        MyDate = "#1/" & strCurrentMonth & "/" & strCurrentYear & "#"
        MonthName = Format(MyDate, "MMMM")
        'label10 to show the month name in local/regional name
        Label10.Text = MonthName.ToUpper
        Label28.Text = MonthName.ToUpper

        TotalDays = System.DateTime.DaysInMonth(strCurrentYear, strCurrentMonth)

        FirstDay = Format(MyDate, "ddd")

        'Here we use the local values stored in Form1_load()
        'and check which is the first day of the month 
        StartingDateLabel = Nothing
        If FirstDay = _LocalValues(0) Then     'if sunday
            StartingDateLabel = "0"
        ElseIf FirstDay = _LocalValues(1) Then 'if monday
            StartingDateLabel = "1"
        ElseIf FirstDay = _LocalValues(2) Then 'if tuesday
            StartingDateLabel = "2"
        ElseIf FirstDay = _LocalValues(3) Then 'if wednesday
            StartingDateLabel = "3"
        ElseIf FirstDay = _LocalValues(4) Then 'if thursday
            StartingDateLabel = "4"
        ElseIf FirstDay = _LocalValues(5) Then 'if friday
            StartingDateLabel = "5"
        ElseIf FirstDay = _LocalValues(6) Then 'if saturday
            StartingDateLabel = "6"
        End If

        'for all buttons
        For i = 0 To 41
            dateButts(i).Enabled = False
            dateButts(i).Text = ""
            dateButts(i).BackColor = Color.LightGray 'bcolor for the non-days
        Next

        'only for buttons representing days
        For i = 1 To TotalDays
            dateButts(StartingDateLabel).Enabled = True

            'here you could check with a database and replace the extra blank spaces 
            'e.g. with an asterisk to show an appointment or change the backcolor
            dateButts(StartingDateLabel).Text = "  " & i.ToString
            dateButts(StartingDateLabel).BackColor = Color.White


            ' If StartingDateLabel <> -1 And StartingDateLabel <> 0 And StartingDateLabel <> 6 And StartingDateLabel <> 7 And StartingDateLabel <> 13 And StartingDateLabel <> 14 And StartingDateLabel <> 20 And StartingDateLabel <> 21 And StartingDateLabel <> 27 And StartingDateLabel <> 28 And StartingDateLabel <> 34 And StartingDateLabel <> 35 Then
            dateButts(StartingDateLabel).ForeColor = Color.Black
            'If Exis_recaudacion Then
            '    For index = 0 To RecaudacionesPers.Length - 1
            '        If RecaudacionesPers(index).fechaPagada.Value.Day = i Then
            '            dateButts(StartingDateLabel).BackColor = Color.LightGreen
            '            index = RecaudacionesPers.Length
            '        Else
            '            dateButts(StartingDateLabel).ForeColor = Color.Black
            '            dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
            '        End If
            '    Next
            'Else
            If OtroMes Then
                For index = 0 To _RecaudacionesFech.Length - 1
                    If _RecaudacionesFech(index).fechaPagada.Value.Day = i Then
                        dateButts(StartingDateLabel).BackColor = Color.LightGreen
                        index = _RecaudacionesFech.Length
                    Else
                        Dim fecha As Date = i.ToString + "-" + monthSelect.Value.ToString + "-" + txtA�o.Value.ToString
                        If fecha < Now Then
                            dateButts(StartingDateLabel).ForeColor = Color.Black
                            dateButts(StartingDateLabel).BackColor = Color.LightPink
                        Else
                            dateButts(StartingDateLabel).ForeColor = Color.Black
                            dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
                        End If
                    End If
                Next
            End If

            If _RecaudacionesFech.Length = 0 Then
                Dim fecha As Date = i.ToString + "-" + monthSelect.Value.ToString + "-" + txtA�o.Value.ToString
                If fecha < Now Then
                    dateButts(StartingDateLabel).ForeColor = Color.Black
                    dateButts(StartingDateLabel).BackColor = Color.LightPink
                Else
                    dateButts(StartingDateLabel).ForeColor = Color.Black
                    dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
                End If
            End If

            If StartingDateLabel <> -1 And StartingDateLabel <> 0 And StartingDateLabel <> 6 And StartingDateLabel <> 7 And StartingDateLabel <> 13 And StartingDateLabel <> 14 And StartingDateLabel <> 20 And StartingDateLabel <> 21 And StartingDateLabel <> 27 And StartingDateLabel <> 28 And StartingDateLabel <> 34 And StartingDateLabel <> 35 Then
                For index = 0 To _Pico_Placa.Length - 1

                    If _Pico_Placa(index).Dia.Day = i And _Pico_Placa(index).Dia.Month = monthSelect.Value And _Pico_Placa(index).Dia.Year = txtA�o.Value Then
                        'MsgBox(_Pico_Placa(index).Dia)
                        dateButts(StartingDateLabel).Enabled = True
                        dateButts(StartingDateLabel).ForeColor = Color.Black
                        dateButts(StartingDateLabel).BackColor = Color.LightSteelBlue
                        dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black

                        Exit For
                    End If

                Next
            End If

            For index = 0 To _Festivos.Length - 1
                If _Festivos(index).diaFestivo.Day = i And _Festivos(index).diaFestivo.Month = monthSelect.Value And _Festivos(index).diaFestivo.Year = txtA�o.Value Then
                    dateButts(StartingDateLabel).ForeColor = Color.Purple
                    dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
                    Exit For
                End If

            Next
            If StartingDateLabel = -1 Or StartingDateLabel = 0 Or StartingDateLabel = 6 Or StartingDateLabel = 7 Or StartingDateLabel = 13 Or StartingDateLabel = 14 Or StartingDateLabel = 20 Or StartingDateLabel = 21 Or StartingDateLabel = 27 Or StartingDateLabel = 28 Or StartingDateLabel = 34 Or StartingDateLabel = 35 Then
                dateButts(StartingDateLabel).ForeColor = Color.Purple
                dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
            End If



            StartingDateLabel += 1
        Next




        'after all the changes make them visibe
        For i = 0 To 41
            dateButts(i).Visible = True
        Next

    End Sub
    'Private Sub ObtenerConductores()
    '    Dim d As New DataClasses1DataContext(getSettings("dbConnection"))
    '    Dim Per = d.proc_ObtenerPersonas.ToList
    '    cboConductor.DataSource = Per
    '    cboConductor.DisplayMember = "Nombre"
    '    cboConductor.ValueMember = "idPersona"

    'End Sub

    Function Validar()
        If txtRecaudacion.Value <> _d.proc_ConfigGlobalLoadByPrimaryKey(0).ToList.First.pagoDiario Then
            If MsgBox("Va a realizar un pago de " & LbTotal.Text & " correspondiente a " & Label31.Text + " Dia(s)" & vbCrLf & "                                           �Esta Seguro?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, getSettings("Pregunta")) = MsgBoxResult.Yes Then
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Public Sub AbrirFrmRecaudacion(PersonaFromP As proc_PersonasLoadByPrimaryKeyResult)
        If PersonaFromP Is Nothing Then
            Me._Persona = _d.proc_PersonasLoadByPrimaryKey(_d.proc_Conductores_VehiculosLoadAll().ToList.First.idConductor).ToList.First
        Else
            Me._Persona = PersonaFromP
        End If

        Label14.Text = "Hoy: " + Format(Now, "dd - MMMMMM - yyyy").ToString

        ReDim dateButts(41) '41 total flat-buttons created at run time
        Dim i, lblTop, lblLeft, datesTotalRow, datesTotalCol As Integer
        lblTop = 220
        lblLeft = 29
        datesTotalRow = 1
        datesTotalCol = 1

        'First rename the day-name labels according to the local-regional names
        'each name gets stored in a variable used later by the Checkdates()
        Dim DNdate As Date
        Dim DayName As String

        DNdate = "#1/6/2008#" '(the first of june of 2008 is sunday)
        DayName = Format(DNdate, "ddd") 'in USA DayName should have the value "sun" now
        Label1.Text = DayName.ToUpper
        Label27.Text = DayName.ToUpper
        _LocalValues(0) = DayName

        DNdate = "#2/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label2.Text = DayName.ToUpper
        Label26.Text = DayName.ToUpper
        _LocalValues(1) = DayName

        DNdate = "#3/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label3.Text = DayName.ToUpper
        Label25.Text = DayName.ToUpper
        _LocalValues(2) = DayName
        DNdate = "#4/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label4.Text = DayName.ToUpper
        Label24.Text = DayName.ToUpper
        _LocalValues(3) = DayName
        DNdate = "#5/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label5.Text = DayName.ToUpper
        Label23.Text = DayName.ToUpper
        _LocalValues(4) = DayName
        DNdate = "#6/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label6.Text = DayName.ToUpper
        Label22.Text = DayName.ToUpper
        _LocalValues(5) = DayName
        DNdate = "#7/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label7.Text = DayName.ToUpper
        Label21.Text = DayName.ToUpper
        _LocalValues(6) = DayName

        'create all the buttons at run time
        For i = 0 To 41
            dateButts(i) = New myButt
            dateButts(i).Text = ""
            dateButts(i).Font = New Font(Me.Font.FontFamily, 6.5, FontStyle.Bold)
            dateButts(i).Top = lblTop
            dateButts(i).Left = lblLeft
            dateButts(i).Width = 35
            dateButts(i).Height = 22
            'add handler to use events with the run time created buttons
            AddHandler dateButts(i).Click, AddressOf anybutt_click
            dateButts(i).Cursor = Cursors.Hand
            dateButts(i).FlatStyle = FlatStyle.Flat
            dateButts(i).FlatAppearance.MouseOverBackColor = Color.AliceBlue
            dateButts(i).FlatAppearance.MouseDownBackColor = Color.AliceBlue
            dateButts(i).Tag = i 'this just for test
            dateButts(i).Visible = False

            'select: if the day is located in the sunday column make the number red
            Select Case i
                Case -1, 0
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 6, 7
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 13, 14
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 20, 21
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 27, 28
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 34, 35
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
            End Select

            Me.Controls.Add(dateButts(i))

            datesTotalRow = datesTotalRow + 1
            datesTotalCol = datesTotalCol + 1
            lblLeft = lblLeft + 34 '40
            If datesTotalRow > 7 Then
                lblTop = lblTop + 21 '27
                datesTotalRow = 1
            End If
            If datesTotalCol > 7 Then
                lblLeft = 29
                datesTotalCol = 1
            End If

        Next
        Me.TabControl1.SendToBack()
        LlenarDatos()
        Button1_Click(Me, Nothing)
        txtMovil.Enabled = True
    End Sub

    Public Sub LlenarDatos()
        Dim img As New SQLImages.Main
        Dim aux() As Byte


        If _d.proc_PersonasLoadByPrimaryKey(_Persona.idPersona).ToList.First.Imagen Is Nothing Then
            PbPersona.Image = Nothing
        Else
            aux = _d.proc_PersonasLoadByPrimaryKey(_Persona.idPersona).ToList.First.Imagen.ToArray
            If aux.Length = 0 Then
                PbPersona.Image = Nothing
            Else
                PbPersona.Image = img.ByteToBitmap(aux)
            End If
        End If

        Me._Vehiculos = _d.proc_obtenerVehiculosPorConductor(_Persona.idPersona).ToArray
        If _Vehiculos.Count = 0 Then
            MsgBox("Esta persona no es conductor de ningun vehiculo." & vbCrLf & "Por favor primero as�gnelo como conductor en la ventana de Vehiculos y luego intente nuevamente.")
        Else
            txtMovil.Value = _Vehiculos.First.NroMovil
        End If

        gbPersona.Text = _Persona.nombre
        If _d.proc_RecaudacionLoadByIdPersona(_Persona.idPersona).Count > 0 Then
            _RecaudacionesPers = _d.proc_RecaudacionLoadByIdPersona(_Persona.idPersona).ToArray
            txtA�o.Text = _RecaudacionesPers(0).fechaPagada.Value.Year
            monthSelect.Value = _RecaudacionesPers(0).fechaPagada.Value.Month
        Else
            txtA�o.Text = Now.Year
            monthSelect.Value = Now.Month
            _RecaudacionesPers = Nothing
        End If
        _Festivos = _d.proc_FestivosLoadByMES(Now.ToShortDateString).ToArray
        'MsgBox(Date.DaysInMonth(txtA�o.Value.ToString, monthSelect.Value.ToString))
        _Pico_Placa = _d.proc_ObtenerDiasPicoYPlacaPorMovilyFecha(_d.proc_VehiculosLoadByPrimaryKey(_Vehiculos.First.idVehiculo).First.Placa, ("1-1-" + txtA�o.Value.ToString), ("31-12-" + txtA�o.Value.ToString)).ToArray ' ((Date.DaysInMonth(txtA�o.Value.ToString, monthSelect.Value.ToString).ToString).ToString + "-" + monthSelect.Value.ToString + "-" + txtA�o.Value.ToString)).ToArray
        'MsgBox(_Pico_Placa.Length)
        Dim CTA = _d.proc_ObtenerCuentaAhorro(Int32.Parse(_Persona.idPersona)).ToList.First
        Me.txtAhorroObligatorio.Value = CTA.importeAhorro
        Me.txtTotalAdicional.Value = CTA.importeAhorroAdicional
        Me.txtDeuda.Value = CTA.importeDeuda
    End Sub

    Public Sub activarBotones(FV As Boolean)
        dtpDesde.Enabled = False
        dtpHasta.Enabled = FV
        txtAhorroAdicional.Enabled = FV
        txtObservaciones.Enabled = FV
        BtnGuardar.Enabled = FV
        'BtnImprimir.Enabled = FV
        CboImpTiquete.Enabled = FV
    End Sub
    Private Sub totalizar()
        Try
            If BtnGuardar.Enabled And _Perdonar = False And _Calendario = False Then
                Dim d1 = DateValue(dtpDesde.Value.ToShortDateString)
                Dim d2 = DateValue(dtpHasta.Value.ToShortDateString)
                Dim d3 = d2 - d1
                EsFestivo = False
                Label31.Text = d3.TotalDays + 1
                _DiasPagar = d3.TotalDays + 1
                Dim conteo As DateTime = dtpDesde.Value.ToShortDateString

                For index = 1 To (d3.TotalDays + 1)

                    For i = 0 To _Pico_Placa.Length - 1

                        If conteo.Equals(_Pico_Placa(i).Dia) And conteo.DayOfWeek <> DayOfWeek.Saturday And conteo.DayOfWeek <> DayOfWeek.Sunday Then

                            For j = 0 To _Festivos.Length - 1
                                If conteo.Equals(_Festivos(j).diaFestivo) Then
                                    EsFestivo = True
                                    Exit For
                                End If
                            Next
                            If Not EsFestivo Then
                                Label31.Text = Int32.Parse(Label31.Text) - 1
                            End If
                            Exit For
                        End If
                    Next
                    conteo = DateAdd(DateInterval.Day, 1, conteo)
                Next
                txtRecaudacion.Value = (_d.proc_ConfigGlobalLoadByPrimaryKey(0).ToList.First.pagoDiario * Int32.Parse(Label31.Text))
                txtAhorro.Value = (_d.proc_ConfigGlobalLoadByPrimaryKey(0).ToList.First.ahorroObligatorio * Int32.Parse(Label31.Text))



            End If

            LbTotal.Text = "TOTAL :"
            LbTotal.Text = LbTotal.Text + " " + FormatCurrency((txtRecaudacion.Value + txtAhorro.Value + txtAhorroAdicional.Value), 0).ToString
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ImprimirTickete(comprobante As Boolean)
        Dim ids As New clsImpresionDataSet
        Dim ps As New System.Drawing.Printing.PageSettings
        Dim size As New System.Drawing.Printing.PaperSize

        With ids
            .TipoImpresora = clsImpresionDataSet.TipoImpresora_t.Grafica

            size = New System.Drawing.Printing.PaperSize("76mm Roll Paper", 76, 297)

            ps.PaperSize = size

            .ConfiguracionPagina = ps
            .NombreDocumentoImpresion = "RecaudacionDiario"
            .PathArchivoXML = My.Application.Info.DirectoryPath & "\RecaudacionDiario.xml"
            .DataSet = DataSetImpresion(comprobante)

            .Imprimir()

        End With

        'Hago esto porque si se quiere reimprimir, arroja un error al querer agregar nuevamente los DataTables al DataSet:
        ids.DataSet.Tables.Clear()
        ids = Nothing
    End Sub
    Private Function DataSetImpresion(comprobante As Boolean) As DataSet
        Try

            Dim compr As String
            Dim ret As New DataSet
            Dim dr As DataRow
            Dim CTA = _d.proc_ObtenerCuentaAhorro(Int32.Parse(_Persona.idPersona)).ToList.First

            If comprobante Then
                compr = "Comprobante"
            Else
                compr = ""
            End If


            _DataTableEncabezado = New DataTable
            _DataTableDetalle = New DataTable
            Me._DataTableEncabezado.Rows.Clear()
            Me._DataTableEncabezado.Columns.Add("Titulo")
            Me._DataTableEncabezado.Columns.Add("NIT")

            Me._DataTableEncabezado.Columns.Add("Anuncio")
            Me._DataTableEncabezado.Columns.Add("FechaPago")
            Me._DataTableEncabezado.Columns.Add("FechaDesde")
            Me._DataTableEncabezado.Columns.Add("FechaHasta")
            Me._DataTableEncabezado.Columns.Add("Dias")
            Me._DataTableEncabezado.Columns.Add("Recaudaciones")
            Me._DataTableEncabezado.Columns.Add("Ahorro")
            Me._DataTableEncabezado.Columns.Add("AhorroAdicional")
            Me._DataTableEncabezado.Columns.Add("Total")
            Me._DataTableEncabezado.Columns.Add("Observaciones")
            Me._DataTableEncabezado.Columns.Add("Operador")
            Me._DataTableEncabezado.Columns.Add("Nombre")
            Me._DataTableEncabezado.Columns.Add("Movil")
            Me._DataTableEncabezado.Columns.Add("TotalAhorro")
            Me._DataTableEncabezado.Columns.Add("TotalAhoAdicional")
            dr = Me._DataTableEncabezado.NewRow

            dr.Item("Titulo") = getSettings("Titulo")
            dr.Item("NIT") = String.Format("{0}{1}Tel: 4129491{2}www.taxiportal.com.co{3}Registro de recaudacion diaria", getSettings("NIT"), vbCrLf, vbCrLf, vbCrLf)

            dr.Item("FechaPago") = "Fecha Actual: " & Now.ToShortDateString
            dr.Item("FechaDesde") = "Pag� desde: " & Microsoft.VisualBasic.Left(dtpDesde.Value.ToShortDateString, 25)
            dr.Item("FechaHasta") = "Pag� Hasta: " & dtpHasta.Value.ToShortDateString
            dr.Item("Dias") = "# Dias Pagados: " & Label31.Text
            dr.Item("Recaudaciones") = "Pago Diario: " & FormatCurrency(txtRecaudacion.Value, 0).ToString
            dr.Item("Ahorro") = "Pago Ahorro: " & FormatCurrency(txtAhorro.Value, 0).ToString
            dr.Item("AhorroAdicional") = "Pago Ahorro Adicional: " & FormatCurrency(txtAhorroAdicional.Value, 0).ToString
            dr.Item("Observaciones") = "Obs: " & Microsoft.VisualBasic.Left((txtObservaciones.Text & compr), 23)
            dr.Item("Operador") = "Operador: " & USER_GLOBAL.nombre
            dr.Item("Nombre") = "Conductor: " & Microsoft.VisualBasic.Left(_Persona.nombre, 25)
            dr.Item("Movil") = "N� de Movil : " & txtMovil.Text
            dr.Item("TotalAhorro") = "Ahorro Total : " & FormatCurrency(CTA.importeAhorro, 0).ToString
            dr.Item("TotalAhoAdicional") = "Ahorro Adicional Total : " & FormatCurrency(CTA.importeAhorroAdicional, 0).ToString


            Me._DataTableEncabezado.Rows.Add(dr)

            ret.Tables.Add(Me._DataTableEncabezado)
            ret.Tables.Add(Me._DataTableDetalle)

            Return ret
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Public Sub Limpiar()
        dtpHasta.Value = Now
        dtpDesde.Value = Now
        txtRecaudacion.Value = 0
        txtAhorro.Value = 0
        txtAhorroAdicional.Value = 0
        txtObservaciones.Text = ""
        Label31.Text = 1
    End Sub

    Public Sub VerificarPico_placa(Fecha As DateTime)
        Try
            Dim fechaRecaCal As Date = Fecha
            EsFestivo = False
            For index = 0 To _Pico_Placa.Length - 1
                If fechaRecaCal.Equals(_Pico_Placa(index).Dia) And fechaRecaCal.DayOfWeek <> DayOfWeek.Saturday And fechaRecaCal.DayOfWeek <> DayOfWeek.Sunday Then

                    For j = 0 To _Festivos.Length - 1
                        If fechaRecaCal.Equals(_Festivos(j).diaFestivo) Then
                            EsFestivo = True
                            Exit For
                        End If
                    Next
                    If Not EsFestivo Then
                        PicBpico_placa.Visible = True
                        txtAhorroAdicional.Enabled = False
                        btnNuevo.Enabled = False
                        txtObservaciones.Enabled = False
                    End If

                    Exit For
                Else
                    PicBpico_placa.Visible = False
                    txtAhorroAdicional.Enabled = True
                    btnNuevo.Enabled = True
                    txtObservaciones.Enabled = True
                End If
            Next
        Catch ex As Exception

        End Try

    End Sub


#End Region

#Region "EVENTOS"


    Private Sub Recaudacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            CheckDates1(monthSelect.Value, txtA�o.Text, False, True)
        Catch ex As Exception
            ' MsgBox("Make sure you only write the year number." & vbLf & ex.Message, MsgBoxStyle.Exclamation, "Error")
        End Try

    End Sub
    'this sub is called anytime a run time button is pressed, sender is the respective button  AQUI CUALQUIER BOTON
    Private Sub anybutt_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If Not _Credito Then

            _EsNuevo = False
            _Calendario = True
            BtnImprimir.Enabled = True
            Dim fechaRecaCal As Date = sender.text + "-" + monthSelect.Value.ToString + "-" + txtA�o.Value.ToString

            VerificarPico_placa(fechaRecaCal)


            For index = 0 To _RecaudacionesFech.Length - 1
                If fechaRecaCal.Equals(_RecaudacionesFech(index).fechaPagada.Value) Then
                    txtRecaudacion.Value = _RecaudacionesFech(index).importeRecaudacion
                    txtAhorro.Value = _RecaudacionesFech(index).importeAhorro
                    txtAhorroAdicional.Value = _RecaudacionesFech(index).importeAhorroAdicional
                    txtObservaciones.Text = _RecaudacionesFech(index).Observaciones
                    dtpHasta.Value = _RecaudacionesFech(index).fechaPagada
                    dtpDesde.Value = _RecaudacionesFech(index).fechaPagada
                    activarBotones(False)
                    txtObservaciones.Enabled = True
                    BtnGuardar.Enabled = True
                    BtnImprimir.Enabled = True
                    CboImpTiquete.Enabled = True
                    Exit For
                Else
                    dtpHasta.Value = sender.text + "-" + monthSelect.Value.ToString + "-" + txtA�o.Value.ToString
                    dtpDesde.Value = sender.text + "-" + monthSelect.Value.ToString + "-" + txtA�o.Value.ToString
                    txtRecaudacion.Value = 0
                    txtAhorro.Value = 0
                    txtAhorroAdicional.Value = 0
                    txtObservaciones.Text = ""
                    activarBotones(False)
                End If
            Next
            totalizar()
            Label31.Text = 1
            activarBotones(False)
            btnNuevo.Visible = True
            btnCancelar.Visible = False

        Else

        End If

    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If monthSelect.Value = 1 Then
            monthSelect.Value = 12
            txtA�o.Text = txtA�o.Text - 1
            Button1_Click(Me, e)
            Exit Sub
        End If
        monthSelect.Value = monthSelect.Value - 1
        Button1_Click(Me, e)
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        If monthSelect.Value = 12 Then
            monthSelect.Value = 1
            txtA�o.Text = txtA�o.Text + 1
            Button1_Click(Me, e)
            Exit Sub
        End If
        monthSelect.Value = monthSelect.Value + 1
        Button1_Click(Me, e)
    End Sub



    Private Sub txtA�o_ValueChanged(sender As Object, e As System.EventArgs) Handles txtA�o.ValueChanged
        Try
            _Festivos = _d.proc_FestivosLoadByMES(Now.ToShortDateString).ToArray
        Catch ex As Exception

        End Try
    End Sub



    Private Sub BtnGuardar_Click(sender As Object, e As System.EventArgs) Handles BtnGuardar.Click
        Try
            dtpHasta_ValueChanged(Me, e)
            If Validar() Then
                If _EsNuevo Then
                    Dim FechaInh As Date = DateAdd(DateInterval.Day, 3, dtpHasta.Value)
                    Dim Tipo As Integer = 1
                    Dim conteo As DateTime = dtpDesde.Value.ToShortDateString
                    Dim EntroPrimero As Boolean = False
                    EsFestivo = False
                    For index = 1 To _DiasPagar
                        Dim pico As Boolean = False

                        'se busca si es pico y placa y si se encuentra paga con 0
                        For i = 0 To _Pico_Placa.Length - 1
                            If conteo.Equals(_Pico_Placa(i).Dia) And conteo.DayOfWeek <> DayOfWeek.Saturday And conteo.DayOfWeek <> DayOfWeek.Sunday Then

                                For j = 0 To _Festivos.Length - 1
                                    If conteo.Equals(_Festivos(j).diaFestivo) Then
                                        EsFestivo = True
                                        Exit For
                                    End If
                                Next
                                If Not EsFestivo Then
                                    _d.proc_RecaudacionInsert(_idRecaudacion, _Persona.idPersona, Now.ToShortDateString, conteo, FechaInh, 0, 0, 0, USER_GLOBAL.idUsuario, "Pico y Placa", _Persona.idPersona, Tipo, Nothing)
                                    pico = True
                                End If


                                Exit For

                            End If
                        Next
                        'Evaluar el pico y placa y paga el ahorro adicional solo al primero si son varios dias, los demas se pagan dividiendose el monto por el numero de dias con ahorro adicional 0
                        If pico Then
                            conteo = DateAdd(DateInterval.Day, 1, conteo)
                            Continue For
                        Else
                            If EntroPrimero = False Then
                                _d.proc_RecaudacionInsert(_idRecaudacion, _Persona.idPersona, Now.ToShortDateString, conteo, FechaInh, Decimal.Parse(txtRecaudacion.Value / Int32.Parse(Label31.Text)), Decimal.Parse(txtAhorro.Value / Int32.Parse(Label31.Text)), Decimal.Parse(txtAhorroAdicional.Value), USER_GLOBAL.idUsuario, txtObservaciones.Text, _Persona.idPersona, Tipo, Nothing)
                                EntroPrimero = True
                            Else
                                _d.proc_RecaudacionInsert(_idRecaudacion, _Persona.idPersona, Now.ToShortDateString, conteo, FechaInh, Decimal.Parse(txtRecaudacion.Value / Int32.Parse(Label31.Text)), Decimal.Parse(txtAhorro.Value / Int32.Parse(Label31.Text)), 0, USER_GLOBAL.idUsuario, txtObservaciones.Text, _Persona.idPersona, Tipo, Nothing)
                            End If
                        End If
                        conteo = DateAdd(DateInterval.Day, 1, conteo)

                    Next
                    If CboImpTiquete.Checked Then
                        ImprimirTickete(False)
                    End If
                    LlenarDatos()
                End If
            End If
            activarBotones(False)
            btnCancelar.Visible = False
            btnNuevo.Visible = True
            Button1_Click(Me, e)
            activarBotones(False)
            btnNuevo.Visible = True
            btnCancelar.Visible = False
            BtnPerdonar.Visible = False
            _Perdonar = False
            _Calendario = True
            Limpiar()
            StartBroadcast(getSettings("frmPrincipal_PORT"), "RefrescarMoviles")
            StartBroadcast(getSettings("Vehiculos_PORT"), "RefrescarMoviles")

            _DiasPagar = 0
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try



    End Sub
    Private Sub btnNuevo_Click(sender As Object, e As System.EventArgs) Handles btnNuevo.Click
        EsFestivo = False
        activarBotones(True)
        If USER_GLOBAL.idTipoUsuario = 1 Then
            BtnPerdonar.Visible = True
        End If
        If Not _RecaudacionesPers Is Nothing Then
            Dim UltimaFecha = _d.proc_RecaudacionUltimaFechaIdPersona(_Persona.idPersona).ToList.First.fechaPagada.Value
            UltimaFecha = DateAdd(DateInterval.Day, 1, UltimaFecha)
            For i = 0 To _Pico_Placa.Length - 1

                If UltimaFecha.ToShortDateString.Equals(_Pico_Placa(i).Dia.ToShortDateString) And UltimaFecha.DayOfWeek <> DayOfWeek.Saturday And UltimaFecha.DayOfWeek <> DayOfWeek.Sunday Then

                    For j = 0 To _Festivos.Length - 1
                        If UltimaFecha.ToShortDateString.Equals(_Festivos(j).diaFestivo.ToShortDateString) Then
                            EsFestivo = True
                            Exit For
                        End If
                    Next

                    If Not EsFestivo Then
                        UltimaFecha = DateAdd(DateInterval.Day, 1, UltimaFecha)
                    End If


                    Exit For
                End If
            Next
            If dtpDesde.Value > UltimaFecha Then
                dtpDesde.Value = UltimaFecha
                dtpHasta.Value = UltimaFecha
            Else
                dtpHasta.Value = UltimaFecha
                dtpDesde.Value = UltimaFecha
            End If

        Else
            dtpDesde.Enabled = True
        End If
        txtRecaudacion.Value = (_d.proc_ConfigGlobalLoadByPrimaryKey(0).ToList.First.pagoDiario * Int32.Parse(Label31.Text))
        txtAhorro.Value = (_d.proc_ConfigGlobalLoadByPrimaryKey(0).ToList.First.ahorroObligatorio * Int32.Parse(Label31.Text))
        txtAhorroAdicional.Value = 0
        btnNuevo.Visible = False
        btnCancelar.Visible = True
        _EsNuevo = True
        _Calendario = False
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click
        BtnPerdonar.Visible = False
        activarBotones(False)
        btnNuevo.Visible = True
        btnCancelar.Visible = False
        Limpiar()
    End Sub
    Private Sub txtAhorro_ValueChanged(sender As Object, e As System.EventArgs) Handles txtAhorro.ValueChanged
        totalizar()
    End Sub

    Private Sub txtAhorroAdicional_ValueChanged(sender As Object, e As System.EventArgs) Handles txtAhorroAdicional.ValueChanged
        totalizar()
    End Sub

    Private Sub txtRecaudacion_ValueChanged(sender As Object, e As System.EventArgs) Handles txtRecaudacion.ValueChanged
        totalizar()
    End Sub
    Private Sub txtMovil_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtMovil.KeyDown

        If e.KeyCode = Keys.Enter Then


            Try
                Me.TabControl1.SelectedTab = TabPage1
                btnCancelar_Click(Me, Nothing)
                Limpiar()
                Dim vehiculos = _d.proc_ObtenerVehiculoPorNroMovil(Int32.Parse(txtMovil.Value)).ToList
                Dim vehiculo = _d.proc_ObtenerVehiculoPorNroMovil(Int32.Parse(txtMovil.Value)).ToList.First
                For index = 0 To vehiculos.Count - 1
                    If vehiculos(index).Habilitado = True Then
                        vehiculo = vehiculos(index)
                        Exit For
                    End If
                Next
                _Persona = _d.proc_PersonasLoadByPrimaryKey(_d.proc_Conductores_VehiculosLoadByidVehiculo(vehiculo.idVehiculo).ToList.First.idConductor).ToList.First
                LlenarDatos()
                VerificarPico_placa(dtpDesde.Value)
                Button1_Click(Me, Nothing)
            Catch ex As Exception
                MsgBox("El m�vil no existe o no esta relacionado a un conductor", MsgBoxStyle.Information)
            End Try
        End If
    End Sub
    Private Sub dtpDesde_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpDesde.ValueChanged
        If BtnGuardar.Enabled = True And dtpHasta.Value < dtpDesde.Value Then
            MsgBox("La fecha DESDE debe ser menor que la fecha HASTA", MsgBoxStyle.Information)
            dtpDesde.Value = dtpHasta.Value
        Else
            totalizar()
        End If

    End Sub
    Private Sub dtpHasta_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpHasta.ValueChanged
        If BtnGuardar.Enabled = True And dtpHasta.Value < dtpDesde.Value Then
            MsgBox("La fecha HASTA debe ser mayor que la fecha DESDE", MsgBoxStyle.Information)
            dtpHasta.Value = dtpDesde.Value
        Else
            totalizar()
        End If
    End Sub
    Private Sub BtnGuardar_EnabledChanged(sender As Object, e As System.EventArgs) Handles BtnGuardar.EnabledChanged
        totalizar()
    End Sub

    Private Sub BtnPerdonar_Click(sender As System.Object, e As System.EventArgs) Handles BtnPerdonar.Click
        _Perdonar = True
        txtAhorro.Value = 0
        txtRecaudacion.Value = 0
        txtAhorroAdicional.Value = 0
        totalizar()
    End Sub
    Private Sub BtnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles BtnImprimir.Click
        ImprimirTickete(True)
    End Sub
#End Region

#End Region


    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        Try
            If _Credito Then
                _Credito = False
                Button1_Click(Me, Nothing)
            Else
                _Credito = True

                LlenarCreditos(_Persona)
                LlenarDatos2()
                Button11_Click(Me, Nothing)
            End If

        Catch ex As Exception

        End Try
    End Sub


#Region "CREDITOS"
    Sub LlenarCreditos(Persona As proc_PersonasLoadByPrimaryKeyResult)
        Me.GridEX1.SetDataBinding(_d.proc_obtenerPrestamosPorPagar(Persona.idPersona).ToList, String.Empty)
        'GridEX1.SetDataBinding(_d.proc_obtenerPrestamosPorPagar(Persona.idPersona).ToList, " ")
        GridEX1.RetrieveStructure()
        GridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False
        GridEX1.RootTable.Columns("idPrestamo").Caption = "N� Prestamo"
        GridEX1.RootTable.Columns("FechaRegistro").Visible = False
        GridEX1.RootTable.Columns("Usuario").Visible = False
        GridEX1.RootTable.Columns("idTipoPrestamo").Visible = False
        GridEX1.RootTable.Columns("Finalizado").Visible = False
        GridEX1.RootTable.Columns("Concepto").Visible = False
        GridEX1.RootTable.Columns("ImporteCuotas").Visible = False
        GridEX1.RootTable.Columns("CuotasSaldadas").Visible = False
        GridEX1.RootTable.Columns("ImporteSaldado").Visible = False
        GridEX1.RootTable.Columns("Observaciones").Visible = False
        GridEX1.RootTable.Columns("TipoPrestamo").Caption = "Tipo de prestamo"
        GridEX1.RootTable.Columns("FechaInicio").Caption = "F. Inicial"
        GridEX1.RootTable.Columns("FechaFin").Caption = "F. Final"
        GridEX1.RootTable.Columns("ImportePrestado").Caption = "Total  Prestado"
        GridEX1.RootTable.Columns("NroCuotas").Caption = "Numero   cuotas"
        GridEX1.RootTable.Columns("ImportePrestado").FormatString = "C0"
        GridEX1.AutoSizeColumns()



    End Sub

    Private Sub GridEX1_RowDoubleClick(sender As Object, e As Janus.Windows.GridEX.RowActionEventArgs) Handles GridEX1.RowDoubleClick
        'txtConcepto.Text = GridEX1.GetValue("Concepto")
        'txtValorCuota.Value = GridEX1.GetValue("ImporteCuotas")
        'txtImporteRegresado.Value = GridEX1.GetValue("ImporteSaldado")
        'txtCuotasPagadas.Text = GridEX1.GetValue("CuotasSaldadas")
        'txtRichObservaciones.Text = GridEX1.GetValue("Observaciones")
        'txtImporteCredito.Value = GridEX1.GetValue("ImporteCuotas")

        'Button11_Click(Me, Nothing)
    End Sub
    Private Sub GridEX1_SelectionChanged(sender As Object, e As System.EventArgs) Handles GridEX1.SelectionChanged
        Try
            txtConcepto.Text = GridEX1.GetValue("Concepto")
            txtValorCuota.Value = GridEX1.GetValue("ImporteCuotas")
            txtImporteRegresado.Value = GridEX1.GetValue("ImporteSaldado")
            txtCuotasPagadas.Text = GridEX1.GetValue("CuotasSaldadas")
            txtRichObservaciones.Text = GridEX1.GetValue("Observaciones")
            txtImporteCredito.Value = GridEX1.GetValue("ImporteCuotas")
            dtDesde2.Value = GridEX1.GetValue("FechaInicio")
            DtHasta2.Value = GridEX1.GetValue("FechaInicio")
            _creditoDesde = GridEX1.GetValue("FechaInicio")
            _CreditoHasta = GridEX1.GetValue("FechaFin")
            Dim Fec As Date = GridEX1.GetValue("FechaInicio")
            A�o2.Value = Fec.Year
            MothSelect2.Value = Fec.Month
            BtnCancelar2_Click(Me, Nothing)
            Button11_Click(Me, Nothing)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button11_Click(sender As System.Object, e As System.EventArgs) Handles Button11.Click
        Try
            CheckDates2(MothSelect2.Value, A�o2.Text, False, True)
        Catch ex As Exception
            ' MsgBox("Make sure you only write the year number." & vbLf & ex.Message, MsgBoxStyle.Exclamation, "Error")
        End Try
    End Sub

    Private Sub CheckDates2(ByVal strCurrentMonth As Integer, ByVal strCurrentYear As Integer, Exis_recaudacion As Boolean, OtroMes As Boolean)
        Try

            Dim FirstDay, MonthName As String
            Dim MyDate As Date
            Dim i, TotalDays As Integer


            If OtroMes Then
                Dim fecha As Date = "1-" + MothSelect2.Value.ToString + "-" + A�o2.Value.ToString
                _RecaudacionesFech = _d.proc_RecaudacionLoadByFECHA(_Persona.idPersona, fecha, 2).ToArray
            End If

            'make all buttons invisible until changes are done
            For i = 0 To 41
                dateButts(i).Visible = False
            Next

            MyDate = "#1/" & strCurrentMonth & "/" & strCurrentYear & "#"
            MonthName = Format(MyDate, "MMMM")
            'label10 to show the month name in local/regional name
            Label28.Text = MonthName.ToUpper

            TotalDays = System.DateTime.DaysInMonth(strCurrentYear, strCurrentMonth)

            FirstDay = Format(MyDate, "ddd")

            'Here we use the local values stored in Form1_load()
            'and check which is the first day of the month 
            StartingDateLabel = Nothing
            If FirstDay = _LocalValues(0) Then     'if sunday
                StartingDateLabel = "0"
            ElseIf FirstDay = _LocalValues(1) Then 'if monday
                StartingDateLabel = "1"
            ElseIf FirstDay = _LocalValues(2) Then 'if tuesday
                StartingDateLabel = "2"
            ElseIf FirstDay = _LocalValues(3) Then 'if wednesday
                StartingDateLabel = "3"
            ElseIf FirstDay = _LocalValues(4) Then 'if thursday
                StartingDateLabel = "4"
            ElseIf FirstDay = _LocalValues(5) Then 'if friday
                StartingDateLabel = "5"
            ElseIf FirstDay = _LocalValues(6) Then 'if saturday
                StartingDateLabel = "6"
            End If

            'for all buttons
            For i = 0 To 41
                dateButts(i).Enabled = False
                dateButts(i).Text = ""
                dateButts(i).BackColor = Color.LightGray 'bcolor for the non-days
            Next

            For i = 1 To TotalDays
                dateButts(StartingDateLabel).Text = "  " & i.ToString
                dateButts(StartingDateLabel).BackColor = Color.White
                dateButts(StartingDateLabel).ForeColor = Color.Black
                StartingDateLabel += 1
            Next
            StartingDateLabel = StartingDateLabel - TotalDays
            'only for buttons representing days
            For i = 1 To TotalDays
                dateButts(StartingDateLabel).Enabled = True

                'here you could check with a database and replace the extra blank spaces 
                'e.g. with an asterisk to show an appointment or change the backcolor
                dateButts(StartingDateLabel).Text = "  " & i.ToString
                dateButts(StartingDateLabel).BackColor = Color.White


                ' If StartingDateLabel <> -1 And StartingDateLabel <> 0 And StartingDateLabel <> 6 And StartingDateLabel <> 7 And StartingDateLabel <> 13 And StartingDateLabel <> 14 And StartingDateLabel <> 20 And StartingDateLabel <> 21 And StartingDateLabel <> 27 And StartingDateLabel <> 28 And StartingDateLabel <> 34 And StartingDateLabel <> 35 Then
                dateButts(StartingDateLabel).ForeColor = Color.Black


                '@desde datetime, @hasta datetime,@idtipoRecaudacion int,@idTipoPrestamo int

                TraerCreditos()
               If GridEX1.RowCount > 0 Then
                    BtnNuevo2.Enabled = True
                    BtnImprimir2.Enabled = True
                    Dim fecha As Date = i.ToString + "-" + MothSelect2.Value.ToString + "-" + A�o2.Value.ToString
                    If fecha >= _creditoDesde And fecha <= _CreditoHasta Then
                        dateButts(StartingDateLabel).ForeColor = Color.Black
                        dateButts(StartingDateLabel).BackColor = Color.Yellow
                    Else
                        dateButts(StartingDateLabel).ForeColor = Color.Black
                        dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
                    End If
                Else
                    txtImporteCredito.Value = 0
                    txtConcepto.Text = ""
                    txtValorCuota.Value = 0
                    txtImporteRegresado.Value = 0
                    txtCuotasPagadas.Text = ""
                    txtRichObservaciones.Text = ""
                    txtImporteCredito.Value = 0
                    BtnNuevo2.Enabled = False
                    BtnImprimir2.Enabled = False
                End If

                

                Dim Desde As DateTime = "01-" + MothSelect2.Value.ToString + "-" + A�o2.Value.ToString
                Dim Hasta As DateTime = DateAdd(DateInterval.Day, (System.DateTime.DaysInMonth(Desde.Year.ToString, Desde.Month.ToString) - 1), Desde)
                _PrestamoCredito = _d.proc_ObtenerRecaudacionPrestamoCalendar(Desde.ToShortDateString, Hasta.ToShortDateString, 2, GridEX1.GetValue("idPrestamo"), _Persona.idPersona).ToArray

                'If i Then
                '    dateButts(StartingDateLabel).ForeColor = Color.Purple
                '    dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
                'End If

                If _PrestamoCredito.Length > 0 Then

                    For index = 0 To _PrestamoCredito.Length - 1
                       
                        If _PrestamoCredito(index).fechaPagada.Value.Day = i Then
                            dateButts(StartingDateLabel).BackColor = Color.LightGreen
                            index = _PrestamoCredito.Length
                        Else
                            dateButts(StartingDateLabel).ForeColor = Color.Black
                            dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
                        End If
                    Next

                End If

                If StartingDateLabel <> -1 And StartingDateLabel <> 0 And StartingDateLabel <> 6 And StartingDateLabel <> 7 And StartingDateLabel <> 13 And StartingDateLabel <> 14 And StartingDateLabel <> 20 And StartingDateLabel <> 21 And StartingDateLabel <> 27 And StartingDateLabel <> 28 And StartingDateLabel <> 34 And StartingDateLabel <> 35 Then
                    For index = 0 To _Pico_Placa.Length - 1

                        If _Pico_Placa(index).Dia.Day = i And _Pico_Placa(index).Dia.Month = MothSelect2.Value And _Pico_Placa(index).Dia.Year = A�o2.Value Then
                            'MsgBox(_Pico_Placa(index).Dia)
                            dateButts(StartingDateLabel).Enabled = True
                            dateButts(StartingDateLabel).ForeColor = Color.Black
                            dateButts(StartingDateLabel).BackColor = Color.LightSteelBlue
                            dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black

                            Exit For
                        End If

                    Next
                End If



                For index = 0 To _Festivos.Length - 1
                    If _Festivos(index).diaFestivo.Day = i And _Festivos(index).diaFestivo.Month = MothSelect2.Value And _Festivos(index).diaFestivo.Year = A�o2.Value Then
                        dateButts(StartingDateLabel).ForeColor = Color.Purple
                        dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
                        Exit For
                    End If

                Next
                If StartingDateLabel = -1 Or StartingDateLabel = 0 Or StartingDateLabel = 6 Or StartingDateLabel = 7 Or StartingDateLabel = 13 Or StartingDateLabel = 14 Or StartingDateLabel = 20 Or StartingDateLabel = 21 Or StartingDateLabel = 27 Or StartingDateLabel = 28 Or StartingDateLabel = 34 Or StartingDateLabel = 35 Then
                    dateButts(StartingDateLabel).ForeColor = Color.Purple
                    dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
                End If

            


                StartingDateLabel += 1
            Next




            'after all the changes make them visibe
            For i = 0 To 41
                dateButts(i).Visible = True
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button9_Click(sender As System.Object, e As System.EventArgs) Handles Button9.Click
        If MothSelect2.Value = 1 Then
            MothSelect2.Value = 12
            A�o2.Text = A�o2.Text - 1
            Button11_Click(Me, e)
            Exit Sub
        End If
        MothSelect2.Value = MothSelect2.Value - 1
        Button11_Click(Me, e)

    End Sub

    Private Sub Button10_Click(sender As System.Object, e As System.EventArgs) Handles Button10.Click
        If MothSelect2.Value = 12 Then
            MothSelect2.Value = 1
            A�o2.Text = A�o2.Text + 1
            Button11_Click(Me, e)
            Exit Sub
        End If
        MothSelect2.Value = MothSelect2.Value + 1
        Button11_Click(Me, e)
    End Sub

    Private Sub BtnNuevo2_Click(sender As System.Object, e As System.EventArgs) Handles BtnNuevo2.Click
        Try

        
        'If Not IsDBNull(_d.proc_RecaudacionPrestamoUltimaFechaIdPersona(_Persona.idPersona, GridEX1.GetValue("idPrestamo")).ToList.First.fechaPagada.Value) Then
        '    Dim UltimaFecha = _d.proc_RecaudacionPrestamoUltimaFechaIdPersona(_Persona.idPersona, GridEX1.GetValue("idPrestamo")).ToList.First.fechaPagada.Value
        '    UltimaFecha = DateAdd(DateInterval.Day, 1, UltimaFecha)
        '    dtDesde2.Value = UltimaFecha
        'Else
        '    dtDesde2.Value = GridEX1.GetValue("FechaInicio")
        'End If
        TraerCreditos()
        Dim UltimaFecha As Date
        EsFestivo = False
        If _PrestamoCredito.Length > 0 Then
            UltimaFecha = _d.proc_RecaudacionPrestamoUltimaFechaIdPersona(_Persona.idPersona, GridEX1.GetValue("idPrestamo")).ToList.First.fechaPagada.Value
            UltimaFecha = DateAdd(DateInterval.Day, 1, UltimaFecha)

        Else
            'UltimaFecha = "01-" + MothSelect2.Value.ToString + "-" + A�o2.Value.ToString
            UltimaFecha = GridEX1.GetValue("FechaInicio")
             DtHasta2.Enabled = True
        End If
        For i = 0 To _Pico_Placa.Length - 1

            If UltimaFecha.ToShortDateString.Equals(_Pico_Placa(i).Dia.ToShortDateString) And UltimaFecha.DayOfWeek <> DayOfWeek.Saturday And UltimaFecha.DayOfWeek <> DayOfWeek.Sunday Then

                For j = 0 To _Festivos.Length - 1
                    If UltimaFecha.ToShortDateString.Equals(_Festivos(j).diaFestivo.ToShortDateString) Then
                        EsFestivo = True
                        Exit For
                    End If
                Next

                If Not EsFestivo Then
                    UltimaFecha = DateAdd(DateInterval.Day, 1, UltimaFecha)
                    EsFestivo = False
                End If


                Exit For
            End If
        Next



        If dtDesde2.Value > UltimaFecha Then
            DtHasta2.Value = UltimaFecha
            dtDesde2.Value = UltimaFecha
        Else
            DtHasta2.Value = UltimaFecha
            dtDesde2.Value = UltimaFecha
        End If

        
        activarBotones2(True)
        BtnNuevo2.Visible = False
        BtnCancelar2.Visible = True
        _EsNuevo2 = True
            _Calendario = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DtHasta2_ValueChanged(sender As Object, e As System.EventArgs) Handles DtHasta2.ValueChanged
        Try
            If BtnGuardar2.Enabled And DtHasta2.Value >= dtDesde2.Value Then
                'If DtHasta2.Value <= GridEX1.GetValue("FechaFin") Then
                totalizar2()
                'End If
                'Else
                '    MsgBox("La fecha HASTA debe ser mayor que la fecha DESDE", MsgBoxStyle.Information)
                '    DtHasta2.Value = dtDesde2.Value
            End If

            'If BtnGuardar2.Enabled And DtHasta2.Value > GridEX1.GetValue("FechaFin") Then
            '    MsgBox("Fecha Mayor a la fecha final", MsgBoxStyle.Information)
            '    DtHasta2.Value = GridEX1.GetValue("FechaFin")
            'End If


        Catch ex As Exception

        End Try
    End Sub

    Sub totalizar2()
        Try
            If BtnGuardar2.Enabled And _Calendario = False Then
                Dim d1 = DateValue(dtDesde2.Value.ToShortDateString)
                Dim d2 = DateValue(DtHasta2.Value.ToShortDateString)
                Dim d3 = d2 - d1
                Label35.Text = d3.TotalDays + 1

                _DiasPagar2 = d3.TotalDays + 1
                Dim conteo As DateTime = dtDesde2.Value.ToShortDateString

                For index = 1 To (d3.TotalDays + 1)

                    For i = 0 To _Pico_Placa.Length - 1

                        If conteo.ToShortDateString.Equals(_Pico_Placa(i).Dia.ToShortDateString) And conteo.DayOfWeek <> DayOfWeek.Saturday And conteo.DayOfWeek <> DayOfWeek.Sunday Then

                            For j = 0 To _Festivos.Length - 1
                                If conteo.Equals(_Festivos(j).diaFestivo) Then
                                    EsFestivo = True
                                    Exit For
                                End If
                            Next
                            If Not EsFestivo Then
                                Label35.Text = Int32.Parse(Label35.Text) - 1
                                MsgBox(Label35.Text)
                            End If
                            EsFestivo = False
                            Exit For
                        End If
                    Next
                    conteo = DateAdd(DateInterval.Day, 1, conteo)
                Next

                If Integer.Parse(GridEX1.GetValue("NroCuotas")) < Integer.Parse(Integer.Parse(txtCuotasPagadas.Text) + Integer.Parse(Label35.Text)) Then

                    _DiasPagar2 = _DiasPagar2 - (Integer.Parse(Label35.Text) - (Integer.Parse(GridEX1.GetValue("NroCuotas")) - Integer.Parse(Integer.Parse(txtCuotasPagadas.Text))))
                    Label35.Text = (Integer.Parse(GridEX1.GetValue("NroCuotas")) - Integer.Parse(Integer.Parse(txtCuotasPagadas.Text)))

                    ' MsgBox("PASO" & Label35.Text) DtHasta2.Value.AddDays(-(Integer.Parse(GridEX1.GetValue("NroCuotas")) - Integer.Parse(Label35.Text)))
                End If

                txtImporteCredito.Value = (txtValorCuota.Value * Int32.Parse(Label35.Text))


            End If

        Catch ex As Exception

        End Try

    End Sub


    Public Sub activarBotones2(FV As Boolean)
        DtHasta2.Enabled = FV
        txtObservaciones2.Enabled = FV
        BtnGuardar2.Enabled = FV
        'BtnImprimir.Enabled = FV
        ChImprimirTiquete.Enabled = FV
    End Sub
    Private Sub BtnGuardar2_Click(sender As System.Object, e As System.EventArgs) Handles BtnGuardar2.Click
        Try

            If _EsNuevo2 Then
                'Dim FechaInh As Date = DateAdd(DateInterval.Day, 3, dtpHasta.Value)
                Dim Tipo As Integer = 2
                Dim conteo As DateTime = dtDesde2.Value
                'Dim EntroPrimero As Boolean = False
                EsFestivo = False
                Dim _idRec As Integer
                Dim pico = False
                For index = 1 To _DiasPagar2
                    For i = 0 To _Pico_Placa.Length - 1
                        If conteo.ToShortDateString.Equals(_Pico_Placa(i).Dia.ToShortDateString) And conteo.DayOfWeek <> DayOfWeek.Saturday And conteo.DayOfWeek <> DayOfWeek.Sunday Then
                            For j = 0 To _Festivos.Length - 1
                                If conteo.ToShortDateString.Equals(_Festivos(j).diaFestivo.ToShortDateString) Then
                                    EsFestivo = True
                                    Exit For
                                End If
                            Next
                            If Not EsFestivo Then
                                 pico = True
                            End If
                            Exit For
                        End If
                    Next
                    If Not pico Then
                        _d.proc_RecaudacionInsert(_idRec, _Persona.idPersona, Now.ToShortDateString, conteo, Nothing, Decimal.Parse(txtImporteCredito.Value / Integer.Parse(Label35.Text)), 0, 0, USER_GLOBAL.idUsuario, txtObservaciones2.Text, _Persona.idPersona, Tipo, Integer.Parse(GridEX1.GetValue("idPrestamo")))
                    End If
                    conteo = DateAdd(DateInterval.Day, 1, conteo)
                    pico = False
                Next

                If Integer.Parse(GridEX1.GetValue("NroCuotas")) <= Integer.Parse(Integer.Parse(txtCuotasPagadas.Text) + Integer.Parse(Label35.Text)) Then
                    _d.proc_PrestamoUpdate(Integer.Parse(GridEX1.GetValue("idPrestamo")), GridEX1.GetValue("Concepto"), Date.Parse(GridEX1.GetValue("FechaRegistro")), Date.Parse(GridEX1.GetValue("FechaInicio")), Date.Parse(GridEX1.GetValue("FechaFin")), GridEX1.GetValue("Observaciones"), Integer.Parse(GridEX1.GetValue("idTipoPrestamo")), Decimal.Parse(GridEX1.GetValue("ImportePrestado")), Integer.Parse(GridEX1.GetValue("NroCuotas")), Decimal.Parse(GridEX1.GetValue("ImporteCuotas")), Decimal.Parse((txtImporteRegresado.Value + txtImporteCredito.Value)), Integer.Parse(Integer.Parse(txtCuotasPagadas.Text) + Integer.Parse(Label35.Text)), _Persona.idPersona, USER_GLOBAL.idUsuario, True)
                Else
                    _d.proc_PrestamoUpdate(Integer.Parse(GridEX1.GetValue("idPrestamo")), GridEX1.GetValue("Concepto"), Date.Parse(GridEX1.GetValue("FechaRegistro")), Date.Parse(GridEX1.GetValue("FechaInicio")), Date.Parse(GridEX1.GetValue("FechaFin")), GridEX1.GetValue("Observaciones"), Integer.Parse(GridEX1.GetValue("idTipoPrestamo")), Decimal.Parse(GridEX1.GetValue("ImportePrestado")), Integer.Parse(GridEX1.GetValue("NroCuotas")), Decimal.Parse(GridEX1.GetValue("ImporteCuotas")), Decimal.Parse((txtImporteRegresado.Value + txtImporteCredito.Value)), Integer.Parse(Integer.Parse(txtCuotasPagadas.Text) + Integer.Parse(Label35.Text)), _Persona.idPersona, USER_GLOBAL.idUsuario, Boolean.Parse(GridEX1.GetValue("Finalizado")))
                End If


                If ChImprimirTiquete.Checked Then
                    ImprimirTickete2(False)
                End If
                TraerCreditos()
                LlenarDatos2()
                LlenarDatos()
            End If
            activarBotones2(False)
            BtnCancelar2.Visible = False
            BtnNuevo2.Visible = True
            Button11_Click(Me, e)
            activarBotones2(False)
            Label35.Text = 1
            _DiasPagar2 = 1
            BtnNuevo2.Visible = True
            BtnCancelar2.Visible = False
            _Calendario = True
            LlenarCreditos(_Persona)
            IrUltimaFecha()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub
    Public Sub LlenarDatos2()
        Try
            If _d.proc_RecaudacionPrestamoLoadByIdPersona(_Persona.idPersona, GridEX1.GetValue("idPrestamo")).Count > 0 Then
                _PrestamosPer = _d.proc_RecaudacionPrestamoLoadByIdPersona(_Persona.idPersona, GridEX1.GetValue("idPrestamo")).ToArray
                A�o2.Text = _PrestamosPer(0).fechaPagada.Value.Year
                MothSelect2.Value = _PrestamosPer(0).fechaPagada.Value.Month

            Else
                txtA�o.Text = Now.Year
                monthSelect.Value = Now.Month
                _RecaudacionesPers = Nothing
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub BtnCancelar2_Click(sender As Object, e As System.EventArgs) Handles BtnCancelar2.Click

        activarBotones2(False)
        BtnNuevo2.Visible = True
        BtnCancelar2.Visible = False
        limpiar2()
    End Sub

    Sub limpiar2()
        txtObservaciones2.Text = ""
        Label35.Text = 1
        _DiasPagar2 = 1
        txtImporteCredito.Value = GridEX1.GetValue("ImporteCuotas")
    End Sub


    Private Sub ImprimirTickete2(comprobante As Boolean)
        Dim ids As New clsImpresionDataSet
        Dim ps As New System.Drawing.Printing.PageSettings
        Dim size As New System.Drawing.Printing.PaperSize

        With ids
            .TipoImpresora = clsImpresionDataSet.TipoImpresora_t.Grafica

            size = New System.Drawing.Printing.PaperSize("76mm Roll Paper", 76, 297)

            ps.PaperSize = size

            .ConfiguracionPagina = ps
            .NombreDocumentoImpresion = "RecaudacionPrestamo"
            .PathArchivoXML = My.Application.Info.DirectoryPath & "\RecaudacionPrestamo.xml"
            .DataSet = DataSetImpresion2(comprobante)

            .Imprimir()

        End With

        'Hago esto porque si se quiere reimprimir, arroja un error al querer agregar nuevamente los DataTables al DataSet:
        ids.DataSet.Tables.Clear()
        ids = Nothing
    End Sub

    Private Function DataSetImpresion2(comprobante As Boolean) As DataSet
        Try

            Dim compr As String
            Dim ret As New DataSet
            Dim dr As DataRow

            Dim CTA As proc_PrestamoLoadByPrimaryKeyResult = _d.proc_PrestamoLoadByPrimaryKey(Int32.Parse(GridEX1.GetValue("IdPrestamo"))).ToList.First

            If comprobante Then
                compr = "Comprobante"
            Else
                compr = ""
            End If


            _DataTableEncabezado = New DataTable
            _DataTableDetalle = New DataTable
            Me._DataTableEncabezado.Rows.Clear()
            Me._DataTableEncabezado.Columns.Add("Titulo")
            Me._DataTableEncabezado.Columns.Add("NIT")

            Me._DataTableEncabezado.Columns.Add("Anuncio")
            Me._DataTableEncabezado.Columns.Add("FechaPago")
            Me._DataTableEncabezado.Columns.Add("FechaDesde")
            Me._DataTableEncabezado.Columns.Add("FechaHasta")
            Me._DataTableEncabezado.Columns.Add("Dias")
            Me._DataTableEncabezado.Columns.Add("NroCredito")
            Me._DataTableEncabezado.Columns.Add("Recaudaciones")
            Me._DataTableEncabezado.Columns.Add("CuotasPagadas")
            Me._DataTableEncabezado.Columns.Add("NroCuotas")
            Me._DataTableEncabezado.Columns.Add("TotalPagado")
            Me._DataTableEncabezado.Columns.Add("TotalAPagar")
            Me._DataTableEncabezado.Columns.Add("Observaciones")
            Me._DataTableEncabezado.Columns.Add("Operador")
            Me._DataTableEncabezado.Columns.Add("Nombre")
            Me._DataTableEncabezado.Columns.Add("Movil")
            dr = Me._DataTableEncabezado.NewRow

            dr.Item("Titulo") = getSettings("Titulo")
            dr.Item("NIT") = String.Format("{0}{1}Tel: 4129491{2}www.taxiportal.com.co{3}Registro de recaudacion diaria Prestamo", getSettings("NIT"), vbCrLf, vbCrLf, vbCrLf)

            dr.Item("FechaPago") = "Fecha Actual: " & Now.ToShortDateString
            dr.Item("FechaDesde") = "Pag� desde: " & Microsoft.VisualBasic.Left(dtDesde2.Value.ToShortDateString, 25)
            dr.Item("FechaHasta") = "Pag� Hasta: " & DtHasta2.Value.ToShortDateString
            dr.Item("Dias") = "# Dias Pagados: " & Label35.Text
            dr.Item("NroCredito") = "Numero de Prestamo : " & GridEX1.GetValue("idPrestamo").ToString
            dr.Item("Recaudaciones") = "Pago Prestamo: " & FormatCurrency(txtImporteCredito.Value, 0).ToString
            dr.Item("CuotasPagadas") = "Cuotas Pagadas: " & CTA.CuotasSaldadas
            dr.Item("NroCuotas") = "Nro Cuotas: " & GridEX1.GetValue("NroCuotas").ToString
            dr.Item("TotalPagado") = "Total Pagado : " & FormatCurrency(CTA.ImporteSaldado, 0).ToString
            dr.Item("TotalAPagar") = "Total a pagar : " & FormatCurrency(CTA.ImportePrestado, 0).ToString
            dr.Item("Observaciones") = "Obs: " & Microsoft.VisualBasic.Left((txtObservaciones2.Text & compr), 23)
            dr.Item("Operador") = "Operador: " & USER_GLOBAL.nombre
            dr.Item("Nombre") = "Conductor: " & Microsoft.VisualBasic.Left(_Persona.nombre, 25)
            dr.Item("Movil") = "N� de Movil : " & txtMovil.Text


            Me._DataTableEncabezado.Rows.Add(dr)

            ret.Tables.Add(Me._DataTableEncabezado)
            ret.Tables.Add(Me._DataTableDetalle)

            Return ret
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Private Sub BtnImprimir2_Click(sender As Object, e As System.EventArgs) Handles BtnImprimir2.Click
        ImprimirTickete2(True)
    End Sub

    Private Sub IrUltimaFecha()
        Try
            Dim UltimaFecha = _d.proc_RecaudacionPrestamoUltimaFechaIdPersona(_Persona.idPersona, GridEX1.GetValue("idPrestamo")).ToList.First.fechaPagada.Value
            UltimaFecha = DateAdd(DateInterval.Day, 1, UltimaFecha)
            MothSelect2.Value = UltimaFecha.Month
            A�o2.Value = UltimaFecha.Year
            Button11_Click(Me, Nothing)
        Catch ex As Exception
            activarBotones2(False)
            BtnNuevo2.Enabled = False
            BtnImprimir2.Enabled = False
        End Try
    End Sub

    Private Sub TraerCreditos()
        Dim Desde As DateTime = "01-" + MothSelect2.Value.ToString + "-" + A�o2.Value.ToString
        Dim Hasta As DateTime = DateAdd(DateInterval.Day, (System.DateTime.DaysInMonth(Desde.Year.ToString, Desde.Month.ToString) - 1), Desde)
        _PrestamoCredito = _d.proc_ObtenerRecaudacionPrestamoCalendar(Desde.ToShortDateString, Hasta.ToShortDateString, 2, GridEX1.GetValue("idPrestamo"), _Persona.idPersona).ToArray

        If _PrestamoCredito.Length = 0 Then
            Desde = DateAdd(DateInterval.Day, -Date.DaysInMonth(Desde.Year, Desde.Month), Desde)
            Hasta = DateAdd(DateInterval.Day, (System.DateTime.DaysInMonth(Desde.Year.ToString, Desde.Month.ToString) - 1), Desde)
            _PrestamoCredito = _d.proc_ObtenerRecaudacionPrestamoCalendar(Desde.ToShortDateString, Hasta.ToShortDateString, 2, GridEX1.GetValue("idPrestamo"), _Persona.idPersona).ToArray

        End If


    End Sub
    

#End Region


End Class
