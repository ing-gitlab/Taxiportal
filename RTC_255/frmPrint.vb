﻿Imports Common
Public Class frmPrint
    Dim d As New DataClasses1DataContext

    Private Sub frmPrint_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            My.Application.DoEvents()

            PrintForm1.Print(Me, PowerPacks.Printing.PrintForm.PrintOption.FullWindow)

            Close()
        Catch ex As Exception

        End Try

    End Sub

    Public Function AbrirParaImpresion(ByVal id As Integer) As Boolean
        Try
            Dim ser = d.proc_ServiciosLoadByPrimaryKey(id).ToList.First
            Dim movil = d.proc_VehiculosLoadByPrimaryKey(ser.idVehiculo1).ToList.First
            Dim barrio = d.proc_BarriosLoadByPrimaryKey(ser.idBarrioDestino).ToList.First

            lblDestino.Text = "Destino: " & barrio.descripcion
            lblTarifa.Text = "Tarifa: " & ser.tarifa
            lblMovil.Text = "Móvil: " & movil.NroMovil
            lblFecha.Text = "Fecha: " & Date.Parse(ser.fechaCreacion).Date
            lblHora.Text = String.Format("Hora: {0}:{1}:{2}", Date.Parse(ser.fechaCreacion).Hour, Date.Parse(ser.fechaCreacion).Minute, Date.Parse(ser.fechaCreacion).Second)
            PrintForm1.DocumentName = "Servicio" & id
            d.proc_LogInsert(USER_GLOBAL.idUsuario, "idServicio: " & id.ToString, NOW_GLOBAL, "Imprimiendo tickete de servicio")
            Show()

        Catch ex As Exception

        End Try

        Return True
    End Function
End Class