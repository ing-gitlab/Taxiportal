﻿Imports Common
Imports System.IO
Imports System.Data
Imports System.Drawing.Imaging
Imports SQLImages

Public Class frmMoviles

#Region "DECLARACIONES"
    Private esNuevo As Boolean = False
    Private numeroOriginal As String
    Private _idVehiculo As Integer
    Private Estado As String
    Private Modelo As Integer
    Private placaOriginal As String
    Private _ObtenerMoviles As Boolean = True
    Private _ModifiqueMovil As Boolean = False
    Dim bmp As Bitmap                             'Guardar las imagenes en disco

    Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")
    Private _ModoEdicion As Boolean = False
    Private _haAceptado As Boolean = False

    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))

#End Region

#Region "METODOS"

    Private Sub ObtenerMoviles()

        Dim LISTA = d.proc_obtenerVehiculos.ToList

        Me.GridEX1.DataSource = LISTA
        GridEX1.RetrieveStructure()
        FormatearGrid()

    End Sub

    Private Sub FormatearGrid()
        Me.GridEX1.RootTable.Columns("idPropietario").Visible = False
        Me.GridEX1.RootTable.Columns("idConductor").Visible = False
        Me.GridEX1.RootTable.Columns("idVehiculo").Visible = False
        Me.GridEX1.RootTable.Columns("Cedula").Visible = False
        Me.GridEX1.RootTable.Columns("FechaFinSancion").Visible = False
        Me.GridEX1.RootTable.Columns("habilitado").ColumnType = Janus.Windows.GridEX.ColumnType.CheckBox
        Me.GridEX1.RootTable.Columns("habilitado").EditType = Janus.Windows.GridEX.EditType.CheckBox
        Me.GridEX1.ColumnAutoResize = True
        Me.lbCantidad.Text = "Cantidad: " & GridEX1.RowCount

    End Sub

    Private Function ValidarN_Movil() As Boolean
        Try

            Dim i = d.proc_ObtenerVehiculoPorNroMovil(txtnroMovil.Text)

            If txtnroMovil.Text = "" Then
                MsgBox("Debe Ingresar un número de móvil", MsgBoxStyle.Critical, "Error")
                txtnroMovil.Focus()
                Return False
            End If

            If Me.esNuevo OrElse txtnroMovil.Text <> numeroOriginal Then
                Dim a = d.proc_ValidarNumeroMovil(txtnroMovil.Text).ToList
                If a.Count > 0 Then
                    If (MsgBox("El Número Del Movil Esta Repetido. Para poder usar este número debe inhabilitar el móvil que actualmente lo posee. " & vbCrLf & "Desea inhabilitarlo?.", MsgBoxStyle.YesNo, "Consulta") = MsgBoxResult.Yes) Then
                        d.proc_VehiculoUpdateEstado(a.ToList.First.idVehiculo, False)
                        Return True
                    Else

                    End If
                    Me.txtnroMovil.Focus()
                    Return False
                End If
            End If


            If i.Count > 0 And txtnroMovil.Text <> GridEX1.GetValue("NroMovil") Then
                MsgBox("El móvil existe con placa " & i.ToList.First.Placa & ". Elija otro número de móvil")
                Return False
            End If


            Return True
        Catch ex As Exception

        End Try



    End Function

    Private Function ValidarPlaca()

        If txtPlaca.Text = "" Then
            MsgBox("Debe Ingresar Placa", MsgBoxStyle.Critical, "Error")
            txtPlaca.Focus()
            Return False
        End If
        Dim i = d.proc_ObtenerVehiculoPorPlaca(txtPlaca.Text).ToList
        If i.Count > 0 And txtPlaca.Text <> GridEX1.GetValue("Placa") Then
            MsgBox("El móvil número " & i.First.NroMovil & " está guardado con la placa que intenta registrar.", MsgBoxStyle.Information)
            Return False
        End If

        Return True
    End Function

    Private Function ValidarModelo()
        If txtModelo.Text = "" Then
            MsgBox(" Ingresar Un Modelo", MsgBoxStyle.Critical, "Error")
            txtModelo.Focus()
            Return False
        End If
        Return True
    End Function

    Private Function validar()
        If Not ValidarN_Movil() Then
            Return False
        End If
        If Not ValidarModelo() Then
            Return False
        End If
        If Not ValidarPlaca() Then
            Return False
        End If


        Return True
    End Function

    Private Sub iniciarEdicion(ByVal esInicio As Boolean)
        GridEX1.Enabled = Not esInicio

        btnNuevo.Visible = Not esInicio
        btnModificar.Visible = Not esInicio

        txtnroMovil.ReadOnly = Not esInicio

        txtModelo.ReadOnly = Not esInicio
        txtPlaca.ReadOnly = Not esInicio
        txtMarca.ReadOnly = Not esInicio
        cboConductor.Enabled = esInicio
        cboPropietario.Enabled = esInicio

        chkSi.Enabled = esInicio
        txtAño.ReadOnly = Not esInicio
        dtpSoat.Enabled = esInicio
        dtpAfiliacion.Enabled = esInicio
        btnAceptar.Visible = esInicio
        btnAhorro.Enabled = Not esInicio
        BtnHistorial.Enabled = Not esInicio
        btnEliminar.Enabled = Not esInicio
        btnPrestamo.Enabled = Not esInicio
        btnMovimientos.Enabled = Not esInicio
        btnRecaudacion.Enabled = Not esInicio
        btnCancelar.Visible = esInicio
        btnSoat.Enabled = esInicio
        btnTarjeta.Enabled = esInicio
        btnLicencia.Enabled = esInicio
        btnSOATNoDisp.Enabled = esInicio
        btnTarjetaNoDisp.Enabled = esInicio
        btnLicenciaNoDisp.Enabled = esInicio

        btnReloadConductor.Enabled = Not btnReloadConductor.Enabled
        btnReloadPropietarios.Enabled = Not btnReloadPropietarios.Enabled
    End Sub

    Private Sub AbrirImagen(ByVal Imagen As Image)
        On Error Resume Next
        bmp = Imagen
        bmp.Save(My.Application.Info.DirectoryPath & "\imagen.jpg")
        Shell("rundll32.exe " & Environment.GetEnvironmentVariable("WINDIR") & "\system32\shimgvw.dll,ImageView_Fullscreen " & My.Application.Info.DirectoryPath & "\imagen.jpg")
    End Sub

    Private Sub GuardarHistorial()
        Try
            Dim img As New SQLImages.Main
            Dim i As Integer

            Dim movil = d.proc_VehiculosLoadByPrimaryKey(GridEX1.GetValue("idVehiculo")).ToList.First
            Dim conductor = d.proc_Conductores_VehiculosLoadByidVehiculo(GridEX1.GetValue("idVehiculo")).ToList.First

            d.proc_Historial_VehiculoInsert(i, NOW_GLOBAL, movil.idVehiculo, movil.NroMovil, movil.Habilitado, movil.Modelo, movil.Placa, movil.ImagenSoat, _
                                            movil.ImagenTarjeta, movil.FechaSOAT, movil.idUsuario, movil.ImagenLicencia, movil.fechaFinSancion, movil.marca, movil.idPropietario, movil.ano, _
                                             movil.ImagenSoatReducida, movil.ImagenTarjetaReducida, movil.ImagenLicenciaReducida, movil.fechaUltimaHabilitacion, conductor.idConductor)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Function AbrirFormParaEdicion(ByVal NroMovil As String) As Boolean
        ObtenerMoviles()
        GridEX1.Find(GridEX1.RootTable.Columns("nroMovil"), Janus.Windows.GridEX.ConditionOperator.Equal, NroMovil, 0, 1)
        iniciarEdicion(True)
        _ObtenerMoviles = False
        _ModoEdicion = True
        Me.ShowDialog()

        Return _ModifiqueMovil
    End Function



    Private Sub AsignarColores(ByRef r As Janus.Windows.GridEX.GridEXRow)

        Try

            If r.RowType <> Janus.Windows.GridEX.RowType.Record Then Exit Sub
            Dim s As Janus.Windows.GridEX.GridEXFormatStyle
            s = New Janus.Windows.GridEX.GridEXFormatStyle

            Dim ListaPrestamo = d.proc_obtenerPrestamosPorPagar(r.Cells("idConductor").Value).ToList

      


            If ListaPrestamo.Count <> 0 Then
                For Each prestamo In ListaPrestamo
                    Dim fechaPagada As Date = d.proc_ObtenerMaximaFechaPagadaPrestamo(prestamo.idPrestamo).ToList.First.FechaMax
                    Dim fecha_actual = NOW_GLOBAL()

                    Dim pyp = d.proc_Pico_placaLoadByPrimaryKey(fechaPagada.Date).ToList.First

                    If fechaPagada.Date = pyp.dia Then
                        fechaPagada = fechaPagada.AddDays(-1)
                    End If

                    'NOW_GLOBAL()
                    If fechaPagada <= fecha_actual.Date Then
                        s.BackColor = Color.Gold
                        r.Cells("nroMovil").ToolTipText = "DEBE PRESTAMO"
                        r.RowStyle = s
                        Exit Sub
                    End If

                Next


            Else

                If r.Cells.Item("FechaInhab").Value < NOW_GLOBAL().AddDays(-CONFIG_GLOBAL.diasAlertaInhab) Then

                    s.BackColor = Color.LightCoral
                    r.Cells("nroMovil").ToolTipText = "Móvil inhabilitado por falta de pago."
                    r.RowStyle = s
                    Exit Sub

                End If

                Dim fech As Date = r.Cells.Item("FechaInhab").Value
                Dim diasPP As Integer = d.proc_ObtenerDiasPicoYPlacaPorMovilyFecha(r.Cells("Placa").Value, fech, NOW_GLOBAL).ToList.Count
                Dim fin = NOW_GLOBAL.AddDays(-diasPP)

                Console.WriteLine(fin.Date)

                If fech = NOW_GLOBAL().Date Or fech = fin.Date Then
                    s.BackColor = Color.White
                    'r.Cells("nroMovil").ToolTipText = "Ok"
                    r.RowStyle = s
                    Exit Sub
                End If

            End If

            If DateDiff(DateInterval.Day, NOW_GLOBAL, r.Cells.Item("FechaInhab").Value) < CONFIG_GLOBAL.diasAlertaInhab Then
                s.BackColor = Color.LightBlue
                r.Cells("nroMovil").ToolTipText = "Menos de 3 dias para inhabilitacion"
                r.RowStyle = s
                Exit Sub
            End If

        Catch ex As Exception

        End Try
    End Sub

   

    Sub Moviles_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ToolTip3.SetToolTip(BtnHistorial, "Historial")
        ToolTip3.SetToolTip(btnEliminar, "Eliminar")
        ToolTip3.SetToolTip(btnAhorro, "Ahorro")
        ToolTip3.SetToolTip(btnPrestamo, "Prestamo")
        ToolTip3.SetToolTip(btnRecaudacion, "Recaudaciòn")
        ToolTip3.SetToolTip(btnMovimientos, "Movimientos")

        GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
        GridEX1.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic
        If _ObtenerMoviles Then ObtenerMoviles()
        ObtenerConductores()
        ObtenerPropietarios()

        cmdActualizar_Click(Me, e)

    End Sub

    Private Sub CargarImagenes()
        Try

            Dim vehiculo = d.proc_VehiculosLoadByPrimaryKey(GridEX1.GetValue("idVehiculo")).ToList.First


            Dim img As New SQLImages.Main
            Dim aux() As Byte

            If vehiculo.ImagenSoat Is Nothing Then
                pbSoat.Image = Nothing
            Else
                aux = vehiculo.ImagenSoat.ToArray
                If aux.Length = 0 Then
                    pbSoat.Image = Nothing
                Else
                    pbSoat.Image = img.ByteToBitmap(aux)
                End If
            End If




            If vehiculo.ImagenTarjeta Is Nothing Then
                pbTarjeta.Image = Nothing
            Else
                aux = vehiculo.ImagenTarjeta.ToArray
                If aux.Length = 0 Then
                    pbTarjeta.Image = Nothing
                Else
                    pbTarjeta.Image = img.ByteToBitmap(aux)
                End If
            End If

            If vehiculo.ImagenLicencia Is Nothing Then
                pbLicencia.Image = Nothing
            Else
                aux = vehiculo.ImagenLicencia.ToArray
                If aux.Length = 0 Then
                    pbLicencia.Image = Nothing
                Else
                    pbLicencia.Image = img.ByteToBitmap(aux)
                End If
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub ObtenerConductores()
        Dim Per = d.proc_ObtenerPersonas.ToList
        cboConductor.DataSource = Per
        cboConductor.DisplayMember = "Nombre"
        cboConductor.ValueMember = "idPersona"

    End Sub

    Private Sub ObtenerPropietarios()
        Dim Per = d.proc_ObtenerPersonas.ToList
        cboPropietario.DataSource = Per
        cboPropietario.DisplayMember = "Nombre"
        cboPropietario.ValueMember = "idPersona"

    End Sub

    Public Function EditarVehiculo(ByVal idVehiculo As Integer) As Boolean
        _ModoEdicion = True
        Me._idVehiculo = idVehiculo

        Me.ShowDialog()
        Return _haAceptado
    End Function



#End Region

#Region "Eventos"


    Private Sub btnReloadPropietarios_Click(sender As System.Object, e As System.EventArgs) Handles btnReloadPropietarios.Click
        ObtenerPropietarios()
    End Sub

    Private Sub btnReloadConductor_Click(sender As System.Object, e As System.EventArgs) Handles btnReloadConductor.Click
        ObtenerConductores()
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            Me.GroupBox2.Enabled = True
            If GridEX1.Enabled Then CargarImagenes()
        Else
            Me.GroupBox2.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnSOATNoDisp.Click
        pbSoat.Image = My.Resources.nodisponible
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles btnTarjetaNoDisp.Click
        pbTarjeta.Image = My.Resources.nodisponible
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles btnLicenciaNoDisp.Click
        pbLicencia.Image = My.Resources.nodisponible
    End Sub

    Private Sub GridEX1_LoadingRow(sender As Object, e As Janus.Windows.GridEX.RowLoadEventArgs) Handles GridEX1.LoadingRow
        AsignarColores(e.Row)
    End Sub

    Private Sub GridEX1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.SelectionChanged
        Try
            If Me.GridEX1.GetRow.RowType = Janus.Windows.GridEX.RowType.FilterRow OrElse Me.GridEX1.RowCount = 0 Then Exit Sub

            Me.txtnroMovil.Text = GridEX1.GetValue("NroMovil")
            Me.txtPlaca.Text = GridEX1.GetValue("Placa")
            Me.chkSi.Checked = GridEX1.GetValue("Habilitado")
            Me.txtModelo.Text = GridEX1.GetValue("Modelo")
            Me.cboConductor.SelectedValue = GridEX1.GetValue("idConductor")
            Me.cboPropietario.SelectedValue = GridEX1.GetValue("idPropietario")
            Me.txtMarca.Text = GridEX1.GetValue("Marca")
            txtAño.Value = GridEX1.GetValue("Año")
            dtpSoat.Value = GridEX1.GetValue("FechaSoat")
            dtpAfiliacion.Value = GridEX1.GetValue("Fecha_Afiliacion")
            Me._idVehiculo = GridEX1.GetValue("idVehiculo")
            Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            'cambiar el store para q no devuelva todos los campos
            'ObtenerMoviles()
            If CheckBox1.Checked Then CargarImagenes()
        Catch ex As System.Exception
            ' MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        esNuevo = False
        iniciarEdicion(True)
        CargarImagenes()
        Me.txtnroMovil.Focus()
        placaOriginal = txtPlaca.Text
        Me.numeroOriginal = Me.txtnroMovil.Text
        Me.AcceptButton = btnAceptar


    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        esNuevo = True
        iniciarEdicion(True)
        ObtenerConductores()
        ObtenerPropietarios()
        txtModelo.Text = ""
        txtnroMovil.Text = ""
        txtMarca.Text = ""
        cboConductor.Text = ""
        cboPropietario.Text = ""
        dtpAfiliacion.Value = NOW_GLOBAL.Date
        dtpSoat.Value = NOW_GLOBAL.Date
        txtPlaca.Text = ""
        txtnroMovil.Focus()
        txtAño.Value = NOW_GLOBAL.Year
        chkSi.Checked = True
        Me.pbSoat.Image = My.Resources.nodisponible 'My.Resources.NoDisp.GetThumbnailImage(pbSoat.Width, pbSoat.Height, Nothing, Nothing)
        Me.pbLicencia.Image = My.Resources.nodisponible
        Me.pbTarjeta.Image = My.Resources.nodisponible
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try
            'On Error Resume Next
            Dim imagenSoatReducida(-1), imagenTarjetaReducida(-1), imagenLicenciaReducida(-1) As Byte
            Dim imagenSoat(-1), imagenTarjeta(-1), imagenLicencia(-1) As Byte
            'Dim ImagenAux As Image
            Dim img As New SQLImages.Main
            Dim idVehiculo As Integer
            Dim Posicion As Integer
            ' Dim Aux As System.Data.Linq.Binary

            If validar() Then
                If esNuevo Then
                    'Dim nuevo = d.proc_VehiculosInsert(idVehiculo, txtnroMovil.Text, chkSi.Checked, txtModelo.Text, txtPlaca.Text, Nothing, _
                    '                                    Nothing, dtpSoat.Value, USER_GLOBAL.idUsuario, _
                    '                                    Nothing, Nothing, txtMarca.Text, _
                    '                                     cboPropietario.SelectedValue, txtAño.Value, dtpAfiliacion.Value, imagenSoatReducida, imagenTarjetaReducida, imagenLicenciaReducida, _
                    '                                       NOW_GLOBAL, NOW_GLOBAL)

                    Try

                        Dim con = d.proc_Conductores_VehiculosLoadByidPersonas(cboConductor.SelectedValue).ToList.First

                        If con.idConductor = cboConductor.SelectedValue Then
                            Dim vehi = d.proc_VehiculosLoadByPrimaryKey(con.idVehiculo).ToList.First
                            Dim per = d.proc_PersonasLoadByPrimaryKey(con.idConductor).ToList.First
                            If vehi.Habilitado Then
                                If (MsgBox(per.nombre & " ya es conductor del movíl " & vehi.NroMovil & ". Para poder agregar esta Persona como conductor del movíl " & txtnroMovil.Text & ", debe inhabilitar el móvil que actualmente lo posee. " & vbCrLf & "Desea inhabilitarlo?.", MsgBoxStyle.YesNo, "Consulta") = MsgBoxResult.Yes) Then
                                    d.proc_VehiculoUpdateEstado(con.idVehiculo, False)
                                    d.proc_Conductores_VehiculosDelete(cboConductor.SelectedValue, con.idVehiculo, NOW_GLOBAL)
                                Else

                                    Exit Sub

                                End If

                            Else
                                d.proc_Conductores_VehiculosDelete(cboConductor.SelectedValue, con.idVehiculo, NOW_GLOBAL)
                                Exit Try
                            End If

                        End If



                    Catch ex As Exception

                    End Try


                    Dim nuevo = d.proc_VehiculosInsert(idVehiculo, txtnroMovil.Text, chkSi.Checked, txtModelo.Text, txtPlaca.Text, img.ImageToByte(pbSoat.Image, pbSoat.Image.RawFormat), _
                                                                     img.ImageToByte(pbTarjeta.Image, pbTarjeta.Image.RawFormat), dtpSoat.Value, USER_GLOBAL.idUsuario, _
                                                                     img.ImageToByte(pbLicencia.Image, pbLicencia.Image.RawFormat), Nothing, txtMarca.Text, _
                                                                      cboPropietario.SelectedValue, txtAño.Value, dtpAfiliacion.Value, imagenSoatReducida, imagenTarjetaReducida, imagenLicenciaReducida, _
                                                                        NOW_GLOBAL, NOW_GLOBAL)

                    d.proc_Conductores_VehiculosInsert(cboConductor.SelectedValue, idVehiculo, NOW_GLOBAL, Nothing)

                    'MsgBox("NUEVO")
                Else
                    'modificar
                    Me.GuardarHistorial()

                    'If Me.esNuevo OrElse txtnroMovil.Text <> numeroOriginal Then
                    '    Dim a = d.proc_ValidarNumeroMovil(txtnroMovil.Text).ToList
                    '    If a.Count > 0 Then
                    '        If (MsgBox("El Número Del Movil Esta Repetido. Para poder usar este número debe inhabilitar el móvil que actualmente lo posee. " & vbCrLf & "Desea inhabilitarlo?.", MsgBoxStyle.YesNo, "Consulta") = MsgBoxResult.Yes) Then
                    '            d.proc_VehiculoUpdateEstado(a.ToList.First.idVehiculo, False)

                    '        Else

                    '        End If
                    '        Me.txtnroMovil.Focus()
                    '        Exit Sub
                    '    End If
                    'End If



                    '**********************************
                    'verificar si hay otro movil habilitado con el mismo Nro
                    Dim vehi = d.proc_VehiculosLoadByPrimaryKey(GridEX1.GetValue("idVehiculo")).ToList.First
                    If Not vehi.Habilitado And chkSi.Checked Then
                        Dim a = d.proc_ValidarNumeroMovil(txtnroMovil.Text).ToList
                        If a.Count > 0 Then
                            If (MsgBox("El Número Del móvil está repetido. Para poder usar este número debe inhabilitar el móvil que actualmente lo posee. " & vbCrLf & "Desea inhabilitarlo?.", MsgBoxStyle.YesNo, "Consulta") = MsgBoxResult.Yes) Then
                                d.proc_VehiculoUpdateEstado(a.ToList.First.idVehiculo, False)

                            Else
                                Exit Sub
                            End If
                        End If
                    End If


                    Posicion = Me.GridEX1.GetRow().Position


                    Try

                        Dim con = d.proc_Conductores_VehiculosLoadByidPersonas(cboConductor.SelectedValue).ToList.First

                        Dim ve = d.proc_VehiculosLoadByPrimaryKey(GridEX1.GetValue("idVehiculo")).ToList.First

                        If Not ve.Habilitado Then

                            Dim per = d.proc_PersonasLoadByPrimaryKey(GridEX1.GetValue("idConductor")).ToList.First
                            Dim v = d.proc_ValidarVehiculo(GridEX1.GetValue("idConductor")).ToList
                            If v.Count > 0 Then
                                If con.idConductor = v.First.idConductor Then
                                    If (MsgBox(per.nombre & " Actualmente es conductor del movíl " & v.ToList.First.NroMovil & ". Para poder agregar este Persona como conductor del movíl " & txtnroMovil.Text & ", debe inhabilitar el móvil que actualmente lo posee. " & vbCrLf & "Desea inhabilitarlo?.", MsgBoxStyle.YesNo, "Consulta") = MsgBoxResult.Yes) Then
                                        d.proc_VehiculoUpdateEstado(v.ToList.First.idVehiculo, False)
                                        d.proc_Conductores_VehiculosDelete(cboConductor.SelectedValue, v.ToList.First.idVehiculo, NOW_GLOBAL)

                                    Else

                                        Exit Sub

                                    End If
                                End If
                            End If


                        Else
                            Exit Try
                        End If




                    Catch ex As Exception

                    End Try


                    d.proc_VehiculosUpdate(GridEX1.GetValue("idVehiculo"), txtnroMovil.Text, chkSi.Checked, txtModelo.Text, txtPlaca.Text, img.ImageToByte(pbSoat.Image, pbSoat.Image.RawFormat), img.ImageToByte(pbTarjeta.Image, pbTarjeta.Image.RawFormat), dtpSoat.Value, USER_GLOBAL.idUsuario, _
                                                                                 img.ImageToByte(pbLicencia.Image, pbLicencia.Image.RawFormat), Nothing, txtMarca.Text, _
                                                                                  cboPropietario.SelectedValue, txtAño.Value, dtpAfiliacion.Value, imagenSoatReducida, imagenTarjetaReducida, imagenLicenciaReducida, _
                                                                                    NOW_GLOBAL, NOW_GLOBAL)

                    d.proc_Conductores_VehiculosUpdate(cboConductor.SelectedValue, GridEX1.GetValue("idVehiculo"), NOW_GLOBAL, Nothing)

                    _ModifiqueMovil = True
                    If _ModoEdicion Then
                        StartBroadcast(PORT_Principal, "RefrescarMoviles")
                        Me.Close()
                    End If

                    '**************************************************************************



                End If

                iniciarEdicion(False)

                ObtenerMoviles()
                StartBroadcast(PORT_Principal, "RefrescarMoviles")
                If esNuevo Then
                    Posicion = Me.GridEX1.RowCount - 1
                End If
                Me.GridEX1.MoveTo(Posicion)
                If Me._ModoEdicion Then
                    _haAceptado = True
                    Me.Close()
                End If
            End If
            Me.AcceptButton = Nothing
            Me.GridEX1.Focus()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
     

    End Sub

    Private Sub pbSoat_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbSoat.DoubleClick



        If pbSoat.Image Is Nothing Then
            MsgBox("Imagen No Disponible", MsgBoxStyle.Information, "Aviso")



        Else
            Me.AbrirImagen(Me.pbSoat.Image)
        End If
    End Sub

    Private Sub pbTarjeta_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbTarjeta.DoubleClick
        Dim vehiculo = d.proc_ObtenerImagenTarjeta(GridEX1.GetValue("idVehiculo")).ToList.First

        Dim img As New SQLImages.Main

        If pbTarjeta.Image Is Nothing Then
            MsgBox("Imagen No Disponible", MsgBoxStyle.Information, "Aviso")
        Else

            Me.AbrirImagen(Me.pbTarjeta.Image)
        End If


    End Sub

    Private Sub pbLicencia_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbLicencia.DoubleClick
        Dim vehiculo = d.proc_ObtenerImagenLicencia(GridEX1.GetValue("idVehiculo")).ToList.First

        Dim img As New SQLImages.Main
        If pbLicencia.Image Is Nothing Then
            MsgBox("Imagen No Disponible", MsgBoxStyle.Information, "Aviso")
        Else
            Me.AbrirImagen(Me.pbLicencia.Image)
        End If

    End Sub

    Private Sub btnSoat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSoat.Click
        Dim sqlimages As New SQLImages.Main
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then

            pbSoat.Image = Image.FromFile(OpenFileDialog1.FileName)
        End If
    End Sub

    Private Sub btnTarjeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTarjeta.Click
        If OpenFileDialog2.ShowDialog = Windows.Forms.DialogResult.OK Then

            pbTarjeta.Image = Image.FromFile(OpenFileDialog2.FileName)
        End If
    End Sub

    Private Sub btnLicencia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLicencia.Click
        If OpenFileDialog3.ShowDialog = Windows.Forms.DialogResult.OK Then

            pbLicencia.Image = Image.FromFile(OpenFileDialog3.FileName)
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.iniciarEdicion(False)
        Me.ObtenerMoviles()
        If _ModoEdicion Then
            Me.Close()
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Try
            If MsgBox("¿Está seguro de eliminar el vehiculo?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Confirmación de eliminación") = MsgBoxResult.Yes Then
                GuardarHistorial()
                d.proc_VehiculoUpdateEstado(GridEX1.GetValue("idVehiculo"), False)
                ObtenerMoviles()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub BnHistorial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnHistorial.Click

        Dim f As New frmHistorial
        f.abrirFormulario("Historial de Moviles", d.proc_Historial_VehiculoLoadByIdVehiculo(GridEX1.GetValue("idVehiculo")).ToList)
        f.GridEX1.RootTable.Columns("idHistorialVehiculo").Visible = False
        f.GridEX1.RootTable.Columns("idVehiculo").Visible = False
        f.GridEX1.RootTable.Columns("idPropietario").Visible = False
        f.GridEX1.RootTable.Columns("Habilitado").ColumnType = Janus.Windows.GridEX.ColumnType.CheckBox

        f.GridEX1.RootTable.Columns("FechaSOAT").Caption = "Fecha del SOAT"
        f.GridEX1.RootTable.Columns("Conductor").Caption = "Conductor"
        f.GridEX1.RootTable.Columns("idUsuario").Caption = "Usuario"
        f.GridEX1.ColumnAutoResize = True
    End Sub

    Private Sub btnAhorro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAhorro.Click
        Dim f As New frmPagoAhorros
        f.AbrirFormConPersona(GridEX1.GetValue("idConductor"))

    End Sub

    Private Sub btnPrestamo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrestamo.Click
        Try
           
                Dim f As New frmPrestamo
                Dim img As New SQLImages.Main
                Dim aux() As Byte
                Dim imagen As Image
            Dim Persona = d.proc_PersonasLoadByPrimaryKey(GridEX1.GetValue("idConductor")).ToList.First
                If Persona.Imagen Is Nothing Then
                    imagen = Nothing
                Else
                    aux = Persona.Imagen.ToArray
                    If aux.Length = 0 Then
                        imagen = Nothing
                    Else
                        imagen = img.ByteToBitmap(aux)
                    End If
                End If
                f.AbrirFormulario(GridEX1.GetValue("idConductor"), imagen)

        Catch ex As Exception

        End Try


    End Sub

    Private Sub btnMovimientos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMovimientos.Click
        Dim f As New frmMovimientos
        f.AbrirForm(GridEX1.GetValue("idConductor"))
    End Sub

    Private Sub btnRecaudacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecaudacion.Click
        'Dim f As New frmRecaudacion

        'f.AbrirFrmRecaudacion(d.proc_PersonasLoadByPrimaryKey(GridEX1.GetValue("idPropietario")).ToList.First)
        'f.ShowDialog()
        Try
            Dim f As New frmRecaudacion
            Dim nroMovil As Integer = GridEX1.GetValue("NroMovil")
            Dim vehiculo = d.proc_ObtenerVehiculoPorNroMovil(nroMovil).ToList.First
            'f.ObtenerIdVehiculo(GridEX1.GetValue("idVehiculo"))
            f.AbrirFrmRecaudacion(d.proc_PersonasLoadByPrimaryKey(d.proc_Conductores_VehiculosLoadByidVehiculo(vehiculo.idVehiculo).ToList.First.idConductor).ToList.First)
            f.ShowDialog()
        Catch ex As Exception
            MsgBox("No hay datos para este movil")
        End Try
    End Sub


#End Region


    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub cmdActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActualizar.Click
        ObtenerMoviles()
    End Sub

End Class
