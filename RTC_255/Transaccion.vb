﻿Imports Common

Public Class frmTransaccion

#Region "Declaraciones"
    Dim _d As New DataClasses1DataContext(getSettings("dbConnection"))
    Dim _Vehiculos() As proc_obtenerVehiculosPorConductorResult
    Dim _Persona As proc_PersonasLoadByPrimaryKeyResult
    Dim LISTA As List(Of Integer)
    Dim idVales As Integer = 0
    Dim totales As Integer
    Dim listaRecaudaciones As New List(Of proc_RecaudacionLoadByPrimaryKeyResult)
    Dim _DataTableEncabezado As DataTable
    Dim _DataTableDetalle As DataTable
    Dim idTrans As Integer = 0
    Dim listaVales As New List(Of proc_ValesLoadByPrimaryKeyResult)
    Dim totalRecaudo As Decimal = 0
    Dim totalAhorro As Decimal = 0
    Dim totalAhorroAdicional As Decimal = 0
    Dim totalVale As Decimal = 0
    Dim valida As Boolean = False
#End Region


#Region "Eventos"


    Private Sub frmTransaccion_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
        GridEX2.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."

        ToolTip1.SetToolTip(btnRecaudacion, "Recaudación")
        ToolTip1.SetToolTip(btnImprimir, "Imprimir")
        ToolTip1.SetToolTip(btnVale, "Vale")
        txtMovil.Focus()
        ObtenerRecaudaciones()
        CalcularTotal()
        DetectarVehiculo()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        valida = True
        Limpiar()
        FormatearGrid(listaRecaudaciones)
        ObtenerVales()
        GridEX1.Refetch()
        CalcularTotal()
        txtMovil.Focus()
        HabilitarControles(False)

    End Sub

    Private Sub btnRecaudacion_Click(sender As System.Object, e As System.EventArgs) Handles btnRecaudacion.Click

        Try
            Dim f As New frmRecaudacion
            Dim nroMovil As Integer = txtMovil.Text
            Dim vehiculo = _d.proc_ObtenerVehiculoPorNroMovil(nroMovil).ToList.First
            f.AbrirFrmRecaudacion(_d.proc_PersonasLoadByPrimaryKey(_d.proc_Conductores_VehiculosLoadByidVehiculo(vehiculo.idVehiculo).ToList.First.idConductor).ToList.First)
            If f.cierre Then
                f.Close()
            End If
            f.ShowDialog()

            If f.transaccion Then
                LISTA = f.AbrirTransaccion
                ObtenerRecaudaciones()
                ObtenerPersona()
                If GridEX1.RowCount <> 0 Then
                    HabilitarControles(True)
                End If
            End If
           
        Catch ex As Exception
            MsgBox("No hay datos para este movil")
        End Try

    End Sub


    Private Sub btnVale_Click(sender As System.Object, e As System.EventArgs) Handles btnVale.Click
        Dim vale As New frmVales
        vale.Tarifa(True)
        vale.txtMovil.Value = Me.txtMovil.Value
        vale.txtMovil.Enabled = False
        vale.txtvale.Focus()
        vale.ShowDialog()
        If vale.cierre Then
            Try
                idVales = vale.idVale

                Dim vales = _d.proc_ValesLoadByPrimaryKey(idVales).ToList.First
                listaVales.Add(vales)
                ObtenerVales()
                valida = False
                Limpiar()
                CalcularTotal()
                DetectarVehiculo()
            Catch ex As Exception
                Limpiar()
                CalcularTotal()
            End Try
            
        End If
    End Sub

    Private Sub ObtenerPersona()
        Dim vehiculos = _d.proc_ObtenerVehiculoPorNroMovil(Int32.Parse(txtMovil.Value)).ToList
        Dim vehiculo = _d.proc_ObtenerVehiculoPorNroMovil(Int32.Parse(txtMovil.Value)).ToList.First
        For index = 0 To vehiculos.Count - 1
            If vehiculos(index).Habilitado = True Then
                vehiculo = vehiculos(index)
                Exit For
            End If
        Next

        _Persona = _d.proc_PersonasLoadByPrimaryKey(_d.proc_Conductores_VehiculosLoadByidVehiculo(vehiculo.idVehiculo).ToList.First.idConductor).ToList.First

    End Sub


    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Try

            If txtTotalPagar.Value < 0 Then

                If (MsgBox("Hay un excedente por devolver de : " & (txtTotalPagar.Value * -1), MsgBoxStyle.OkCancel, "Consulta") = MsgBoxResult.Cancel) Then
                    Exit Sub
                End If

            End If

            Dim idTransaccion As Integer
            Dim recaudacion As Decimal = txtTotalPagar.Value
            Dim ahorro As Decimal = totalAhorro
            Dim ahorroAdicional As Decimal = totalAhorroAdicional

            _d.proc_TransaccionInsert(idTransaccion, _Persona.idPersona, NOW_GLOBAL.Date, recaudacion, ahorro, ahorroAdicional, USER_GLOBAL.idUsuario, _Persona.idPersona)

            For Each v In listaVales
                _d.proc_ValesUpdateEstadoTarifa(v.idVale, 1)
            Next


            idTrans = idTransaccion
            If ChImprimir.Checked Then
                ImprimirTickete(True)
            Else
                ChImprimir.Checked = False
            End If
            valida = True
            Limpiar()
            FormatearGrid(listaRecaudaciones)
            ObtenerVales()
            GridEX1.Refetch()
            CalcularTotal()
            txtMovil.Focus()
            HabilitarControles(False)

        Catch ex As Exception
            MsgBox("Error al Guardar:" & vbCrLf & ex.Message)
        End Try

    End Sub


    Private Sub txtMovil_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtMovil.KeyUp
        DetectarVehiculo()
    End Sub

    Private Sub btnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimir.Click
        If GridEX1.RowCount <> 0 Then
            ImprimirTickete(True)
        End If
    End Sub

#End Region

#Region "Metodos"

    Private Sub DetectarVehiculo()
        Try
            Dim _ListaVehiculo = Nothing
            If Not IsNumeric(txtMovil.Text) Then Exit Sub
            If _d.proc_ObtenerVehiculoPorNroMovil(Integer.Parse(txtMovil.Text)).ToList.Count > 0 Then
                _ListaVehiculo = _d.proc_ObtenerVehiculoPorNroMovil(Integer.Parse(txtMovil.Text)).ToList.First
            Else
                _ListaVehiculo = Nothing
            End If

            If _ListaVehiculo Is Nothing Then
                PictureBox1.Image = My.Resources.redx
            Else
                PictureBox1.Image = My.Resources.check2
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Limpiar()
        txtTotalPagar.Value = 0
        totalAhorro = 0
        totalAhorroAdicional = 0
        listaRecaudaciones.Clear()
        totalRecaudo = 0
        totales = 0
        totalVale = 0

        If valida Then
            listaVales.Clear()
        End If
        lblExcedente.Visible = False
        PictureBox1.Image = My.Resources.redx
    End Sub

    Private Sub ObtenerRecaudaciones()

        Try

            If Not LISTA Is Nothing Then

                For Each index In LISTA
                    listaRecaudaciones.Add(_d.proc_RecaudacionLoadByPrimaryKey(index).ToList.First)
                Next

                FormatearGrid(listaRecaudaciones)
                CalcularTotal()
            End If

            Me.GridEX1.ColumnAutoResize = True

        Catch ex As Exception

        End Try

    End Sub

    Private Sub FormatearGrid(ByVal lista As List(Of proc_RecaudacionLoadByPrimaryKeyResult))
        Me.GridEX1.DataSource = lista.ToList
        GridEX1.RetrieveStructure()

        Me.GridEX1.RootTable.Columns("idRecaudacion").Visible = False
        Me.GridEX1.RootTable.Columns("idPersona").Visible = False
        Me.GridEX1.RootTable.Columns("idUsuario").Visible = False
        Me.GridEX1.RootTable.Columns("idConductor").Visible = False
        Me.GridEX1.RootTable.Columns("idTipoRecaudacion").Visible = False
        Me.GridEX1.RootTable.Columns("idPrestamo").Visible = False
        Me.GridEX1.RootTable.Columns("idPrestamo").Caption = "Recaudo"
        'Dim cc As New Janus.Windows.GridEX.GridEXGroup(GridEX1.Tables(0).Columns("FechaInhabilitacion"))
        'GridEX1.Tables(0).Columns("FechaInhabilitacion").Table.Groups.Add(cc)
    End Sub

    Private Sub ObtenerVales()
        Try
       

            GridEX2.DataSource = listaVales.ToList

            GridEX2.RetrieveStructure()
            GridEX2.RootTable.Columns("idVale").Visible = False
            GridEX2.RootTable.Columns("idCliente").Visible = False
            GridEX2.RootTable.Columns("idUsuario").Visible = False
            GridEX2.RootTable.Columns("idPersona").Visible = False
            GridEX2.RootTable.Columns("idVehiculo").Visible = False
            GridEX2.RootTable.Columns("estado").Visible = False
            GridEX2.RootTable.Columns("estadoTarifa").Visible = False
            'GridEX2.RootTable.Columns("estadoTarifa").Caption = "Hecho por Transacción"
            'GridEX2.RootTable.Columns("estadoTarifa").EditType = Janus.Windows.GridEX.EditType.CheckBox
            'GridEX2.RootTable.Columns("estadoTarifa").ColumnType = Janus.Windows.GridEX.ColumnType.CheckBox
            GridEX2.RootTable.Columns("nroVale").Caption = "Nro. Vale"
            GridEX2.RootTable.Columns("tarifa").Caption = "Tarifa"
            GridEX2.RootTable.Columns("pasajero").Caption = "Pasajero"
            GridEX2.RootTable.Columns("pago").Caption = "Pago"
            GridEX2.RootTable.Columns("peaje").Caption = "Peaje"
            GridEX2.RootTable.Columns("CentroCosto").Caption = "Centro de Costo"
            GridEX2.RootTable.Columns("fechaCarga").Caption = "Fecha Registro"
            GridEX2.RootTable.Columns("fechaVale").Caption = "Fecha Vale"
            GridEX2.RootTable.Columns("Tarifa").FormatString = " ###,###,##0"
            GridEX2.RootTable.Columns("Pago").FormatString = " ###,###,##0"

            idVales = 0
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub FormatearGrid2()

    End Sub



    Private Sub CalcularTotal()
        Try
            txtTotalPagar.Value = 0
            totalAhorro = 0
            totalAhorroAdicional = 0
            totalRecaudo = 0
            totales = 0
            totalVale = 0
            Dim totalCredito As Integer = 0
            If GridEX1.RowCount <> 0 Then

                For index = 0 To GridEX1.RowCount - 1
                    totalRecaudo += GridEX1.GetRow(index).Cells.Item("importeRecaudacion").Value
                    totalAhorro += GridEX1.GetRow(index).Cells.Item("importeAhorro").Value
                    totalAhorroAdicional += GridEX1.GetRow(index).Cells.Item("importeAhorroAdicional").Value
                Next

                totales = (totalRecaudo + totalAhorro + totalAhorroAdicional)

                lblTotalRecaudacionDiaria.Text = "Total Recaudacion Diaria : " & Format(totalRecaudo, "$ #,##0").ToString
                lbltotalAhorro.Text = "Total Ahorro : " & Format(totalAhorro, "$ #,##0").ToString
                lblTotalAhorroAdicional.Text = "Total Ahorro Adicional : " & Format(totalAhorroAdicional, "$ #,##0").ToString

                If GridEX2.RowCount <> 0 Then

                    For index = 0 To GridEX2.RowCount - 1
                        totalVale += GridEX2.GetRow(index).Cells.Item("tarifa").Value
                    Next

                End If

                txtTotalPagar.Value = totales - totalVale
            End If



            If txtTotalPagar.Value < 0 Then
                Label6.Visible = False
                lblExcedente.ForeColor = Color.Red
                lblExcedente.Visible = True
            Else
                Label6.Visible = True
            End If

            lblTotalVale.Text = "Total Vales: " & Format(totalVale, "$ #,##0").ToString
            lblTotalRecaudacionDiaria.Text = "Total Recaudacion Diaria : " & Format(totalRecaudo, "$ #,##0").ToString
            lbltotalAhorro.Text = "Total Ahorro : " & Format(totalAhorro, "$ #,##0").ToString
            lblTotalAhorroAdicional.Text = "Total Ahorro Adicional : " & Format(totalAhorroAdicional, "$ #,##0").ToString
            lblTotalRecDiaria.Text = "Total Recaudación Diaria : " & Format(totales, "$ #,##0").ToString

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub HabilitarControles(ByVal edicion As Boolean)
        ChImprimir.Enabled = edicion
        btnImprimir.Enabled = edicion
        btnVale.Enabled = edicion
    End Sub


    Private Sub ImprimirTickete(comprobante As Boolean)
        Try
            Dim ids As New clsImpresionDataSet
            Dim ps As New System.Drawing.Printing.PageSettings
            Dim size As New System.Drawing.Printing.PaperSize

            With ids
                .TipoImpresora = clsImpresionDataSet.TipoImpresora_t.Grafica

                size = New System.Drawing.Printing.PaperSize("76mm Roll Paper", 76, 297)

                ps.PaperSize = size

                .ConfiguracionPagina = ps
                .NombreDocumentoImpresion = "Transaccion"
                .PathArchivoXML = My.Application.Info.DirectoryPath & "\Transaccion.xml"
                .DataSet = DataSetImpresion(comprobante)

                .Imprimir()

            End With

            'Hago esto porque si se quiere reimprimir, arroja un error al querer agregar nuevamente los DataTables al DataSet:
            ids.DataSet.Tables.Clear()
            ids = Nothing
        Catch ex As Exception
            MsgBox("Error al Imprimir")
        End Try
      
    End Sub
    Private Function DataSetImpresion(comprobante As Boolean) As DataSet
        Try

            Dim ret As New DataSet
            Dim dr As DataRow
          

            _DataTableEncabezado = New DataTable
            _DataTableDetalle = New DataTable
            Me._DataTableEncabezado.Rows.Clear()
            Me._DataTableEncabezado.Columns.Add("Titulo")
            Me._DataTableEncabezado.Columns.Add("NIT")

            Me._DataTableEncabezado.Columns.Add("FechaPago")
            Me._DataTableEncabezado.Columns.Add("TotalRecaudaciones")
            Me._DataTableEncabezado.Columns.Add("TotalAhorro")
            Me._DataTableEncabezado.Columns.Add("TotalAhorroAdicional")
            Me._DataTableEncabezado.Columns.Add("Total")
            Me._DataTableEncabezado.Columns.Add("TotalPagar")
            Me._DataTableEncabezado.Columns.Add("TotalVales")
            Me._DataTableEncabezado.Columns.Add("Movil")
            Me._DataTableEncabezado.Columns.Add("Operador")

            dr = Me._DataTableEncabezado.NewRow

            dr.Item("Titulo") = getSettings("Titulo")
            dr.Item("NIT") = String.Format("{0}{1}Tel: 4129491{2}www.taxiportal.com.co{3}Registro de recaudacion diaria", getSettings("NIT"), vbCrLf, vbCrLf, vbCrLf)

            dr.Item("FechaPago") = "Fecha Actual: " & Now.ToShortDateString
            'dr.Item("TotalRecaudaciones") = "Total Recaudaciones: " & FormatCurrency(txtTotalRecaudacion.Value, 0).ToString
            'dr.Item("TotalAhorro") = "Total Ahorro: " & FormatCurrency(txtTotalAhorro.Value, 0).ToString
            'dr.Item("TotalAhorroAdicional") = "Total Ahorro Adicional: " & FormatCurrency(txtTotalAhorroAdicional.Value, 0).ToString
            dr.Item("Total") = "Total: " & totales
            'dr.Item("TotalVales") = "Total Vales: " & totalVale
            'dr.Item("TotalPagar") = "Total a Pagar: " & FormatCurrency(txtTotalPagar.Value, 0).ToString
            dr.Item("Movil") = "N° de Movil : " & txtMovil.Text
            dr.Item("Operador") = "Operador: " & USER_GLOBAL.nombre

            Me._DataTableEncabezado.Rows.Add(dr)

            ret.Tables.Add(Me._DataTableEncabezado)
            ret.Tables.Add(Me._DataTableDetalle)

            Return ret
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

#End Region




End Class