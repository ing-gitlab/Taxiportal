﻿Imports Common
Public Class frmPico_Placa
    Public Class myButt
        Inherits System.Windows.Forms.Button
    End Class

#Region "Declaraciones"

    Private dateButts() As myButt
    Dim LocalValues(6) As String 'to store the local day names
    Dim DB As New DataClasses1DataContext(getSettings("dbConnection"))
    Dim Festivos() As proc_FestivosLoadByMESResult

#End Region

#Region "Eventos"
    Private Sub Pico_Placa_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtAño.Value = Now.Year
        monthSelect.Value = Now.Month
        ReDim dateButts(41) '41 total flat-buttons created at run time
        Dim i, lblTop, lblLeft, datesTotalRow, datesTotalCol As Integer
        lblTop = 100
        lblLeft = 275
        datesTotalRow = 1
        datesTotalCol = 1

        'First rename the day-name labels according to the local-regional names
        'each name gets stored in a variable used later by the Checkdates()
        Dim DNdate As Date
        Dim DayName As String

        DNdate = "#1/6/2008#" '(the first of june of 2008 is sunday)
        DayName = Format(DNdate, "ddd") 'in USA DayName should have the value "sun" now
        Label1.Text = DayName.ToUpper
        LocalValues(0) = DayName

        DNdate = "#2/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label2.Text = DayName.ToUpper
        LocalValues(1) = DayName

        DNdate = "#3/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label3.Text = DayName.ToUpper
        LocalValues(2) = DayName
        DNdate = "#4/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label4.Text = DayName.ToUpper
        LocalValues(3) = DayName
        DNdate = "#5/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label5.Text = DayName.ToUpper
        LocalValues(4) = DayName
        DNdate = "#6/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label6.Text = DayName.ToUpper
        LocalValues(5) = DayName
        DNdate = "#7/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label7.Text = DayName.ToUpper
        LocalValues(6) = DayName

        'create all the buttons at run time
        For i = 0 To 41
            dateButts(i) = New myButt
            dateButts(i).Text = ""
            dateButts(i).Font = New Font(Me.Font, FontStyle.Bold)
            dateButts(i).Top = lblTop
            dateButts(i).Left = lblLeft
            dateButts(i).Width = 39
            'add handler to use events with the run time created buttons
            AddHandler dateButts(i).Click, AddressOf anybutt_click
            dateButts(i).Cursor = Cursors.Hand
            dateButts(i).FlatStyle = FlatStyle.Flat
            dateButts(i).FlatAppearance.MouseOverBackColor = Color.AliceBlue
            dateButts(i).FlatAppearance.MouseDownBackColor = Color.LightBlue
            dateButts(i).Tag = i 'this just for test
            dateButts(i).Visible = False

            'select: if the day is located in the sunday column make the number red
            Select Case i
                Case -1, 0
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 6, 7
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 13, 14
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 20, 21
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 27, 28
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 34, 35
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
            End Select

            Me.Controls.Add(dateButts(i))

            datesTotalRow = datesTotalRow + 1
            datesTotalCol = datesTotalCol + 1
            lblLeft = lblLeft + 40
            If datesTotalRow > 7 Then
                lblTop = lblTop + 27
                datesTotalRow = 1
            End If
            If datesTotalCol > 7 Then
                lblLeft = 275
                datesTotalCol = 1
            End If

        Next

        CheckDatesFestivos(monthSelect.Value, txtAño.Value)
        Me.GroupBox1.SendToBack()
    End Sub

    Private Sub CheckDatesFestivos(ByVal strCurrentMonth As Integer, ByVal strCurrentYear As Integer)
        Festivos = DB.proc_FestivosLoadByMES("1-1-" + txtAño.Value.ToString).ToArray
        Dim FirstDay, MonthName As String
        Dim MyDate As Date
        Dim i, TotalDays, StartingDateLabel As Integer

        'make all buttons invisible until changes are done
        For i = 0 To 41
            dateButts(i).Visible = False
        Next

        MyDate = "#1/" & strCurrentMonth & "/" & strCurrentYear & "#"
        MonthName = Format(MyDate, "MMMM")
        'label10 to show the month name in local/regional name
        Label10.Text = MonthName.ToUpper

        TotalDays = System.DateTime.DaysInMonth(strCurrentYear, strCurrentMonth)

        FirstDay = Format(MyDate, "ddd")

        'Here we use the local values stored in Form1_load()
        'and check which is the first day of the month 
        StartingDateLabel = Nothing
        If FirstDay = LocalValues(0) Then     'if sunday
            StartingDateLabel = "0"
        ElseIf FirstDay = LocalValues(1) Then 'if monday
            StartingDateLabel = "1"
        ElseIf FirstDay = LocalValues(2) Then 'if tuesday
            StartingDateLabel = "2"
        ElseIf FirstDay = LocalValues(3) Then 'if wednesday
            StartingDateLabel = "3"
        ElseIf FirstDay = LocalValues(4) Then 'if thursday
            StartingDateLabel = "4"
        ElseIf FirstDay = LocalValues(5) Then 'if friday
            StartingDateLabel = "5"
        ElseIf FirstDay = LocalValues(6) Then 'if saturday
            StartingDateLabel = "6"
        End If

        'for all buttons
        For i = 0 To 41
            dateButts(i).Enabled = False
            dateButts(i).Text = ""
            dateButts(i).BackColor = Color.LightGray 'bcolor for the non-days
        Next

        'only for buttons representing days
        For i = 1 To TotalDays
            dateButts(StartingDateLabel).Enabled = True

            'here you could check with a database and replace the extra blank spaces 
            'e.g. with an asterisk to show an appointment or change the backcolor
            dateButts(StartingDateLabel).Text = "  " & i.ToString
            dateButts(StartingDateLabel).ForeColor = Color.Black
            dateButts(StartingDateLabel).BackColor = Color.White
            For index = 0 To Festivos.Length - 1
                If Festivos(index).diaFestivo.Day = i And Festivos(index).diaFestivo.Month = monthSelect.Value And Festivos(index).diaFestivo.Year = txtAño.Value Then
                    dateButts(StartingDateLabel).BackColor = Color.MediumPurple
                    dateButts(StartingDateLabel).FlatAppearance.BorderColor = Color.Black
                    Exit For
                End If

            Next
            StartingDateLabel += 1
        Next

        StartingDateLabel = Nothing
        'after all the changes make them visibe
        For i = 0 To 41
            dateButts(i).Visible = True
        Next

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try

            CheckDatesFestivos(monthSelect.Value, txtAño.Value)

        Catch ex As Exception
            ' MsgBox("Make sure you only write the year number." & vbLf & ex.Message, MsgBoxStyle.Exclamation, "Error")
        End Try

    End Sub

    'this sub is called anytime a run time button is pressed, sender is the respective button  AQUI CUALQUIER BOTON
    Private Sub anybutt_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'MsgBox(sender.text, MsgBoxStyle.Information, "info")

        Dim fecha As Date = sender.text + Label10.Text + txtAño.Value.ToString
        If sender.BackColor = Color.MediumPurple Then
            DB.proc_FestivosDelete(fecha)
            sender.BackColor = Color.White
        Else
            DB.proc_FestivosInsert(fecha)
            sender.BackColor = Color.MediumPurple
        End If


    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If monthSelect.Value = 1 Then
            monthSelect.Value = 12
            txtAño.Value = txtAño.Value - 1
            Button1_Click(Me, e)
            Exit Sub
        End If
        monthSelect.Value = monthSelect.Value - 1
        Button1_Click(Me, e)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If monthSelect.Value = 12 Then
            monthSelect.Value = 1
            txtAño.Value = txtAño.Value + 1
            Button1_Click(Me, e)
            Exit Sub
        End If
        monthSelect.Value = monthSelect.Value + 1
        Button1_Click(Me, e)
    End Sub

    Private Sub Crear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Crear.Click
        Try


            If Not CboDiaSemana.Value.DayOfWeek.ToString.Equals("Monday") Then
                MsgBox("Debe seleccionar un lunes", MsgBoxStyle.Information)
                Exit Sub
            End If

            Dim da As Date = "31/12/" + CboDiaSemana.Value.Year.ToString
            Dim D As Integer = DatePart("ww", CboDiaSemana.Value.Date)

            Dim fechaRef As New Date(Now.Year, 1, 1)
            ' Dim fechaRef2 As Date
            'Buscamos el primer Lunes del Año
            While fechaRef.DayOfWeek() <> DayOfWeek.Monday

                'Vamos sumando dia a dia, hasta encontrar el primer lunes 
                fechaRef = fechaRef.AddDays(1)
            End While
            'Multiplicamos y obtenemos el rango inferior de la semana
            fechaRef = fechaRef.AddDays(7 * (1 - 2))
            'Obtenemos el rango superior de la semana
            fechaRef = fechaRef.AddDays(7)
            If fechaRef.DayOfYear = 2 Then
                D = D + 1
            End If

            'MsgBox(fechaRef + " = " + fechaRef.DayOfYear.ToString)


            Dim Semanas As Integer = DatePart("ww", da)
            'MsgBox(D)
            'MsgBox(Semanas)

            For index As Integer = 1 To Semanas
                For index2 As Integer = 1 To 7
                    Dim PiyPl As Integer = 2 * (index + index2 - D) + 9
                    Dim DIA As String

                    Select Case index2
                        Case 1
                            DIA = "Lunes"
                        Case 2
                            DIA = "Martes"
                        Case 3
                            DIA = "Miercoles"
                        Case 4
                            DIA = "Jueves"
                        Case 5
                            DIA = "Viernes"
                        Case 6
                            DIA = "Sabado"
                        Case 7
                            DIA = "Domingo"
                    End Select

                    'MsgBox(DIA + " " + PiyPl.ToString)
                    Dim Dias As Integer = ((7 * (index - 1)) + index2 - 1)
                    Dim Año As Date = ("01-01-" + txtAño.Value.ToString)
                    Dim fecha As DateTime = Format(DateAdd(DateInterval.Day, Dias, Año), "dd/MM/yyyy").ToString

                    If fecha.Year <> txtAño.Value Then
                        Exit Sub
                    End If


                    Dim Numeros As String
                    Dim N As DateTime
                    If PiyPl.ToString.Length < 2 Or PiyPl < 0 Then
                        If PiyPl < 0 Then
                            'MsgBox("Pico y Placa 1: " + DIA + " " + (PiyPl + 10).ToString + " y " + (PiyPl + 1).ToString)
                            If PiyPl = -1 Then
                                Numeros = ((PiyPl + 10).ToString + "," + (PiyPl + 1).ToString)
                            Else
                                Numeros = ((PiyPl + 10).ToString + "," + (PiyPl + 11).ToString)
                            End If
                            DB.proc_Pico_placaInsert(fecha, Numeros)
                        Else
                            'MsgBox("Pico y Placa 2: " + DIA + " " + PiyPl.ToString + " y " + (Integer.Parse(GetChar((PiyPl + 1).ToString, (PiyPl + 1).ToString.Length).ToString)).ToString)
                            Numeros = (PiyPl.ToString + "," + (Integer.Parse(GetChar((PiyPl + 1).ToString, (PiyPl + 1).ToString.Length)).ToString))
                            DB.proc_Pico_placaInsert(fecha, Numeros)
                        End If

                    Else
                        Dim Pico As Integer = (Integer.Parse(GetChar(PiyPl.ToString, PiyPl.ToString.Length).ToString) + 1)
                        'MsgBox("Pico y Placa 3: " + DIA + " " + GetChar(PiyPl.ToString, PiyPl.ToString.Length) + " y " + (Integer.Parse(GetChar((PiyPl + 1).ToString, (PiyPl + 1).ToString.Length).ToString)).ToString)
                        Numeros = (GetChar(PiyPl.ToString, PiyPl.ToString.Length) + "," + (Integer.Parse(GetChar((PiyPl + 1).ToString, (PiyPl + 1).ToString.Length).ToString)).ToString)
                        DB.proc_Pico_placaInsert(fecha, Numeros)
                    End If

                    ' MsgBox(DIA + " " + PiyPl.ToString)
                    ' "Pico y Placa 2: " + DIA + " " + GetChar(PiyPl.ToString, PiyPl.ToString.Length) + " y " + (Integer.Parse(GetChar(PiyPl.ToString, PiyPl.ToString.Length).ToString) + 1).ToString)
                Next
            Next
        Catch ex As Exception

            If MsgBox("  Ya estan creados los pico y placa para este año" & vbCrLf & "    ¿Desea eliminarlos y crearlos nuevamente?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, getSettings("ADVERTENCIA")) = MsgBoxResult.Yes Then
                DB.proc_Pico_placaDeletePorAño(txtAño.Value)
                Crear_Click(Me, Nothing)
            End If
        End Try

    End Sub

    'Private Sub btn_Semana_Click(sender As System.Object, e As System.EventArgs)

    '    Dim fechaRef As New Date(Now.Year, 1, 1)
    '    Dim fechaRef2 As Date
    '    'Buscamos el primer Lunes del Año
    '    While fechaRef.DayOfWeek() <> DayOfWeek.Monday

    '        'Vamos sumando dia a dia, hasta encontrar el primer lunes 
    '        fechaRef = fechaRef.AddDays(1)
    '    End While
    '    'Multiplicamos y obtenemos el rango inferior de la semana
    '    fechaRef = fechaRef.AddDays(7 * (txtSem.Text - 2))
    '    'Obtenemos el rango superior de la semana
    '    fechaRef = fechaRef.AddDays(7)

    '    MsgBox(fechaRef + " = " + fechaRef.DayOfYear.ToString)

    'End Sub

    Private Sub txtAño_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAño.ValueChanged
        Try
            Button1_Click(Me, e)
            CboDiaSemana.Value = CboDiaSemana.Value.Day.ToString + "-" + CboDiaSemana.Value.Month.ToString + "-" + txtAño.Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub monthSelect_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles monthSelect.ValueChanged
        Try
            Button1_Click(Me, e)
        Catch ex As Exception

        End Try
    End Sub
#End Region


End Class