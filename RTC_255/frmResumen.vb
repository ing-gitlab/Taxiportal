﻿Imports Common
Imports System.Net.Sockets
Imports System.Net

Public Class frmResumen

#Region "Declaraciones"
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim PORT As Integer = CInt(getSettings("frmResumen_PORT"))
    Dim udp As New UdpClient(PORT)
    Dim _DataTableEncabezado As DataTable
    Dim _DataTableDetalle As DataTable
#End Region


#Region "Eventos"

    Private Sub filtroHora_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles filtroHora.CheckedChanged

        Me.lblHoras.Visible = filtroHora.Checked
        Me.lblHoras2.Visible = filtroHora.Checked
        Me.lblDia.Visible = Not filtroHora.Checked
        Me.dtpDia.Visible = Not filtroHora.Checked
        Me.txtHoras.Visible = filtroHora.Checked

    End Sub

    Private Sub ImprimirTickete()

        Try
            Dim ids As New clsImpresionDataSet
            Dim ps As New System.Drawing.Printing.PageSettings
            Dim size As New System.Drawing.Printing.PaperSize

            With ids
                .TipoImpresora = clsImpresionDataSet.TipoImpresora_t.Grafica

                size = New System.Drawing.Printing.PaperSize("76mm Roll Paper", 76, 297)

                ps.PaperSize = size

                .ConfiguracionPagina = ps
                .NombreDocumentoImpresion = "Servicio"
                .PathArchivoXML = My.Application.Info.DirectoryPath & "\Tickete.xml"
                .DataSet = DataSetImpresion()

                .Imprimir()

            End With

            If Not DataSetImpresion() Is Nothing Then
                'Hago esto porque si se quiere reimprimir, arroja un error al querer agregar nuevamente los DataTables al DataSet:
                ids.DataSet.Tables.Clear()
                ids = Nothing
           
            End If



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

   

    End Sub

    Private Function DataSetImpresion() As DataSet
        Try
            Dim ret As New DataSet
            Dim dr As DataRow
            Dim ser = d.proc_ServiciosLoadByPrimaryKey(GridEX1.GetValue("idServicio")).ToList.First
            Dim barrio = d.proc_BarriosLoadByPrimaryKey(ser.idBarrioDestino).ToList.First
            Dim movil = d.proc_VehiculosLoadByPrimaryKey(ser.idVehiculo1).ToList.First
            _DataTableEncabezado = New DataTable
            _DataTableDetalle = New DataTable
            Me._DataTableEncabezado.Rows.Clear()
            Me._DataTableEncabezado.Columns.Add("Titulo")
            Me._DataTableEncabezado.Columns.Add("PBX")
            Me._DataTableEncabezado.Columns.Add("Cel")
            Me._DataTableEncabezado.Columns.Add("Destino")
            Me._DataTableEncabezado.Columns.Add("Tarifa")
            Me._DataTableEncabezado.Columns.Add("Movil")
            Me._DataTableEncabezado.Columns.Add("Fecha")

            dr = Me._DataTableEncabezado.NewRow

            dr.Item("Titulo") = String.Format(getSettings("Titulo"))
            dr.Item("PBX") = getSettings("PBX")
            dr.Item("Cel") = getSettings("Cel")
            dr.Item("Destino") = "Destino: " & barrio.descripcion.ToString
            dr.Item("Tarifa") = String.Format("Tarifa: {0}", ser.tarifa)
            dr.Item("Movil") = "Móvil: " & movil.NroMovil.ToString
            dr.Item("Fecha") = "Fecha/Hora: " & ser.fechaServicio.ToString



            Me._DataTableEncabezado.Rows.Add(dr)

            ret.Tables.Add(Me._DataTableEncabezado)
            ret.Tables.Add(Me._DataTableDetalle)

            Return ret
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
        
    End Function

    Private Sub UiCommandBar1_CommandClick(ByVal sender As System.Object, ByVal e As Janus.Windows.UI.CommandBars.CommandEventArgs) Handles UiCommandBar1.CommandClick
        Try
            Select Case e.Command.Text
                Case "Terminado"
                    SetearEstado(EstadosServicios.Terminado)
                Case "En Curso"
                    SetearEstado(EstadosServicios.EnCurso)
                Case "Cancelado"
                    SetearEstado(EstadosServicios.Cancelado)
                Case "Móvil en Camino"
                    SetearEstado(EstadosServicios.MovilEnCamino)
                Case "Nuevo"
                    'Dim f As New frmServicios
                    'Dim lista = d.proc_ObtenerMovilesPorZona.ToList
                    'If lista.Count = 0 Then
                    '    f.AbrirForm(True)
                    'Else
                    '    f.AbrirConVehiculo(lista(0).idVehiculo)

                    'End If
                Case "Editar"
                    If GridEX1.SelectedItems.Count = 0 Then Exit Sub
                    EditarServicio()

                Case "Servicio Rapido"
                    ' Dim f As New frmServicioRapido
                    'If f.AbrirForm(True) Then ObtenerServicios()
                    'Dim lista = d.proc_ObtenerMovilesPorZona.ToList
                    'If lista.Count = 0 Then
                    '    f.AbrirForm(True)
                    'Else
                    '    f.AbrirFormConVehiculo(lista(0).idVehiculo)

                    'End If
                Case "Imprimir"

                    ImprimirTickete()
                    'Dim f As New frmPrint : f.AbrirParaImpresion(GridEX1.GetValue("idServicio"))

            End Select

        Catch ex As Exception

        End Try
       
        
    End Sub

    Private Sub frmResumen_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        udp.Close()

    End Sub

    Private Sub frmResumen_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarConfiguracion()
        inicializarControles()
        udp.BeginReceive(AddressOf Delegate_Data, Nothing)
        ObtenerServicios()
    End Sub

    Private Sub cmdActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActualizar.Click
        ObtenerServicios()
    End Sub

    Private Sub GridEX1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.DoubleClick
        On Error Resume Next
        If GridEX1.GetRow.RowType = Janus.Windows.GridEX.RowType.Record Then
            EditarServicio()
        End If
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        GridEX1.Refresh()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            GridEX1.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic
        Else
            GridEX1.FilterMode = Janus.Windows.GridEX.FilterMode.None
        End If
    End Sub

    Private Sub GridEX1_LoadingRow(ByVal sender As Object, ByVal e As Janus.Windows.GridEX.RowLoadEventArgs) Handles GridEX1.LoadingRow
        On Error Resume Next
        If Not e.Row.RowType = Janus.Windows.GridEX.RowType.Record Then Exit Sub
        Dim r As Janus.Windows.GridEX.GridEXRow
        r = e.Row
        If r.Cells("Estado").Value <> "Terminado" And r.Cells("Estado").Value <> "Cancelado" Then

            Dim segundos As Integer = DateDiff(DateInterval.Second, r.Cells("FechaServicio").Value, NOW_GLOBAL)
            Dim s As New Janus.Windows.GridEX.GridEXFormatStyle
            If segundos > CONFIG_GLOBAL.minutosNotificacionDemora * 60 Then
                s.BackColor = Color.Gold

            Else
                If segundos < 0 Then
                    s.BackColor = Color.LightBlue
                Else
                    s.BackColor = Color.White
                End If

            End If
            Dim minutos = Decimal.Truncate(segundos / 60)
            segundos = segundos - (minutos * 60)
            r.BeginEdit()

            r.Cells("Reloj").Value = String.Format("{0:00}:{1:00}", minutos, segundos).Replace("-", "").Replace("-", "")

            r.EndEdit()

            r.RowStyle = s

        End If
    End Sub

#End Region



#Region "Metodos"

    Private Sub CargarConfiguracion()
        Me.chkCancelado.Checked = CONFIG_GLOBAL.mostrarCancelado
        Me.chkTerminado.Checked = CONFIG_GLOBAL.mostrarTerminado
        Me.chkEnCurso.Checked = CONFIG_GLOBAL.mostrarEnCurso
        Me.chkEnCamino.Checked = CONFIG_GLOBAL.mostrarEnCamino

        GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
    End Sub

    Private Function SetearEstado(ByVal estado As EstadosServicios) As Boolean
        For index As Integer = 0 To GridEX1.SelectedItems.Count - 1
            Dim r = GridEX1.SelectedItems(index).GetRow
            Dim s = d.proc_ServiciosLoadByPrimaryKey(r.Cells("idServicio").Value).ToList.First
            If s.idEstado = EstadosServicios.Terminado Then
                MsgBox("El servicio está terminado por lo tanto no puede cambiarle su estado.")
            Else
                d.proc_ServiciosUpdate(s.idServicio, s.idVehiculo1, s.idVehiculo2, s.idCliente, s.fechaCreacion, s.fechaServicio, _
                                     IIf(estado = EstadosServicios.Terminado, NOW_GLOBAL, s.fechaHR), s.fechaFinalizacion, s.idBarrioOrigen, s.direccionOrigen, s.idBarrioDestino, _
                                       s.direccionDestino, s.tarifa, s.ObservacionesConductor, "", estado, USER_GLOBAL.idUsuario, s.tipoServicio, s.radioFrecuencia)
            End If
        Next

        ObtenerServicios()
    End Function

    Private Sub Delegate_Data(ByVal Data As IAsyncResult)
        Try
            Dim groupEP As New IPEndPoint(IPAddress.Any, PORT)
            Dim bytes As Byte() = udp.EndReceive(Data, groupEP)

            Me.Invoke(New MethodInvoker(AddressOf ObtenerServicios))

            udp.BeginReceive(AddressOf Delegate_Data, Nothing)
        Catch ex As Exception

        End Try


    End Sub

    Private Sub inicializarControles()
        Me.dtpDia.Value = NOW_GLOBAL.Date
        filtroHora.Checked = True
    End Sub

    Private Sub ObtenerServicios()
        Try
            Dim idRegistroSeleccionado As Integer
            If GridEX1.SelectedItems.Count > 0 Then
                idRegistroSeleccionado = GridEX1.GetValue("idServicio")
            End If

            Dim listaServicio As DataTable
            If filtroFecha.Checked Then
                listaServicio = Funciones.proc_ObtenerServicios(dtpDia.Value.Date, Nothing, chkEnCamino.Checked, chkEnCurso.Checked, chkCancelado.Checked, chkTerminado.Checked, CONFIG_GLOBAL.radioFrecuencia).Tables(0)
            Else
                listaServicio = Funciones.proc_ObtenerServicios(Nothing, txtHoras.Value, chkEnCamino.Checked, chkEnCurso.Checked, chkCancelado.Checked, chkTerminado.Checked, CONFIG_GLOBAL.radioFrecuencia).Tables(0)
            End If

            Me.lblCantidad.Text = "Cantidad: " & listaServicio.Rows.Count
            Me.GridEX1.SetDataBinding(listaServicio, "")
            Me.GridEX1.RetrieveStructure()


            GridEX1.Tables(0).Columns("Tarifa").FormatString = "$ ###,###,##0"
            GridEX1.Tables(0).Columns("Tarifa").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far
            GridEX1.Tables(0).Columns("nroMovil").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
            '  GridEX1.Tables(0).Columns("Tiempo").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
            GridEX1.Tables(0).Columns("FechaCreacion").Visible = getSettings("MostrarFechaCreacion")
            GridEX1.Tables(0).Columns("FechaFinalizacion").Visible = getSettings("MostrarFechaTerminado")
            GridEX1.Tables(0).Columns("FechaHR").FormatString = "dd/MM/yyyy hh:mm:ss"
            GridEX1.Tables(0).Columns("FechaServicio").FormatString = "dd/MM/yyyy hh:mm:ss"
            For index As Integer = 0 To GridEX1.RootTable.Columns.Count - 1
                GridEX1.RootTable.Columns(index).AutoSize()

            Next

            Try
                GridEX1.Find(GridEX1.RootTable.Columns("idServicio"), Janus.Windows.GridEX.ConditionOperator.Equal, idRegistroSeleccionado, 1, 1)
            Catch ex As Exception

            End Try
            GridEX1.Tables(0).Columns("idBarrioDestino").Visible = False
            GridEX1.Tables(0).Columns("tipoServicio").Visible = False

        Catch ex As Exception
            'algun log 
        End Try

    End Sub

    Private Sub EditarServicio()
        If GridEX1.GetValue("tipoServicio") = TipoServicios.Normal Then
            '    Dim f As New frmServicios
            '   f.EditarServicio(GridEX1.GetValue("idServicio"))
        Else
            '    Dim f As New frmServicioRapido
            '    f.EditarServicio(GridEX1.GetValue("idServicio"))
        End If

    End Sub


#End Region



End Class