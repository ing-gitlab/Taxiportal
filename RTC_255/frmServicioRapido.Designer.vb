﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmServicioRapido
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblRecargoNocturno = New System.Windows.Forms.Label()
        Me.chkImprimir = New System.Windows.Forms.CheckBox()
        Me.btnBarrio = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.txtTarifa = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnServicio = New System.Windows.Forms.Button()
        Me.txtMovil1 = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtObs = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboBarrio = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboOrigen = New System.Windows.Forms.ComboBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblRecargoNocturno)
        Me.GroupBox1.Controls.Add(Me.chkImprimir)
        Me.GroupBox1.Controls.Add(Me.btnBarrio)
        Me.GroupBox1.Controls.Add(Me.btnCerrar)
        Me.GroupBox1.Controls.Add(Me.txtTarifa)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.btnServicio)
        Me.GroupBox1.Controls.Add(Me.txtMovil1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtObs)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cboBarrio)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 41)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(542, 196)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Destino"
        '
        'lblRecargoNocturno
        '
        Me.lblRecargoNocturno.AutoSize = True
        Me.lblRecargoNocturno.ForeColor = System.Drawing.Color.Red
        Me.lblRecargoNocturno.Location = New System.Drawing.Point(160, 68)
        Me.lblRecargoNocturno.Name = "lblRecargoNocturno"
        Me.lblRecargoNocturno.Size = New System.Drawing.Size(23, 13)
        Me.lblRecargoNocturno.TabIndex = 12
        Me.lblRecargoNocturno.Text = "RN"
        Me.lblRecargoNocturno.Visible = False
        '
        'chkImprimir
        '
        Me.chkImprimir.AutoSize = True
        Me.chkImprimir.Location = New System.Drawing.Point(75, 161)
        Me.chkImprimir.Name = "chkImprimir"
        Me.chkImprimir.Size = New System.Drawing.Size(100, 17)
        Me.chkImprimir.TabIndex = 11
        Me.chkImprimir.Text = "&Imprimir Tickete"
        Me.chkImprimir.UseVisualStyleBackColor = True
        '
        'btnBarrio
        '
        Me.btnBarrio.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnBarrio.Location = New System.Drawing.Point(496, 32)
        Me.btnBarrio.Name = "btnBarrio"
        Me.btnBarrio.Size = New System.Drawing.Size(21, 21)
        Me.btnBarrio.TabIndex = 3
        Me.btnBarrio.Text = "..."
        Me.btnBarrio.UseVisualStyleBackColor = True
        Me.btnBarrio.Visible = False
        '
        'btnCerrar
        '
        Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCerrar.Location = New System.Drawing.Point(384, 157)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(93, 23)
        Me.btnCerrar.TabIndex = 10
        Me.btnCerrar.Text = "&Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'txtTarifa
        '
        Me.txtTarifa.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtTarifa.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtTarifa.Location = New System.Drawing.Point(75, 64)
        Me.txtTarifa.Name = "txtTarifa"
        Me.txtTarifa.Size = New System.Drawing.Size(78, 20)
        Me.txtTarifa.TabIndex = 4
        Me.txtTarifa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtTarifa.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(32, 68)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 13)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "&Tarifa:"
        '
        'btnServicio
        '
        Me.btnServicio.Location = New System.Drawing.Point(285, 157)
        Me.btnServicio.Name = "btnServicio"
        Me.btnServicio.Size = New System.Drawing.Size(93, 23)
        Me.btnServicio.TabIndex = 9
        Me.btnServicio.Text = "A&signar Servicio"
        Me.btnServicio.UseVisualStyleBackColor = True
        '
        'txtMovil1
        '
        Me.txtMovil1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtMovil1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtMovil1.Location = New System.Drawing.Point(253, 64)
        Me.txtMovil1.Name = "txtMovil1"
        Me.txtMovil1.Size = New System.Drawing.Size(49, 20)
        Me.txtMovil1.TabIndex = 6
        Me.txtMovil1.Text = "0"
        Me.txtMovil1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtMovil1.Value = CType(0, Short)
        Me.txtMovil1.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(203, 68)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "&Móvil:"
        '
        'txtObs
        '
        Me.txtObs.Location = New System.Drawing.Point(75, 94)
        Me.txtObs.Multiline = True
        Me.txtObs.Name = "txtObs"
        Me.txtObs.Size = New System.Drawing.Size(406, 48)
        Me.txtObs.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(32, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "&Obs:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(32, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "&Barrio:"
        '
        'cboBarrio
        '
        Me.cboBarrio.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBarrio.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBarrio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBarrio.FormattingEnabled = True
        Me.cboBarrio.Location = New System.Drawing.Point(75, 32)
        Me.cboBarrio.Name = "cboBarrio"
        Me.cboBarrio.Size = New System.Drawing.Size(406, 21)
        Me.cboBarrio.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(44, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "Or&igen:"
        '
        'cboOrigen
        '
        Me.cboOrigen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboOrigen.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboOrigen.FormattingEnabled = True
        Me.cboOrigen.Location = New System.Drawing.Point(87, 12)
        Me.cboOrigen.Name = "cboOrigen"
        Me.cboOrigen.Size = New System.Drawing.Size(406, 21)
        Me.cboOrigen.TabIndex = 32
        '
        'frmServicioRapido
        '
        Me.AcceptButton = Me.btnServicio
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCerrar
        Me.ClientSize = New System.Drawing.Size(561, 251)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboOrigen)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmServicioRapido"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Servicios"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTarifa As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnServicio As System.Windows.Forms.Button
    Friend WithEvents txtMovil1 As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtObs As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboBarrio As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboOrigen As System.Windows.Forms.ComboBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnBarrio As System.Windows.Forms.Button
    Friend WithEvents chkImprimir As System.Windows.Forms.CheckBox
    Friend WithEvents lblRecargoNocturno As System.Windows.Forms.Label
End Class
