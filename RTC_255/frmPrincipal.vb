﻿Imports Common
Imports System.Net.Sockets
Imports System.Net
Imports System.Text

Public Class frmPrincipal

#Region "Declaraciones"
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim PORT As Integer = CInt(getSettings("frmPrincipal_PORT"))
    'Dim ZonaOrigen As Integer = System.Configuration.ConfigurationSettings.AppSettings.Get("Origen")
    Dim udp As New UdpClient(PORT)
    Dim segundos As Integer = 0
#End Region

#Region "Metodos"

    Private Sub Delegate_Data(ByVal Data As IAsyncResult)
        On Error Resume Next
        Dim groupEP As New IPEndPoint(IPAddress.Any, PORT)
        Dim bytes As Byte() = udp.EndReceive(Data, groupEP)
        Dim STR As String = Encoding.ASCII.GetString(bytes, 0, bytes.Length)

        If STR.Equals("RefrescarMoviles") Then

            Me.Invoke(New MethodInvoker(AddressOf ObtenerVehiculos))
        Else
            If STR.Equals("InicioActualizacion") Then
                lblUpdate.Text = "Estado: Actualizando Sistema..."
            Else
                If STR.Equals("FinActualizacion") Then
                    lblUpdate.Text = "Estado: Actualizado"
                End If
            End If
        End If
        udp.BeginReceive(AddressOf Delegate_Data, Nothing)

    End Sub

    Private Sub ObtenerVehiculos()
        OrdenarVehiculos()
        Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
        GridEX1.SetDataBinding(d.proc_ObtenerMovilesPorZona.ToList, "")
        GridEX1.RetrieveStructure()
        Dim cc As New Janus.Windows.GridEX.GridEXGroup(GridEX1.Tables(0).Columns("Zona"))
        GridEX1.GroupByBoxVisible = True
        GridEX1.Tables(0).Columns("Zona").Table.Groups.Add(cc)
        GridEX1.Tables(0).Columns("idVehiculo").Visible = False
        GridEX1.Tables(0).Columns("placa").Visible = False
        GridEX1.Tables(0).Columns("FechaFinSancion").Visible = False
        GridEX1.Tables(0).Columns("idConductor").Visible = False
        GridEX1.Tables(0).Columns("idZona").Visible = False
        GridEX1.Tables(0).Columns("Zona").HideWhenGrouped = Janus.Windows.GridEX.InheritableBoolean.True
        GridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False
        GridEX1.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.False
        GridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False
        For index As Integer = 0 To GridEX1.RootTable.Columns.Count - 1
            GridEX1.RootTable.Columns(index).AutoSize()

        Next


    End Sub

   

    Private Sub AsignarColores(ByRef r As Janus.Windows.GridEX.GridEXRow)


        Try

            If r.RowType <> Janus.Windows.GridEX.RowType.Record Then Exit Sub
            Dim s As Janus.Windows.GridEX.GridEXFormatStyle
            s = New Janus.Windows.GridEX.GridEXFormatStyle

            Dim ListaPrestamo = d.proc_obtenerPrestamosPorPagar(r.Cells("idConductor").Value).ToList

            If ListaPrestamo.Count <> 0 Then
                For Each prestamo In ListaPrestamo
                    Dim fechaPagada As Date = d.proc_ObtenerMaximaFechaPagadaPrestamo(prestamo.idPrestamo).ToList.First.FechaMax
                    Dim fecha_actual = NOW_GLOBAL()

                    Dim pyp = d.proc_Pico_placaLoadByPrimaryKey(fechaPagada.Date).ToList.First

                    If fechaPagada.Date = pyp.dia Then
                        fechaPagada = fechaPagada.AddDays(-1)
                    End If

                    'NOW_GLOBAL()
                    If fechaPagada <= fecha_actual.Date Then
                        s.BackColor = Color.Gold
                        r.Cells("nroMovil").ToolTipText = "DEBE PRESTAMO"
                        r.RowStyle = s
                        Exit Sub
                    End If

                Next


            Else

                If r.Cells.Item("FechaInhab").Value < NOW_GLOBAL().AddDays(-CONFIG_GLOBAL.diasAlertaInhab) Then

                    s.BackColor = Color.LightCoral
                    r.Cells("nroMovil").ToolTipText = "Móvil inhabilitado por falta de pago."
                    r.RowStyle = s
                    Exit Sub

                End If

                Dim fech As Date = r.Cells.Item("FechaInhab").Value
                Dim diasPP As Integer = d.proc_ObtenerDiasPicoYPlacaPorMovilyFecha(r.Cells("Placa").Value, fech, NOW_GLOBAL).ToList.Count
                Dim fin = NOW_GLOBAL.AddDays(-diasPP)

                Console.WriteLine(fin.Date)

                If fech = NOW_GLOBAL().Date Or fech = fin.Date Then
                    s.BackColor = Color.White
                    'r.Cells("nroMovil").ToolTipText = "Ok"
                    r.RowStyle = s
                    Exit Sub
                End If

            End If

            If DateDiff(DateInterval.Day, NOW_GLOBAL, r.Cells.Item("FechaInhab").Value) < CONFIG_GLOBAL.diasAlertaInhab Then
                s.BackColor = Color.LightBlue
                r.Cells("nroMovil").ToolTipText = "Menos de 3 dias para inhabilitacion"
                r.RowStyle = s
                Exit Sub
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub AgregarMovil()
        Dim listaZona = d.proc_ObtenerMovilesPorZona.ToList
        If MovilEnCola(listaZona) Then
            txtMovil1.Focus()
            txtMovil1.SelectAll()
            Exit Sub
        End If
        Dim posicion = txtPosicion.Value
        Dim MaxPosicion As Int16
        Dim idVehiculo As Integer = d.proc_ObtenerVehiculoPorNroMovil(Convert.ToInt16(txtMovil1.Value)).ToList.First.idVehiculo
        Dim lista = From c In listaZona Where c.idZona = cboOrigen.SelectedValue Select c
        If lista.Count = 0 Then
            MaxPosicion = 0
        Else
            MaxPosicion = lista.Last.Posicion
        End If
        If posicion >= MaxPosicion + 1 Then
            posicion = MaxPosicion + 1

            d.proc_ColaInsert(cboOrigen.SelectedValue, idVehiculo, posicion, NOW_GLOBAL, NOW_GLOBAL)
            ObtenerVehiculos()

            StartBroadcast(getSettings("frmPrincipal_PORT"), "RefrescarMoviles")
            StartBroadcast(getSettings("Rampa_PORT"), "RefrescarMoviles")
            txtMovil1.SelectAll()
            Exit Sub
        Else
            ' posicion intermedia en la lista
            Dim p As New proc_ObtenerMovilesPorZonaResult
            p.idVehiculo = idVehiculo
            p.Posicion = posicion
            p.idZona = cboOrigen.SelectedValue
            Dim lis As List(Of proc_ObtenerMovilesPorZonaResult) = lista.ToList
            lis.Insert(posicion - 1, p)
            'borrar lista de la bd 
            d.proc_ColaDeleteByZona(cboOrigen.SelectedValue)
            For index As Integer = 1 To lis.Count
                d.proc_ColaInsert(cboOrigen.SelectedValue, lis(index - 1).idVehiculo, Integer.Parse(index), NOW_GLOBAL, NOW_GLOBAL)


            Next
            StartBroadcast(getSettings("frmPrincipal_PORT"), "RefrescarMoviles")
            StartBroadcast(getSettings("Rampa_PORT"), "RefrescarMoviles")
        End If

    End Sub

    Private Function MovilEnCola(ByVal listaZona As List(Of proc_ObtenerMovilesPorZonaResult)) As Boolean
        For Each movil In listaZona
            If movil.nroMovil = txtMovil1.Value Then
                MsgBox(String.Format("El vehículo ya esta en la cola. Posición: {0} de la zona {1}", movil.Posicion, movil.Zona))

                Return True
            End If
        Next

        Return False
    End Function

    Sub inicializarMenu()
        Dim user = d.proc_UsuariosLoadByPrimaryKey(USER_GLOBAL.idUsuario).ToList.First
        ValesToolStripMenuItem.Visible = user.idTipoUsuario = 1 Or user.idTipoUsuario = 2
        TarifasToolStripMenuItem.Visible = user.idTipoUsuario = 1 Or user.idTipoUsuario = 2

        UsuariosToolStripMenuItem.Visible = user.idTipoUsuario = 1
    End Sub

    Private Sub ObtenerOrigen()
        Dim ListaZonas = d.proc_ZonasLoadAll.ToList
        Me.cboOrigen.DisplayMember = "descripcion"
        Me.cboOrigen.ValueMember = "idZona"
        Me.cboOrigen.DataSource = ListaZonas
        Me.cboOrigen.SelectedValue = CInt(CONFIG_GLOBAL.zonaDefault)
    End Sub

    Private Sub OrdenarVehiculos()
        Dim listaMoviles = d.proc_ObtenerMovilesPorZona.ToList

        For Each i As proc_ZonasLoadAllResult In cboOrigen.Items

            Dim listaZona = From c In listaMoviles Where c.idZona = cboOrigen.SelectedValue Select c

            For index As Integer = 0 To listaZona.Count - 1
                d.proc_ColaUpdate(i.idZona, listaZona(index).idVehiculo, index + 1, NOW_GLOBAL, NOW_GLOBAL)

            Next
            If i.idZona = cboOrigen.SelectedValue Then Me.txtPosicion.Value = listaZona.Count + 1
        Next

    End Sub

#End Region

#Region "Eventos"



    Private Sub frmPrincipal_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = Not Salir()
    End Sub

    Public Sub EditarMovil(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim f As New frmMoviles
        f.EditarVehiculo(GridEX1.GetValue("idVehiculo"))
    End Sub

    Public Sub AbrirPagos(ByVal sender As Object, ByVal e As System.EventArgs)
        '    Dim f As New frmControlPagos
        '   f.AbrirVentana(GridEX1.GetValue("idVehiculo"))
    End Sub

    Private Sub AbrirSanciones()
        Dim f As New frmSancion
        f.AbrirForm(GridEX1.GetValue("idVehiculo"))
    End Sub

    Private Sub EnviarARampa()

        StartBroadcast(getSettings("Rampa_PORT"), "Movil" & GridEX1.GetValue("nroMovil"))
        d.proc_ColaDelete(1, GridEX1.GetValue("idVehiculo"))
        OrdenarVehiculos()
        StartBroadcast(getSettings("Rampa_PORT"), "Refresca")
        StartBroadcast(getSettings("frmPrincipal_PORT"), "RefrescarMoviles")
        ObtenerVehiculos()
    End Sub

    Private Sub frmPrincipal_HandleDestroyed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.HandleDestroyed

    End Sub

    Private Sub frmPrincipal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Alt AndAlso e.Control AndAlso e.KeyCode = Keys.F Then
            Clipboard.SetText(Environment.CurrentDirectory)
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mnu As New System.Windows.Forms.ContextMenu
        mnu.MenuItems.Add("Editar Móvil", AddressOf EditarMovil)
        mnu.MenuItems.Add("Editar Pagos", AddressOf AbrirPagos)
        mnu.MenuItems.Add("Quitar de la Cola", AddressOf BorrarRegistroZona)
        mnu.MenuItems.Add("Sanciones", AddressOf AbrirSanciones)
        mnu.MenuItems.Add("Enviar a Rampa", AddressOf EnviarARampa)

        GridEX1.ContextMenu = mnu

        Dim l As New LoginForm1
        If l.Abrir() Then
            d = New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))

            Me.inicializarMenu()
            Me.Text = String.Format("{0} - Usuario: {1}", getSettings("Titulo"), USER_GLOBAL.idUsuario)
            GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
            ObtenerOrigen()
            ObtenerVehiculos()
            udp.BeginReceive(AddressOf Delegate_Data, Nothing)
            Dim f As New frmResumen 'frmPico_Placa 'frmPersonas  'frmRecaudacionfrmPersonas '
            f.MdiParent = Me
            f.Show()
        End If

    End Sub

    Private Sub GridEX1_DeletingRecords(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GridEX1.DeletingRecords
        If Not BorrarRegistroZona() Then e.Cancel = True
    End Sub

    Private Sub GridEX1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.DoubleClick
        If GridEX1.RowCount = 0 Then Exit Sub
        If GridEX1.SelectedItems(0).RowType = Janus.Windows.GridEX.RowType.Record Then

            If GridEX1.GetValue("FechaInhab") < Now Then
                If Not CONFIG_GLOBAL.permitirAsignacionMovilInhabilitado Then
                    MsgBox(String.Format("El móvil está inhabilitado para asigarle carreras.{0}Puede permitir la asignación de carreras en la pantalla de configuración", vbCrLf))
                    Exit Sub
                End If

                If CONFIG_GLOBAL.notificarAsignacionMovilInhab Then
                    MsgBox(String.Format("Este móvil está inhabilitado. La carrera se asignará igualmente.{0}Puede desactivar esta notificación en la pantalla de configuración", vbCrLf))

                End If
            End If

            If CONFIG_GLOBAL.tipoServicioDefault Then
                'Dim f As New frmServicios
                ' If f.AbrirConVehiculo(GridEX1.GetValue("idVehiculo")) Then ObtenerVehiculos()
            Else
                '   Dim f As New frmServicioRapido
                '   If f.AbrirFormConVehiculo(GridEX1.GetValue("idVehiculo")) Then ObtenerVehiculos()
            End If

        End If

    End Sub

    Private Sub ServiciosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServiciosToolStripMenuItem.Click

        For Each h In Me.MdiChildren
            If h.Name = "frmResumen" Then Exit Sub
        Next
        Dim F As New frmResumen
        AbrirFormulario(F, Me, False)
    End Sub

    Private Sub btnAgregarMovil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarMovil.Click
        Try
            Dim listaMoviles = d.proc_ObtenerVehiculoPorNroMovil(Me.txtMovil1.Text).ToList
            If listaMoviles.Count = 0 Then
                MsgBox("eL Móvil no existe o está inhabilitado")
                txtMovil1.Focus()

                txtMovil1.SelectAll()
            Else
                AgregarMovil()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtMovil1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMovil1.GotFocus
        Me.AcceptButton = btnAgregarMovil
    End Sub

    Private Sub txtMovil1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMovil1.LostFocus
        Me.AcceptButton = Nothing
    End Sub

    Private Sub txtPosicion_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPosicion.GotFocus
        Me.AcceptButton = btnAgregarMovil
    End Sub

    Private Sub txtPosicion_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPosicion.LostFocus
        Me.AcceptButton = Nothing
    End Sub

    Private Sub cboOrigen_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOrigen.GotFocus
        Me.AcceptButton = btnAgregarMovil
    End Sub

    Private Sub cboOrigen_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOrigen.LostFocus
        Me.AcceptButton = Nothing
    End Sub

    Private Sub GridEX1_LoadingRow(ByVal sender As Object, ByVal e As Janus.Windows.GridEX.RowLoadEventArgs) Handles GridEX1.LoadingRow
        AsignarColores(e.Row)
    End Sub

    Private Sub cmdActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ObtenerVehiculos()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        OrdenarVehiculos()
    End Sub

    Private Sub ZonasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New frmZonas
        'f.MdiParent = Me
        'f.Show()
        AbrirFormulario(f, Me)

    End Sub


    'Private Sub AcercaDeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AcercaDeToolStripMenuItem.Click
    '    Dim a As New AboutBox1
    '    a.Show()
    'End Sub

    Private Sub ViajesPorBarrioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim w As New WebBrowser
        w.WebBrowser1.Url = New Uri(getSettings("ReportePorBarrio"))
        w.Show()
    End Sub



    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        ObtenerVehiculos()
        ObtenerOrigen()
    End Sub

    Private Sub CoonfiguracionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CoonfiguracionesToolStripMenuItem.Click
        Dim f As New frmConfig
        f.ShowDialog()
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        GridEX1.Find(GridEX1.RootTable.Columns("nroMovil"), Janus.Windows.GridEX.ConditionOperator.Equal, txtMovil1.Value, 0, 1)
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'If segundos <= 300 Then
        '    NOW_GLOBAL = NOW_GLOBAL.AddSeconds(1)
        '    segundos += 1
        'Else
        '    NOW_GLOBAL = d.proc_GetDate.ToList(0).Now
        '    segundos = 0
        'End If

        'Try
        '    LBLhora.Text = "  " & NOW_GLOBAL
        'Catch ex As Exception

        'End Try

    End Sub



#End Region


    Private Sub GridEX1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridEX1.KeyDown
        If e.KeyCode = Keys.Delete Then
            If BorrarRegistroZona() Then
                ObtenerVehiculos()
            End If
        End If
    End Sub

    Private Function BorrarRegistroZona() As Boolean
        If MsgBox("Está seguro que desea quitar el Vehículo de la Cola?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

            d.proc_ColaDelete(1, GridEX1.GetValue("idVehiculo"))
            OrdenarVehiculos()

            StartBroadcast(getSettings("frmPrincipal_PORT"), "RefrescarMoviles")
            StartBroadcast(getSettings("Rampa_PORT"), "RefrescarMoviles")
            ObtenerVehiculos()
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub EstadoDeMóvilesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim w As New WebBrowser
        w.WebBrowser1.Url = New Uri(getSettings("ReporteEstadoVehiculos"))
        w.Show()
    End Sub




    Private Sub MovilesEnColaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New WebBrowser
        f.WebBrowser1.Url = New Uri(getSettings("ReporteMovilesEnCola"))
        f.WindowState = FormWindowState.Maximized
        f.Show()
    End Sub

    Private Sub RecaudaciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New WebBrowser
        f.WebBrowser1.Url = New Uri(getSettings("ReporteRecaudacion"))
        f.WindowState = FormWindowState.Maximized
        f.Show()
    End Sub

    Private Sub mnuMovil_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles mnuMovil.ItemClicked
        If e.ClickedItem.Name = "mnuEditarMovil Then" Then
            MsgBox("movil " & GridEX1.GetValue("idVehiculo"))
        End If
    End Sub

    Private Sub ValesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New WebBrowser
        f.WebBrowser1.Url = New Uri(getSettings("ReporteVales"))
        f.WindowState = FormWindowState.Maximized
        f.Show()
    End Sub

    Private Sub cboOrigen_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOrigen.SelectedValueChanged
        Dim listaMoviles = d.proc_ObtenerMovilesPorZona.ToList

        For Each i As proc_ZonasLoadAllResult In cboOrigen.Items

            Dim listaZona = From c In listaMoviles Where c.idZona = cboOrigen.SelectedValue Select c

            Me.txtPosicion.Value = listaZona.Count + 1
        Next
    End Sub

    Private Sub EnviarActualizaciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnviarActualizaciónToolStripMenuItem.Click
        If MsgBox("Esta operación puede demorar varios minutos. Desea continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

        BackgroundWorker1.RunWorkerAsync()



    End Sub

    Private Sub TarifasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New WebBrowser
        f.WebBrowser1.Url = New Uri(getSettings("ReporteTarifas") & "?Zona=1")
        f.WindowState = FormWindowState.Maximized
        f.Show()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        Me.ToolStripStatusLabel1.Text = "Sistema Actualizado"

    End Sub

    Private Sub SalirToolStripMenuItem_Click_1(sender As System.Object, e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        If Salir() Then End
    End Sub
   

    'Private Sub BackgroundWorker1_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
    '    Me.ToolStripStatusLabel1.Text = "Actualizando"

    '    Dim s As New WebService.Service

    '    Dim ListaMoviles = s.ObternerMovilesActualizacion
    '    ''''''''''''''''''''''''''''

    '    Dim idVehiculo As Integer : Dim idPago As Integer
    '    Try
    '        Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    '        For Each movil In ListaMoviles
    '            'actualizar administradores
    '            Try
    '                Dim ListaAdmin = d.proc_AdministradoresLoadByPrimaryKey(movil.cedula).ToList
    '                If ListaAdmin.Count = 0 Then
    '                    d.proc_AdministradoresInsert(movil.nombre, movil.cedula, movil.telefono, (movil.fecha), movil.idUsuario)
    '                Else
    '                    'update admin
    '                    d.proc_AdministradoresUpdate(movil.nombre, movil.cedula, movil.telefono, (movil.fecha), movil.idUsuario)
    '                End If
    '            Catch ex As Exception

    '            End Try

    '            Try
    '                'actualizar vehiculos
    '                Dim listaVehiculos As DataTable = Funciones.proc_ObtenerVehiculoPorNroMovil(movil.NroMovil).Tables(0)

    '                If listaVehiculos.Rows.Count = 0 Then
    '                    'crear movil
    '                    d.proc_VehiculosInsert(idVehiculo, movil.NroMovil, movil.Habilitado, movil.Modelo, movil.Placa, Nothing, Nothing, movil.EstadoSoat, movil.idUsuario, Nothing, _
    '                                           movil.fechaFinSancion, movil.TipoVehiculo, movil.AdminTemp, movil.AdminTemp2, movil.cedula, movil.fechaAfiliacion, _
    '                                            Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    '                Else
    '                    'update movil
    '                    d.proc_VehiculosUpdate(listaVehiculos.Rows(0)("idVehiculo"), movil.NroMovil, movil.Habilitado, movil.Modelo, movil.Placa, Nothing, Nothing, movil.EstadoSoat, movil.idUsuario, Nothing, _
    '                                           movil.fechaFinSancion, movil.TipoVehiculo, movil.AdminTemp, movil.AdminTemp2, movil.cedula, movil.fechaAfiliacion, _
    '                                            Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    '                    idVehiculo = listaVehiculos.Rows(0)("idVehiculo")
    '                End If
    '            Catch ex As Exception


    '            End Try


    '            Try
    '                'actualizar pagosRF
    '                If (movil.FechaPago) IsNot Nothing Then
    '                    Dim ListaPagos As DataTable = Funciones.proc_ObtenerPago(idVehiculo, movil.FechaPago).Tables(0)

    '                    If ListaPagos.Rows.Count = 0 Then
    '                        d.proc_PagosRFInsert(idPago, idVehiculo, movil.FechaPago, movil.FechaInhabilitacion, movil.Importe, movil.Usuario, Nothing, Nothing, Nothing)
    '                    Else
    '                        d.proc_PagosRFUpdate(ListaPagos.Rows(0)("idPago"), idVehiculo, movil.FechaPago, movil.FechaInhabilitacion, movil.Importe, movil.Usuario, Nothing, Nothing, Nothing)
    '                        idPago = ListaPagos.Rows(0)("idPago")
    '                    End If
    '                End If
    '            Catch ex As Exception



    '            End Try



    '        Next
    '        MsgBox("Actualización finalizada")
    '    Catch ex As Exception



    '    Finally
    '        Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")
    '        StartBroadcast(PORT_Principal, "RefrescarMoviles")

    '    End Try
    'End Sub


    Private Sub AdministradoresToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        ' Dim f As New frmAdministradores
        ' AbrirFormulario(f, Me, 0)
    End Sub

    Private Sub BarriosToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles BarriosToolStripMenuItem.Click
        Dim F As New frmBarrios
        'F.MdiParent = Me
        'F.Show()
        AbrirFormulario(F, Me)
    End Sub

    Private Sub ClientesToolStripMenuItem_Click_1(sender As System.Object, e As System.EventArgs) Handles ClientesToolStripMenuItem.Click
        Dim f As New frmClientes
        AbrirFormulario(f, Me, False)
    End Sub

    Private Sub PersonasToolStripMenuItem_Click_1(sender As System.Object, e As System.EventArgs) Handles PersonasToolStripMenuItem.Click
        Dim f As New frmPersonas
        AbrirFormulario(f, Me, False)
    End Sub

    Private Sub TarifasToolStripMenuItem_Click_1(sender As System.Object, e As System.EventArgs) Handles TarifasToolStripMenuItem.Click
        Dim f As New frmTarifas
        'f.MdiParent = Me
        'f.Show()
        AbrirFormulario(f, Me)
    End Sub

    Private Sub UsuariosToolStripMenuItem_Click_1(sender As System.Object, e As System.EventArgs) Handles UsuariosToolStripMenuItem.Click
        Dim d As New frmUsuarios
        AbrirFormulario(d, Me, 0)
    End Sub

    Private Sub ValesToolStripMenuItem_Click_1(sender As System.Object, e As System.EventArgs) Handles ValesToolStripMenuItem.Click
        Dim f As New frmVales
        'f.MdiParent = Me
        'f.Show()
        AbrirFormulario(f, Me)
    End Sub

    Private Sub VehiculosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles VehiculosToolStripMenuItem.Click
        Dim f As New frmMoviles
        'f.MdiParent = Me
        'f.Show()
        AbrirFormulario(f, Me)
    End Sub

    Private Sub PicoYPlacaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PicoYPlacaToolStripMenuItem.Click
        Dim f As New frmPico_Placa
        'f.MdiParent = Me
        'f.Show()
        AbrirFormulario(f, Me)
    End Sub

    'Private Sub RecaudacionesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TransaccionesToolStripMenuItem.Click
    '    Try
    '        Dim f As New frmRecaudacion
    '        ' f.ObtenerIdVehiculo(d.proc_VehiculosLoadAll.ToList.First.idVehiculo)
    '        f.AbrirFrmRecaudacion(Nothing)
    '        'f.ver()
    '        f.ShowDialog()
    '    Catch ex As Exception

    '    End Try

    'End Sub

    Private Sub TransaccionesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TransaccionesToolStripMenuItem.Click
        Try
            Dim f As New frmTransaccion
            'f.MdiParent = Me
            'f.Show()
            AbrirFormulario(f, Me)
        Catch ex As Exception

        End Try
    End Sub

   
    Private Sub RecaudaciónDiariaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RecaudaciónDiariaToolStripMenuItem.Click
        Dim u As New Uri(getSettings("ReporteRecaudacion"))
        Dim Browser As New WebBrowser
        Browser.WebBrowser1.Url = u
        Browser.Show()
    End Sub
End Class
