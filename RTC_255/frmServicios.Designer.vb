﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmServicios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnRF = New System.Windows.Forms.Button()
        Me.btnBarrio = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.txtTarifa = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtMovil2 = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.btnServicio = New System.Windows.Forms.Button()
        Me.txtMovil1 = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtObs = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboBarrio = New System.Windows.Forms.ComboBox()
        Me.txtRef = New System.Windows.Forms.TextBox()
        Me.lblRef = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.btnCliente = New System.Windows.Forms.Button()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnRF)
        Me.GroupBox1.Controls.Add(Me.btnBarrio)
        Me.GroupBox1.Controls.Add(Me.btnSalir)
        Me.GroupBox1.Controls.Add(Me.txtTarifa)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtMovil2)
        Me.GroupBox1.Controls.Add(Me.btnServicio)
        Me.GroupBox1.Controls.Add(Me.txtMovil1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.dtpFecha)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtObs)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtDireccion)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cboBarrio)
        Me.GroupBox1.Controls.Add(Me.txtRef)
        Me.GroupBox1.Controls.Add(Me.lblRef)
        Me.GroupBox1.Controls.Add(Me.txtNombre)
        Me.GroupBox1.Controls.Add(Me.lblNombre)
        Me.GroupBox1.Controls.Add(Me.btnCliente)
        Me.GroupBox1.Controls.Add(Me.txtTelefono)
        Me.GroupBox1.Controls.Add(Me.lblTelefono)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(496, 319)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Cliente"
        '
        'btnRF
        '
        Me.btnRF.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnRF.Location = New System.Drawing.Point(374, 27)
        Me.btnRF.Name = "btnRF"
        Me.btnRF.Size = New System.Drawing.Size(97, 23)
        Me.btnRF.TabIndex = 32
        Me.btnRF.Text = "Enviar a RF"
        Me.btnRF.UseVisualStyleBackColor = True
        '
        'btnBarrio
        '
        Me.btnBarrio.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnBarrio.Location = New System.Drawing.Point(475, 103)
        Me.btnBarrio.Name = "btnBarrio"
        Me.btnBarrio.Size = New System.Drawing.Size(21, 21)
        Me.btnBarrio.TabIndex = 7
        Me.btnBarrio.Text = "..."
        Me.btnBarrio.UseVisualStyleBackColor = True
        Me.btnBarrio.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(377, 277)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(97, 23)
        Me.btnSalir.TabIndex = 31
        Me.btnSalir.Text = "Cer&rar"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'txtTarifa
        '
        Me.txtTarifa.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtTarifa.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtTarifa.Enabled = False
        Me.txtTarifa.Location = New System.Drawing.Point(270, 184)
        Me.txtTarifa.Name = "txtTarifa"
        Me.txtTarifa.Size = New System.Drawing.Size(78, 20)
        Me.txtTarifa.TabIndex = 21
        Me.txtTarifa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtTarifa.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Enabled = False
        Me.Label7.Location = New System.Drawing.Point(227, 186)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 13)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "&Tarifa:"
        '
        'txtMovil2
        '
        Me.txtMovil2.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtMovil2.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtMovil2.Location = New System.Drawing.Point(173, 277)
        Me.txtMovil2.Name = "txtMovil2"
        Me.txtMovil2.Size = New System.Drawing.Size(49, 20)
        Me.txtMovil2.TabIndex = 27
        Me.txtMovil2.Text = "0"
        Me.txtMovil2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtMovil2.Value = CType(0, Short)
        Me.txtMovil2.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16
        '
        'btnServicio
        '
        Me.btnServicio.Location = New System.Drawing.Point(270, 277)
        Me.btnServicio.Name = "btnServicio"
        Me.btnServicio.Size = New System.Drawing.Size(97, 23)
        Me.btnServicio.TabIndex = 28
        Me.btnServicio.Text = "A&signar Servicio"
        Me.btnServicio.UseVisualStyleBackColor = True
        '
        'txtMovil1
        '
        Me.txtMovil1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtMovil1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtMovil1.Location = New System.Drawing.Point(68, 277)
        Me.txtMovil1.Name = "txtMovil1"
        Me.txtMovil1.Size = New System.Drawing.Size(49, 20)
        Me.txtMovil1.TabIndex = 25
        Me.txtMovil1.Text = "0"
        Me.txtMovil1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtMovil1.Value = CType(0, Short)
        Me.txtMovil1.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 282)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 13)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "&Movil 1:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(123, 282)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Mo&vil 2:"
        '
        'dtpFecha
        '
        Me.dtpFecha.CustomFormat = "dd/MM/yyyy hh:mm"
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFecha.Location = New System.Drawing.Point(68, 184)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(139, 20)
        Me.dtpFecha.TabIndex = 19
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 184)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "&Fecha:"
        '
        'txtObs
        '
        Me.txtObs.Location = New System.Drawing.Point(68, 215)
        Me.txtObs.Multiline = True
        Me.txtObs.Name = "txtObs"
        Me.txtObs.Size = New System.Drawing.Size(406, 48)
        Me.txtObs.TabIndex = 23
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 215)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "&Obs:"
        '
        'txtDireccion
        '
        Me.txtDireccion.Location = New System.Drawing.Point(68, 129)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(406, 20)
        Me.txtDireccion.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 132)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "&Dirección:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 105)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "&Barrio:"
        '
        'cboBarrio
        '
        Me.cboBarrio.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBarrio.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBarrio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBarrio.FormattingEnabled = True
        Me.cboBarrio.Location = New System.Drawing.Point(68, 102)
        Me.cboBarrio.Name = "cboBarrio"
        Me.cboBarrio.Size = New System.Drawing.Size(403, 21)
        Me.cboBarrio.TabIndex = 6
        '
        'txtRef
        '
        Me.txtRef.Location = New System.Drawing.Point(68, 155)
        Me.txtRef.Name = "txtRef"
        Me.txtRef.Size = New System.Drawing.Size(406, 20)
        Me.txtRef.TabIndex = 10
        '
        'lblRef
        '
        Me.lblRef.AutoSize = True
        Me.lblRef.Location = New System.Drawing.Point(15, 158)
        Me.lblRef.Name = "lblRef"
        Me.lblRef.Size = New System.Drawing.Size(27, 13)
        Me.lblRef.TabIndex = 9
        Me.lblRef.Text = "&Ref:"
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(68, 76)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(406, 20)
        Me.txtNombre.TabIndex = 4
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(15, 79)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(47, 13)
        Me.lblNombre.TabIndex = 3
        Me.lblNombre.Text = "&Nombre:"
        '
        'btnCliente
        '
        Me.btnCliente.Location = New System.Drawing.Point(194, 25)
        Me.btnCliente.Name = "btnCliente"
        Me.btnCliente.Size = New System.Drawing.Size(57, 23)
        Me.btnCliente.TabIndex = 2
        Me.btnCliente.Text = "B&uscar"
        Me.btnCliente.UseVisualStyleBackColor = True
        '
        'txtTelefono
        '
        Me.txtTelefono.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtTelefono.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtTelefono.Location = New System.Drawing.Point(68, 27)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(120, 20)
        Me.txtTelefono.TabIndex = 1
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(15, 30)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(25, 13)
        Me.lblTelefono.TabIndex = 0
        Me.lblTelefono.Text = "&Tel:"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'frmServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnSalir
        Me.ClientSize = New System.Drawing.Size(522, 342)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Servicios"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRef As System.Windows.Forms.TextBox
    Friend WithEvents lblRef As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents btnCliente As System.Windows.Forms.Button
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboBarrio As System.Windows.Forms.ComboBox
    Friend WithEvents txtObs As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMovil2 As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents btnServicio As System.Windows.Forms.Button
    Friend WithEvents txtMovil1 As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtTarifa As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnBarrio As System.Windows.Forms.Button
    Friend WithEvents btnRF As System.Windows.Forms.Button
End Class
