﻿
Imports Common
Public Class frmMovimientos

#Region "Declaraciones"
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim _idPersona
#End Region


#Region "Eventos"
    Private Sub cmdActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActualizar.Click
        ObtenerMovimientos()
    End Sub

    Private Sub SetDatepickers()
        dtpDesde.Value = New Date(Now.Year, Now.Month, 1)
        dtpHasta.Value = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, dtpDesde.Value))
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MsgBox("Averiguar Comprobante")
    End Sub

    Private Sub frmMovimientos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
    End Sub

#End Region


#Region "Metodos"
    Public Sub AbrirForm(ByVal idPersona As Integer)
        SetDatepickers()
        _idPersona = idPersona
        ObtenerMovimientos()
        Me.ShowDialog()
    End Sub

    Private Sub ObtenerMovimientos()


        GridEX1.SetDataBinding(d.proc_ObtenerMovimientos(_idPersona, dtpDesde.Value, dtpHasta.Value).ToList, "")
        GridEX1.RetrieveStructure()
        GridEX1.RootTable.Columns("idMovimiento").Caption = "Movimiento"
        GridEX1.RootTable.Columns("idPersona").Visible = False
        GridEX1.RootTable.Columns("idTipoMovimiento").Visible = False
        GridEX1.RootTable.Columns("idRecaudacion").Visible = False
        GridEX1.RootTable.Columns("idPrestamo").Caption = "Nro. Préstamo"
        GridEX1.RootTable.Columns("importeRecaudacion").Caption = "Valor Pago"
        GridEX1.RootTable.Columns("importeAhorro").Caption = "Ahorro"
        GridEX1.RootTable.Columns("importeAhorroAdicional").Caption = "Ahorro Adicional"
        GridEX1.RootTable.Columns("balanceAhorro").Caption = "Total Ahorro"
        GridEX1.RootTable.Columns("balanceAhorroAdicional").Caption = "Total Ahorro Adic."
        GridEX1.RootTable.Columns("balanceDeuda").Caption = "Total Deuda"
        GridEX1.RootTable.Columns("Cedula").Caption = "Cédula"
        GridEX1.RootTable.Columns("importeRecaudacion").FormatString = "#,#"
        GridEX1.RootTable.Columns("importeAhorro").FormatString = "#,#"
        GridEX1.RootTable.Columns("importeAhorroAdicional").FormatString = "#,#"
        GridEX1.RootTable.Columns("balanceDeuda").FormatString = "#,#"
        GridEX1.RootTable.Columns("balanceAhorro").FormatString = "#,#"
        GridEX1.RootTable.Columns("balanceAhorroAdicional").FormatString = "#,#"

    End Sub
#End Region





End Class