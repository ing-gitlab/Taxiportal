﻿Public Class frmCreditos
    Public Class myButt
        Inherits System.Windows.Forms.Button
    End Class


#Region "Declaraciones"
    Private dateButts() As myButt
    Dim LocalValues(6) As String 'to store the local day names
#End Region


#Region "Eventos"


    Private Sub Creditos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TextBox2.Text = Now.Year
        monthSelect.Value = Now.Month
        ReDim dateButts(41) '41 total flat-buttons created at run time
        Dim i, lblTop, lblLeft, datesTotalRow, datesTotalCol As Integer
        lblTop = 413
        lblLeft = 45
        datesTotalRow = 1
        datesTotalCol = 1

        'First rename the day-name labels according to the local-regional names
        'each name gets stored in a variable used later by the Checkdates()
        Dim DNdate As Date
        Dim DayName As String

        DNdate = "#1/6/2008#" '(the first of june of 2008 is sunday)
        DayName = Format(DNdate, "ddd") 'in USA DayName should have the value "sun" now
        Label1.Text = DayName.ToUpper
        LocalValues(0) = DayName

        DNdate = "#2/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label2.Text = DayName.ToUpper
        LocalValues(1) = DayName

        DNdate = "#3/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label3.Text = DayName.ToUpper
        LocalValues(2) = DayName
        DNdate = "#4/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label4.Text = DayName.ToUpper
        LocalValues(3) = DayName
        DNdate = "#5/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label5.Text = DayName.ToUpper
        LocalValues(4) = DayName
        DNdate = "#6/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label6.Text = DayName.ToUpper
        LocalValues(5) = DayName
        DNdate = "#7/6/2008#"
        DayName = Format(DNdate, "ddd")
        Label7.Text = DayName.ToUpper
        LocalValues(6) = DayName

        'create all the buttons at run time
        For i = 0 To 41
            dateButts(i) = New myButt
            dateButts(i).Text = ""
            dateButts(i).Font = New Font(Me.Font, FontStyle.Bold)
            dateButts(i).Top = lblTop
            dateButts(i).Left = lblLeft
            dateButts(i).Width = 36
            'add handler to use events with the run time created buttons
            AddHandler dateButts(i).Click, AddressOf anybutt_click
            dateButts(i).Cursor = Cursors.Hand
            dateButts(i).FlatStyle = FlatStyle.Flat
            dateButts(i).FlatAppearance.MouseOverBackColor = Color.AliceBlue
            dateButts(i).FlatAppearance.MouseDownBackColor = Color.LightBlue
            dateButts(i).Tag = i 'this just for test
            dateButts(i).Visible = False

            'select: if the day is located in the sunday column make the number red
            Select Case i
                Case -1, 0
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 6, 7
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 13, 14
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 20, 21
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 27, 28
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
                Case 34, 35
                    dateButts(i).ForeColor = Color.Purple
                    dateButts(i).FlatAppearance.BorderColor = Color.Black
            End Select

            Me.Controls.Add(dateButts(i))

            datesTotalRow = datesTotalRow + 1
            datesTotalCol = datesTotalCol + 1
            lblLeft = lblLeft + 40
            If datesTotalRow > 7 Then
                lblTop = lblTop + 27
                datesTotalRow = 1
            End If
            If datesTotalCol > 7 Then
                lblLeft = 45
                datesTotalCol = 1
            End If

        Next

        CheckDates(monthSelect.Value, TextBox2.Text)
        Me.GroupBox1.SendToBack()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            CheckDates(monthSelect.Value, TextBox2.Text)
        Catch ex As Exception
            MsgBox("Make sure you only write the year number." & vbLf & ex.Message, MsgBoxStyle.Exclamation, "Error")
        End Try

    End Sub

    'this sub is called anytime a run time button is pressed, sender is the respective button  AQUI CUALQUIER BOTON
    Private Sub anybutt_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox(sender.text, MsgBoxStyle.Information, "info")
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If monthSelect.Value = 1 Then
            monthSelect.Value = 12
            TextBox2.Text = TextBox2.Text - 1
            Button1_Click(Me, e)
            Exit Sub
        End If
        monthSelect.Value = monthSelect.Value - 1
        Button1_Click(Me, e)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If monthSelect.Value = 12 Then
            monthSelect.Value = 1
            TextBox2.Text = TextBox2.Text + 1
            Button1_Click(Me, e)
            Exit Sub
        End If
        monthSelect.Value = monthSelect.Value + 1
        Button1_Click(Me, e)
    End Sub


#End Region



#Region "Metodos"

    Private Sub CheckDates(ByVal strCurrentMonth As Integer, ByVal strCurrentYear As Integer)
        Dim FirstDay, MonthName As String
        Dim MyDate As Date
        Dim i, TotalDays, StartingDateLabel As Integer

        'make all buttons invisible until changes are done
        For i = 0 To 41
            dateButts(i).Visible = False
        Next

        MyDate = "#1/" & strCurrentMonth & "/" & strCurrentYear & "#"
        MonthName = Format(MyDate, "MMMM")
        'label10 to show the month name in local/regional name
        Label10.Text = MonthName.ToUpper

        TotalDays = System.DateTime.DaysInMonth(strCurrentYear, strCurrentMonth)

        FirstDay = Format(MyDate, "ddd")

        'Here we use the local values stored in Form1_load()
        'and check which is the first day of the month 
        StartingDateLabel = Nothing
        If FirstDay = LocalValues(0) Then     'if sunday
            StartingDateLabel = "0"
        ElseIf FirstDay = LocalValues(1) Then 'if monday
            StartingDateLabel = "1"
        ElseIf FirstDay = LocalValues(2) Then 'if tuesday
            StartingDateLabel = "2"
        ElseIf FirstDay = LocalValues(3) Then 'if wednesday
            StartingDateLabel = "3"
        ElseIf FirstDay = LocalValues(4) Then 'if thursday
            StartingDateLabel = "4"
        ElseIf FirstDay = LocalValues(5) Then 'if friday
            StartingDateLabel = "5"
        ElseIf FirstDay = LocalValues(6) Then 'if saturday
            StartingDateLabel = "6"
        End If

        'for all buttons
        For i = 0 To 41
            dateButts(i).Enabled = False
            dateButts(i).Text = ""
            dateButts(i).BackColor = Color.LightGray 'bcolor for the non-days
        Next

        'only for buttons representing days
        For i = 1 To TotalDays
            dateButts(StartingDateLabel).Enabled = True

            'here you could check with a database and replace the extra blank spaces 
            'e.g. with an asterisk to show an appointment or change the backcolor
            dateButts(StartingDateLabel).Text = "  " & i.ToString
            dateButts(StartingDateLabel).BackColor = Color.White
            StartingDateLabel += 1
        Next

        'after all the changes make them visibe
        For i = 0 To 41
            dateButts(i).Visible = True
        Next

    End Sub
#End Region



End Class