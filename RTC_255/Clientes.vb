﻿Imports Common

Public Class Clientes
    Dim esNuevo As Boolean = False

    Private Sub Clientes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim d As New DataClasses1DataContext
        GridEX1.SetDataBinding(d.proc_ClientesLoadAll, "")
        GridEX1.RetrieveStructure()

        ObtenerBarrios()
        centrarGrilla()
    End Sub

    Private Sub GridEX1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.Click
        Me.btnModificar.Visible = True
    End Sub
    Private Sub GridEX1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.SelectionChanged
        Try
            If Me.GridEX1.GetRow.RowType = Janus.Windows.GridEX.RowType.FilterRow Or Me.GridEX1.RowCount = 0 Then Exit Sub

            Me.txtTelefono.Text = GridEX1.GetValue("Telefono")
            GridEX1.Tables(0).Columns("telefono").Caption = "Teléfono"

            Me.txtNombre.Text = GridEX1.GetValue("Nombre")
            GridEX1.Tables(0).Columns("nombre").Caption = "Nombre"

            Me.txtDirección.Text = GridEX1.GetValue("direccion")
            GridEX1.Tables(0).Columns("direccion").Caption = "Dirección"

            Me.txtReferencia.Text = GridEX1.GetValue("Referencia")
            GridEX1.Tables(0).Columns("referencia").Caption = "Referencia"

            Me.cboBarrio.SelectedValue = GridEX1.GetValue("idBarrio")

            Me.txtObservaciones.Text = GridEX1.GetValue("Observaciones")
            GridEX1.Tables(0).Columns("observaciones").Caption = "Observaciones"

            Me.chkEstado.Checked = GridEX1.GetValue("estado")
            GridEX1.Tables(0).Columns("estado").Caption = "Estado"
            Me.iniciarEdicion(False)
        Catch ex As System.Exception
        End Try
        For Each centrar As Janus.Windows.GridEX.GridEXColumn In GridEX1.Tables(0).Columns
            centrar.HeaderAlignment = Janus.Windows.GridEX.TextAlignment.Center
        Next
        centrarGrilla()

    End Sub
    Private Sub ObtenerBarrios()
        Dim d As New DataClasses1DataContext
        Dim ListaBarrios = d.proc_BarriosLoadAll.ToList()
        Me.cboBarrio.DisplayMember = "descripcion"
        Me.cboBarrio.ValueMember = "idBarrio"
        Me.cboBarrio.DataSource = ListaBarrios


    End Sub
    'Private Function validaciones() As Boolean
    '    If ValidarBarrio() AndAlso ValidarDireccion() AndAlso ValidarNombre() AndAlso ValidarReferencia() AndAlso ValidarTelefono() Then
    '        Return False
    '    End If
    '    Return True
    'End Function
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        esNuevo = True
        iniciarEdicion(True)

        Me.txtTelefono.Text = ""
        Me.txtReferencia.Text = ""
        Me.txtNombre.Text = ""
        Me.txtDirección.Text = ""
        Me.txtObservaciones.Text = ""

        Me.txtNombre.Focus()
        Me.chkEstado.Checked = True
        Me.btnModificar.Visible = False
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        esNuevo = False

        iniciarEdicion(True)

        Me.txtNombre.Focus()
        Me.btnModificar.Visible = False
    End Sub

    Private Function ValidarBarrio() As Boolean
        If Me.cboBarrio.SelectedIndex < 0 Then
            MsgBox("Debe Seleccionar Un Barrio", MsgBoxStyle.Critical, "Error")
            Me.cboBarrio.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidarDireccion()
        If Me.txtDirección.Text = "" Then
            MsgBox("Debe Ingresar La Dirección", MsgBoxStyle.Critical, "Error")
            Me.txtDirección.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidarNombre()
        If Me.txtNombre.Text = "" Then
            MsgBox("Debe Ingresar El Nombre", MsgBoxStyle.Critical, "Error")
            Me.txtNombre.Focus()
            Me.txtNombre.ReadOnly = False
            Return False
        End If

        Return True
    End Function

    Private Function ValidarReferencia()
        If Me.txtReferencia.Text = "" Then
            MsgBox(" Ingresar Una Referencia", MsgBoxStyle.Critical, "Error")
            Me.txtReferencia.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidarTelefono()
        If Me.txtTelefono.Text = "" Then
            MsgBox("Ingrear Numero De Telefono", MsgBoxStyle.Critical, "Error")
            Me.txtTelefono.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim d As New DataClasses1DataContext
        Dim idCliente As Integer
        'Dim Guardado As Boolean = False
        Dim Posicion As Integer
        'Ingresar Clientes
        If esNuevo Then

            If validar() Then
                Dim nuevo = d.proc_ClientesInsert(idCliente, txtNombre.Text, txtDirección.Text, txtReferencia.Text, txtTelefono.Text, Me.cboBarrio.SelectedValue, Me.chkEstado.Checked, txtObservaciones.Text)
                'Guardado = True

            End If
        Else
            'modificar Clientes
            Posicion = Me.GridEX1.GetRow().Position
            d.proc_ClientesUpdate(GridEX1.GetValue("idCliente"), txtNombre.Text, txtDirección.Text, txtReferencia.Text, txtTelefono.Text, Me.cboBarrio.SelectedValue, Me.chkEstado.Checked, txtObservaciones.Text)
            'Guardado = True
        End If
        centrarGrilla()
        iniciarEdicion(False)
        'If Guardado Then
        '    iniciarEdicion(False)

        'End If
        If esNuevo Then
            Posicion = Me.GridEX1.RowCount - 1
        End If

        Me.GridEX1.MoveTo(Posicion)

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.iniciarEdicion(False)
    End Sub

    Private Sub iniciarEdicion(ByVal esInicio As Boolean)
        Me.GridEX1.Enabled = Not esInicio

        Me.btnNuevo.Visible = Not esInicio
        Me.btnModificar.Visible = Not esInicio

        Me.txtTelefono.ReadOnly = Not esInicio
        Me.txtNombre.ReadOnly = Not esInicio
        Me.txtReferencia.ReadOnly = Not esInicio
        Me.txtObservaciones.ReadOnly = Not esInicio
        Me.txtDirección.ReadOnly = Not esInicio
        Me.chkEstado.Enabled = esInicio
        Me.cboBarrio.Enabled = esInicio
        Me.btnAceptar.Visible = esInicio
        Me.btnCancelar.Visible = esInicio

    End Sub
    Private Sub centrarGrilla()


        GridEX1.Tables(0).Columns("Telefono").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("Nombre").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("Observaciones").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("idBarrio").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("Referencia").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("Direccion").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("Estado").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("idCliente").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center

    End Sub
    Private Function validar()
        If Not ValidarNombre() Then
            Return False
        End If
        If Not ValidarTelefono() Then
            Return False
        End If
        If Not ValidarDireccion() Then
            Return False
        End If
        If Not ValidarReferencia() Then
            Return False
        End If

        Return True
    End Function
End Class