﻿Imports Common

Public Class frmServicioRapido

    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim RefrescarGrid As Boolean = True
    Dim esNuevo As Boolean = True
    Dim mVehiculo As proc_VehiculosLoadByPrimaryKeyResult
    Dim _idServicio As Integer = 0
    Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")
    Dim PORT_Resumen As Integer = getSettings("frmResumen_PORT")
    Dim _DataTableEncabezado As DataTable
    Dim _DataTableDetalle As DataTable

    Private Sub frmServicioRapido_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        chkImprimir.Checked = CONFIG_GLOBAL.imprimirTicketeAutomatico

    End Sub

    Private Sub ImprimirTickete(ByVal ID As Integer)
        Dim ids As New clsImpresionDataSet
        Dim ps As New System.Drawing.Printing.PageSettings
        Dim size As New System.Drawing.Printing.PaperSize

        With ids
            .TipoImpresora = clsImpresionDataSet.TipoImpresora_t.Grafica

            size = New System.Drawing.Printing.PaperSize("76mm Roll Paper", 76, 297)

            ps.PaperSize = size

            .ConfiguracionPagina = ps
            .NombreDocumentoImpresion = "Servicio"
            .PathArchivoXML = My.Application.Info.DirectoryPath & "\Tickete.xml"
            .DataSet = DataSetImpresion(ID)

            .Imprimir()

        End With

        'Hago esto porque si se quiere reimprimir, arroja un error al querer agregar nuevamente los DataTables al DataSet:
        ids.DataSet.Tables.Clear()
        ids = Nothing
    End Sub

    Private Function DataSetImpresion(ByVal id As Integer) As DataSet
        Try
            Dim ret As New DataSet
            Dim dr As DataRow
            Dim ser = d.proc_ServiciosLoadByPrimaryKey(id).ToList.First
            Dim barrio = d.proc_BarriosLoadByPrimaryKey(ser.idBarrioDestino).ToList.First
            Dim movil = d.proc_VehiculosLoadByPrimaryKey(ser.idVehiculo1).ToList.First
            _DataTableEncabezado = New DataTable
            _DataTableDetalle = New DataTable
            Me._DataTableEncabezado.Rows.Clear()
            Me._DataTableEncabezado.Columns.Add("Titulo")
            Me._DataTableEncabezado.Columns.Add("PBX")
            Me._DataTableEncabezado.Columns.Add("Cel")
            _DataTableEncabezado.Columns.Add("Destino")
            Me._DataTableEncabezado.Columns.Add("Tarifa")
            Me._DataTableEncabezado.Columns.Add("Movil")
            Me._DataTableEncabezado.Columns.Add("Fecha")

            dr = Me._DataTableEncabezado.NewRow

            dr.Item("Titulo") = String.Format(getSettings("Titulo"))
            dr.Item("PBX") = getSettings("PBX")
            dr.Item("Cel") = getSettings("Cel")
            dr.Item("Destino") = "Destino: " & barrio.descripcion.ToString
            dr.Item("Tarifa") = String.Format("Tarifa: {0}", ser.tarifa) & vbCrLf & "Con carga 3.000 - 5.000"
            dr.Item("Movil") = "Móvil: " & movil.NroMovil.ToString
            dr.Item("Fecha") = "Fecha/Hora: " & ser.fechaServicio.ToString


            Me._DataTableEncabezado.Rows.Add(dr)

            ret.Tables.Add(Me._DataTableEncabezado)
            ret.Tables.Add(Me._DataTableDetalle)

            Return ret
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Public Function AbrirFormConVehiculo(ByVal idVehiculo As Integer) As Boolean
        esNuevo = True
        ObtenerBarrios()
        ObtenerOrigen()
        ObtenerTarifa()
        cboBarrio.Focus()
        mVehiculo = d.proc_VehiculosLoadByPrimaryKey(idVehiculo).ToList.First
        txtMovil1.Value = mVehiculo.NroMovil
        Show()
        Return True
    End Function

    Public Function EditarServicio(ByVal idServicio As Integer) As Boolean
        esNuevo = False

        Dim servicio = d.proc_ServiciosLoadByPrimaryKey(idServicio).ToList.First
        Dim movil = d.proc_VehiculosLoadByPrimaryKey(servicio.idVehiculo1).ToList.First
        ObtenerBarrios()
        ObtenerOrigen()
        ObtenerTarifa()
        cboBarrio.Focus()
        _idServicio = servicio.idServicio

        cboOrigen.SelectedValue = CInt(CONFIG_GLOBAL.zonaDefault)
        cboBarrio.SelectedValue = servicio.idBarrioDestino
        txtTarifa.Value = servicio.tarifa
        Me.txtMovil1.Value = movil.NroMovil
        txtObs.Text = servicio.ObservacionesCliente

        Me.Show()
        Return True
    End Function

    Private Sub ObtenerBarrios()
        Dim ListaBarrios = d.proc_BarriosLoadAll.ToList
        Me.cboBarrio.DisplayMember = "descripcion"
        Me.cboBarrio.ValueMember = "idBarrio"
        Me.cboBarrio.DataSource = ListaBarrios
        Me.cboBarrio.SelectedValue = CInt(CONFIG_GLOBAL.zonaDefault)

    End Sub

    Private Sub ObtenerOrigen()
        Dim ListaZonas = d.proc_ZonasLoadAll.ToList
        Me.cboOrigen.DisplayMember = "descripcion"
        Me.cboOrigen.ValueMember = "idZona"
        Me.cboOrigen.DataSource = ListaZonas
        Me.cboOrigen.SelectedValue = CInt(CONFIG_GLOBAL.zonaDefault)
    End Sub

    Private Sub cboBarrio_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBarrio.GotFocus
        Me.AcceptButton = Nothing
    End Sub

    Private Sub cboBarrio_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBarrio.SelectedValueChanged
        ObtenerTarifa()
    End Sub

    Private Sub ObtenerTarifa()
        Dim ListaTarifa = d.proc_ObtenerTarifa(cboOrigen.SelectedValue, cboBarrio.SelectedValue).ToList
        If ListaTarifa.Count = 0 Then
            txtTarifa.Text = ""
        Else
            If (NOW_GLOBAL.Hour >= CONFIG_GLOBAL.horaInicioRecargoNocturno Or NOW_GLOBAL.Hour < CONFIG_GLOBAL.horaFinRecargoNocturno) And esNuevo Then
                txtTarifa.Value = ListaTarifa(0).tarifa + CONFIG_GLOBAL.ImporteRecargoNocturno
                lblRecargoNocturno.Visible = True
            Else
                txtTarifa.Value = ListaTarifa(0).tarifa
                lblRecargoNocturno.Visible = False
            End If


        End If
    End Sub

    Private Sub btnServicio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnServicio.Click
        AsignarServicio()

    End Sub

    Private Sub AsignarServicio()

        Try
            Dim listaMovil = d.proc_ObtenerVehiculoPorNroMovil(Convert.ToInt16(txtMovil1.Value)).ToList
            If listaMovil.Count = 0 Then
                MsgBox("Número de móvil inexistente. Corríjalo e intente nuevamente.")
                txtMovil1.Focus()
                txtMovil1.SelectAll()
                Exit Sub
            End If
            'Dim fechaInhab = d.fnObtenerFechaInhabilitacion(listaMovil(0).idVehiculo).Value
            'If fechaInhab > NOW_GLOBAL Then
            '    MsgBox("Móvil inhabilitado. Seleccione otro Móvil")
            '    txtMovil1.Focus()
            '    Exit Sub
            'End If
            If Not Validar() Then Exit Sub
            Dim id, barrio As Integer

            If cboBarrio.SelectedIndex < 0 Then
                d.proc_BarriosInsert(barrio, cboBarrio.Text, 0)
                d.proc_TarifasInsert(Nothing, cboOrigen.SelectedValue, barrio, NOW_GLOBAL, txtTarifa.Value, USER_GLOBAL)
            Else
                barrio = cboBarrio.SelectedValue
            End If

            If esNuevo Then
                d.proc_ServiciosInsert(id, listaMovil(0).idVehiculo, listaMovil(0).idVehiculo, _
                                       Nothing, NOW_GLOBAL, NOW_GLOBAL, NOW_GLOBAL, Nothing, Nothing, "", barrio, _
                                       Nothing, txtTarifa.Value, Nothing, txtObs.Text, EstadosServicios.Terminado, _
                                        USER_GLOBAL, TipoServicios.Rapido, CONFIG_GLOBAL.radioFrecuencia)
            Else
                Dim servicio = d.proc_ServiciosLoadByPrimaryKey(_idServicio).ToList(0)
                id = _idServicio
                d.proc_ServiciosUpdate(_idServicio, listaMovil(0).idVehiculo, listaMovil(0).idVehiculo, _
                                       Nothing, servicio.fechaCreacion, servicio.fechaServicio, servicio.fechaHR, _
                                       servicio.fechaFinalizacion, Nothing, Nothing, barrio, Nothing, txtTarifa.Value, Nothing, _
                                        txtObs.Text, servicio.idEstado, USER_GLOBAL, servicio.tipoServicio, CONFIG_GLOBAL.radioFrecuencia)
            End If
            Try
                d.proc_ColaDelete(1, listaMovil(0).idVehiculo)
                OrdenarVehiculos()
            Catch ex As Exception
                d.proc_LogInsert(USER_GLOBAL, "movil: " & listaMovil(0).idVehiculo, NOW_GLOBAL, "Error borrando movil de cola")
            End Try

            If chkImprimir.Checked Then ImprimirTickete(id)
            StartBroadcast(PORT_Principal, "RefrescarMoviles")
            StartBroadcast(PORT_Resumen, "1")

            StartBroadcast(getSettings("Rampa_PORT"), "Refresca")
            System.Threading.Thread.Sleep(1000)
            StartBroadcast(getSettings("Rampa_PORT"), "Servicio" & id)

            RefrescarGrid = True
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Function Validar() As Boolean
        If cboOrigen.SelectedValue < 0 Or cboOrigen.SelectedValue Is Nothing Then
            MsgBox("Seleccione un Origen")
            cboOrigen.Focus()
            Return False
        End If

        'If cboBarrio.SelectedValue < 0 Or cboBarrio.SelectedValue Is Nothing Then
        '    MsgBox("Seleccione un Barrio")
        '    cboBarrio.Focus()
        '    Return False
        'End If

        If txtTarifa.Text.Equals(String.Empty) Or txtTarifa.Text < 0 Then
            MsgBox("Ingrese una Tarifa válida")
            txtTarifa.Focus()
            txtTarifa.SelectAll()
            Return False
        End If

        Return True
    End Function

    Public Function AbrirForm(ByVal pEsNuevo As Boolean) As Boolean
        esNuevo = pEsNuevo
        ObtenerBarrios()
        ObtenerOrigen()
        ObtenerTarifa()
        cboBarrio.Focus()
        Me.Show()
        Return RefrescarGrid
    End Function

    Private Sub OrdenarVehiculos()
        Dim listaMoviles = d.proc_ObtenerMovilesPorZona.ToList

        For Each i As proc_ZonasLoadAllResult In cboOrigen.Items

            Dim listaZona = From c In listaMoviles Where c.idZona = cboOrigen.SelectedValue Select c

            For index As Integer = 0 To listaZona.Count - 1
                d.proc_ColaUpdate(i.idZona, listaZona(index).idVehiculo, index + 1, NOW_GLOBAL, NOW_GLOBAL)

            Next

        Next

    End Sub

    Private Sub cboOrigen_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOrigen.SelectedValueChanged
        ObtenerTarifa()
    End Sub

    Private Sub cboBarrio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBarrio.SelectedIndexChanged

    End Sub

    Private Sub cboBarrio_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBarrio.LostFocus
        Me.AcceptButton = btnServicio
    End Sub

    Private Sub btnBarrio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBarrio.Click
        Dim f As New frmBarrios
        Dim barrio = f.AbrirForm()
        If barrio > 0 Then
            ObtenerBarrios()
            Me.cboBarrio.SelectedValue = barrio
        End If
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

End Class