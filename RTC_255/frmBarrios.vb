﻿Imports Common
Imports System.Data.Linq

Public Class frmBarrios


#Region "Declaraciones"
    Dim DevolverBarrio As Boolean = False
    Dim RetBarrio As Integer
    Dim FueraBarr As Boolean
    Dim _Seleccionar As Boolean = False
    Dim _Barrio As proc_BarriosLoadByPrimaryKeyResult
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
#End Region


#Region "Eventos"

    Private Sub frmBarrios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ObtenerBarrios()
        GridEX1.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic
    End Sub

    Private Sub GridEX1_AddingRecord(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GridEX1.AddingRecord
        Try

            Dim id As Integer
            d.proc_BarriosInsert(id, GridEX1.GetValue("Descripcion"), GridEX1.GetValue("FueraBarranquilla"))

            Dim listaZona = d.proc_ZonasLoadAll.ToList
            For Each z In listaZona
                d.proc_TarifasInsert(z.idZona, id, NOW_GLOBAL, 0, USER_GLOBAL.idUsuario)
            Next
            If DevolverBarrio Then
                RetBarrio = id
                Me.Close()
            Else
                ObtenerBarrios()
                GridEX1.Find(Me.GridEX1.RootTable.Columns("idBarrio"), Janus.Windows.GridEX.ConditionOperator.Equal, id, 1, 1)

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            e.Cancel = True
        End Try
    End Sub

    Private Sub GridEX1_DeletingRecords(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GridEX1.DeletingRecords
        Try
            If MsgBox("Está seguro que desea eliminar el registro?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                e.Cancel = True
                Exit Sub
            End If

            d.proc_BarriosDelete(GridEX1.GetValue("idBarrio"))
            d.proc_LogInsert(NOW_GLOBAL, USER_GLOBAL.idUsuario, "Barrio: " & (GridEX1.GetValue("idBarrio")) & " Desc: " & GridEX1.GetValue("Descripcion"), "Elimina barrio")
        Catch ex As Exception
            If ex.Message.Contains("FK_Tarifas_Barrios") Then
                MsgBox("No puede eliminar el barrio porque tiene una tarifa asociada.")
            Else
                MsgBox(ex.Message)
            End If

            e.Cancel = True
        End Try
    End Sub

    Private Sub GridEX1_RowDoubleClick(ByVal sender As Object, ByVal e As Janus.Windows.GridEX.RowActionEventArgs) Handles GridEX1.RowDoubleClick
        SeleccionarBarrio()
    End Sub


    Private Sub GridEX1_UpdatingRecord(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GridEX1.UpdatingRecord
        Try
            'Dim barrio = d.proc_BarriosLoadByPrimaryKey(GridEX1.GetValue("idBarrio")).ToList.First
            ' MsgBox(GridEX1.GetValue("Descripcion").ToString & " " & Boolean.Parse(GridEX1.GetValue("FueraBarranquilla")))
            d.proc_BarriosUpdate(GridEX1.GetValue("idBarrio"), GridEX1.GetValue("Descripcion"), GridEX1.GetValue("FueraBarranquilla")) ' Boolean.Parse(GridEX1.GetValue("FueraBarranquilla")))
            ' d.proc_LogInsert(NOW_GLOBAL, USER_GLOBAL, "Barrio: " & (GridEX1.GetValue("idBarrio")) & " Desc: " & GridEX1.GetValue("Descripcion"), "Modifica barrio")
        Catch ex As Exception
            MsgBox(ex.Message)
            'e.Cancel = True

        End Try
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()

    End Sub

#End Region


#Region "Metodos"

    Public Function AbrirForm() As Integer
        DevolverBarrio = True
        Me.GridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False
        Me.GridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False
        Me.GridEX1.Focus()
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowDialog()
        Return RetBarrio
    End Function

    Private Sub ObtenerBarrios()
        Me.GridEX1.SetDataBinding(d.proc_BarriosLoadAll.ToList, String.Empty)
        Me.GridEX1.RetrieveStructure()
        GridEX1.RootTable.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.True
        Me.GridEX1.Tables(0).Columns("idBarrio").Visible = False
        Me.GridEX1.Tables(0).Columns("FueraBarranquilla").ColumnType = Janus.Windows.GridEX.ColumnType.CheckBox
        Me.GridEX1.Tables(0).Columns("FueraBarranquilla").EditType = Janus.Windows.GridEX.EditType.CheckBox
        Me.GridEX1.RootTable.Columns("descripcion").Caption = "Descripción"

        Me.GridEX1.ColumnAutoResize = True
    End Sub

    Public Function AbrirParaSeleccionar() As proc_BarriosLoadByPrimaryKeyResult
        _Seleccionar = True
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowDialog()

        Return _Barrio
    End Function

    Private Sub SeleccionarBarrio()
        If _Seleccionar Then
            _Barrio = d.proc_BarriosLoadByPrimaryKey(GridEX1.GetValue("idBarrio")).ToList.First
            Me.Close()
        End If

    End Sub


    'Private Sub GridEX1_EditingCell(sender As Object, e As Janus.Windows.GridEX.EditingCellEventArgs) Handles GridEX1.EditingCell
    '    If e.Column.Caption = "FueraBarranquilla" Then
    '        FueraBarr = GridEX1.GetValue("FueraBarranquilla")
    '    End If
    'End Sub


#End Region













  





End Class