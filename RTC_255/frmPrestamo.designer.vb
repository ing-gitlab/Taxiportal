﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrestamo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrestamo))
        Me.gbPersona = New System.Windows.Forms.GroupBox()
        Me.txtMovil = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.PbPersona = New System.Windows.Forms.PictureBox()
        Me.txtDeuda = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtAhorroAdicional = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.txtAhorroObligatorio = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdActualizar = New Janus.Windows.EditControls.UIButton()
        Me.txtValorSaldado = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCuotasSaldadas = New System.Windows.Forms.NumericUpDown()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtValorCuota = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNroCuotas = New System.Windows.Forms.NumericUpDown()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtValorPrestamo = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtConcepto = New System.Windows.Forms.TextBox()
        Me.btnMovimiento = New System.Windows.Forms.Button()
        Me.cboTipoPrestamo = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.GridEX1 = New Janus.Windows.GridEX.GridEX()
        Me.gbPersona.SuspendLayout()
        CType(Me.PbPersona, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtCuotasSaldadas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNroCuotas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbPersona
        '
        Me.gbPersona.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbPersona.Controls.Add(Me.txtMovil)
        Me.gbPersona.Controls.Add(Me.Label15)
        Me.gbPersona.Controls.Add(Me.PbPersona)
        Me.gbPersona.Controls.Add(Me.txtDeuda)
        Me.gbPersona.Controls.Add(Me.Label12)
        Me.gbPersona.Controls.Add(Me.txtAhorroAdicional)
        Me.gbPersona.Controls.Add(Me.txtAhorroObligatorio)
        Me.gbPersona.Controls.Add(Me.Label13)
        Me.gbPersona.Controls.Add(Me.Label14)
        Me.gbPersona.Location = New System.Drawing.Point(12, 12)
        Me.gbPersona.Name = "gbPersona"
        Me.gbPersona.Size = New System.Drawing.Size(805, 200)
        Me.gbPersona.TabIndex = 0
        Me.gbPersona.TabStop = False
        Me.gbPersona.Text = "Nombre de la Persona"
        '
        'txtMovil
        '
        Me.txtMovil.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtMovil.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtMovil.Enabled = False
        Me.txtMovil.Location = New System.Drawing.Point(512, 54)
        Me.txtMovil.Name = "txtMovil"
        Me.txtMovil.Size = New System.Drawing.Size(48, 20)
        Me.txtMovil.TabIndex = 7
        Me.txtMovil.Text = "0"
        Me.txtMovil.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtMovil.Value = CType(0, Short)
        Me.txtMovil.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(461, 57)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(35, 13)
        Me.Label15.TabIndex = 6
        Me.Label15.Text = "&Móvil:"
        '
        'PbPersona
        '
        Me.PbPersona.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PbPersona.Location = New System.Drawing.Point(25, 27)
        Me.PbPersona.Name = "PbPersona"
        Me.PbPersona.Size = New System.Drawing.Size(120, 160)
        Me.PbPersona.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PbPersona.TabIndex = 93
        Me.PbPersona.TabStop = False
        '
        'txtDeuda
        '
        Me.txtDeuda.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtDeuda.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtDeuda.DecimalDigits = 0
        Me.txtDeuda.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtDeuda.Location = New System.Drawing.Point(512, 94)
        Me.txtDeuda.Name = "txtDeuda"
        Me.txtDeuda.ReadOnly = True
        Me.txtDeuda.Size = New System.Drawing.Size(122, 20)
        Me.txtDeuda.TabIndex = 5
        Me.txtDeuda.Text = "$ 0"
        Me.txtDeuda.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtDeuda.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(461, 97)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(45, 13)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Deuda: "
        '
        'txtAhorroAdicional
        '
        Me.txtAhorroAdicional.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtAhorroAdicional.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtAhorroAdicional.DecimalDigits = 0
        Me.txtAhorroAdicional.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtAhorroAdicional.Location = New System.Drawing.Point(297, 94)
        Me.txtAhorroAdicional.Name = "txtAhorroAdicional"
        Me.txtAhorroAdicional.ReadOnly = True
        Me.txtAhorroAdicional.Size = New System.Drawing.Size(122, 20)
        Me.txtAhorroAdicional.TabIndex = 3
        Me.txtAhorroAdicional.Text = "$ 0"
        Me.txtAhorroAdicional.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtAhorroAdicional.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'txtAhorroObligatorio
        '
        Me.txtAhorroObligatorio.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtAhorroObligatorio.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtAhorroObligatorio.DecimalDigits = 0
        Me.txtAhorroObligatorio.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtAhorroObligatorio.Location = New System.Drawing.Point(297, 57)
        Me.txtAhorroObligatorio.Name = "txtAhorroObligatorio"
        Me.txtAhorroObligatorio.ReadOnly = True
        Me.txtAhorroObligatorio.Size = New System.Drawing.Size(122, 20)
        Me.txtAhorroObligatorio.TabIndex = 1
        Me.txtAhorroObligatorio.Text = "$ 0"
        Me.txtAhorroObligatorio.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtAhorroObligatorio.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(196, 97)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(90, 13)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "Ahorro Adicional: "
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(196, 60)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(97, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Ahorro Obligatorio: "
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.cmdActualizar)
        Me.GroupBox1.Controls.Add(Me.txtValorSaldado)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtCuotasSaldadas)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtValorCuota)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtNroCuotas)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtValorPrestamo)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtObservaciones)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtConcepto)
        Me.GroupBox1.Controls.Add(Me.btnMovimiento)
        Me.GroupBox1.Controls.Add(Me.cboTipoPrestamo)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.btnEliminar)
        Me.GroupBox1.Controls.Add(Me.btnImprimir)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.dtpHasta)
        Me.GroupBox1.Controls.Add(Me.dtpDesde)
        Me.GroupBox1.Controls.Add(Me.lblCliente)
        Me.GroupBox1.Controls.Add(Me.btnCancelar)
        Me.GroupBox1.Controls.Add(Me.btnAceptar)
        Me.GroupBox1.Controls.Add(Me.btnNuevo)
        Me.GroupBox1.Controls.Add(Me.GridEX1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 218)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(805, 463)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Préstamos"
        '
        'cmdActualizar
        '
        Me.cmdActualizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdActualizar.Icon = CType(resources.GetObject("cmdActualizar.Icon"), System.Drawing.Icon)
        Me.cmdActualizar.Image = Global.TaxiPortal.My.Resources.Resources.Button_20Refresh
        Me.cmdActualizar.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Center
        Me.cmdActualizar.Location = New System.Drawing.Point(763, 21)
        Me.cmdActualizar.Name = "cmdActualizar"
        Me.cmdActualizar.Size = New System.Drawing.Size(30, 26)
        Me.cmdActualizar.TabIndex = 115
        Me.cmdActualizar.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003
        '
        'txtValorSaldado
        '
        Me.txtValorSaldado.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtValorSaldado.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtValorSaldado.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtValorSaldado.DecimalDigits = 0
        Me.txtValorSaldado.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtValorSaldado.Location = New System.Drawing.Point(636, 391)
        Me.txtValorSaldado.Name = "txtValorSaldado"
        Me.txtValorSaldado.ReadOnly = True
        Me.txtValorSaldado.Size = New System.Drawing.Size(75, 20)
        Me.txtValorSaldado.TabIndex = 19
        Me.txtValorSaldado.Text = "$ 0"
        Me.txtValorSaldado.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtValorSaldado.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(553, 394)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(77, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Valor saldado: "
        '
        'txtCuotasSaldadas
        '
        Me.txtCuotasSaldadas.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCuotasSaldadas.Enabled = False
        Me.txtCuotasSaldadas.Location = New System.Drawing.Point(490, 391)
        Me.txtCuotasSaldadas.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.txtCuotasSaldadas.Name = "txtCuotasSaldadas"
        Me.txtCuotasSaldadas.ReadOnly = True
        Me.txtCuotasSaldadas.Size = New System.Drawing.Size(52, 20)
        Me.txtCuotasSaldadas.TabIndex = 17
        Me.txtCuotasSaldadas.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(401, 393)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(88, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "C&uotas saldadas:"
        '
        'txtValorCuota
        '
        Me.txtValorCuota.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtValorCuota.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtValorCuota.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtValorCuota.DecimalDigits = 0
        Me.txtValorCuota.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtValorCuota.Location = New System.Drawing.Point(636, 359)
        Me.txtValorCuota.Name = "txtValorCuota"
        Me.txtValorCuota.ReadOnly = True
        Me.txtValorCuota.Size = New System.Drawing.Size(75, 20)
        Me.txtValorCuota.TabIndex = 15
        Me.txtValorCuota.Text = "$ 0"
        Me.txtValorCuota.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtValorCuota.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(553, 363)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Valor cuota: "
        '
        'txtNroCuotas
        '
        Me.txtNroCuotas.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtNroCuotas.Location = New System.Drawing.Point(490, 359)
        Me.txtNroCuotas.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.txtNroCuotas.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtNroCuotas.Name = "txtNroCuotas"
        Me.txtNroCuotas.ReadOnly = True
        Me.txtNroCuotas.Size = New System.Drawing.Size(52, 20)
        Me.txtNroCuotas.TabIndex = 13
        Me.txtNroCuotas.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(401, 363)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(43, 13)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "C&uotas:"
        '
        'txtValorPrestamo
        '
        Me.txtValorPrestamo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtValorPrestamo.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtValorPrestamo.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtValorPrestamo.DecimalDigits = 0
        Me.txtValorPrestamo.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtValorPrestamo.Location = New System.Drawing.Point(490, 327)
        Me.txtValorPrestamo.Name = "txtValorPrestamo"
        Me.txtValorPrestamo.ReadOnly = True
        Me.txtValorPrestamo.Size = New System.Drawing.Size(113, 20)
        Me.txtValorPrestamo.TabIndex = 7
        Me.txtValorPrestamo.Text = "$ 0"
        Me.txtValorPrestamo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtValorPrestamo.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(401, 327)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Valor préstamo: "
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtObservaciones.Location = New System.Drawing.Point(490, 277)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.ReadOnly = True
        Me.txtObservaciones.Size = New System.Drawing.Size(221, 37)
        Me.txtObservaciones.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(401, 281)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "&Observaciones"
        '
        'txtConcepto
        '
        Me.txtConcepto.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtConcepto.Location = New System.Drawing.Point(104, 277)
        Me.txtConcepto.Multiline = True
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.ReadOnly = True
        Me.txtConcepto.Size = New System.Drawing.Size(221, 37)
        Me.txtConcepto.TabIndex = 1
        '
        'btnMovimiento
        '
        Me.btnMovimiento.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMovimiento.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.file2
        Me.btnMovimiento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMovimiento.Location = New System.Drawing.Point(765, 277)
        Me.btnMovimiento.Name = "btnMovimiento"
        Me.btnMovimiento.Size = New System.Drawing.Size(30, 30)
        Me.btnMovimiento.TabIndex = 114
        Me.btnMovimiento.UseVisualStyleBackColor = True
        '
        'cboTipoPrestamo
        '
        Me.cboTipoPrestamo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboTipoPrestamo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboTipoPrestamo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTipoPrestamo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoPrestamo.Enabled = False
        Me.cboTipoPrestamo.FormattingEnabled = True
        Me.cboTipoPrestamo.Location = New System.Drawing.Point(104, 324)
        Me.cboTipoPrestamo.Name = "cboTipoPrestamo"
        Me.cboTipoPrestamo.Size = New System.Drawing.Size(221, 21)
        Me.cboTipoPrestamo.TabIndex = 5
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(16, 327)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(78, 13)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "&Tipo Préstamo:"
        '
        'btnEliminar
        '
        Me.btnEliminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEliminar.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.delete
        Me.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnEliminar.Location = New System.Drawing.Point(765, 339)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(30, 30)
        Me.btnEliminar.TabIndex = 112
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImprimir.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.printer
        Me.btnImprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnImprimir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnImprimir.Location = New System.Drawing.Point(765, 308)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(30, 30)
        Me.btnImprimir.TabIndex = 111
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 394)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(24, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "&Fin:"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 362)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "&Inicio:"
        '
        'dtpHasta
        '
        Me.dtpHasta.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtpHasta.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.dtpHasta.Enabled = False
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(104, 391)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(105, 20)
        Me.dtpHasta.TabIndex = 11
        '
        'dtpDesde
        '
        Me.dtpDesde.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtpDesde.CustomFormat = "dd/MM/yyyy"
        Me.dtpDesde.Enabled = False
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(104, 359)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(105, 20)
        Me.dtpDesde.TabIndex = 9
        '
        'lblCliente
        '
        Me.lblCliente.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(16, 283)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(56, 13)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "&Concepto:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.Location = New System.Drawing.Point(636, 428)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 21
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        Me.btnCancelar.Visible = False
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.Location = New System.Drawing.Point(556, 428)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 20
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        Me.btnAceptar.Visible = False
        '
        'btnNuevo
        '
        Me.btnNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnNuevo.Location = New System.Drawing.Point(636, 428)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 100
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'GridEX1
        '
        Me.GridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.[True]
        Me.GridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridEX1.Location = New System.Drawing.Point(16, 18)
        Me.GridEX1.Name = "GridEX1"
        Me.GridEX1.Size = New System.Drawing.Size(779, 243)
        Me.GridEX1.TabIndex = 84
        Me.GridEX1.TabKeyBehavior = Janus.Windows.GridEX.TabKeyBehavior.ControlNavigation
        '
        'frmPrestamo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(839, 693)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gbPersona)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(855, 572)
        Me.Name = "frmPrestamo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Préstamos"
        Me.gbPersona.ResumeLayout(False)
        Me.gbPersona.PerformLayout()
        CType(Me.PbPersona, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtCuotasSaldadas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNroCuotas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbPersona As System.Windows.Forms.GroupBox
    Friend WithEvents txtDeuda As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtAhorroAdicional As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents txtAhorroObligatorio As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnMovimiento As System.Windows.Forms.Button
    Friend WithEvents cboTipoPrestamo As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents GridEX1 As Janus.Windows.GridEX.GridEX
    Friend WithEvents txtMovil As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtConcepto As System.Windows.Forms.TextBox
    Friend WithEvents txtValorPrestamo As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtValorSaldado As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCuotasSaldadas As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtValorCuota As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNroCuotas As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmdActualizar As Janus.Windows.EditControls.UIButton
    Friend WithEvents PbPersona As System.Windows.Forms.PictureBox
End Class
