﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CHRF2 = New System.Windows.Forms.CheckBox()
        Me.CHRF1 = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBoxPrompter = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtImporte = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtFin = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtInicio = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMinuto = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.chkImprimir = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDias = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbRapido = New System.Windows.Forms.RadioButton()
        Me.rbNormal = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboOrigen = New System.Windows.Forms.ComboBox()
        Me.chkMostrarEnCurso = New System.Windows.Forms.CheckBox()
        Me.chkMostrarTerminado = New System.Windows.Forms.CheckBox()
        Me.chkMostrarCancelado = New System.Windows.Forms.CheckBox()
        Me.chkAsignarInhab = New System.Windows.Forms.CheckBox()
        Me.chkNotifInhab = New System.Windows.Forms.CheckBox()
        Me.chkMostrarEnCamino = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TextBoxPrompter)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtImporte)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtFin)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtInicio)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtMinuto)
        Me.GroupBox1.Controls.Add(Me.chkImprimir)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtDias)
        Me.GroupBox1.Controls.Add(Me.btnSalir)
        Me.GroupBox1.Controls.Add(Me.btnGuardar)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cboOrigen)
        Me.GroupBox1.Controls.Add(Me.chkMostrarEnCurso)
        Me.GroupBox1.Controls.Add(Me.chkMostrarTerminado)
        Me.GroupBox1.Controls.Add(Me.chkMostrarCancelado)
        Me.GroupBox1.Controls.Add(Me.chkAsignarInhab)
        Me.GroupBox1.Controls.Add(Me.chkNotifInhab)
        Me.GroupBox1.Controls.Add(Me.chkMostrarEnCamino)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(327, 609)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.CHRF2)
        Me.GroupBox3.Controls.Add(Me.CHRF1)
        Me.GroupBox3.Location = New System.Drawing.Point(175, 369)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(160, 69)
        Me.GroupBox3.TabIndex = 20
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "RadioFrecuencia"
        '
        'CHRF2
        '
        Me.CHRF2.AutoSize = True
        Me.CHRF2.Location = New System.Drawing.Point(6, 44)
        Me.CHRF2.Name = "CHRF2"
        Me.CHRF2.Size = New System.Drawing.Size(49, 17)
        Me.CHRF2.TabIndex = 1
        Me.CHRF2.Text = "RF 2"
        Me.CHRF2.UseVisualStyleBackColor = True
        '
        'CHRF1
        '
        Me.CHRF1.AutoSize = True
        Me.CHRF1.Location = New System.Drawing.Point(6, 21)
        Me.CHRF1.Name = "CHRF1"
        Me.CHRF1.Size = New System.Drawing.Size(49, 17)
        Me.CHRF1.TabIndex = 0
        Me.CHRF1.Text = "RF 1"
        Me.CHRF1.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 451)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(79, 13)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Texto Prompter"
        '
        'TextBoxPrompter
        '
        Me.TextBoxPrompter.Location = New System.Drawing.Point(9, 467)
        Me.TextBoxPrompter.Multiline = True
        Me.TextBoxPrompter.Name = "TextBoxPrompter"
        Me.TextBoxPrompter.Size = New System.Drawing.Size(312, 94)
        Me.TextBoxPrompter.TabIndex = 22
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 279)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(126, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Importe recargo nocturno"
        '
        'txtImporte
        '
        Me.txtImporte.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtImporte.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtImporte.DecimalDigits = 0
        Me.txtImporte.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General
        Me.txtImporte.Location = New System.Drawing.Point(153, 276)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(32, 20)
        Me.txtImporte.TabIndex = 14
        Me.txtImporte.Text = "0"
        Me.txtImporte.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtImporte.Value = 0
        Me.txtImporte.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 253)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Hora fin recargo nocturno"
        '
        'txtFin
        '
        Me.txtFin.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtFin.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtFin.DecimalDigits = 0
        Me.txtFin.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General
        Me.txtFin.Location = New System.Drawing.Point(153, 250)
        Me.txtFin.Name = "txtFin"
        Me.txtFin.Size = New System.Drawing.Size(32, 20)
        Me.txtFin.TabIndex = 12
        Me.txtFin.Text = "0"
        Me.txtFin.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtFin.Value = 0
        Me.txtFin.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 227)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(141, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Hora inicio recargo nocturno"
        '
        'txtInicio
        '
        Me.txtInicio.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtInicio.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtInicio.DecimalDigits = 0
        Me.txtInicio.Location = New System.Drawing.Point(153, 224)
        Me.txtInicio.Name = "txtInicio"
        Me.txtInicio.Size = New System.Drawing.Size(32, 20)
        Me.txtInicio.TabIndex = 10
        Me.txtInicio.Text = "0"
        Me.txtInicio.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtInicio.Value = 0
        Me.txtInicio.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 201)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(141, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Minutos Notificacion demora"
        '
        'txtMinuto
        '
        Me.txtMinuto.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtMinuto.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtMinuto.DecimalDigits = 0
        Me.txtMinuto.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General
        Me.txtMinuto.Location = New System.Drawing.Point(153, 198)
        Me.txtMinuto.Name = "txtMinuto"
        Me.txtMinuto.Size = New System.Drawing.Size(32, 20)
        Me.txtMinuto.TabIndex = 8
        Me.txtMinuto.Text = "0"
        Me.txtMinuto.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtMinuto.Value = 0
        Me.txtMinuto.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'chkImprimir
        '
        Me.chkImprimir.AutoSize = True
        Me.chkImprimir.Location = New System.Drawing.Point(6, 170)
        Me.chkImprimir.Name = "chkImprimir"
        Me.chkImprimir.Size = New System.Drawing.Size(100, 17)
        Me.chkImprimir.TabIndex = 6
        Me.chkImprimir.Text = "&Imprimir Tickete"
        Me.chkImprimir.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 347)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Dias Alerta"
        '
        'txtDias
        '
        Me.txtDias.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtDias.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtDias.DecimalDigits = 0
        Me.txtDias.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General
        Me.txtDias.Location = New System.Drawing.Point(101, 343)
        Me.txtDias.Name = "txtDias"
        Me.txtDias.Size = New System.Drawing.Size(32, 20)
        Me.txtDias.TabIndex = 18
        Me.txtDias.Text = "8"
        Me.txtDias.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtDias.Value = 8
        Me.txtDias.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(246, 568)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 24
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(165, 568)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 23
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbRapido)
        Me.GroupBox2.Controls.Add(Me.rbNormal)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 369)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(160, 69)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tipo de Servicio por defecto"
        '
        'rbRapido
        '
        Me.rbRapido.AutoSize = True
        Me.rbRapido.Location = New System.Drawing.Point(7, 44)
        Me.rbRapido.Name = "rbRapido"
        Me.rbRapido.Size = New System.Drawing.Size(59, 17)
        Me.rbRapido.TabIndex = 1
        Me.rbRapido.TabStop = True
        Me.rbRapido.Text = "Rápido"
        Me.rbRapido.UseVisualStyleBackColor = True
        '
        'rbNormal
        '
        Me.rbNormal.AutoSize = True
        Me.rbNormal.Location = New System.Drawing.Point(7, 20)
        Me.rbNormal.Name = "rbNormal"
        Me.rbNormal.Size = New System.Drawing.Size(58, 17)
        Me.rbNormal.TabIndex = 0
        Me.rbNormal.TabStop = True
        Me.rbNormal.Text = "Normal"
        Me.rbNormal.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 319)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Zona por defecto"
        '
        'cboOrigen
        '
        Me.cboOrigen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboOrigen.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboOrigen.FormattingEnabled = True
        Me.cboOrigen.Location = New System.Drawing.Point(101, 316)
        Me.cboOrigen.Name = "cboOrigen"
        Me.cboOrigen.Size = New System.Drawing.Size(188, 21)
        Me.cboOrigen.TabIndex = 16
        '
        'chkMostrarEnCurso
        '
        Me.chkMostrarEnCurso.AutoSize = True
        Me.chkMostrarEnCurso.Location = New System.Drawing.Point(6, 55)
        Me.chkMostrarEnCurso.Name = "chkMostrarEnCurso"
        Me.chkMostrarEnCurso.Size = New System.Drawing.Size(150, 17)
        Me.chkMostrarEnCurso.TabIndex = 1
        Me.chkMostrarEnCurso.Text = "Mostrar servicios en Curso"
        Me.chkMostrarEnCurso.UseVisualStyleBackColor = True
        '
        'chkMostrarTerminado
        '
        Me.chkMostrarTerminado.AutoSize = True
        Me.chkMostrarTerminado.Location = New System.Drawing.Point(6, 78)
        Me.chkMostrarTerminado.Name = "chkMostrarTerminado"
        Me.chkMostrarTerminado.Size = New System.Drawing.Size(163, 17)
        Me.chkMostrarTerminado.TabIndex = 2
        Me.chkMostrarTerminado.Text = "Mostrar servicios Terminados"
        Me.chkMostrarTerminado.UseVisualStyleBackColor = True
        '
        'chkMostrarCancelado
        '
        Me.chkMostrarCancelado.AutoSize = True
        Me.chkMostrarCancelado.Location = New System.Drawing.Point(6, 101)
        Me.chkMostrarCancelado.Name = "chkMostrarCancelado"
        Me.chkMostrarCancelado.Size = New System.Drawing.Size(164, 17)
        Me.chkMostrarCancelado.TabIndex = 3
        Me.chkMostrarCancelado.Text = "Mostrar servicios Cancelados"
        Me.chkMostrarCancelado.UseVisualStyleBackColor = True
        '
        'chkAsignarInhab
        '
        Me.chkAsignarInhab.AutoSize = True
        Me.chkAsignarInhab.Location = New System.Drawing.Point(6, 124)
        Me.chkAsignarInhab.Name = "chkAsignarInhab"
        Me.chkAsignarInhab.Size = New System.Drawing.Size(246, 17)
        Me.chkAsignarInhab.TabIndex = 4
        Me.chkAsignarInhab.Text = "Permitir asignar carreras a móviles inhabilitados"
        Me.chkAsignarInhab.UseVisualStyleBackColor = True
        '
        'chkNotifInhab
        '
        Me.chkNotifInhab.AutoSize = True
        Me.chkNotifInhab.Location = New System.Drawing.Point(6, 147)
        Me.chkNotifInhab.Name = "chkNotifInhab"
        Me.chkNotifInhab.Size = New System.Drawing.Size(283, 17)
        Me.chkNotifInhab.TabIndex = 5
        Me.chkNotifInhab.Text = "Notificar asignacion de carreras a móviles inhabilitados"
        Me.chkNotifInhab.UseVisualStyleBackColor = True
        '
        'chkMostrarEnCamino
        '
        Me.chkMostrarEnCamino.AutoSize = True
        Me.chkMostrarEnCamino.Location = New System.Drawing.Point(6, 32)
        Me.chkMostrarEnCamino.Name = "chkMostrarEnCamino"
        Me.chkMostrarEnCamino.Size = New System.Drawing.Size(158, 17)
        Me.chkMostrarEnCamino.TabIndex = 0
        Me.chkMostrarEnCamino.Text = "Mostrar servicios en Camino"
        Me.chkMostrarEnCamino.UseVisualStyleBackColor = True
        '
        'frmConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnSalir
        Me.ClientSize = New System.Drawing.Size(366, 633)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmConfig"
        Me.Text = "Configuraciones"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkMostrarEnCurso As System.Windows.Forms.CheckBox
    Friend WithEvents chkMostrarTerminado As System.Windows.Forms.CheckBox
    Friend WithEvents chkMostrarCancelado As System.Windows.Forms.CheckBox
    Friend WithEvents chkAsignarInhab As System.Windows.Forms.CheckBox
    Friend WithEvents chkNotifInhab As System.Windows.Forms.CheckBox
    Friend WithEvents chkMostrarEnCamino As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboOrigen As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbRapido As System.Windows.Forms.RadioButton
    Friend WithEvents rbNormal As System.Windows.Forms.RadioButton
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDias As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents chkImprimir As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMinuto As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtImporte As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFin As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtInicio As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBoxPrompter As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CHRF2 As System.Windows.Forms.CheckBox
    Friend WithEvents CHRF1 As System.Windows.Forms.CheckBox
End Class
