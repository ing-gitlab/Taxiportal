﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPersonas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPersonas))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnRecaudacion = New System.Windows.Forms.Button()
        Me.btnMovimientos = New System.Windows.Forms.Button()
        Me.btnPrestamo = New System.Windows.Forms.Button()
        Me.btnAhorro = New System.Windows.Forms.Button()
        Me.lbCantidad = New System.Windows.Forms.Label()
        Me.GridEX1 = New Janus.Windows.GridEX.GridEX()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtDeuda = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtAhorroAdicional = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.txtAhorroObligatorio = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CboEstado_Tipo = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TxtNombre = New System.Windows.Forms.TextBox()
        Me.TxtCedula = New System.Windows.Forms.TextBox()
        Me.TxtDireccion = New System.Windows.Forms.TextBox()
        Me.TxtTelefono = New System.Windows.Forms.TextBox()
        Me.BnNuevo = New System.Windows.Forms.Button()
        Me.BnModificar = New System.Windows.Forms.Button()
        Me.BnGuardar = New System.Windows.Forms.Button()
        Me.BnCancelar = New System.Windows.Forms.Button()
        Me.BtnHistorial = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CgFoto = New System.Windows.Forms.Button()
        Me.PbCadete = New System.Windows.Forms.PictureBox()
        Me.BtnEliminar = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.RbConductores = New System.Windows.Forms.RadioButton()
        Me.RbPropietarios = New System.Windows.Forms.RadioButton()
        Me.RbTodos = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PbCadete, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btnRecaudacion)
        Me.GroupBox1.Controls.Add(Me.btnMovimientos)
        Me.GroupBox1.Controls.Add(Me.btnPrestamo)
        Me.GroupBox1.Controls.Add(Me.btnAhorro)
        Me.GroupBox1.Controls.Add(Me.lbCantidad)
        Me.GroupBox1.Controls.Add(Me.GridEX1)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.BtnHistorial)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.BtnEliminar)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 35)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1000, 527)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Personas"
        '
        'btnRecaudacion
        '
        Me.btnRecaudacion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRecaudacion.BackColor = System.Drawing.Color.Transparent
        Me.btnRecaudacion.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.recauda2
        Me.btnRecaudacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnRecaudacion.Location = New System.Drawing.Point(952, 416)
        Me.btnRecaudacion.Name = "btnRecaudacion"
        Me.btnRecaudacion.Size = New System.Drawing.Size(30, 30)
        Me.btnRecaudacion.TabIndex = 6
        Me.btnRecaudacion.UseVisualStyleBackColor = False
        '
        'btnMovimientos
        '
        Me.btnMovimientos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMovimientos.BackColor = System.Drawing.Color.Transparent
        Me.btnMovimientos.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.file2
        Me.btnMovimientos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMovimientos.Location = New System.Drawing.Point(952, 447)
        Me.btnMovimientos.Name = "btnMovimientos"
        Me.btnMovimientos.Size = New System.Drawing.Size(30, 30)
        Me.btnMovimientos.TabIndex = 7
        Me.btnMovimientos.UseVisualStyleBackColor = False
        '
        'btnPrestamo
        '
        Me.btnPrestamo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrestamo.BackColor = System.Drawing.Color.Transparent
        Me.btnPrestamo.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.dollar
        Me.btnPrestamo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnPrestamo.Location = New System.Drawing.Point(952, 385)
        Me.btnPrestamo.Name = "btnPrestamo"
        Me.btnPrestamo.Size = New System.Drawing.Size(30, 30)
        Me.btnPrestamo.TabIndex = 5
        Me.btnPrestamo.UseVisualStyleBackColor = False
        '
        'btnAhorro
        '
        Me.btnAhorro.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAhorro.BackColor = System.Drawing.Color.Transparent
        Me.btnAhorro.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.alcancia
        Me.btnAhorro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAhorro.Location = New System.Drawing.Point(952, 354)
        Me.btnAhorro.Name = "btnAhorro"
        Me.btnAhorro.Size = New System.Drawing.Size(30, 30)
        Me.btnAhorro.TabIndex = 4
        Me.btnAhorro.UseVisualStyleBackColor = False
        '
        'lbCantidad
        '
        Me.lbCantidad.AutoSize = True
        Me.lbCantidad.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lbCantidad.Location = New System.Drawing.Point(3, 511)
        Me.lbCantidad.Name = "lbCantidad"
        Me.lbCantidad.Size = New System.Drawing.Size(39, 13)
        Me.lbCantidad.TabIndex = 16
        Me.lbCantidad.Text = "Label5"
        '
        'GridEX1
        '
        Me.GridEX1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridEX1.Location = New System.Drawing.Point(17, 19)
        Me.GridEX1.Name = "GridEX1"
        Me.GridEX1.Size = New System.Drawing.Size(965, 268)
        Me.GridEX1.TabIndex = 9
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.txtDeuda)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.txtAhorroAdicional)
        Me.GroupBox3.Controls.Add(Me.txtAhorroObligatorio)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.CboEstado_Tipo)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.TxtNombre)
        Me.GroupBox3.Controls.Add(Me.TxtCedula)
        Me.GroupBox3.Controls.Add(Me.TxtDireccion)
        Me.GroupBox3.Controls.Add(Me.TxtTelefono)
        Me.GroupBox3.Controls.Add(Me.BnNuevo)
        Me.GroupBox3.Controls.Add(Me.BnModificar)
        Me.GroupBox3.Controls.Add(Me.BnGuardar)
        Me.GroupBox3.Controls.Add(Me.BnCancelar)
        Me.GroupBox3.Location = New System.Drawing.Point(171, 293)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(666, 212)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        '
        'txtDeuda
        '
        Me.txtDeuda.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtDeuda.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtDeuda.DecimalDigits = 0
        Me.txtDeuda.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtDeuda.Location = New System.Drawing.Point(419, 130)
        Me.txtDeuda.Name = "txtDeuda"
        Me.txtDeuda.ReadOnly = True
        Me.txtDeuda.Size = New System.Drawing.Size(122, 20)
        Me.txtDeuda.TabIndex = 15
        Me.txtDeuda.Text = "$ 0"
        Me.txtDeuda.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtDeuda.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(316, 133)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Deuda: "
        '
        'txtAhorroAdicional
        '
        Me.txtAhorroAdicional.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtAhorroAdicional.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtAhorroAdicional.DecimalDigits = 0
        Me.txtAhorroAdicional.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtAhorroAdicional.Location = New System.Drawing.Point(419, 100)
        Me.txtAhorroAdicional.Name = "txtAhorroAdicional"
        Me.txtAhorroAdicional.ReadOnly = True
        Me.txtAhorroAdicional.Size = New System.Drawing.Size(122, 20)
        Me.txtAhorroAdicional.TabIndex = 13
        Me.txtAhorroAdicional.Text = "$ 0"
        Me.txtAhorroAdicional.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtAhorroAdicional.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'txtAhorroObligatorio
        '
        Me.txtAhorroObligatorio.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtAhorroObligatorio.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtAhorroObligatorio.DecimalDigits = 0
        Me.txtAhorroObligatorio.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtAhorroObligatorio.Location = New System.Drawing.Point(419, 71)
        Me.txtAhorroObligatorio.Name = "txtAhorroObligatorio"
        Me.txtAhorroObligatorio.ReadOnly = True
        Me.txtAhorroObligatorio.Size = New System.Drawing.Size(122, 20)
        Me.txtAhorroObligatorio.TabIndex = 11
        Me.txtAhorroObligatorio.Text = "$ 0"
        Me.txtAhorroObligatorio.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtAhorroObligatorio.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(316, 103)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(90, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Ahorro Adicional: "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(316, 74)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(97, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Ahorro Obligatorio: "
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(316, 45)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(83, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "&Tipo Asociación"
        '
        'CboEstado_Tipo
        '
        Me.CboEstado_Tipo.Enabled = False
        Me.CboEstado_Tipo.FormattingEnabled = True
        Me.CboEstado_Tipo.Location = New System.Drawing.Point(419, 41)
        Me.CboEstado_Tipo.Name = "CboEstado_Tipo"
        Me.CboEstado_Tipo.Size = New System.Drawing.Size(122, 21)
        Me.CboEstado_Tipo.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "&Nombre Completo"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "&Cedula"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 107)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "&Dirección"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(10, 137)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "&Teléfono"
        '
        'TxtNombre
        '
        Me.TxtNombre.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TxtNombre.Location = New System.Drawing.Point(124, 45)
        Me.TxtNombre.Name = "TxtNombre"
        Me.TxtNombre.ReadOnly = True
        Me.TxtNombre.Size = New System.Drawing.Size(172, 20)
        Me.TxtNombre.TabIndex = 1
        '
        'TxtCedula
        '
        Me.TxtCedula.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TxtCedula.Location = New System.Drawing.Point(124, 74)
        Me.TxtCedula.Name = "TxtCedula"
        Me.TxtCedula.ReadOnly = True
        Me.TxtCedula.Size = New System.Drawing.Size(172, 20)
        Me.TxtCedula.TabIndex = 3
        '
        'TxtDireccion
        '
        Me.TxtDireccion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TxtDireccion.Location = New System.Drawing.Point(124, 104)
        Me.TxtDireccion.Name = "TxtDireccion"
        Me.TxtDireccion.ReadOnly = True
        Me.TxtDireccion.Size = New System.Drawing.Size(172, 20)
        Me.TxtDireccion.TabIndex = 5
        '
        'TxtTelefono
        '
        Me.TxtTelefono.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TxtTelefono.Location = New System.Drawing.Point(124, 134)
        Me.TxtTelefono.Name = "TxtTelefono"
        Me.TxtTelefono.ReadOnly = True
        Me.TxtTelefono.Size = New System.Drawing.Size(172, 20)
        Me.TxtTelefono.TabIndex = 7
        '
        'BnNuevo
        '
        Me.BnNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BnNuevo.Location = New System.Drawing.Point(221, 172)
        Me.BnNuevo.Name = "BnNuevo"
        Me.BnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.BnNuevo.TabIndex = 16
        Me.BnNuevo.Text = "&Nuevo"
        Me.BnNuevo.UseVisualStyleBackColor = True
        '
        'BnModificar
        '
        Me.BnModificar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BnModificar.Location = New System.Drawing.Point(302, 172)
        Me.BnModificar.Name = "BnModificar"
        Me.BnModificar.Size = New System.Drawing.Size(75, 23)
        Me.BnModificar.TabIndex = 17
        Me.BnModificar.Text = "&Modificar"
        Me.BnModificar.UseVisualStyleBackColor = True
        '
        'BnGuardar
        '
        Me.BnGuardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BnGuardar.Location = New System.Drawing.Point(221, 172)
        Me.BnGuardar.Name = "BnGuardar"
        Me.BnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.BnGuardar.TabIndex = 12
        Me.BnGuardar.Text = "Guardar"
        Me.BnGuardar.UseVisualStyleBackColor = True
        '
        'BnCancelar
        '
        Me.BnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BnCancelar.Location = New System.Drawing.Point(302, 172)
        Me.BnCancelar.Name = "BnCancelar"
        Me.BnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.BnCancelar.TabIndex = 13
        Me.BnCancelar.Text = "&Cancelar"
        Me.BnCancelar.UseVisualStyleBackColor = True
        '
        'BtnHistorial
        '
        Me.BtnHistorial.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnHistorial.BackColor = System.Drawing.Color.Transparent
        Me.BtnHistorial.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.historial
        Me.BtnHistorial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnHistorial.Location = New System.Drawing.Point(952, 293)
        Me.BtnHistorial.Name = "BtnHistorial"
        Me.BtnHistorial.Size = New System.Drawing.Size(30, 30)
        Me.BtnHistorial.TabIndex = 2
        Me.BtnHistorial.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.CgFoto)
        Me.GroupBox2.Controls.Add(Me.PbCadete)
        Me.GroupBox2.Location = New System.Drawing.Point(17, 293)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(151, 212)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'CgFoto
        '
        Me.CgFoto.Enabled = False
        Me.CgFoto.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CgFoto.Location = New System.Drawing.Point(40, 185)
        Me.CgFoto.Name = "CgFoto"
        Me.CgFoto.Size = New System.Drawing.Size(70, 23)
        Me.CgFoto.TabIndex = 0
        Me.CgFoto.Text = "Cargar Foto"
        Me.CgFoto.UseVisualStyleBackColor = True
        '
        'PbCadete
        '
        Me.PbCadete.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PbCadete.Location = New System.Drawing.Point(16, 19)
        Me.PbCadete.Name = "PbCadete"
        Me.PbCadete.Size = New System.Drawing.Size(120, 160)
        Me.PbCadete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PbCadete.TabIndex = 14
        Me.PbCadete.TabStop = False
        '
        'BtnEliminar
        '
        Me.BtnEliminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnEliminar.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.delete
        Me.BtnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnEliminar.Location = New System.Drawing.Point(952, 323)
        Me.BtnEliminar.Name = "BtnEliminar"
        Me.BtnEliminar.Size = New System.Drawing.Size(30, 30)
        Me.BtnEliminar.TabIndex = 3
        Me.BtnEliminar.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'RbConductores
        '
        Me.RbConductores.AutoSize = True
        Me.RbConductores.Checked = True
        Me.RbConductores.Location = New System.Drawing.Point(334, 12)
        Me.RbConductores.Name = "RbConductores"
        Me.RbConductores.Size = New System.Drawing.Size(85, 17)
        Me.RbConductores.TabIndex = 0
        Me.RbConductores.TabStop = True
        Me.RbConductores.Text = "Conductores"
        Me.RbConductores.UseVisualStyleBackColor = True
        '
        'RbPropietarios
        '
        Me.RbPropietarios.AutoSize = True
        Me.RbPropietarios.Location = New System.Drawing.Point(425, 12)
        Me.RbPropietarios.Name = "RbPropietarios"
        Me.RbPropietarios.Size = New System.Drawing.Size(80, 17)
        Me.RbPropietarios.TabIndex = 1
        Me.RbPropietarios.Text = "Propietarios"
        Me.RbPropietarios.UseVisualStyleBackColor = True
        '
        'RbTodos
        '
        Me.RbTodos.AutoSize = True
        Me.RbTodos.Location = New System.Drawing.Point(509, 12)
        Me.RbTodos.Name = "RbTodos"
        Me.RbTodos.Size = New System.Drawing.Size(55, 17)
        Me.RbTodos.TabIndex = 2
        Me.RbTodos.Text = "Todos"
        Me.RbTodos.UseVisualStyleBackColor = True
        '
        'frmPersonas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 576)
        Me.Controls.Add(Me.RbTodos)
        Me.Controls.Add(Me.RbPropietarios)
        Me.Controls.Add(Me.RbConductores)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(946, 500)
        Me.Name = "frmPersonas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Personas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.PbCadete, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GridEX1 As Janus.Windows.GridEX.GridEX
    Friend WithEvents lbCantidad As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TxtNombre As System.Windows.Forms.TextBox
    Friend WithEvents TxtCedula As System.Windows.Forms.TextBox
    Friend WithEvents TxtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents TxtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents BnNuevo As System.Windows.Forms.Button
    Friend WithEvents BnModificar As System.Windows.Forms.Button
    Friend WithEvents BtnEliminar As System.Windows.Forms.Button
    Friend WithEvents BnGuardar As System.Windows.Forms.Button
    Friend WithEvents BnCancelar As System.Windows.Forms.Button
    Friend WithEvents BtnHistorial As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CgFoto As System.Windows.Forms.Button
    Friend WithEvents PbCadete As System.Windows.Forms.PictureBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CboEstado_Tipo As System.Windows.Forms.ComboBox
    Friend WithEvents txtDeuda As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtAhorroAdicional As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents txtAhorroObligatorio As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnAhorro As System.Windows.Forms.Button
    Friend WithEvents btnPrestamo As System.Windows.Forms.Button
    Friend WithEvents btnMovimientos As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnRecaudacion As System.Windows.Forms.Button
    Friend WithEvents RbConductores As System.Windows.Forms.RadioButton
    Friend WithEvents RbPropietarios As System.Windows.Forms.RadioButton
    Friend WithEvents RbTodos As System.Windows.Forms.RadioButton
End Class
