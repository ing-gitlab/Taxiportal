﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPagoAhorros
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.txtDeuda = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtAhorroAdicional = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.txtAhorroObligatorio = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnReloadPropietarios = New System.Windows.Forms.Button()
        Me.cboPropietario = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtRetiroAdicional = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.txtRetiroObligatorio = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnAhorroAdicional = New System.Windows.Forms.Button()
        Me.btnAhorroObligatorio = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.LinkLabel3)
        Me.GroupBox1.Controls.Add(Me.txtDeuda)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtAhorroAdicional)
        Me.GroupBox1.Controls.Add(Me.txtAhorroObligatorio)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.btnReloadPropietarios)
        Me.GroupBox1.Controls.Add(Me.cboPropietario)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(460, 181)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Conductor"
        '
        'LinkLabel3
        '
        Me.LinkLabel3.AutoSize = True
        Me.LinkLabel3.Location = New System.Drawing.Point(278, 82)
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.Size = New System.Drawing.Size(125, 13)
        Me.LinkLabel3.TabIndex = 9
        Me.LinkLabel3.TabStop = True
        Me.LinkLabel3.Text = "Consulta de Movimientos"
        '
        'txtDeuda
        '
        Me.txtDeuda.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtDeuda.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtDeuda.DecimalDigits = 0
        Me.txtDeuda.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtDeuda.Location = New System.Drawing.Point(115, 141)
        Me.txtDeuda.Name = "txtDeuda"
        Me.txtDeuda.ReadOnly = True
        Me.txtDeuda.Size = New System.Drawing.Size(122, 20)
        Me.txtDeuda.TabIndex = 8
        Me.txtDeuda.Text = "$ 0"
        Me.txtDeuda.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtDeuda.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 145)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Deuda: "
        '
        'txtAhorroAdicional
        '
        Me.txtAhorroAdicional.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtAhorroAdicional.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtAhorroAdicional.DecimalDigits = 0
        Me.txtAhorroAdicional.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtAhorroAdicional.Location = New System.Drawing.Point(115, 113)
        Me.txtAhorroAdicional.Name = "txtAhorroAdicional"
        Me.txtAhorroAdicional.ReadOnly = True
        Me.txtAhorroAdicional.Size = New System.Drawing.Size(122, 20)
        Me.txtAhorroAdicional.TabIndex = 6
        Me.txtAhorroAdicional.Text = "$ 0"
        Me.txtAhorroAdicional.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtAhorroAdicional.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'txtAhorroObligatorio
        '
        Me.txtAhorroObligatorio.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtAhorroObligatorio.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtAhorroObligatorio.DecimalDigits = 0
        Me.txtAhorroObligatorio.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtAhorroObligatorio.Location = New System.Drawing.Point(115, 79)
        Me.txtAhorroObligatorio.Name = "txtAhorroObligatorio"
        Me.txtAhorroObligatorio.ReadOnly = True
        Me.txtAhorroObligatorio.Size = New System.Drawing.Size(122, 20)
        Me.txtAhorroObligatorio.TabIndex = 4
        Me.txtAhorroObligatorio.Text = "$ 0"
        Me.txtAhorroObligatorio.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtAhorroObligatorio.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 117)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Ahorro Adicional: "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 82)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Ahorro Obligatorio: "
        '
        'btnReloadPropietarios
        '
        Me.btnReloadPropietarios.Location = New System.Drawing.Point(404, 32)
        Me.btnReloadPropietarios.Name = "btnReloadPropietarios"
        Me.btnReloadPropietarios.Size = New System.Drawing.Size(16, 23)
        Me.btnReloadPropietarios.TabIndex = 2
        Me.btnReloadPropietarios.Text = "..."
        Me.btnReloadPropietarios.UseVisualStyleBackColor = True
        '
        'cboPropietario
        '
        Me.cboPropietario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboPropietario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPropietario.DisplayMember = "nombre"
        Me.cboPropietario.FormattingEnabled = True
        Me.cboPropietario.Location = New System.Drawing.Point(115, 34)
        Me.cboPropietario.Name = "cboPropietario"
        Me.cboPropietario.Size = New System.Drawing.Size(277, 21)
        Me.cboPropietario.TabIndex = 1
        Me.cboPropietario.ValueMember = "idAdministrador"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 37)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "&Conductor:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtRetiroAdicional)
        Me.GroupBox2.Controls.Add(Me.txtRetiroObligatorio)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.btnAhorroAdicional)
        Me.GroupBox2.Controls.Add(Me.btnAhorroObligatorio)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 216)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(460, 121)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Retiros"
        '
        'txtRetiroAdicional
        '
        Me.txtRetiroAdicional.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtRetiroAdicional.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtRetiroAdicional.DecimalDigits = 0
        Me.txtRetiroAdicional.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtRetiroAdicional.Location = New System.Drawing.Point(115, 69)
        Me.txtRetiroAdicional.Name = "txtRetiroAdicional"
        Me.txtRetiroAdicional.Size = New System.Drawing.Size(122, 20)
        Me.txtRetiroAdicional.TabIndex = 4
        Me.txtRetiroAdicional.Text = "$ 0"
        Me.txtRetiroAdicional.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtRetiroAdicional.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'txtRetiroObligatorio
        '
        Me.txtRetiroObligatorio.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtRetiroObligatorio.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtRetiroObligatorio.DecimalDigits = 0
        Me.txtRetiroObligatorio.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.Currency
        Me.txtRetiroObligatorio.Location = New System.Drawing.Point(115, 33)
        Me.txtRetiroObligatorio.Name = "txtRetiroObligatorio"
        Me.txtRetiroObligatorio.Size = New System.Drawing.Size(122, 20)
        Me.txtRetiroObligatorio.TabIndex = 1
        Me.txtRetiroObligatorio.Text = "$ 0"
        Me.txtRetiroObligatorio.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtRetiroObligatorio.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Ahorro Adicional: "
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 36)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(97, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Ahorro Obligatorio: "
        '
        'btnAhorroAdicional
        '
        Me.btnAhorroAdicional.Location = New System.Drawing.Point(266, 69)
        Me.btnAhorroAdicional.Name = "btnAhorroAdicional"
        Me.btnAhorroAdicional.Size = New System.Drawing.Size(75, 20)
        Me.btnAhorroAdicional.TabIndex = 5
        Me.btnAhorroAdicional.Text = "Retirar"
        Me.btnAhorroAdicional.UseVisualStyleBackColor = True
        '
        'btnAhorroObligatorio
        '
        Me.btnAhorroObligatorio.Location = New System.Drawing.Point(266, 33)
        Me.btnAhorroObligatorio.Name = "btnAhorroObligatorio"
        Me.btnAhorroObligatorio.Size = New System.Drawing.Size(75, 20)
        Me.btnAhorroObligatorio.TabIndex = 2
        Me.btnAhorroObligatorio.Text = "Retirar"
        Me.btnAhorroObligatorio.UseVisualStyleBackColor = True
        '
        'frmPagoAhorros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(513, 354)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(458, 271)
        Me.Name = "frmPagoAhorros"
        Me.Text = "Pago de Ahorros"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnReloadPropietarios As System.Windows.Forms.Button
    Friend WithEvents cboPropietario As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtAhorroAdicional As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents txtAhorroObligatorio As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents LinkLabel3 As System.Windows.Forms.LinkLabel
    Friend WithEvents txtDeuda As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRetiroAdicional As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents txtRetiroObligatorio As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnAhorroAdicional As System.Windows.Forms.Button
    Friend WithEvents btnAhorroObligatorio As System.Windows.Forms.Button
End Class
