﻿Module Funcion
    Public Sub AbrirFormulario(ByVal Ventana As Form, ByRef Madre As Form, Optional ByVal Modal As Boolean = False)
        Dim Hijas As Form(), Hija As Form

        Hijas = Madre.MdiChildren

        For Each Hija In Hijas
            If Hija.Name = Ventana.Name Then
                Hija.Focus()
                Exit Sub
            End If
        Next

        Ventana.MdiParent = Madre
        If Modal Then
            Ventana.ShowDialog()
        Else
            Ventana.Show()
        End If
    End Sub
End Module
