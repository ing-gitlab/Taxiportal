Imports System.Xml
Imports System.Data

Public Class clsImpresionDataSet

    Public Enum TipoImpresora_t
        MatrizDePuntos
        Grafica
    End Enum

    Private mDS As DataSet
    Private mPathArchivoXML As String = ""
    Private mPathArchivoComandos As String = ""
    Private mIncluirSaltoDePaginaFinal As Boolean = False
    Private mArchivoXML As New XmlDocument
    Private mIMG As clsImpresionModoGrafico
    Private mTipoImpresora As TipoImpresora_t = TipoImpresora_t.Grafica
    Private mNombreDocumentoImpresion As String
    Private mConfiguracionPagina As New System.Drawing.Printing.PageSettings

    Public Property ConfiguracionPagina() As System.Drawing.Printing.PageSettings
        Get
            Return Me.mConfiguracionPagina
        End Get
        Set(ByVal value As System.Drawing.Printing.PageSettings)
            Me.mConfiguracionPagina = value
        End Set
    End Property

    Public Property TipoImpresora() As TipoImpresora_t
        Get
            Return Me.mTipoImpresora
        End Get
        Set(ByVal value As TipoImpresora_t)
            Me.mTipoImpresora = value
        End Set
    End Property

    Public Property DataSet() As DataSet
        Get
            Return Me.mDS
        End Get
        Set(ByVal value As DataSet)
            Me.mDS = value
        End Set
    End Property

    Public Property PathArchivoXML() As String
        Get
            Return Me.mPathArchivoXML
        End Get
        Set(ByVal value As String)
            Me.mPathArchivoXML = value
        End Set
    End Property

    Public Property PathArchivoComandos() As String
        Get
            Return Me.mPathArchivoComandos
        End Get
        Set(ByVal value As String)
            Me.mPathArchivoComandos = value
        End Set
    End Property

    Public Property IncluirSaltoDePaginaFinal() As Boolean
        Get
            Return Me.mIncluirSaltoDePaginaFinal
        End Get
        Set(ByVal value As Boolean)
            Me.mIncluirSaltoDePaginaFinal = value
        End Set
    End Property

    Public Property NombreDocumentoImpresion() As String
        Get
            Return Me.mNombreDocumentoImpresion
        End Get
        Set(ByVal value As String)
            Me.mNombreDocumentoImpresion = value
        End Set
    End Property

    Private ReadOnly Property DataTableElementosFijos() As DataTable
        Get
            Return Me.DataSet.Tables(0)
        End Get
    End Property

    Private ReadOnly Property DataTableElementosRepetibles() As DataTable
        Get
            Return Me.DataSet.Tables(1)
        End Get
    End Property

    Public Sub Imprimir()
        Try
            Dim n, i, v, f As XmlNode
            Dim valor As String

            Me.mIMG = New clsImpresionModoGrafico

            Me.mIMG.ConfiguracionPagina = Me.ConfiguracionPagina

            Me.mArchivoXML.Load(Me.PathArchivoXML)

            For Each n In Me.mArchivoXML.DocumentElement.ChildNodes
                Select Case n.Name
                    Case "texto"
                        v = n.Attributes.GetNamedItem("valor")

                        Me.AgregarItemImpresionModoGrafico(n, v.Value)

                    Case "campo"
                        v = n.Attributes.GetNamedItem("valor")
                        If v Is Nothing Then
                            'Si no tiene valor asignado, verifico si tiene ID:
                            i = n.Attributes.GetNamedItem("id")
                            If i Is Nothing Then
                                'Si no tiene ID no imprimo nada:
                                valor = ""
                            Else
                                'Si tiene ID busco en el DataTable de elementos fijos una columna cuyo nombre sea el ID del nodo:
                                Try
                                    f = n.Attributes.GetNamedItem("formato")
                                    If f Is Nothing Then
                                        valor = Me.DataTableElementosFijos.Rows(0).Item(i.Value)
                                    Else
                                        valor = Format(Me.DataTableElementosFijos.Rows(0).Item(i.Value), f.Value)
                                    End If

                                Catch ex As Exception
                                    valor = "?"
                                End Try
                            End If
                        Else
                            'Imprimo el valor asignado al atributo:
                            valor = v.Value
                        End If

                        Me.AgregarItemImpresionModoGrafico(n, valor)

                    Case "cuerpo"
                        Me.ProcesarElementosRepetibles(n)
                End Select
            Next

            Me.mIMG.Imprimir(Me.NombreDocumentoImpresion)

        Catch ex As Exception

        End Try
       
    End Sub

    Private Sub ProcesarElementosRepetibles(ByRef body As XmlNode)
        Dim n, i, v, f As XmlNode
        Dim valor As String
        Dim filaInicial As Integer
        Dim posicionVerticalInicialMM, distanciaEntreFilasMM As Integer
        Dim contadorFilas As Integer = 0
        Dim dr As DataRow

        If Me.TipoImpresora = TipoImpresora_t.MatrizDePuntos Then
            filaInicial = CInt(body.Attributes.GetNamedItem("filaInicial").Value)
        Else
            posicionVerticalInicialMM = CInt(body.Attributes.GetNamedItem("posicionVerticalInicialMM").Value)
            distanciaEntreFilasMM = CInt(body.Attributes.GetNamedItem("distanciaEntreFilasMM").Value)
        End If

        For Each dr In Me.DataTableElementosRepetibles.Rows
            For Each n In body.ChildNodes
                Select Case n.Name
                    Case "campoCuerpo"
                        v = n.Attributes.GetNamedItem("valor")
                        If v Is Nothing Then
                            'Si no tiene valor asignado, verifico si tiene ID:
                            i = n.Attributes.GetNamedItem("id")
                            If i Is Nothing Then
                                'Si no tiene ID no imprimo nada:
                                valor = ""
                            Else
                                'Si tiene ID busco en el DataTable de elementos fijos una columna cuyo nombre sea el ID del nodo:
                                Try
                                    f = n.Attributes.GetNamedItem("formato")
                                    If f Is Nothing Then
                                        valor = dr.Item(i.Value)
                                    Else
                                        valor = Format(dr.Item(i.Value), f.Value)
                                    End If
                                Catch ex As Exception
                                    valor = "?"
                                End Try
                            End If
                        Else
                            'Imprimo el valor asignado al atributo:
                            valor = v.Value
                        End If

                        Me.AgregarItemImpresionModoGrafico(n, valor, posicionVerticalInicialMM + distanciaEntreFilasMM * contadorFilas)
                End Select
            Next

            contadorFilas += 1
        Next
    End Sub

    Private Sub AgregarItemImpresionModoGrafico(ByVal n As XmlNode, ByVal Valor As String, Optional ByVal PosicionVerticalMM As Integer = -1)
        Dim posicionHorizontalMM As Integer
        Dim Negrita, Subrayado As Boolean
        Dim Ancho As Integer
        Dim NombreFuente, Alineacion As String
        Dim TamanioFuente As Single
        Dim Aux As XmlNode

        'Si no recibo la posición vertical, la tomo del archivo de configuración:
        If PosicionVerticalMM = -1 Then
            PosicionVerticalMM = CInt(n.Attributes.GetNamedItem("posicionVerticalMM").Value)
        End If

        posicionHorizontalMM = CInt(n.Attributes.GetNamedItem("posicionHorizontalMM").Value)

        Negrita = CBool(n.Attributes.GetNamedItem("negrita").Value)
        Subrayado = CBool(n.Attributes.GetNamedItem("subrayado").Value)

        NombreFuente = n.Attributes.GetNamedItem("nombreFuente").Value
        TamanioFuente = CSng(n.Attributes.GetNamedItem("tamanioFuente").Value)

        'Obtengo el ancho del texto a imprimir:
        Aux = n.Attributes.GetNamedItem("ancho")
        If Not (Aux Is Nothing) Then
            Ancho = Aux.Value

            'Obtengo la alineación:
            Aux = n.Attributes.GetNamedItem("alineacion")

            If Not (Aux Is Nothing) Then
                Alineacion = Aux.Value

                Select Case Alineacion
                    Case "D"
                        Valor = Valor.PadLeft(Ancho)
                    Case "I"
                        Valor = Valor.PadRight(Ancho)
                    Case "C"
                        Valor = Valor.PadLeft(Valor.Length + (Ancho - Valor.Length) / 2)
                End Select
            End If
        End If

        Me.mIMG.AgregarItem(Valor, posicionHorizontalMM, PosicionVerticalMM, NombreFuente, TamanioFuente, Negrita, Subrayado)
    End Sub
End Class
