﻿Public Class frmHistorial

    Public Sub abrirFormulario(ByVal titulo As String, ByVal datasource As Object)

        Text = titulo

        GridEX1.SetDataBinding(datasource, "")
        GridEX1.RetrieveStructure()
        
        DetectarCambios()

        Show()


    End Sub

    Private Sub DetectarCambios()
        Dim s As Janus.Windows.GridEX.GridEXFormatStyle
        s = New Janus.Windows.GridEX.GridEXFormatStyle
        s.BackColor = Color.Yellow

        For index As Integer = 1 To GridEX1.RowCount - 1
            Dim row = GridEX1.GetRow(index)
            Dim rowAnterior = GridEX1.GetRow(index - 1)
            For col As Integer = 0 To row.Cells.Count - 1
                If row.Cells(col).Text <> rowAnterior.Cells(col).Text Then
                    row.Cells(col).FormatStyle = s
                End If
            Next
        Next

    End Sub


End Class