﻿
Imports Common

Public Class frmPassword

#Region "Declaraciones"

    Dim d As New DataClasses1DataContext(getSettings("dbConnection"))
    Dim ret As Boolean = False

#End Region

#Region "Eventos"

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim u = d.proc_UsuariosLoadByPrimaryKey(USER_GLOBAL.idUsuario).ToList(0)

        ret = TextBox1.Text.Equals(u.password)

        If ret = False Then
            MsgBox("Clave incorrecta. Intente nuevamente")
            TextBox1.Focus()
        Else
            Me.Close()
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        ret = False
        Me.Close()
    End Sub

    Private Sub frmPassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "Ingrese la clave del usuario " & USER_GLOBAL.idUsuario
    End Sub

#End Region

#Region "Metodos"

    Public Function ConfirmarClave() As Boolean
        Me.ShowDialog()
        Return ret
    End Function

#End Region





End Class