﻿Imports Common
Public Class frmPagoAhorros

#Region "Declaraciones"

    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim _CTA As proc_ObtenerCuentaAhorroResult
    Dim _AbiertoConParametro As Boolean = False
    Dim _idPersona As Integer
    Dim _DataTableEncabezado As DataTable
    Dim _DataTableDetalle As DataTable

#End Region

#Region "Eventos"


    Private Sub frmPagoAhorros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not _AbiertoConParametro Then ObtenerPersonas()
    End Sub

    Private Sub btnReloadPropietarios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReloadPropietarios.Click
        ObtenerPersonas()

    End Sub

    Private Sub LinkLabel3_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        Dim f As New frmMovimientos
        f.AbrirForm(cboPropietario.SelectedValue)

    End Sub

    Private Sub btnAhorroObligatorio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAhorroObligatorio.Click
        RetirarAhorroObligatorio()
    End Sub

    Private Sub btnAhorroAdicional_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAhorroAdicional.Click
        RetirarAhorroAdicional()
    End Sub

#End Region

#Region "Metodos"

    Public Sub AbrirFormConPersona(ByVal idPersona As Integer)
        _AbiertoConParametro = True

        cboPropietario.DisplayMember = "Nombre"
        cboPropietario.ValueMember = "idPersona"
        cboPropietario.DataSource = d.proc_ObtenerPersonas.ToList

        cboPropietario.SelectedValue = idPersona
        cboPropietario.Enabled = False
        btnReloadPropietarios.Enabled = False

        Me.ShowDialog()
    End Sub

    Private Sub ObtenerPersonas()
        cboPropietario.DisplayMember = "Nombre"
        cboPropietario.ValueMember = "idPersona"
        cboPropietario.DataSource = d.proc_ObtenerPersonas.ToList


    End Sub

    Private Sub cboPropietario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPropietario.SelectedIndexChanged

        ObtenerCuentaAhorro()



    End Sub

    Private Sub ObtenerCuentaAhorro()
        Try
            Dim CTA = d.proc_ObtenerCuentaAhorro(Int32.Parse(cboPropietario.SelectedValue)).ToList.First
            _CTA = CTA

            Me.txtAhorroObligatorio.Value = CTA.importeAhorro
            Me.txtAhorroAdicional.Value = CTA.importeAhorroAdicional
            Me.txtDeuda.Value = CTA.importeDeuda

        Catch ex As Exception
            MsgBox("La persona seleccionada no posee cuenta de ahorro." & vbCrLf & "En la ventana de Personas seleccione el conductor y presione el botón 'Crear Cuenta de Ahorro' y luego intente esta operación nuevamente.")
            Me.txtAhorroObligatorio.Value = 0
            Me.txtAhorroAdicional.Value = 0
            Me.txtDeuda.Value = 0
        End Try

    End Sub

    Private Sub RetirarAhorroObligatorio()
        Try
            If txtRetiroObligatorio.Value > txtAhorroObligatorio.Value Then
                MsgBox("El monto que intenta retirar es mayor a lo que se tiene ahorrado." & vbCrLf & "Por favor corrija el monto e intente nuevamente")
                txtRetiroObligatorio.Focus()
                Exit Sub
            End If

            Dim ahorroMinimo = d.proc_ConfigGlobalLoadByPrimaryKey(0).ToList.First.montoMinimoAhorro
            If txtAhorroObligatorio.Value - txtRetiroObligatorio.Value < ahorroMinimo Then
                If MsgBox("Si retira ese monto quedarán menos de $" & ahorroMinimo & " pesos en la cuenta del conductor." & vbCrLf & "Desea retirar este monto igualmente?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    txtRetiroObligatorio.Focus()
                    Exit Sub
                End If

            End If

            Dim idMov As Integer
            d.proc_MovimientoCuentaAhorroInsert(idMov, _CTA.idPersona, NOW_GLOBAL, TipoMovimientos.EgresoAhorros, _
                                                    Nothing, Nothing, 0, txtRetiroObligatorio.Value, _CTA.importeAhorro - txtRetiroObligatorio.Value, _
                                                    _CTA.importeDeuda, 0, _CTA.importeAhorroAdicional, USER_GLOBAL.idUsuario)
            d.proc_CuentaAhorroUpdate(_CTA.idPersona, _CTA.FechaCreacion, NOW_GLOBAL, _CTA.importeAhorro - txtRetiroObligatorio.Value, _
                                       _CTA.importeAhorroAdicional, _CTA.importeDeuda)


            If MsgBox("Desea imprimir el comprobante de egreso?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                ImprimirTickete(idMov, "Egreso Ahorro Obligatorio")
            End If
            ObtenerCuentaAhorro()
            txtRetiroObligatorio.Value = 0
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub RetirarAhorroAdicional()
        Try
            If txtRetiroAdicional.Value > txtAhorroAdicional.Value Then
                MsgBox("El monto que intenta retirar es mayor a lo que se tiene ahorrado." & vbCrLf & "Por favor corrija el monto e intente nuevamente")
                txtRetiroAdicional.Focus()
                Exit Sub
            End If



            Dim idMov As Integer

            d.proc_MovimientoCuentaAhorroInsert(idMov, _CTA.idPersona, NOW_GLOBAL, TipoMovimientos.EgresoAhorroAdicional, _
                                                    Nothing, Nothing, txtRetiroAdicional.Value, 0, _CTA.importeAhorro, _
                                                    _CTA.importeDeuda, 0, _CTA.importeAhorroAdicional - txtRetiroAdicional.Value, USER_GLOBAL.idUsuario)
            d.proc_CuentaAhorroUpdate(_CTA.idPersona, _CTA.FechaCreacion, NOW_GLOBAL, _CTA.importeAhorro, _
                                       _CTA.importeAhorroAdicional - txtRetiroAdicional.Value, _CTA.importeDeuda)

            If MsgBox("Desea imprimir el comprobante de egreso?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                ImprimirTickete(idMov, "Egreso Ahorro Adicional")
            End If
            ObtenerCuentaAhorro()
            txtRetiroAdicional.Value = 0
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub



    Private Sub ImprimirTickete(ByVal ID As Integer, ByVal descrip As String)
        Dim ids As New clsImpresionDataSet
        Dim ps As New System.Drawing.Printing.PageSettings
        Dim size As New System.Drawing.Printing.PaperSize

        With ids
            .TipoImpresora = clsImpresionDataSet.TipoImpresora_t.Grafica

            size = New System.Drawing.Printing.PaperSize("76mm Roll Paper", 76, 297)

            ps.PaperSize = size

            .ConfiguracionPagina = ps
            .NombreDocumentoImpresion = "Vale"
            .PathArchivoXML = My.Application.Info.DirectoryPath & "\EgresoAhorro.xml"
            .DataSet = DataSetImpresion(ID, descrip)

            .Imprimir()

        End With

        'Hago esto porque si se quiere reimprimir, arroja un error al querer agregar nuevamente los DataTables al DataSet:
        ids.DataSet.Tables.Clear()
        ids = Nothing
    End Sub

    Private Function DataSetImpresion(ByVal id As Integer, ByVal Descrip As String) As DataSet
        Try
            Dim ret As New DataSet
            Dim dr As DataRow
            Dim movimiento = d.proc_MovimientoCuentaAhorroLoadByPrimaryKey(id).ToList.First
            Dim cta = d.proc_CuentaAhorroLoadByPrimaryKey(movimiento.idPersona).ToList.First
            Dim persona = d.proc_PersonasLoadByPrimaryKey(movimiento.idPersona).ToList.First

            _DataTableEncabezado = New DataTable
            _DataTableDetalle = New DataTable
            Me._DataTableEncabezado.Rows.Clear()
            Me._DataTableEncabezado.Columns.Add("Titulo")
            Me._DataTableEncabezado.Columns.Add("NIT")

            Me._DataTableEncabezado.Columns.Add("Descripcion")
            Me._DataTableEncabezado.Columns.Add("Persona")
            Me._DataTableEncabezado.Columns.Add("importePagado")
            Me._DataTableEncabezado.Columns.Add("ImporteAhorro")
            Me._DataTableEncabezado.Columns.Add("ImporteAhorroAdicional")
            Me._DataTableEncabezado.Columns.Add("importeDeuda")
            Me._DataTableEncabezado.Columns.Add("FechaRegistro")
            Me._DataTableEncabezado.Columns.Add("Usuario")
            Me._DataTableEncabezado.Columns.Add("Transaccion")

            dr = Me._DataTableEncabezado.NewRow

            dr.Item("Titulo") = getSettings("Titulo")
            dr.Item("NIT") = String.Format("{0}{1}Tel: 4129491{2}www.taxiportal.com.co", getSettings("NIT"), vbCrLf, vbCrLf)


            dr.Item("Descripcion") = Descrip
            dr.Item("Persona") = "Cond: " & Microsoft.VisualBasic.Left(persona.nombre, 25)
            dr.Item("importePagado") = "Ahorro Retirado: " & Format(movimiento.importeAhorro, "C")
            dr.Item("ImporteAhorro") = "Ahorro Oblig: " & Format(cta.importeAhorro, "C")
            dr.Item("ImporteAhorroAdicional") = "Ahorro Adic: " & Format(cta.importeAhorroAdicional, "C")
            dr.Item("importeDeuda") = "Deuda: " & Format(cta.importeDeuda, "C")
            dr.Item("FechaRegistro") = "Registro: " & movimiento.fechaMovimiento.Value.Date
            dr.Item("Usuario") = "Usuario: " & movimiento.usuario
            dr.Item("Transaccion") = "Transacción: " & movimiento.idMovimiento

            Me._DataTableEncabezado.Rows.Add(dr)

            ret.Tables.Add(Me._DataTableEncabezado)
            ret.Tables.Add(Me._DataTableDetalle)

            Return ret
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function
#End Region


End Class