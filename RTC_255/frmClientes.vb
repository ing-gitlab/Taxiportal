﻿Imports Common

Public Class frmClientes

#Region "Declaraciones"

    Dim esNuevo As Boolean = False

#End Region


#Region "Eventos"

    Private Sub Clientes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
        GridEX1.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic
        ObtenerBarrios()
        'GenerarIniciales()
        LlenarGrilla()

    End Sub


    Private Sub GridEX1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.SelectionChanged
        Try
            If GridEX1.GetRow.RowType = Janus.Windows.GridEX.RowType.FilterRow Or GridEX1.RowCount = 0 Then Exit Sub

            txtTelefono.Text = GridEX1.GetValue("Telefono")
            GridEX1.Tables(0).Columns("telefono").Caption = "Teléfono"

            txtNombre.Text = GridEX1.GetValue("Nombre")
            GridEX1.Tables(0).Columns("nombre").Caption = "Nombre"

            txtDirección.Text = GridEX1.GetValue("direccion")
            GridEX1.Tables(0).Columns("direccion").Caption = "Dirección"

            txtReferencia.Text = GridEX1.GetValue("Referencia")
            GridEX1.Tables(0).Columns("referencia").Caption = "Referencia"

            cboBarrio.SelectedValue = GridEX1.GetValue("idBarrio")

            txtObservaciones.Text = GridEX1.GetValue("Observaciones")
            GridEX1.Tables(0).Columns("observaciones").Caption = "Observaciones"

            txtObservaciones.Text = GridEX1.GetValue("nit")
            GridEX1.Tables(0).Columns("nit").Caption = "Nit"

            chkHabilitado.Checked = GridEX1.GetValue("Estado")
            chkUsaVale.Checked = GridEX1.GetValue("usavales")
        Catch ex As System.Exception
            Using d As New DataClasses1DataContext
                'd.proc_LogInsert(USER_GLOBAL.idUsuario, "", NOW_GLOBAL, "Error cambiando cliente")
                Try
                    d.proc_LogInsert(Now, USER_GLOBAL.idUsuario, "", "Error cambiando cliente")
                Catch exc As Exception
                    MsgBox(exc.Message)
                End Try

            End Using
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        esNuevo = True
        iniciarEdicion(True)

        txtTelefono.Text = ""
        txtReferencia.Text = ""
        txtNombre.Text = ""
        txtDirección.Text = ""
        txtObservaciones.Text = ""
        txtNit.Text = ""
        cboBarrio.SelectedIndex = -1
        chkHabilitado.Checked = True
        txtNombre.Focus()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        esNuevo = False

        iniciarEdicion(True)

        txtNombre.Focus()
    End Sub



    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim d As DataClasses1DataContext
        Dim idCliente As Integer
        Dim Posicion As Integer

        If validar() Then
            Try
                d = New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))

                If esNuevo Then
                    'Ingresar Clientes
                    Dim nuevo = d.proc_ClientesInsert(idCliente, txtNombre.Text, txtDirección.Text, txtReferencia.Text, txtTelefono.Text, cboBarrio.SelectedValue, chkHabilitado.Checked, txtObservaciones.Text, chkUsaVale.Checked, USER_GLOBAL.idUsuario, txtNit.Text)
                    d.proc_LogInsert(NOW_GLOBAL, USER_GLOBAL.idUsuario, "Cliente: " & idCliente.ToString & " Nombre: " & txtNombre.Text, "Inserta cliente")
                Else
                    'modificar Clientes
                    Posicion = GridEX1.GetRow().Position
                    d.proc_ClientesUpdate(GridEX1.GetValue("idCliente"), txtNombre.Text, txtDirección.Text, txtReferencia.Text, txtTelefono.Text, cboBarrio.SelectedValue, chkHabilitado.Checked, txtObservaciones.Text, chkUsaVale.Checked, USER_GLOBAL.idUsuario, txtNit.Text)
                    d.proc_LogInsert(NOW_GLOBAL, USER_GLOBAL.idUsuario, "Cliente: " & GridEX1.GetValue("idCliente").ToString & " Nombre: " & txtNombre.Text, "Modifica cliente")
                End If

                iniciarEdicion(False)
                LlenarGrilla()

                If esNuevo Then
                    Posicion = GridEX1.RowCount - 1
                End If

                GridEX1.MoveTo(Posicion)
                GridEX1.Focus()
            Catch ex As Exception

            End Try

        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        iniciarEdicion(False)
        LlenarGrilla()
        GridEX1.Focus()
    End Sub



    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Try
            If MsgBox("¿Está seguro de eliminar el cliente?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Confirmación de eliminación") = MsgBoxResult.Yes Then
                Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
                d.proc_ClientesDelete(GridEX1.GetValue("idCliente"))
                LlenarGrilla()
            End If
        Catch ex As Exception
            Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            d.proc_LogInsert(USER_GLOBAL.idUsuario, "Integridad referencial", NOW_GLOBAL, "Error eliminando Cliente")
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub cboInicial_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LlenarGrilla()
    End Sub



#End Region

#Region "Metodos"

    'Private Sub GenerarIniciales()

    '    For index As Integer = 65 To 90
    '        cboInicial.Items.Add(Chr(index))
    '    Next

    '    cboInicial.SelectedIndex = 0
    'End Sub

    Private Sub ObtenerBarrios()
        Using d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            Dim ListaBarrios = d.proc_ObtenerBarriosTodos.ToList()
            cboBarrio.DisplayMember = "Barrio"
            cboBarrio.ValueMember = "idBarrio"
            cboBarrio.DataSource = ListaBarrios
        End Using
    End Sub

    'Private Function validaciones() As Boolean
    '    If ValidarNombre() ValidarTelefono()AndAlso ValidarBarrio() AndAlso ValidarDireccion() AndAlso ValidarReferencia()=true  AndAlso  Then
    '        Return False
    '    End If
    '    Return True
    'End Function



    Private Function ValidarBarrio() As Boolean
        If cboBarrio.SelectedIndex < 0 Then
            MsgBox("Debe Seleccionar Un Barrio", MsgBoxStyle.Critical, "Error")
            cboBarrio.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidarDireccion()
        If txtDirección.Text = "" Then
            MsgBox("Debe Ingresar La Dirección", MsgBoxStyle.Critical, "Error")
            txtDirección.Focus()
            Return False
        End If

        Return True
    End Function
    Private Function ValidarNombre()
        If txtNombre.Text = "" Then
            MsgBox("Debe Ingresar El Nombre", MsgBoxStyle.Critical, "Error")
            txtNombre.Focus()
            Return False
        End If
        Return True
    End Function
    Private Function ValidarReferencia()
        If txtReferencia.Text = "" Then
            MsgBox(" Ingresar Una Referencia", MsgBoxStyle.Critical, "Error")
            txtReferencia.Focus()
            Return False
        End If
        Return True
    End Function
    Private Function ValidarTelefono()
        If txtTelefono.Text = "" Then
            MsgBox("Ingrear Numero De Telefono", MsgBoxStyle.Critical, "Error")
            txtTelefono.Focus()
            Return False
        End If
        Return True
    End Function

    Private Function ValidarNit()
        If txtNit.Text = "" Then
            MsgBox("Ingrear Nit", MsgBoxStyle.Critical, "Error")
            txtNit.Focus()
            Return False
        End If
        Return True
    End Function

    Private Sub iniciarEdicion(ByVal esInicio As Boolean)
        GridEX1.Enabled = Not esInicio

        btnNuevo.Visible = Not esInicio
        btnModificar.Visible = Not esInicio
        btnEliminar.Visible = Not esInicio
        txtTelefono.ReadOnly = Not esInicio
        txtNombre.ReadOnly = Not esInicio
        txtReferencia.ReadOnly = Not esInicio
        txtObservaciones.ReadOnly = Not esInicio
        txtDirección.ReadOnly = Not esInicio
        txtNit.ReadOnly = Not esInicio
        chkHabilitado.Enabled = esInicio
        chkUsaVale.Enabled = esInicio
        cboBarrio.Enabled = esInicio
        btnAceptar.Visible = esInicio
        btnCancelar.Visible = esInicio
        'cboInicial.Enabled = Not esInicio
    End Sub

    Private Sub LlenarGrilla()

        Try
            Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))

            Me.GridEX1.SetDataBinding(d.proc_ClientesLoadAll.ToList, String.Empty)
            Me.GridEX1.RetrieveStructure()
            Me.GridEX1.RootTable.Columns("idCliente").Visible = False
            Me.GridEX1.RootTable.Columns("idBarrio").Visible = False
            Me.GridEX1.RootTable.Columns("idUsuario").Visible = False
            Me.GridEX1.RootTable.Columns("estado").Caption = "Habilitado"
            Me.GridEX1.RootTable.Columns("usaVales").Caption = "Usa Vales"
            Me.GridEX1.RootTable.Columns("nit").Caption = "Nit"
            Me.GridEX1.RootTable.Columns("estado").ColumnType = Janus.Windows.GridEX.ColumnType.CheckBox
            Me.GridEX1.RootTable.Columns("estado").EditType = Janus.Windows.GridEX.EditType.CheckBox
            Me.GridEX1.RootTable.Columns("usaVales").ColumnType = Janus.Windows.GridEX.ColumnType.CheckBox
            Me.GridEX1.RootTable.Columns("usaVales").EditType = Janus.Windows.GridEX.EditType.CheckBox

            For Each centrar As Janus.Windows.GridEX.GridEXColumn In GridEX1.Tables(0).Columns
                centrar.HeaderAlignment = Janus.Windows.GridEX.TextAlignment.Center
            Next

            btnModificar.Enabled = (GridEX1.RowCount > 0)
            btnEliminar.Enabled = (GridEX1.RowCount > 0)

        Catch ex As Exception

        End Try




    End Sub


    Private Function validar()
        If Not ValidarNombre() Then
            Return False
        End If
        If Not ValidarTelefono() Then
            Return False
        End If
        If Not ValidarNit() Then
            Return False
        End If
        If Not ValidarDireccion() Then
            Return False
        End If
        'If Not ValidarReferencia() Then
        '    Return False
        'End If
        If Not ValidarBarrio() Then
            Return False
        End If
        Return True
    End Function


    'Private Function Validar() As Boolean
    '    If txtNombre.Text = "" Then
    '        MsgBox("Debe Ingresar El Nombre", MsgBoxStyle.Critical, "Error")
    '        txtNombre.Focus()
    '        Return False
    '    End If
    '    If txtTelefono.Text = "" Then
    '        MsgBox("Ingrear Numero De Telefono", MsgBoxStyle.Critical, "Error")
    '        txtTelefono.Focus()
    '        Return False
    '    End If
    '    If txtDirección.Text = "" Then
    '        MsgBox("Debe Ingresar La Dirección", MsgBoxStyle.Critical, "Error")
    '        txtDirección.Focus()
    '        Return False
    '    End If
    '    If txtReferencia.Text = "" Then
    '        MsgBox(" Ingresar Una Referencia", MsgBoxStyle.Critical, "Error")
    '        txtReferencia.Focus()
    '        Return False
    '    End If
    '    Return True
    'End Function



#End Region











End Class