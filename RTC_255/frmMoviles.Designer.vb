﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMoviles
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMoviles))
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPlaca = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtnroMovil = New System.Windows.Forms.TextBox()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.chkSi = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtModelo = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdActualizar = New Janus.Windows.EditControls.UIButton()
        Me.btnRecaudacion = New System.Windows.Forms.Button()
        Me.btnMovimientos = New System.Windows.Forms.Button()
        Me.btnPrestamo = New System.Windows.Forms.Button()
        Me.btnAhorro = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnLicenciaNoDisp = New System.Windows.Forms.Button()
        Me.btnTarjetaNoDisp = New System.Windows.Forms.Button()
        Me.btnSOATNoDisp = New System.Windows.Forms.Button()
        Me.btnLicencia = New System.Windows.Forms.Button()
        Me.pbLicencia = New System.Windows.Forms.PictureBox()
        Me.pbTarjeta = New System.Windows.Forms.PictureBox()
        Me.pbSoat = New System.Windows.Forms.PictureBox()
        Me.btnTarjeta = New System.Windows.Forms.Button()
        Me.btnSoat = New System.Windows.Forms.Button()
        Me.dtpAfiliacion = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtAño = New System.Windows.Forms.NumericUpDown()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtMarca = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnReloadConductor = New System.Windows.Forms.Button()
        Me.cboConductor = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnReloadPropietarios = New System.Windows.Forms.Button()
        Me.cboPropietario = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.BtnHistorial = New System.Windows.Forms.Button()
        Me.lbCantidad = New System.Windows.Forms.Label()
        Me.EstadoSoat = New System.Windows.Forms.Label()
        Me.dtpSoat = New System.Windows.Forms.DateTimePicker()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.GridEX1 = New Janus.Windows.GridEX.GridEX()
        Me.OpenFileDialog3 = New System.Windows.Forms.OpenFileDialog()
        Me.ToolTip3 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.pbLicencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbSoat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAño, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "Archivo jpg|*.jpg|Archivo gif|*.gif|Archivo bmp|*.bmp|Todos Los Archivos|*.*"
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.Filter = "Archivo jpg|*.jpg|Archivo gif|*.gif|Archivo bmp|*.bmp|Todos Los Archivos|*.*"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 297)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "&Placa:"
        '
        'txtPlaca
        '
        Me.txtPlaca.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtPlaca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPlaca.Location = New System.Drawing.Point(145, 290)
        Me.txtPlaca.MaxLength = 7
        Me.txtPlaca.Name = "txtPlaca"
        Me.txtPlaca.ReadOnly = True
        Me.txtPlaca.Size = New System.Drawing.Size(200, 20)
        Me.txtPlaca.TabIndex = 4
        Me.txtPlaca.Text = " "
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 263)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Número del Mó&vil:"
        '
        'txtnroMovil
        '
        Me.txtnroMovil.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtnroMovil.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtnroMovil.Location = New System.Drawing.Point(145, 256)
        Me.txtnroMovil.MaxLength = 4
        Me.txtnroMovil.Name = "txtnroMovil"
        Me.txtnroMovil.ReadOnly = True
        Me.txtnroMovil.Size = New System.Drawing.Size(200, 20)
        Me.txtnroMovil.TabIndex = 2
        '
        'btnNuevo
        '
        Me.btnNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnNuevo.Location = New System.Drawing.Point(157, 538)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnModificar.Location = New System.Drawing.Point(238, 538)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 1
        Me.btnModificar.Text = "Mo&dificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.Location = New System.Drawing.Point(157, 538)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 24
        Me.btnAceptar.Text = "&Guardar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        Me.btnAceptar.Visible = False
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.Location = New System.Drawing.Point(238, 538)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 25
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        Me.btnCancelar.Visible = False
        '
        'chkSi
        '
        Me.chkSi.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkSi.AutoSize = True
        Me.chkSi.Enabled = False
        Me.chkSi.Location = New System.Drawing.Point(429, 259)
        Me.chkSi.Name = "chkSi"
        Me.chkSi.Size = New System.Drawing.Size(104, 17)
        Me.chkSi.TabIndex = 21
        Me.chkSi.Text = "Movil  &Habilitado"
        Me.chkSi.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 360)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "M&odelo:"
        '
        'txtModelo
        '
        Me.txtModelo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtModelo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModelo.Location = New System.Drawing.Point(145, 353)
        Me.txtModelo.MaxLength = 7
        Me.txtModelo.Name = "txtModelo"
        Me.txtModelo.ReadOnly = True
        Me.txtModelo.Size = New System.Drawing.Size(200, 20)
        Me.txtModelo.TabIndex = 8
        Me.txtModelo.Text = " "
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.cmdActualizar)
        Me.GroupBox1.Controls.Add(Me.btnRecaudacion)
        Me.GroupBox1.Controls.Add(Me.btnMovimientos)
        Me.GroupBox1.Controls.Add(Me.btnPrestamo)
        Me.GroupBox1.Controls.Add(Me.btnAhorro)
        Me.GroupBox1.Controls.Add(Me.CheckBox1)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.dtpAfiliacion)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtAño)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtMarca)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.btnReloadConductor)
        Me.GroupBox1.Controls.Add(Me.cboConductor)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.btnReloadPropietarios)
        Me.GroupBox1.Controls.Add(Me.cboPropietario)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.BtnHistorial)
        Me.GroupBox1.Controls.Add(Me.lbCantidad)
        Me.GroupBox1.Controls.Add(Me.EstadoSoat)
        Me.GroupBox1.Controls.Add(Me.dtpSoat)
        Me.GroupBox1.Controls.Add(Me.btnEliminar)
        Me.GroupBox1.Controls.Add(Me.txtModelo)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.chkSi)
        Me.GroupBox1.Controls.Add(Me.btnCancelar)
        Me.GroupBox1.Controls.Add(Me.btnAceptar)
        Me.GroupBox1.Controls.Add(Me.btnModificar)
        Me.GroupBox1.Controls.Add(Me.btnNuevo)
        Me.GroupBox1.Controls.Add(Me.txtnroMovil)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtPlaca)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.GridEX1)
        Me.GroupBox1.Location = New System.Drawing.Point(3, -2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1005, 567)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'cmdActualizar
        '
        Me.cmdActualizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdActualizar.Icon = CType(resources.GetObject("cmdActualizar.Icon"), System.Drawing.Icon)
        Me.cmdActualizar.Location = New System.Drawing.Point(577, 256)
        Me.cmdActualizar.Name = "cmdActualizar"
        Me.cmdActualizar.Size = New System.Drawing.Size(114, 20)
        Me.cmdActualizar.TabIndex = 33
        Me.cmdActualizar.Text = "Actualizar"
        Me.cmdActualizar.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003
        '
        'btnRecaudacion
        '
        Me.btnRecaudacion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRecaudacion.BackColor = System.Drawing.Color.Transparent
        Me.btnRecaudacion.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.recauda2
        Me.btnRecaudacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnRecaudacion.Location = New System.Drawing.Point(969, 383)
        Me.btnRecaudacion.Name = "btnRecaudacion"
        Me.btnRecaudacion.Size = New System.Drawing.Size(30, 30)
        Me.btnRecaudacion.TabIndex = 30
        Me.btnRecaudacion.UseVisualStyleBackColor = False
        '
        'btnMovimientos
        '
        Me.btnMovimientos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMovimientos.BackColor = System.Drawing.Color.Transparent
        Me.btnMovimientos.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.file2
        Me.btnMovimientos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMovimientos.Location = New System.Drawing.Point(969, 415)
        Me.btnMovimientos.Name = "btnMovimientos"
        Me.btnMovimientos.Size = New System.Drawing.Size(30, 30)
        Me.btnMovimientos.TabIndex = 31
        Me.btnMovimientos.UseVisualStyleBackColor = False
        '
        'btnPrestamo
        '
        Me.btnPrestamo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrestamo.BackColor = System.Drawing.Color.Transparent
        Me.btnPrestamo.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.dollar
        Me.btnPrestamo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnPrestamo.Location = New System.Drawing.Point(969, 351)
        Me.btnPrestamo.Name = "btnPrestamo"
        Me.btnPrestamo.Size = New System.Drawing.Size(30, 30)
        Me.btnPrestamo.TabIndex = 29
        Me.btnPrestamo.UseVisualStyleBackColor = False
        '
        'btnAhorro
        '
        Me.btnAhorro.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAhorro.BackColor = System.Drawing.Color.Transparent
        Me.btnAhorro.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.alcancia
        Me.btnAhorro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAhorro.Location = New System.Drawing.Point(969, 319)
        Me.btnAhorro.Name = "btnAhorro"
        Me.btnAhorro.Size = New System.Drawing.Size(30, 30)
        Me.btnAhorro.TabIndex = 28
        Me.btnAhorro.UseVisualStyleBackColor = False
        '
        'CheckBox1
        '
        Me.CheckBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(429, 311)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(91, 17)
        Me.CheckBox1.TabIndex = 22
        Me.CheckBox1.Text = "Ver Imágenes"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.btnLicenciaNoDisp)
        Me.GroupBox2.Controls.Add(Me.btnTarjetaNoDisp)
        Me.GroupBox2.Controls.Add(Me.btnSOATNoDisp)
        Me.GroupBox2.Controls.Add(Me.btnLicencia)
        Me.GroupBox2.Controls.Add(Me.pbLicencia)
        Me.GroupBox2.Controls.Add(Me.pbTarjeta)
        Me.GroupBox2.Controls.Add(Me.pbSoat)
        Me.GroupBox2.Controls.Add(Me.btnTarjeta)
        Me.GroupBox2.Controls.Add(Me.btnSoat)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(429, 323)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(518, 228)
        Me.GroupBox2.TabIndex = 23
        Me.GroupBox2.TabStop = False
        '
        'btnLicenciaNoDisp
        '
        Me.btnLicenciaNoDisp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnLicenciaNoDisp.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.delete
        Me.btnLicenciaNoDisp.Enabled = False
        Me.btnLicenciaNoDisp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLicenciaNoDisp.Location = New System.Drawing.Point(494, 46)
        Me.btnLicenciaNoDisp.Name = "btnLicenciaNoDisp"
        Me.btnLicenciaNoDisp.Size = New System.Drawing.Size(16, 17)
        Me.btnLicenciaNoDisp.TabIndex = 4
        Me.btnLicenciaNoDisp.Text = "..."
        Me.btnLicenciaNoDisp.UseVisualStyleBackColor = True
        '
        'btnTarjetaNoDisp
        '
        Me.btnTarjetaNoDisp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnTarjetaNoDisp.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.delete
        Me.btnTarjetaNoDisp.Enabled = False
        Me.btnTarjetaNoDisp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTarjetaNoDisp.Location = New System.Drawing.Point(316, 46)
        Me.btnTarjetaNoDisp.Name = "btnTarjetaNoDisp"
        Me.btnTarjetaNoDisp.Size = New System.Drawing.Size(16, 17)
        Me.btnTarjetaNoDisp.TabIndex = 2
        Me.btnTarjetaNoDisp.Text = "..."
        Me.btnTarjetaNoDisp.UseVisualStyleBackColor = True
        '
        'btnSOATNoDisp
        '
        Me.btnSOATNoDisp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSOATNoDisp.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.delete
        Me.btnSOATNoDisp.Enabled = False
        Me.btnSOATNoDisp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSOATNoDisp.Location = New System.Drawing.Point(141, 46)
        Me.btnSOATNoDisp.Name = "btnSOATNoDisp"
        Me.btnSOATNoDisp.Size = New System.Drawing.Size(16, 17)
        Me.btnSOATNoDisp.TabIndex = 0
        Me.btnSOATNoDisp.Text = "..."
        Me.btnSOATNoDisp.UseVisualStyleBackColor = True
        '
        'btnLicencia
        '
        Me.btnLicencia.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnLicencia.Enabled = False
        Me.btnLicencia.Location = New System.Drawing.Point(356, 159)
        Me.btnLicencia.Name = "btnLicencia"
        Me.btnLicencia.Size = New System.Drawing.Size(156, 23)
        Me.btnLicencia.TabIndex = 5
        Me.btnLicencia.Text = "Cargar &Licencia"
        Me.btnLicencia.UseVisualStyleBackColor = True
        '
        'pbLicencia
        '
        Me.pbLicencia.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pbLicencia.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pbLicencia.Location = New System.Drawing.Point(356, 46)
        Me.pbLicencia.Name = "pbLicencia"
        Me.pbLicencia.Size = New System.Drawing.Size(156, 107)
        Me.pbLicencia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbLicencia.TabIndex = 29
        Me.pbLicencia.TabStop = False
        '
        'pbTarjeta
        '
        Me.pbTarjeta.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pbTarjeta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pbTarjeta.Location = New System.Drawing.Point(178, 46)
        Me.pbTarjeta.Name = "pbTarjeta"
        Me.pbTarjeta.Size = New System.Drawing.Size(156, 107)
        Me.pbTarjeta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbTarjeta.TabIndex = 25
        Me.pbTarjeta.TabStop = False
        '
        'pbSoat
        '
        Me.pbSoat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pbSoat.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pbSoat.Location = New System.Drawing.Point(4, 46)
        Me.pbSoat.Name = "pbSoat"
        Me.pbSoat.Size = New System.Drawing.Size(155, 107)
        Me.pbSoat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbSoat.TabIndex = 24
        Me.pbSoat.TabStop = False
        '
        'btnTarjeta
        '
        Me.btnTarjeta.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnTarjeta.Enabled = False
        Me.btnTarjeta.Location = New System.Drawing.Point(178, 159)
        Me.btnTarjeta.Name = "btnTarjeta"
        Me.btnTarjeta.Size = New System.Drawing.Size(156, 23)
        Me.btnTarjeta.TabIndex = 3
        Me.btnTarjeta.Text = "Cargar &Tarjeta"
        Me.btnTarjeta.UseVisualStyleBackColor = True
        '
        'btnSoat
        '
        Me.btnSoat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSoat.Enabled = False
        Me.btnSoat.Location = New System.Drawing.Point(6, 159)
        Me.btnSoat.Name = "btnSoat"
        Me.btnSoat.Size = New System.Drawing.Size(153, 23)
        Me.btnSoat.TabIndex = 1
        Me.btnSoat.Text = "Cargar &Soat"
        Me.btnSoat.UseVisualStyleBackColor = True
        '
        'dtpAfiliacion
        '
        Me.dtpAfiliacion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtpAfiliacion.Enabled = False
        Me.dtpAfiliacion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAfiliacion.Location = New System.Drawing.Point(145, 494)
        Me.dtpAfiliacion.Name = "dtpAfiliacion"
        Me.dtpAfiliacion.Size = New System.Drawing.Size(91, 20)
        Me.dtpAfiliacion.TabIndex = 20
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(22, 504)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(54, 13)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "&Fecha Afil"
        '
        'txtAño
        '
        Me.txtAño.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtAño.Location = New System.Drawing.Point(145, 383)
        Me.txtAño.Maximum = New Decimal(New Integer() {2200, 0, 0, 0})
        Me.txtAño.Minimum = New Decimal(New Integer() {1900, 0, 0, 0})
        Me.txtAño.Name = "txtAño"
        Me.txtAño.ReadOnly = True
        Me.txtAño.Size = New System.Drawing.Size(52, 20)
        Me.txtAño.TabIndex = 10
        Me.txtAño.Value = New Decimal(New Integer() {2010, 0, 0, 0})
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(22, 389)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(29, 13)
        Me.Label11.TabIndex = 9
        Me.Label11.Text = "A&ño:"
        '
        'txtMarca
        '
        Me.txtMarca.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarca.Location = New System.Drawing.Point(145, 319)
        Me.txtMarca.MaxLength = 50
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.ReadOnly = True
        Me.txtMarca.Size = New System.Drawing.Size(200, 20)
        Me.txtMarca.TabIndex = 6
        Me.txtMarca.Text = " "
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(22, 326)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 13)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "&Marca:"
        '
        'btnReloadConductor
        '
        Me.btnReloadConductor.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReloadConductor.Enabled = False
        Me.btnReloadConductor.Location = New System.Drawing.Point(294, 445)
        Me.btnReloadConductor.Name = "btnReloadConductor"
        Me.btnReloadConductor.Size = New System.Drawing.Size(16, 23)
        Me.btnReloadConductor.TabIndex = 16
        Me.btnReloadConductor.Text = "..."
        Me.btnReloadConductor.UseVisualStyleBackColor = True
        '
        'cboConductor
        '
        Me.cboConductor.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboConductor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboConductor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboConductor.DisplayMember = "nombre"
        Me.cboConductor.Enabled = False
        Me.cboConductor.FormattingEnabled = True
        Me.cboConductor.Location = New System.Drawing.Point(145, 441)
        Me.cboConductor.Name = "cboConductor"
        Me.cboConductor.Size = New System.Drawing.Size(168, 21)
        Me.cboConductor.TabIndex = 15
        Me.cboConductor.ValueMember = "idAdministrador"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(22, 449)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "&Conductor:"
        '
        'btnReloadPropietarios
        '
        Me.btnReloadPropietarios.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReloadPropietarios.Enabled = False
        Me.btnReloadPropietarios.Location = New System.Drawing.Point(294, 413)
        Me.btnReloadPropietarios.Name = "btnReloadPropietarios"
        Me.btnReloadPropietarios.Size = New System.Drawing.Size(16, 23)
        Me.btnReloadPropietarios.TabIndex = 13
        Me.btnReloadPropietarios.Text = "..."
        Me.btnReloadPropietarios.UseVisualStyleBackColor = True
        '
        'cboPropietario
        '
        Me.cboPropietario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboPropietario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboPropietario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPropietario.DisplayMember = "nombre"
        Me.cboPropietario.Enabled = False
        Me.cboPropietario.FormattingEnabled = True
        Me.cboPropietario.Location = New System.Drawing.Point(145, 409)
        Me.cboPropietario.Name = "cboPropietario"
        Me.cboPropietario.Size = New System.Drawing.Size(168, 21)
        Me.cboPropietario.TabIndex = 12
        Me.cboPropietario.ValueMember = "idAdministrador"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(22, 417)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "&Propietario:"
        '
        'BtnHistorial
        '
        Me.BtnHistorial.AccessibleDescription = ""
        Me.BtnHistorial.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnHistorial.BackColor = System.Drawing.Color.Transparent
        Me.BtnHistorial.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.historial
        Me.BtnHistorial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnHistorial.Location = New System.Drawing.Point(969, 254)
        Me.BtnHistorial.Name = "BtnHistorial"
        Me.BtnHistorial.Size = New System.Drawing.Size(30, 30)
        Me.BtnHistorial.TabIndex = 26
        Me.BtnHistorial.UseVisualStyleBackColor = False
        '
        'lbCantidad
        '
        Me.lbCantidad.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbCantidad.AutoSize = True
        Me.lbCantidad.Location = New System.Drawing.Point(871, 248)
        Me.lbCantidad.Name = "lbCantidad"
        Me.lbCantidad.Size = New System.Drawing.Size(39, 13)
        Me.lbCantidad.TabIndex = 32
        Me.lbCantidad.Text = "Label5"
        '
        'EstadoSoat
        '
        Me.EstadoSoat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.EstadoSoat.AutoSize = True
        Me.EstadoSoat.Location = New System.Drawing.Point(22, 475)
        Me.EstadoSoat.Name = "EstadoSoat"
        Me.EstadoSoat.Size = New System.Drawing.Size(63, 13)
        Me.EstadoSoat.TabIndex = 17
        Me.EstadoSoat.Text = "Venc. Soat:"
        '
        'dtpSoat
        '
        Me.dtpSoat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtpSoat.Enabled = False
        Me.dtpSoat.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpSoat.Location = New System.Drawing.Point(145, 468)
        Me.dtpSoat.Name = "dtpSoat"
        Me.dtpSoat.Size = New System.Drawing.Size(91, 20)
        Me.dtpSoat.TabIndex = 18
        '
        'btnEliminar
        '
        Me.btnEliminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEliminar.BackgroundImage = Global.TaxiPortal.My.Resources.Resources.delete
        Me.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnEliminar.Location = New System.Drawing.Point(969, 287)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(30, 30)
        Me.btnEliminar.TabIndex = 27
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'GridEX1
        '
        Me.GridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEX1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridEX1.ColumnAutoResize = True
        Me.GridEX1.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular
        Me.GridEX1.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic
        Me.GridEX1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GridEX1.Location = New System.Drawing.Point(6, 19)
        Me.GridEX1.Name = "GridEX1"
        Me.GridEX1.Size = New System.Drawing.Size(993, 226)
        Me.GridEX1.TabIndex = 0
        Me.GridEX1.TabKeyBehavior = Janus.Windows.GridEX.TabKeyBehavior.ControlNavigation
        '
        'OpenFileDialog3
        '
        Me.OpenFileDialog3.Filter = "Archivo jpg|*.jpg|Archivo gif|*.gif|Archivo bmp|*.bmp|Todos Los Archivos|*.*"
        '
        'frmMoviles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 576)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(933, 568)
        Me.Name = "frmMoviles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Moviles"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.pbLicencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbSoat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAño, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents OpenFileDialog2 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPlaca As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtnroMovil As System.Windows.Forms.TextBox
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents chkSi As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtModelo As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog3 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents GridEX1 As Janus.Windows.GridEX.GridEX
    Friend WithEvents EstadoSoat As System.Windows.Forms.Label
    Friend WithEvents dtpSoat As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbCantidad As System.Windows.Forms.Label
    Friend WithEvents BtnHistorial As System.Windows.Forms.Button
    Friend WithEvents ToolTip2 As System.Windows.Forms.ToolTip
    Friend WithEvents txtAño As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtMarca As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnReloadConductor As System.Windows.Forms.Button
    Friend WithEvents cboConductor As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnReloadPropietarios As System.Windows.Forms.Button
    Friend WithEvents cboPropietario As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpAfiliacion As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnLicencia As System.Windows.Forms.Button
    Friend WithEvents pbLicencia As System.Windows.Forms.PictureBox
    Friend WithEvents pbTarjeta As System.Windows.Forms.PictureBox
    Friend WithEvents pbSoat As System.Windows.Forms.PictureBox
    Friend WithEvents btnTarjeta As System.Windows.Forms.Button
    Friend WithEvents btnSoat As System.Windows.Forms.Button
    Friend WithEvents btnLicenciaNoDisp As System.Windows.Forms.Button
    Friend WithEvents btnTarjetaNoDisp As System.Windows.Forms.Button
    Friend WithEvents btnSOATNoDisp As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents btnRecaudacion As System.Windows.Forms.Button
    Friend WithEvents btnMovimientos As System.Windows.Forms.Button
    Friend WithEvents btnPrestamo As System.Windows.Forms.Button
    Friend WithEvents btnAhorro As System.Windows.Forms.Button
    Friend WithEvents ToolTip3 As System.Windows.Forms.ToolTip
    Friend WithEvents cmdActualizar As Janus.Windows.EditControls.UIButton
End Class
