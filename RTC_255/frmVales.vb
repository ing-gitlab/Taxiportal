﻿Imports Common

Public Class frmVales

#Region "Declaraciones"
    Dim esNuevo As Boolean = True
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim _DataTableEncabezado As DataTable
    Dim _DataTableDetalle As DataTable
    Dim _idConductor As Integer
    Dim barrio1 As Integer = 0
    Dim barrio2 As Integer = 0
    Public cierre As Boolean = False
    Public condicion As Boolean = False
    Dim _ListaVehiculo As proc_ObtenerVehiculoPorNroMovilResult
    Dim _ListaRecorridos As New List(Of RecorridosVale)
    Public idVale As Integer = 0
#End Region


#Region "Eventos"

    Private Sub frmVales_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        cierre = True
    End Sub

    Private Sub frmVales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
        Me.ToolTip1.SetToolTip(btnHistorialVales, "Historial de Vales")
        Me.ToolTip1.SetToolTip(btnImprimir, "Imprimir Vale")
        Me.ToolTip1.SetToolTip(btnEliminar, "Eliminar Vale")

        Me.dtpDesde.Value = NOW_GLOBAL.Date
        Me.dtpHasta.Value = New Date(NOW_GLOBAL.Year, NOW_GLOBAL.Month, NOW_GLOBAL.Day).AddDays(1)
        obtenerDependendencias()
        ObtenerPasajeros()

        ObtenerConductor()

        ObtenerClientes()

        ObtenerVales()

        ObtenerRecorridosVale()
    End Sub

    Private Sub cmdActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActualizar.Click
        esNuevo = False
        ObtenerVales()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        esNuevo = True

        HabilitarControles()

        _ListaRecorridos.Clear()
        GridEX2.Refetch()

        If condicion = False Then
            txtvale.Text = String.Empty
            txtTarifa.Text = String.Empty
            txtMovil.Text = String.Empty
        End If

        cboCliente.Focus()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try

            Dim idVale As Integer
            Dim idRecorrido As Integer


            If validar() Then
                If esNuevo Then

                    d.proc_ValesInsert(idVale, dtpFechaPago.Value, txtvale.Value, CDbl(txtTarifa.Value), USER_GLOBAL.idUsuario, _ListaVehiculo.idVehiculo, cboConductor.SelectedValue, cboCliente.SelectedValue, cboCECO.Text, txtPasajero.Text, txtTarifa.Text, NOW_GLOBAL, Nothing, 0, 0)
                Else
                    idVale = GridEX1.GetValue("idVale")
                    d.proc_ValesUpdate(idVale, dtpFechaPago.Value, txtvale.Value, CDbl(txtTarifa.Value), USER_GLOBAL.idUsuario, _ListaVehiculo.idVehiculo, cboConductor.SelectedValue, cboCliente.SelectedValue, cboCECO.Text, txtPasajero.Text, txtTarifa.Text, NOW_GLOBAL, Nothing, 0, 0)
                End If

                d.proc_RecorridosValeDeleteporVale(idVale)
                d.proc_RecorridosValeDeleteporValeliquidado(idVale)

                For Each recorrido In _ListaRecorridos
                    If recorrido.TipoRecorrido = 1 Or esNuevo = False Then
                        d.proc_RecorridosValeLiquidadoInsert(idRecorrido, idVale, recorrido.Origen, recorrido.Destino, recorrido.Tarifa)
                    Else
                        d.proc_RecorridosValeInsert(idRecorrido, idVale, recorrido.idBarrioDestino, recorrido.idBarrioOrigen, recorrido.Tarifa)
                    End If
                Next

            End If

            Me.idVale = idVale

            HabilitarControles()
            ObtenerVales()
            ' GridEX1.Find(GridEX1.RootTable.Columns("idVale"), Janus.Windows.GridEX.ConditionOperator.Equal, idVale, 1, 1)
            GridEX1.Focus()
            If condicion Then
                cierre = True
                Me.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        esNuevo = False
        DetectarVehiculo()
        HabilitarControles()
        txtvale.Focus()

    End Sub

    Private Sub GridEX2_RecordAdded(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX2.RecordAdded
        'GridEX2.Refetch()

    End Sub


    Private Sub GridEX2_RecordUpdated(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX2.RecordUpdated


        Try
            _ListaRecorridos.Clear()

            For index As Integer = 0 To GridEX2.RowCount - 1
                Dim rec As New RecorridosVale
                rec.idBarrioOrigen = GridEX2.GetRow(index).Cells("idBarrioOrigen").Text
                rec.idBarrioDestino = GridEX2.GetRow(index).Cells("idbarrioDestino").Text
                rec.idRecorridoVale = GridEX2.GetRow(index).Cells("idRecorridoVale").Text
                rec.Tarifa = GridEX2.GetRow(index).Cells("Tarifa").Text
                rec.Origen = GridEX2.GetRow(index).Cells("Origen").Text
                rec.Destino = GridEX2.GetRow(index).Cells("Destino").Text
                _ListaRecorridos.Add(rec)

            Next
            CalcularTotalTarifa()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub GridEX1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.SelectionChanged
        '_ListaRecorridos.Clear()
        esNuevo = False
        Try
            If Not GridEX1.GetRow.RowType = Janus.Windows.GridEX.RowType.Record Then Exit Sub
        Catch ex As Exception

        End Try

        ObtenerDatos()
        ObtenerRecorridosVale()
        GridEX2.Refetch()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        HabilitarControles()
        GridEX1.Focus()
        GridEX2.Enabled = False
        ObtenerVales()

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Try
            ImprimirTickete(Me.GridEX1.GetValue("idVale"))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Try
            If MsgBox("Está seguro que desea eliminar el vale seleccionado?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
            Dim idVale As Integer = GridEX1.GetValue("idVale")
            Dim lista = d.proc_ObtenerRecorridosVale(idVale).ToList

            d.proc_RecorridosValeDeleteporVale(idVale)
            d.proc_RecorridosValeDeleteporValeliquidado(idVale)
            d.proc_HistorialValeDeleteporVale(idVale)
            d.proc_ValesDelete(idVale)

            ObtenerRecorridosVale()

            'd.proc_ValesUpdateEstado(GridEX1.GetValue("idVale"), 1) Procedimiento para cambiar el estado del vale a inabilitado
            ObtenerVales()
        Catch ex As Exception
            MsgBox("Error al eliminar, comuníquese con el administrador del sistema." & vbCrLf & ex.Message)

        End Try
    End Sub



    Private Sub txtMovil_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMovil.KeyUp
        DetectarVehiculo()
    End Sub

    Private Sub DetectarVehiculo()
        If Not IsNumeric(txtMovil.Text) Then Exit Sub
        If d.proc_ObtenerVehiculoPorNroMovil(Integer.Parse(txtMovil.Text)).ToList.Count > 0 Then
            _ListaVehiculo = d.proc_ObtenerVehiculoPorNroMovil(Integer.Parse(txtMovil.Text)).ToList.First
        Else
            _ListaVehiculo = Nothing
        End If

        If _ListaVehiculo Is Nothing Then
            PictureBox1.Image = My.Resources.redx
        Else
            PictureBox1.Image = My.Resources.check2
        End If
    End Sub

    Private Sub txtMovil_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMovil.LostFocus
        Try
            Dim conductor = d.proc_Conductores_VehiculosLoadByidVehiculo(Integer.Parse(d.proc_ObtenerVehiculoPorNroMovil(Integer.Parse(txtMovil.Value)).ToList.First.idVehiculo)).ToList
            cboConductor.SelectedValue = conductor.First.idConductor
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHistorialVales.Click

        Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
        Dim f As New frmHistorial
        f.abrirFormulario("Historial de Vales", d.proc_ObtenerHistorialVale(GridEX1.GetValue("idVale")))
        f.GridEX1.RootTable.Columns("idCliente").Visible = False
        f.GridEX1.RootTable.Columns("idVale").Visible = False
        f.GridEX1.RootTable.Columns("idVehiculo").Visible = False
        f.GridEX1.RootTable.Columns("centroCosto").Caption = "Centro de Costo"

        f.GridEX1.RootTable.Columns("idUsuario").Caption = "Usuario"
        f.GridEX1.ColumnAutoResize = True
    End Sub

    Private Sub GridEX2_AddingRecord(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GridEX2.AddingRecord
        Try
            If validar() Then
                Dim r As New RecorridosVale
                r.idBarrioOrigen = GridEX2.GetValue("idBarrioOrigen")
                r.idBarrioDestino = GridEX2.GetValue("idBarrioDestino")
                r.Origen = GridEX2.GetValue("Origen")
                r.Destino = GridEX2.GetValue("Destino")
                r.Tarifa = GridEX2.GetValue("Tarifa")
                If chLiquidados.Checked Then
                    r.TipoRecorrido = 1
                Else
                    r.TipoRecorrido = 2
                End If
                If Not ValidarRecorrido() Then
                    e.Cancel = True
                    Exit Sub
                End If


                _ListaRecorridos.Add(r)


                'GridEX2.Refetch()


            Else
                e.Cancel = True
                If chLiquidados.Checked Then
                    GridEX2.Refetch()
                End If

                Exit Sub
            End If

            GridEX2.DataSource = _ListaRecorridos
            GridEX2.RetrieveStructure()
            GridEX2.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True
            GridEX2.RootTable.Columns("idRecorridoVale").Visible = False
            GridEX2.RootTable.Columns("idBarrioOrigen").Visible = False
            GridEX2.RootTable.Columns("idbarrioDestino").Visible = False
            GridEX2.RootTable.Columns("TipoRecorrido").Visible = False
            GridEX2.RootTable.Columns("Origen").Caption = "Origen"
            GridEX2.RootTable.Columns("Destino").Caption = "Destino"
            GridEX2.RootTable.Columns("tarifa").Caption = "Tarifa"

            If chLiquidados.Enabled = False Then
                GridEX2.RootTable.Columns("Origen").ButtonStyle = Janus.Windows.GridEX.ButtonStyle.Ellipsis
                GridEX2.RootTable.Columns("Destino").ButtonStyle = Janus.Windows.GridEX.ButtonStyle.Ellipsis

                GridEX2.RootTable.Columns("Origen").EditType = Janus.Windows.GridEX.EditType.NoEdit
                GridEX2.RootTable.Columns("Destino").EditType = Janus.Windows.GridEX.EditType.NoEdit
            End If

            CalcularTotalTarifa()

            chLiquidados.Checked = True

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub CalcularTotalTarifa()

        Dim totalTarifa As Decimal = 0
        For index = 0 To GridEX2.RowCount - 1
            totalTarifa += GridEX2.GetRow(index).Cells.Item("tarifa").Value

        Next

        txtTarifa.Value = totalTarifa


    End Sub

    Private Function ValidarRecorrido() As Boolean
        Return True
    End Function

    Private Sub GridEX2_ColumnButtonClick(ByVal sender As Object, ByVal e As Janus.Windows.GridEX.ColumnActionEventArgs) Handles GridEX2.ColumnButtonClick
        Try

            If e.Column.Key = "Origen" Then
                Dim F As New frmBarrios
                Dim _barrio As Common.proc_BarriosLoadByPrimaryKeyResult = F.AbrirParaSeleccionar
                barrio1 = _barrio.idBarrio
                GridEX2.SetValue("idBarrioOrigen", _barrio.idBarrio)
                GridEX2.SetValue("Origen", _barrio.descripcion)

            End If

            If e.Column.Caption = "Destino" Then
                Dim F As New frmBarrios
                Dim _barrio As Common.proc_BarriosLoadByPrimaryKeyResult = F.AbrirParaSeleccionar
                barrio2 = _barrio.idBarrio
                GridEX2.SetValue("idBarrioDestino", _barrio.idBarrio)
                GridEX2.SetValue("Destino", _barrio.descripcion)

            End If

            If GridEX2.GetRow.Cells.Item("idBarrioOrigen").Text <> "" And GridEX2.GetRow.Cells.Item("idBarrioDestino").Text <> "" Then
                Dim Tarifa = d.proc_ObtenerTarifa(barrio1, barrio2).ToList.First
                GridEX2.SetValue("tarifa", CDbl(Tarifa.tarifa))
            End If


        Catch ex As Exception
            GridEX2.SetValue("tarifa", 0)
            MsgBox("No hay tarifa estipulada para estos barrios")
            GridEX2.Refetch()
        End Try

    End Sub

    Private Sub GridEX2_RecordsDeleted(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX2.RecordsDeleted
        Try
            _ListaRecorridos.Clear()


            For index As Integer = 0 To GridEX2.RowCount - 1
                Dim rec As New RecorridosVale
                rec.idBarrioOrigen = GridEX2.GetRow(index).Cells("idBarrioOrigen").Text
                rec.idBarrioDestino = GridEX2.GetRow(index).Cells("idbarrioDestino").Text
                rec.idRecorridoVale = GridEX2.GetRow(index).Cells("idRecorridoVale").Text
                rec.Tarifa = GridEX2.GetRow(index).Cells("Tarifa").Text
                rec.Origen = GridEX2.GetRow(index).Cells("Origen").Text
                rec.Destino = GridEX2.GetRow(index).Cells("Destino").Text
                _ListaRecorridos.Add(rec)

            Next
            CalcularTotalTarifa()
        Catch ex As Exception

        End Try


    End Sub




#End Region

#Region "Metodos"

    Private Sub ObtenerDatos()
        Try
            Me.cboCliente.SelectedValue = GridEX1.GetValue("idCliente")
            Me.txtTarifa.Text = GridEX1.GetValue("Tarifa")
            Me.txtvale.Text = GridEX1.GetValue("nroVale")
            Me.txtMovil.Text = GridEX1.GetValue("NroMovil")
            cboConductor.SelectedValue = GridEX1.GetValue("idConductor")
            Me.dtpFechaPago.Value = GridEX1.GetValue("Fechavale")
            cboCECO.Text = GridEX1.GetValue("centroCosto")
            txtPasajero.Text = GridEX1.GetValue("Pasajero")
            'cboBarrio.SelectedValue = Integer.Parse(GridEX1.GetValue("idBarrioOrigen"))
            'cboBarrioDestino.SelectedValue = Integer.Parse(GridEX1.GetValue("idBarrioDestino"))
        Catch ex As Exception

        End Try

    End Sub

    Public Function Tarifa(ByVal condicion As Boolean)
        Me.condicion = condicion
        Return Me.condicion
    End Function

    Private Sub ImprimirTickete(ByVal ID As Integer)
        Dim ids As New clsImpresionDataSet
        Dim ps As New System.Drawing.Printing.PageSettings
        Dim size As New System.Drawing.Printing.PaperSize

        With ids
            .TipoImpresora = clsImpresionDataSet.TipoImpresora_t.Grafica

            size = New System.Drawing.Printing.PaperSize("76mm Roll Paper", 76, 297)

            ps.PaperSize = size

            .ConfiguracionPagina = ps
            .NombreDocumentoImpresion = "Vale"
            .PathArchivoXML = My.Application.Info.DirectoryPath & "\Vales.xml"
            .DataSet = DataSetImpresion(ID)

            .Imprimir()

        End With

        'Hago esto porque si se quiere reimprimir, arroja un error al querer agregar nuevamente los DataTables al DataSet:
        ids.DataSet.Tables.Clear()
        ids = Nothing
    End Sub

    Private Function DataSetImpresion(ByVal id As Integer) As DataSet
        Try
            Dim ret As New DataSet
            Dim dr As DataRow
            Dim vale = d.proc_ValesLoadByPrimaryKey(id).ToList.First
            Dim cliente = d.proc_ClientesLoadByPrimaryKey(vale.idCliente).ToList.First
            Dim movil = d.proc_VehiculosLoadByPrimaryKey(vale.idVehiculo).ToList.First
            Dim conductor = d.proc_Conductores_VehiculosLoadByidVehiculo(movil.idVehiculo).ToList.First

            _DataTableEncabezado = New DataTable
            _DataTableDetalle = New DataTable
            Me._DataTableEncabezado.Rows.Clear()
            Me._DataTableEncabezado.Columns.Add("Titulo")
            Me._DataTableEncabezado.Columns.Add("NIT")

            Me._DataTableEncabezado.Columns.Add("Cliente")
            Me._DataTableEncabezado.Columns.Add("nroVale")
            Me._DataTableEncabezado.Columns.Add("fechaVale")
            Me._DataTableEncabezado.Columns.Add("Importe")
            Me._DataTableEncabezado.Columns.Add("Pago")
            Me._DataTableEncabezado.Columns.Add("Movil")
            Me._DataTableEncabezado.Columns.Add("Conductor")
            Me._DataTableEncabezado.Columns.Add("Fecha")
            Me._DataTableEncabezado.Columns.Add("Usuario")

            dr = Me._DataTableEncabezado.NewRow

            dr.Item("Titulo") = getSettings("Titulo")
            dr.Item("NIT") = String.Format("{0}{1}Tel: 4129491{2}www.taxiportal.com.co{3}Pago de Vales", getSettings("NIT"), vbCrLf, vbCrLf, vbCrLf)


            dr.Item("Cliente") = "Cliente: " & cliente.nombre.ToString
            dr.Item("nroVale") = "nroVale: " & vale.nroVale.ToString
            dr.Item("fechaVale") = "Fecha Vale: " & vale.fechaVale.Value.Date
            dr.Item("Importe") = "Importe: " & Format(vale.tarifa, "C")
            dr.Item("Pago") = "Pago: " & Format(vale.pago, "C")
            dr.Item("Movil") = "Móvil: " & movil.NroMovil.ToString
            dr.Item("Conductor") = "Cond.: " & Microsoft.VisualBasic.Left(conductor.Nombre.ToString, 25)
            dr.Item("fecha") = "Registro: " & vale.fechaCarga.ToString
            dr.Item("Usuario") = "Usuario: " & vale.idUsuario.ToString

            Me._DataTableEncabezado.Rows.Add(dr)

            ret.Tables.Add(Me._DataTableEncabezado)
            ret.Tables.Add(Me._DataTableDetalle)

            Return ret
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Private Sub ObtenerClientes()
        Dim listaClientes = d.proc_ObtenerClientesVale.ToList

        Me.cboCliente.DataSource = listaClientes
        Me.cboCliente.DisplayMember = "Nombre"
        cboCliente.ValueMember = "idCliente"

    End Sub

    Private Sub ObtenerPasajeros()
        txtPasajero.DataSource = d.proc_ObtenerPasajeros.ToList()
        txtPasajero.DisplayMember = "Pasajero"
        txtPasajero.ValueMember = "Pasajero"

    End Sub

    Private Sub obtenerDependendencias()
        cboCECO.DataSource = d.proc_ObtenerDependencias.ToList()
        cboCECO.DisplayMember = "Dependencia"
        cboCECO.ValueMember = "Dependencia"

    End Sub

    Private Sub HabilitarControles()

        txtMovil.Enabled = Not txtMovil.Enabled
        txtTarifa.Enabled = Not txtTarifa.Enabled
        GridEX1.Enabled = Not GridEX1.Enabled
        GridEX2.Enabled = Not GridEX2.Enabled
        dtpFechaPago.Enabled = Not dtpFechaPago.Enabled
        txtvale.Enabled = Not txtvale.Enabled
        txtPasajero.Enabled = Not txtPasajero.Enabled
        cboCECO.Enabled = Not cboCECO.Enabled
        cboConductor.Enabled = Not cboConductor.Enabled
        'cboBarrio.Enabled = Not cboBarrio.Enabled
        'cboBarrioDestino.Enabled = Not cboBarrioDestino.Enabled
        cboCliente.Enabled = Not cboCliente.Enabled
        btnNuevo.Visible = Not btnNuevo.Visible
        btnAceptar.Visible = Not btnAceptar.Visible
        btnCancelar.Visible = Not btnCancelar.Visible
        btnModificar.Visible = Not btnModificar.Visible
        chLiquidados.Enabled = Not chLiquidados.Enabled

    End Sub

    Private Sub ObtenerVales()
        Try
            Dim listaVales = d.proc_ObtenerVales(dtpDesde.Value, dtpHasta.Value, Nothing, Me.condicion).ToList
            GridEX1.DataSource = listaVales

            GridEX1.RetrieveStructure()
            GridEX1.RootTable.Columns("idVale").Visible = False
            GridEX1.RootTable.Columns("idCliente").Visible = False
            GridEX1.RootTable.Columns("idVehiculo").Visible = False
            GridEX1.RootTable.Columns("idConductor").Visible = False
            GridEX1.RootTable.Columns("estado").Visible = False
            GridEX1.RootTable.Columns("estadoTarifa").Visible = False
            'GridEX1.RootTable.Columns("estadoTarifa").Caption = "pagado por Transacción"
            'GridEX1.RootTable.Columns("estadoTarifa").EditType = Janus.Windows.GridEX.EditType.CheckBox
            'GridEX1.RootTable.Columns("estadoTarifa").ColumnType = Janus.Windows.GridEX.ColumnType.CheckBox
            GridEX1.RootTable.Columns("nroVale").Caption = "Nro. Vale"
            GridEX1.RootTable.Columns("CentroCosto").Caption = "Centro de Costo"
            GridEX1.RootTable.Columns("fechaCarga").Caption = "Fecha Registro"
            GridEX1.RootTable.Columns("Nombre").Caption = "Cliente"
            GridEX1.RootTable.Columns("Tarifa").FormatString = " ###,###,##0"
            GridEX1.RootTable.Columns("Pago").FormatString = " ###,###,##0"

            Dim total As Decimal = 0
            For Each vale In listaVales
                If vale.Pago IsNot Nothing Then total += vale.Pago
            Next
            lblTotal.Text = "Total: " & total.ToString

        Catch ex As Exception

        End Try

    End Sub


    Private Sub ObtenerRecorridosVale()
        Try

            If Not esNuevo Then
                _ListaRecorridos.Clear()

                Dim listal = d.proc_ObtenerRecorridosValeLiquidados(GridEX1.GetValue("idVale")).ToList

                Dim lista = d.proc_ObtenerRecorridosVale(GridEX1.GetValue("idVale")).ToList

                For Each recorrido In lista
                    Dim rec As New RecorridosVale
                    rec.idBarrioOrigen = recorrido.idBarrioOrigen
                    rec.idBarrioDestino = recorrido.idbarrioDestino
                    rec.idRecorridoVale = recorrido.idRecorridoVale
                    rec.Tarifa = recorrido.Tarifa
                    rec.Origen = recorrido.Origen
                    rec.Destino = recorrido.Destino

                    _ListaRecorridos.Add(rec)

                Next

                For Each recorrido1 In listal
                    Dim rec As New RecorridosVale

                    rec.idRecorridoVale = recorrido1.idRecorridoValeLiquidado
                    rec.Tarifa = recorrido1.Tarifa
                    rec.Origen = recorrido1.Origen
                    rec.Destino = recorrido1.Destino

                    _ListaRecorridos.Add(rec)

                Next

            Else
                _ListaRecorridos.Clear()

                'For index As Integer = 0 To GridEX2.RowCount - 1
                '    Dim rec As New RecorridosVale
                '    rec.idBarrioOrigen = GridEX2.GetRow(index).Cells("idBarrioOrigen").Text
                '    rec.idBarrioDestino = GridEX2.GetRow(index).Cells("idbarrioDestino").Text
                '    rec.idRecorridoVale = GridEX2.GetRow(index).Cells("idRecorridoVale").Text
                '    rec.Tarifa = GridEX2.GetRow(index).Cells("Tarifa").Text
                '    rec.Origen = GridEX2.GetRow(index).Cells("Origen").Text
                '    rec.Destino = GridEX2.GetRow(index).Cells("Destino").Text
                '    _ListaRecorridos.Add(rec)

                'Next

                'Dim totalTarifa As Decimal = 0
                'For index = 0 To GridEX2.RowCount - 1
                '    totalTarifa += GridEX2.GetRow(index).Cells.Item("tarifa").Value

                'Next
            End If

            GridEX2.DataSource = _ListaRecorridos
            GridEX2.RetrieveStructure()
            GridEX2.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True
            GridEX2.RootTable.Columns("idRecorridoVale").Visible = False
            GridEX2.RootTable.Columns("idBarrioOrigen").Visible = False
            GridEX2.RootTable.Columns("idbarrioDestino").Visible = False
            GridEX2.RootTable.Columns("TipoRecorrido").Visible = False
            GridEX2.RootTable.Columns("Origen").Caption = "Origen"
            GridEX2.RootTable.Columns("Destino").Caption = "Destino"
            GridEX2.RootTable.Columns("tarifa").Caption = "Tarifa"

            If chLiquidados.Checked = False Then
                GridEX2.RootTable.Columns("Origen").ButtonStyle = Janus.Windows.GridEX.ButtonStyle.Ellipsis
                GridEX2.RootTable.Columns("Destino").ButtonStyle = Janus.Windows.GridEX.ButtonStyle.Ellipsis

                GridEX2.RootTable.Columns("Origen").EditType = Janus.Windows.GridEX.EditType.NoEdit
                GridEX2.RootTable.Columns("Destino").EditType = Janus.Windows.GridEX.EditType.NoEdit

            End If



        Catch ex As Exception
        End Try



    End Sub

    Private Function validar() As Boolean

        If txtMovil.Text = "" Then
            MsgBox("Debe ingresar un Móvil")
            txtMovil.Focus()
            Return False
        End If
        Dim lista = d.proc_ObtenerVehiculoPorNroMovil(Integer.Parse(txtMovil.Text)).ToList

        If lista.Count = 0 Then
            MsgBox("El móvil no existe o no está habilitado")
            txtMovil.Focus()
            txtMovil.SelectAll()
            GridEX2.Refetch()
            Return False
        End If

        If txtTarifa.Text = "" Or txtTarifa.Value < 0 Then
            MsgBox("Debe ingresar una tarifa válida")
            txtTarifa.Focus()
            Return False
        End If

        If cboCliente.SelectedIndex < 0 Then
            MsgBox("Debe seleccionar un cliente")
            cboCliente.Focus()
            Return False
        End If

        If txtvale.Text = "" Then
            MsgBox("Debe ingresar un número de vale")
            txtvale.Focus()
            Return False
        End If

        'verificar que el numero de vale no este cargado para ese cliente.
        If esNuevo Then
            Dim listaVales = d.proc_ObtenerValesCliente(CStr(txtvale.Text), cboCliente.SelectedValue).ToList

            If listaVales.Count > 0 Then
                MsgBox("Ya hay un vale con ese número para este cliente.")
                txtvale.Focus()
                Return False
            End If
        End If


        If GridEX2.GetRow.Cells.Item("Origen").Text = "" Then
            If chLiquidados.Checked = False Then
                MsgBox("Falta ingresar un Barrio ")
                'GridEX2.Refetch()
                Return False
            End If
        End If

        If GridEX2.GetRow.Cells.Item("Destino").Text = "" Then
            If chLiquidados.Checked = False Then
                MsgBox("Falta ingresar un Barrio ")
                'GridEX2.Refetch()
                Return False
            End If
        End If


        If GridEX2.GetRow.Cells.Item("Tarifa").Value = 0 Then
            MsgBox("No existe una tarifa para este recorrido")
            GridEX2.Enabled = False
            txtvale.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub ObtenerConductor()
        Try
            Dim con = d.proc_ObtenerPersonas.ToList
            cboConductor.DataSource = con
            cboConductor.ValueMember = "idPersona"
            cboConductor.DisplayMember = "Nombre"
        Catch ex As Exception

        End Try
    End Sub

#End Region


    Private Sub GridEX2_FormattingRow(ByVal sender As System.Object, ByVal e As Janus.Windows.GridEX.RowLoadEventArgs) Handles GridEX2.FormattingRow

    End Sub

    Private Sub chLiquidados_CheckedChanged(sender As Object, e As System.EventArgs) Handles chLiquidados.CheckedChanged
        GridEX2.DataSource = _ListaRecorridos
        GridEX2.RetrieveStructure()
        GridEX2.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True
        GridEX2.RootTable.Columns("idRecorridoVale").Visible = False
        GridEX2.RootTable.Columns("idBarrioOrigen").Visible = False
        GridEX2.RootTable.Columns("idbarrioDestino").Visible = False
        GridEX2.RootTable.Columns("TipoRecorrido").Visible = False
        GridEX2.RootTable.Columns("Origen").Caption = "Origen"
        GridEX2.RootTable.Columns("Destino").Caption = "Destino"
        GridEX2.RootTable.Columns("tarifa").Caption = "Tarifa"

        If chLiquidados.Checked = False Then
            GridEX2.RootTable.Columns("Origen").ButtonStyle = Janus.Windows.GridEX.ButtonStyle.Ellipsis
            GridEX2.RootTable.Columns("Destino").ButtonStyle = Janus.Windows.GridEX.ButtonStyle.Ellipsis

            GridEX2.RootTable.Columns("Origen").EditType = Janus.Windows.GridEX.EditType.NoEdit
            GridEX2.RootTable.Columns("Destino").EditType = Janus.Windows.GridEX.EditType.NoEdit

        End If
    End Sub

    Private Sub chLiquidados_Click(sender As Object, e As System.EventArgs) Handles chLiquidados.Click
        GridEX2.DataSource = _ListaRecorridos
        GridEX2.RetrieveStructure()
        GridEX2.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True
        GridEX2.RootTable.Columns("idRecorridoVale").Visible = False
        GridEX2.RootTable.Columns("idBarrioOrigen").Visible = False
        GridEX2.RootTable.Columns("idbarrioDestino").Visible = False
        GridEX2.RootTable.Columns("TipoRecorrido").Visible = False
        GridEX2.RootTable.Columns("Origen").Caption = "Origen"
        GridEX2.RootTable.Columns("Destino").Caption = "Destino"
        GridEX2.RootTable.Columns("tarifa").Caption = "Tarifa"

        If chLiquidados.Checked = False Then
            GridEX2.RootTable.Columns("Origen").ButtonStyle = Janus.Windows.GridEX.ButtonStyle.Ellipsis
            GridEX2.RootTable.Columns("Destino").ButtonStyle = Janus.Windows.GridEX.ButtonStyle.Ellipsis

            GridEX2.RootTable.Columns("Origen").EditType = Janus.Windows.GridEX.EditType.NoEdit
            GridEX2.RootTable.Columns("Destino").EditType = Janus.Windows.GridEX.EditType.NoEdit

        End If
    End Sub

    Private Sub txtMovil_ValueChanged(sender As Object, e As System.EventArgs) Handles txtMovil.ValueChanged
        DetectarVehiculo()
    End Sub
End Class

Public Class RecorridosVale

    Private midRecorridoVale As Integer
    Public Property idRecorridoVale() As Integer
        Get
            Return midRecorridoVale
        End Get
        Set(ByVal value As Integer)
            midRecorridoVale = value
        End Set
    End Property


    Private Tipo As Integer
    Public Property TipoRecorrido() As Integer
        Get
            Return Tipo
        End Get
        Set(ByVal value As Integer)
            Tipo = value
        End Set
    End Property


    Private mIdBarrioOrigen As Integer
    Public Property idBarrioOrigen() As Integer
        Get
            Return mIdBarrioOrigen
        End Get
        Set(ByVal value As Integer)
            mIdBarrioOrigen = value
        End Set
    End Property


    Private mIdBarrioDestino As Integer
    Public Property idBarrioDestino() As Integer
        Get
            Return mIdBarrioDestino
        End Get
        Set(ByVal value As Integer)
            mIdBarrioDestino = value
        End Set
    End Property


    Private mOrigen As String
    Public Property Origen() As String
        Get
            Return mOrigen
        End Get
        Set(ByVal value As String)
            mOrigen = value
        End Set
    End Property


    Private mDestino As String
    Public Property Destino() As String
        Get
            Return mDestino
        End Get
        Set(ByVal value As String)
            mDestino = value
        End Set
    End Property


    Private mTarifa As Double
    Public Property Tarifa() As Double
        Get
            Return mTarifa
        End Get
        Set(ByVal value As Double)
            mTarifa = value
        End Set
    End Property







End Class
