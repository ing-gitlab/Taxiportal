﻿Imports Common
Public Class frmSancion

#Region "Declaraciones"

    Dim _idVehiculo As Integer
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim _esNuevo As Boolean
    Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")

#End Region

#Region "Eventos"

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        _esNuevo = True
        HabilitarControles()
        dtpDesde.Focus()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        Try
            If Not validar() Then
                Exit Sub
            End If
            If _esNuevo Then
                d.proc_SancionesInsert(Nothing, _idVehiculo, dtpDesde.Value, dtpHasta.Value, USER_GLOBAL.idUsuario, NOW_GLOBAL, txtObs.Text, horadesde.Value, horahasta.Value)
            Else
                d.proc_SancionesUpdate(GridEX1.GetValue("idSancion"), _idVehiculo, dtpDesde.Value, dtpHasta.Value, USER_GLOBAL.idUsuario, NOW_GLOBAL, txtObs.Text, horadesde.Value, horahasta.Value)
            End If

            ObtenerSanciones()
            HabilitarControles()
            StartBroadcast(PORT_Principal, "RefrescarMoviles")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        _esNuevo = False
        HabilitarControles()
        dtpDesde.Focus()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        HabilitarControles()
    End Sub

    Private Sub GridEX1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.SelectionChanged
        dtpDesde.Value = GridEX1.GetValue("FechaDesde")
        dtpHasta.Value = GridEX1.GetValue("FechaHasta")
        horadesde.Value = GridEX1.GetValue("HoraDesde")
        horahasta.Value = GridEX1.GetValue("HoraHasta")
        txtObs.Text = GridEX1.GetValue("Observaciones")
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Try
            If MsgBox("Está seguro que desea eliminar la sanción?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
            d.proc_SancionesDelete(GridEX1.GetValue("idSancion"))
            ObtenerSanciones()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

#End Region

#Region "Metodos"

    Public Function AbrirForm(ByVal idVehiculo As Integer)
        _idVehiculo = idVehiculo
        ObtenerSanciones()
        Me.ShowDialog()
    End Function

    Sub ObtenerSanciones()
        Dim lista = d.proc_ObtenerSancionesMovil(CInt(_idVehiculo)).ToList
        If lista.Count > 0 Then
            btnModificar.Enabled = True
            btnEliminar.Visible = True
        End If
        Me.GridEX1.SetDataBinding(lista, "")
        GridEX1.RetrieveStructure()
        GridEX1.RootTable.Columns("idSancion").Visible = False
        GridEX1.RootTable.Columns("FechaDesde").EditType = Janus.Windows.GridEX.EditType.CalendarCombo
        GridEX1.RootTable.Columns("FechaHasta").EditType = Janus.Windows.GridEX.EditType.CalendarCombo
        For index As Integer = 0 To GridEX1.RootTable.Columns.Count - 1
            GridEX1.RootTable.Columns(index).AutoSize()

        Next
    End Sub

    Private Function validar() As Boolean
        If dtpDesde.Value >= dtpHasta.Value Then
            MsgBox("La fecha hasta debe ser menor que la fecha desde.")
            dtpDesde.Focus()
            Return False
        End If

        If Not IsDate(dtpDesde.Value) Then
            MsgBox("La fecha desde debe tener un formato válido.")
            dtpDesde.Focus()
            Return False
        End If

        If Not IsDate(dtpHasta.Value) Then
            MsgBox("La fecha hasta debe tener un formato válido.")
            dtpHasta.Focus()
            Return False
        End If
        Return True
    End Function

    Sub HabilitarControles()
        btnAceptar.Visible = Not btnAceptar.Visible
        btnCancelar.Visible = Not btnCancelar.Visible
        btnModificar.Visible = Not btnModificar.Visible
        btnNuevo.Visible = Not btnNuevo.Visible
        dtpDesde.Enabled = Not dtpDesde.Enabled
        dtpHasta.Enabled = Not dtpHasta.Enabled
        txtObs.Enabled = Not txtObs.Enabled
        GridEX1.Enabled = Not GridEX1.Enabled
        horadesde.Enabled = Not horadesde.Enabled
        horahasta.Enabled = Not horahasta.Enabled
    End Sub


#End Region




End Class