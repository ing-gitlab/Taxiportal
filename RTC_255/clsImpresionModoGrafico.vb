Imports System.Drawing.Printing
Imports System.Collections

Public Class clsImpresionModoGrafico

    Private Const DEFAULT_FONT As String = "Courier New"
    Private Const DEFAULT_FONT_SIZE As Single = 9

    Private Structure ItemImpresion
        Public Texto As String
        Public PosicionHorizontalMM As Integer
        Public PosicionVerticalMM As Integer
        Public NombreFuente As String
        Public TamanioFuente As Single
        Public Negrita As Boolean
        Public Subrayado As Boolean
    End Structure

    Private mItems As New ArrayList
    Private mCopias As Integer = 1
    Private mContadorPaginas As Integer
    Private WithEvents mDocumento As PrintDocument
    Private mConfiguracionPagina As New System.Drawing.Printing.PageSettings

    Public Property ConfiguracionPagina() As System.Drawing.Printing.PageSettings
        Get
            Return Me.mConfiguracionPagina
        End Get
        Set(ByVal value As System.Drawing.Printing.PageSettings)
            Me.mConfiguracionPagina = value
        End Set
    End Property

    Public Property Copias() As Integer
        Get
            Return Me.mCopias
        End Get
        Set(ByVal value As Integer)
            Me.mCopias = value
        End Set
    End Property

    Public Sub AgregarItem(ByVal Texto As String, ByVal PosicionHorizontalMM As Integer, ByVal PosicionVerticalMM As Integer, Optional ByVal NombreFuente As String = DEFAULT_FONT, Optional ByVal TamanioFuente As Single = DEFAULT_FONT_SIZE, Optional ByVal Negrita As Boolean = False, Optional ByVal Subrayado As Boolean = False)
        Dim Item As ItemImpresion

        With Item
            .Texto = Texto
            .PosicionHorizontalMM = PosicionHorizontalMM
            .PosicionVerticalMM = PosicionVerticalMM
            .NombreFuente = NombreFuente
            .TamanioFuente = TamanioFuente
            .Negrita = Negrita
            .Subrayado = Subrayado
        End With

        Me.mItems.Add(Item)
    End Sub

    Public Sub Imprimir(Optional ByVal NombreDocumento As String = "(Sin nombre)")
        Try
            Me.mDocumento = New PrintDocument

            Me.mDocumento.DefaultPageSettings = Me.ConfiguracionPagina

            Me.mDocumento.DocumentName = NombreDocumento

            For Me.mContadorPaginas = 1 To Me.Copias
                Me.mDocumento.Print()
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub mDocumento_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles mDocumento.PrintPage
        e.Graphics.PageUnit = GraphicsUnit.Millimeter
        Me.ImprimirItems(e.Graphics)
        e.HasMorePages = (Me.mContadorPaginas <> Me.Copias)
    End Sub

    Private Sub ImprimirItems(ByRef g As Graphics)
        Dim Item As ItemImpresion
        Dim f As Font
        Dim fs As FontStyle
        Dim posicion As PointF

        For Each Item In Me.mItems
            fs = IIf(Item.Negrita, FontStyle.Bold, FontStyle.Regular) Or IIf(Item.Subrayado, FontStyle.Underline, FontStyle.Regular)
            f = New Font(Item.NombreFuente, Item.TamanioFuente, fs)

            posicion.X = Item.PosicionHorizontalMM
            posicion.Y = Item.PosicionVerticalMM

            g.DrawString(Item.Texto, f, Brushes.Black, posicion)
        Next

        g.Flush()
    End Sub
End Class
