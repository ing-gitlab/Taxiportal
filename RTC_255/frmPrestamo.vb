﻿Imports Common
Public Class frmPrestamo

#Region "DECLARACIONES"
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim _Persona As proc_PersonasLoadByPrimaryKeyResult
    Dim _esNuevo As Boolean
    Dim _Vehiculo As proc_VehiculosLoadByPrimaryKeyResult
    Dim _DataTableEncabezado As DataTable
    Dim _DataTableDetalle As DataTable
    Dim bandera As Boolean = True
#End Region

#Region "METODOS"
    Public Sub AbrirFormulario(idPersona As Integer, imagen As Image)
        GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
        GridEX1.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic
        _Persona = d.proc_PersonasLoadByPrimaryKey(idPersona).ToList.First
        gbPersona.Text = _Persona.nombre
        PbPersona.Image = imagen
        ObtenerCuentaAhorro()
        Dim ListaVehiculo = d.proc_obtenerVehiculosPorConductor(_Persona.idPersona).ToList
        If ListaVehiculo.Count = 0 Then
            MsgBox("Esta persona no es conductor de ningun vehiculo." & vbCrLf & "Por favor primero asígnelo como conductor en la ventana de Vehiculos y luego intente nuevamente.")
            Exit Sub
        Else
            _Vehiculo = d.proc_VehiculosLoadByPrimaryKey(ListaVehiculo.First.idVehiculo).ToList.First
        End If
        txtMovil.Text = ListaVehiculo.First.NroMovil
        ObtenerTiposPrestamos()
        ObtenerPrestamos()
        Me.ShowDialog()

    End Sub

    Private Sub ObtenerTiposPrestamos()
        cboTipoPrestamo.DataSource = d.proc_TipoPrestamoLoadAll.ToList
        cboTipoPrestamo.DisplayMember = "descripcion"
        cboTipoPrestamo.ValueMember = "idTipoPrestamo"
    End Sub

    Private Sub ObtenerPrestamos()
        GridEX1.SetDataBinding(d.proc_obtenerPrestamosPorPersona(_Persona.idPersona).ToList, "")
        GridEX1.RetrieveStructure()

        GridEX1.RootTable.Columns("FechaInicio").Caption = "Inicio"
        GridEX1.RootTable.Columns("FechaFin").Caption = "Fin"
        GridEX1.RootTable.Columns("FechaInicio").Caption = "Inicio"
        GridEX1.RootTable.Columns("idTipoPrestamo").Visible = False
        GridEX1.RootTable.Columns("TipoPrestamo").Caption = "Tipo Prestamo"
        GridEX1.RootTable.Columns("ImportePrestado").Caption = "Valor Inicial"
        GridEX1.RootTable.Columns("idPrestamo").Visible = False
        GridEX1.RootTable.Columns("Finalizado").EditType = Janus.Windows.GridEX.EditType.CheckBox
        GridEX1.RootTable.Columns("Finalizado").ColumnType = Janus.Windows.GridEX.ColumnType.CheckBox
        GridEX1.RootTable.Columns("ImportePrestado").FormatString = "#,#"
        GridEX1.RootTable.Columns("ImporteCuotas").FormatString = "#,#"
        GridEX1.RootTable.Columns("CuotasSaldadas").FormatString = "#,#"
        GridEX1.RootTable.Columns("ImporteSaldado").FormatString = "#,#"

    End Sub

    Private Sub ObtenerCuentaAhorro()
        Try
            Dim CTA = d.proc_ObtenerCuentaAhorro(Int32.Parse(_Persona.idPersona)).ToList.First


            Me.txtAhorroObligatorio.Value = CTA.importeAhorro
            Me.txtAhorroAdicional.Value = CTA.importeAhorroAdicional
            Me.txtDeuda.Value = CTA.importeDeuda

        Catch ex As Exception
            Me.txtAhorroObligatorio.Value = 0
            Me.txtAhorroAdicional.Value = 0
            Me.txtDeuda.Value = 0

        End Try

    End Sub
    Private Sub ModoEdicion()
        txtConcepto.ReadOnly = Not txtConcepto.ReadOnly
        txtObservaciones.ReadOnly = Not txtObservaciones.ReadOnly

        cboTipoPrestamo.Enabled = Not cboTipoPrestamo.Enabled
        txtValorPrestamo.ReadOnly = Not txtValorPrestamo.ReadOnly
        dtpDesde.Enabled = Not dtpDesde.Enabled
        'dtpHasta.Enabled = Not dtpHasta.Enabled
        'txtNroCuotas.ReadOnly = Not txtNroCuotas.ReadOnly
        txtValorCuota.ReadOnly = Not txtValorCuota.ReadOnly
        btnNuevo.Visible = Not btnNuevo.Visible
        btnAceptar.Visible = Not btnAceptar.Visible
        btnCancelar.Visible = Not btnCancelar.Visible
        GridEX1.Enabled = Not GridEX1.Enabled
    End Sub

    Private Sub LimpiarCampos()
        txtConcepto.Text = ""
        txtObservaciones.Text = ""
        txtValorPrestamo.Text = 0
        txtNroCuotas.Value = 1
        If d.proc_ObtenerDiasPicoYPlacaPorMovilyFecha(_Vehiculo.Placa, NOW_GLOBAL.Date, NOW_GLOBAL.Date).ToArray().Length > 0 Then
            dtpDesde.Value = DateAdd(DateInterval.Day, 1, NOW_GLOBAL.Date)
            dtpHasta.Value = DateAdd(DateInterval.Day, 1, NOW_GLOBAL.Date)
        Else
            dtpDesde.Value = NOW_GLOBAL.Date
            dtpHasta.Value = NOW_GLOBAL.Date
        End If

        txtCuotasSaldadas.Value = 0
        txtValorSaldado.Text = 0

        txtConcepto.Focus()
    End Sub

    Private Sub GuardarPrestamo()
        If Not Validar() Then
            bandera = False
            Exit Sub
        End If

        Dim idPrestamo As Integer
        If _esNuevo Then
            d.proc_PrestamoInsert(idPrestamo, txtConcepto.Text, NOW_GLOBAL, dtpDesde.Value, _
                                   dtpHasta.Value, txtObservaciones.Text, cboTipoPrestamo.SelectedValue, _
                                  txtValorPrestamo.Value, txtNroCuotas.Value, txtValorCuota.Value, txtValorSaldado.Value, _
                                  txtCuotasSaldadas.Value, _Persona.idPersona, USER_GLOBAL.idUsuario, False)

        Else

        End If
        ModoEdicion()
        StartBroadcast(getSettings("frmPrincipal_PORT"), "RefrescarMoviles")
        StartBroadcast(getSettings("Vehiculos_PORT"), "RefrescarMoviles")
    End Sub

    Private Function Validar() As Boolean
        If txtConcepto.Text.Equals(String.Empty) Then
            MsgBox("Debe ingresar un concepto para el crédito")
            txtConcepto.Focus()
            Return False
        End If

        If txtValorPrestamo.Value <= 0 Then
            MsgBox("El valor del préstamo debe ser mayor a cero")
            txtValorPrestamo.Focus()
            Return False
        End If

        If txtNroCuotas.Value <= 0 Then
            MsgBox("La cantidad de cuotas debe ser mayor a cero")
            txtNroCuotas.Focus()
            Return False
        End If

        If txtValorCuota.Value <= 0 Then
            MsgBox("El valor de las cuotas debe ser mayor a cero")
            txtValorCuota.Focus()
            Return False
        End If

        'If dtpDesde.Value <= dtpHasta.Value Then
        '    MsgBox("La fecha de inicio del préstamo debe ser anterior a la fecha de fin")
        '    txtValorCuota.Focus()
        '    Return False
        'End If

        Return True

    End Function

    Private Sub ImprimirTickete(ByVal ID As Integer)
        Dim ids As New clsImpresionDataSet
        Dim ps As New System.Drawing.Printing.PageSettings
        Dim size As New System.Drawing.Printing.PaperSize

        With ids
            .TipoImpresora = clsImpresionDataSet.TipoImpresora_t.Grafica

            size = New System.Drawing.Printing.PaperSize("76mm Roll Paper", 76, 297)

            ps.PaperSize = size

            .ConfiguracionPagina = ps
            .NombreDocumentoImpresion = "Prestamo"
            .PathArchivoXML = My.Application.Info.DirectoryPath & "\Prestamos.xml"
            .DataSet = DataSetImpresion(ID)

            .Imprimir()

        End With

        'Hago esto porque si se quiere reimprimir, arroja un error al querer agregar nuevamente los DataTables al DataSet:
        ids.DataSet.Tables.Clear()
        ids = Nothing
    End Sub

    Private Function DataSetImpresion(ByVal id As Integer) As DataSet
        Try
            Dim ret As New DataSet
            Dim dr As DataRow
            Dim prestamo = d.proc_PrestamoLoadByPrimaryKey(id).ToList.First
            Dim persona = d.proc_PersonasLoadByPrimaryKey(prestamo.idPersona).ToList.First
            Dim tipo = d.proc_TipoPrestamoLoadByPrimaryKey(prestamo.idTipoPrestamo).ToList.First
            _DataTableEncabezado = New DataTable
            _DataTableDetalle = New DataTable
            Me._DataTableEncabezado.Rows.Clear()
            Me._DataTableEncabezado.Columns.Add("Titulo")
            Me._DataTableEncabezado.Columns.Add("NIT")

            Me._DataTableEncabezado.Columns.Add("idPrestamo")
            Me._DataTableEncabezado.Columns.Add("Concepto")
            Me._DataTableEncabezado.Columns.Add("fechaRegistro")
            Me._DataTableEncabezado.Columns.Add("Inicio")
            Me._DataTableEncabezado.Columns.Add("Fin")
            Me._DataTableEncabezado.Columns.Add("Observaciones")
            Me._DataTableEncabezado.Columns.Add("TipoPrestamo")
            Me._DataTableEncabezado.Columns.Add("Importe")
            Me._DataTableEncabezado.Columns.Add("NroCuotas")
            Me._DataTableEncabezado.Columns.Add("importeCuotas")
            Me._DataTableEncabezado.Columns.Add("importeSaldado")
            Me._DataTableEncabezado.Columns.Add("CuotasSaldadas")
            Me._DataTableEncabezado.Columns.Add("Nombre")
            Me._DataTableEncabezado.Columns.Add("Usuario")
            Me._DataTableEncabezado.Columns.Add("Finalizado")
            dr = Me._DataTableEncabezado.NewRow

            dr.Item("Titulo") = getSettings("Titulo")
            dr.Item("NIT") = String.Format("{0}{1}Tel: 4129491{2}www.taxiportal.com.co{3}Registro de Préstamo", getSettings("NIT"), vbCrLf, vbCrLf, vbCrLf)

            dr.Item("idPrestamo") = "Nro. Préstamo: " & prestamo.idPrestamo.ToString
            dr.Item("Concepto") = "Cto: " & Microsoft.VisualBasic.Left(prestamo.Descripcion.ToString, 25)
            dr.Item("fechaRegistro") = "Registro: " & prestamo.FechaRegistro.ToString
            dr.Item("Inicio") = "Inicio Pagos: " & prestamo.FechaInicio.Value.Date
            dr.Item("Fin") = "Fin Pagos: " & prestamo.FechaFin.Value.Date
            dr.Item("Observaciones") = "Obs: " & Microsoft.VisualBasic.Left(prestamo.Observaciones, 23)
            dr.Item("TipoPrestamo") = "Tipo Préstamo: " & tipo.Descripcion
            dr.Item("Importe") = "Valor Préstamo: " & FormatCurrency(prestamo.ImportePrestado, 0)
            dr.Item("NroCuotas") = "Cuotas: " & prestamo.NroCuotas
            dr.Item("importeCuotas") = "Valor Cuota: " & FormatCurrency(prestamo.ImporteCuotas, 0)
            dr.Item("importeSaldado") = "Valor Pagado: " & FormatCurrency(prestamo.ImporteSaldado, 0)
            dr.Item("CuotasSaldadas") = "Cuotas Pagadas: " & prestamo.CuotasSaldadas
            dr.Item("Nombre") = "Cond: " & Microsoft.VisualBasic.Left(persona.nombre, 25)
            dr.Item("Usuario") = "Usuario: " & prestamo.usuario
            dr.Item("Finalizado") = "Finalizado: " & IIf(prestamo.finalizado, "Si", "No")

            Me._DataTableEncabezado.Rows.Add(dr)

            ret.Tables.Add(Me._DataTableEncabezado)
            ret.Tables.Add(Me._DataTableDetalle)

            Return ret
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Sub Totalizar()
        txtValorCuota.DecimalDigits = 2
        txtValorCuota.Value = (txtValorPrestamo.Value / txtNroCuotas.Value)
    End Sub


#End Region



#Region "EVENTOS"
    Private Sub GridEX1_SelectionChanged(sender As Object, e As System.EventArgs) Handles GridEX1.SelectionChanged
        Try
            txtConcepto.Text = GridEX1.GetValue("Concepto")
            txtObservaciones.Text = GridEX1.GetValue("Observaciones")
            cboTipoPrestamo.SelectedValue = GridEX1.GetValue("idTipoPrestamo")
            txtValorPrestamo.Value = GridEX1.GetValue("ImportePrestado")
            dtpDesde.Value = GridEX1.GetValue("FechaInicio")
            dtpHasta.Value = GridEX1.GetValue("FechaFin")
            txtNroCuotas.Value = GridEX1.GetValue("NroCuotas")
            txtValorCuota.Value = GridEX1.GetValue("ImporteCuotas")
            txtCuotasSaldadas.Value = GridEX1.GetValue("CuotasSaldadas")
            txtValorSaldado.Value = GridEX1.GetValue("ImporteSaldado")
        Catch ex As Exception

        End Try

    End Sub



    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        _esNuevo = True
        ModoEdicion()
        LimpiarCampos()

    End Sub



    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles btnMovimiento.Click
        Dim f As New frmMovimientos
        f.AbrirForm(GridEX1.GetValue("idPrestamo"))
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        GuardarPrestamo()
        If bandera = False Then Exit Sub
        ObtenerCuentaAhorro()
        ObtenerPrestamos()
    End Sub



    Private Sub cmdActualizar_Click(sender As System.Object, e As System.EventArgs) Handles cmdActualizar.Click
        ObtenerPrestamos()
    End Sub

    Private Sub CalcularDiasPrestamo()
        Try
            Dim _Festivos() As proc_FestivosLoadAllResult = d.proc_FestivosLoadAll().ToArray

            'dtpHasta.Value = DateAdd(DateInterval.Day, 1, dtpHasta.Value)
            Dim diascu As Integer = (txtNroCuotas.Value - 1) - (dtpHasta.Value - dtpDesde.Value).TotalDays
            Dim fecha1 = DateAdd(DateInterval.Day, diascu, dtpHasta.Value)
            Dim fecha2 = DateAdd(DateInterval.Day, diascu + 1, dtpHasta.Value)
            If fecha1.Month <> fecha2.Month Then
                dtpHasta.Value = DateAdd(DateInterval.Day, diascu + 1, dtpHasta.Value)
            Else
                If diascu = 0 Then
                    dtpHasta.Value = DateAdd(DateInterval.Day, diascu + 1, dtpHasta.Value)
                Else
                    dtpHasta.Value = DateAdd(DateInterval.Day, diascu, dtpHasta.Value)
                End If
            End If


            'Dim fechaHastaBu As DateTimePicker
            'fechaHastaBu.Value = DateAdd(DateInterval.Day, 2, dtpHasta.Value)

            Dim _Pico_Placa() As proc_ObtenerDiasPicoYPlacaPorMovilyFechaResult = d.proc_ObtenerDiasPicoYPlacaPorMovilyFecha(_Vehiculo.Placa, dtpDesde.Value, dtpHasta.Value).ToArray
            Dim DiasPYP As Integer = 0
            Dim EsFestivo As Boolean = False
            For i = 0 To _Pico_Placa.Length - 1

                If _Pico_Placa(i).Dia.ToShortDateString = dtpHasta.Value.ToShortDateString And _Pico_Placa(i).Dia.DayOfWeek <> DayOfWeek.Saturday And _Pico_Placa(i).Dia.DayOfWeek <> DayOfWeek.Sunday Then
                    For j = 0 To _Festivos.Length - 1
                        If _Pico_Placa(i).Dia.ToShortDateString.Equals(_Festivos(j).diaFestivo.ToShortDateString) Then
                            EsFestivo = True
                            Exit For
                        End If
                    Next
                    If EsFestivo = False Then
                        DiasPYP += 1
                    End If
                    EsFestivo = False
                End If

            Next



            'If txtNroCuotas.UpButton Then

            'Else
            'dtpHasta.Value = DateAdd(DateInterval.Day, -1, dtpHasta.Value)
            'End If


            'Dim CantidadDias As Integer = (dtpHasta.Value - dtpDesde.Value).TotalDays + DiasPYP


            dtpHasta.Value = DateAdd(DateInterval.Day, DiasPYP, dtpHasta.Value)



            ' dtpHasta.Value = DateAdd(DateInterval.Day, CantidadDias - 1, dtpHasta.Value)

            ' dtpHasta.Value = DateAdd(DateInterval.Day, CantidadDias, dtpDesde.Value)

            'txtNroCuotas.Value += (CantidadDias + 1)
        Catch ex As Exception
            ' MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub dtpDesde_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpDesde.ValueChanged
        If dtpDesde.Enabled Then CalcularDiasPrestamo()
    End Sub

    Private Sub txtNroCuotas_ValueChanged(sender As System.Object, e As System.EventArgs) Handles txtNroCuotas.ValueChanged

        CalcularDiasPrestamo()
        Totalizar()
    End Sub

    Private Sub btnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimir.Click
        ImprimirTickete(GridEX1.GetValue("idPrestamo"))

    End Sub

    Private Sub txtValorPrestamo_ValueChanged(sender As Object, e As System.EventArgs) Handles txtValorPrestamo.ValueChanged
        Totalizar()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click
        ModoEdicion()
    End Sub

    Private Sub dtpHasta_ValueChanged(sender As Object, e As System.EventArgs) Handles dtpHasta.ValueChanged
    End Sub
#End Region




   

End Class