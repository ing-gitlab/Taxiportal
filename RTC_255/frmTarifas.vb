﻿Imports Common
Imports System.IO
Imports System.Data
Imports System.Drawing.Imaging
Imports SQLImages
Public Class frmTarifas

#Region "Declaraciones"

    Dim _Nuevo As Boolean = False
    Dim d As New DataClasses1DataContext(getSettings("dbConnection"))

#End Region

#Region "Eventos"

    Private Sub cboOrigen_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOrigen.SelectedValueChanged
        obtenerTarifas()
    End Sub

    Private Sub frmTarifas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'TelollevoDataSet.Zonas' Puede moverla o quitarla según sea necesario.
        ' Me.ZonasTableAdapter.Fill(Me.TelollevoDataSet.Zonas)
        Me.ToolTip1.SetToolTip(BnHistorial, "Historial de Tarifas")
        obtenerOrigenes()
    End Sub

    Private Sub GridEX1_EditingCell(ByVal sender As Object, ByVal e As Janus.Windows.GridEX.EditingCellEventArgs) Handles GridEX1.EditingCell
        If e.Column.Caption <> "Tarifa" Then
            e.Cancel = True
        End If
    End Sub

    Private Sub GridEX1_UpdatingRecord(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GridEX1.UpdatingRecord
        Try
            If Not IsNumeric(GridEX1.GetValue("Tarifa")) Then
                MsgBox("La tarifa debe ser un valor numérico")
                e.Cancel = 1
                Exit Sub
            End If
            If CDbl(GridEX1.GetValue("Tarifa")) < 0 Then
                MsgBox("La tarifa debe ser un valor positivo")
                e.Cancel = 1
                Exit Sub
            End If
            'Dim id As Integer
            Dim tarifa As Decimal = GridEX1.GetValue("Tarifa")
            d.proc_TarifasInsert(CInt(Me.cboOrigen.SelectedValue), CInt(GridEX1.GetValue("idBarrio")), NOW_GLOBAL, tarifa, USER_GLOBAL.idUsuario)
            d.proc_TarifasInsert(CInt(GridEX1.GetValue("idBarrio")), CInt(Me.cboOrigen.SelectedValue), NOW_GLOBAL, tarifa, USER_GLOBAL.idUsuario)

        Catch ex As Exception
            MsgBox(ex.Message)
            e.Cancel = True

        End Try
    End Sub

    Private Sub GridEX1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridEX1.KeyDown
        Dim suprimir As Boolean = False

        If (e.KeyCode < 48 Or e.KeyCode > 57) Then
            suprimir = True
        End If

        If e.KeyCode = Keys.Enter Then
            suprimir = False
        End If

        If e.KeyCode = Keys.Escape Then
            suprimir = False
        End If
        If e.KeyCode = Keys.Delete Then
            suprimir = False
        End If

        If e.KeyCode = Keys.Back Then
            suprimir = False
        End If

        If e.KeyCode = Keys.Up Or e.KeyCode = Keys.Down Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then
            suprimir = False
        End If

        If e.KeyCode = Keys.Next Or e.KeyCode = Keys.End Or e.KeyCode = Keys.Home Or e.KeyCode = Keys.Prior Then
            suprimir = False
        End If


        e.SuppressKeyPress = suprimir
    End Sub

    Private Sub BnHistorial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BnHistorial.Click
        Dim f As New frmHistorial
        f.abrirFormulario("Historial de Tarifas", d.proc_TarifasLoadByBarrioOrigen(cboOrigen.SelectedValue).ToList)
        f.GridEX1.ColumnAutoResize = True
        f.GridEX1.RootTable.Columns("idBarrioOrigen").Visible = False
        f.GridEX1.RootTable.Columns("idBarrioDestino").Visible = False
        f.GridEX1.RootTable.Columns("fecha").Caption = "Fecha"
        f.GridEX1.RootTable.Columns("tarifa").Caption = "Tarifa"
        f.GridEX1.RootTable.Columns("tarifa").FormatString = " ###,###,##0"
        f.GridEX1.RootTable.Columns("idUsuario").Caption = "USUARIO"
        f.GridEX1.RootTable.Columns("Descripcion").Caption = "Descripción"
    End Sub

#End Region

#Region "Metodos"

    Private Sub obtenerTarifas()
        Try
            'GridEX1.BuiltInTexts(Janus.Windows.GridEX.GridEXBuiltInText.GroupByBoxInfo) = "Arrastre una columna aquí para agrupar los datos."
            'GridEX1.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic

            Dim d As New DataClasses1DataContext(getSettings("dbConnection"))
            GridEX1.SetDataBinding(d.proc_TarifasObtenerPorBarrio(Me.cboOrigen.SelectedValue, d.proc_BarriosLoadByPrimaryKey(Me.cboOrigen.SelectedValue).First.FueraBarranquilla).ToList, String.Empty)
            GridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.True
            GridEX1.RetrieveStructure()
            GridEX1.RootTable.Columns("idBarrio").Visible = False
            GridEX1.Tables(0).Columns("Tarifa").FormatString = " ###,###,##0"
            'GridEX1.RootTable.Columns("tarifa").FormatString = "$ 0"
            GridEX1.ColumnAutoResize = True

        Catch ex As Exception
            ' MsgBox(ex)
        End Try

    End Sub

    Private Sub obtenerOrigenes()
        Dim ListaBarrios = d.proc_ObtenerBarriosTodos.ToList
        Me.cboOrigen.ValueMember = "idBarrio"
        Me.cboOrigen.DisplayMember = "Barrio"
        Me.cboOrigen.DataSource = ListaBarrios
    End Sub

#End Region





End Class