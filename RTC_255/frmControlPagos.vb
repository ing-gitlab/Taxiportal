﻿Imports Common
Imports System.Data

Public Class frmControlPagos
    Private esNuevo As Boolean = False
    Private _idVehiculo As Integer
    Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim _DataTableEncabezado As DataTable
    Dim _DataTableDetalle As DataTable

    Public Sub AbrirVentana(ByVal idVehiculo As Integer)
        On Error Resume Next
        Me._idVehiculo = idVehiculo
        Me.Text = "Control de Pagos. Movil " & New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL)).proc_VehiculosLoadByPrimaryKey(_idVehiculo).First.NroMovil
        Dim user = d.proc_UsuariosLoadByPrimaryKey(USER_GLOBAL).ToList.First
        Dim movil = d.proc_VehiculosLoadByPrimaryKey(idVehiculo).ToList.First
        
        If user.idTipoUsuario = 1 Or user.idTipoUsuario = 2 Then
            If Not movil.Habilitado Then
                MsgBox("El móvil seleccionado esta inhabilitado, no puede editarle pagos.")
                Exit Sub
            End If
            Me.ShowDialog()
        Else
            MsgBox("Usted no tiene permisos para esta ventana")

        End If
    End Sub

    Private Sub frmControlPagos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        Me.dtpDesde.Value = New Date(NOW_GLOBAL.Year, NOW_GLOBAL.Month, 1)
        Me.dtpHasta.Value = New Date(NOW_GLOBAL.Year, NOW_GLOBAL.Month, 1).AddMonths(1).AddDays(-1)
        LlenarComboMes()
        Me.NumericUpDown1.Value = Now.Year
        LlenarGrilla()
    End Sub

    Private Sub LlenarComboMes()
        Dim datetimeFormat = Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat
        Me.cboMes.DataSource = datetimeFormat.MonthNames()
        Me.cboMes.SelectedItem = datetimeFormat.GetMonthName(Date.Today.Month)

    End Sub

    Private Sub LlenarGrilla()

        GridEX1.SetDataBinding(d.proc_ObtenerPagosVehiculo(_idVehiculo, Date.Parse(dtpDesde.Value), Date.Parse(dtpHasta.Value)), "")
        GridEX1.RetrieveStructure()

        GridEX1.Tables(0).Columns("idPago").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("FechaPago").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("FechaPago").Caption = "Fecha De Pago"
        GridEX1.Tables(0).Columns("FechaInhabilitacion").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center
        GridEX1.Tables(0).Columns("FechaInhabilitacion").FormatString = "dd/MM/yyyy"
        GridEX1.Tables(0).Columns("FechaInhabilitacion").Caption = "Fecha Inhabilitación"
        GridEX1.Tables(0).Columns("FechaInhabilitacion").AutoSize()
        GridEX1.Tables(0).Columns("Importe").FormatString = "###,###,##0.00"
        GridEX1.Tables(0).Columns("Importe").CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far
        GridEX1.Tables(0).Columns("FechaPago").AutoSize()


        For Each Columna As Janus.Windows.GridEX.GridEXColumn In GridEX1.Tables(0).Columns
            Columna.HeaderAlignment = Janus.Windows.GridEX.TextAlignment.Center
        Next

        Me.btnEliminar.Enabled = (Me.GridEX1.RowCount > 0)
        Me.btnModificar.Enabled = (Me.GridEX1.RowCount > 0)
    End Sub

    Private Sub GridEX1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridEX1.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()

    End Sub

    Private Sub GridEX1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridEX1.SelectionChanged
        Try
            If Me.GridEX1.GetRow.RowType = Janus.Windows.GridEX.RowType.FilterRow OrElse Me.GridEX1.RowCount = 0 Then Exit Sub
            Me.dtpFechaDeInaHabilitacion.Value = GridEX1.GetValue("FechaInhabilitacion")
            Me.dtpFechaPago.Value = GridEX1.GetValue("FechaPago")
            Me.txtObs.Text = GridEX1.GetValue("Observaciones")
            Me.nudImporte.Value = GridEX1.GetValue("Importe")
            Me.cboMes.SelectedIndex = GridEX1.GetValue("MesPago") - 1
            Me.NumericUpDown1.Value = GridEX1.GetValue("AñoPago")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If USER_GLOBAL.ToUpper <> "IVON" Then
            MsgBox("Usted no tiene permisos para modificar pagos.")
            Exit Sub
        End If
        Dim f As New frmPassword
        If Not f.ConfirmarClave Then
            Exit Sub
        End If
        ' Dim user = d.proc_UsuariosLoadByPrimaryKey(USER_GLOBAL).ToList.First
        ' If user.idTipoUsuario <> 1 Then
       
        '  End If
        esNuevo = False
        IniciarEdicion(True)
        Me.dtpFechaPago.Focus()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Dim f As New frmPassword
        If Not f.ConfirmarClave Then
            Exit Sub
        End If
        esNuevo = True
        IniciarEdicion(True)
        Me.dtpFechaPago.Focus()
        Me.dtpFechaPago.Value = NOW_GLOBAL
        Me.dtpFechaDeInaHabilitacion.Value = Today
        NumericUpDown1.Value = Now.Year
        cboMes.SelectedIndex = Now.Month - 1
        Me.nudImporte.Value = 0
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.IniciarEdicion(False)
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try
          
            Dim idPago As Integer
            Dim Posicion As Integer

            If Validar() Then
                If esNuevo Then
                    'Ingresar Un Pago
                    Dim nuevo = d.proc_PagosRFInsert(idPago, _idVehiculo, dtpFechaPago.Value, dtpFechaDeInaHabilitacion.Value.Date.AddHours(12), nudImporte.Value, USER_GLOBAL, txtObs.Text, cboMes.SelectedIndex + 1, NumericUpDown1.Value)
                    d.proc_LogInsert(USER_GLOBAL, String.Format("Movil: {0} Importe: {1} FechaInhab: {2}", _idVehiculo, nudImporte.Value, dtpFechaDeInaHabilitacion.Value), NOW_GLOBAL, "Inserta Pago")

                Else
                    'modificar 
                    Posicion = Me.GridEX1.GetRow().Position
                    d.proc_PagosRFUpdate(GridEX1.GetValue("idPago"), _idVehiculo, dtpFechaPago.Value, dtpFechaDeInaHabilitacion.Value.Date.AddHours(12), nudImporte.Value, USER_GLOBAL, txtObs.Text, cboMes.SelectedIndex + 1, NumericUpDown1.Value)
                    d.proc_LogInsert(USER_GLOBAL, String.Format("Movil: {0} Importe: {1} FechaInhab: {2}", _idVehiculo, nudImporte.Value, dtpFechaDeInaHabilitacion.Value), NOW_GLOBAL, "Modifica Pago")
                End If

                IniciarEdicion(False)
                LlenarGrilla()

                If esNuevo Then
                    Posicion = Me.GridEX1.RowCount - 1
                End If
                Me.GridEX1.MoveTo(Posicion)
            End If
            ' ActualizarRemotamente()
            StartBroadcast(PORT_Principal, "RefrescarMoviles")
        Catch ex As Exception
            d.proc_LogInsert(USER_GLOBAL, ex.Message, NOW_GLOBAL, "Error en Pago")
        End Try
       
    End Sub

    'Private Sub ActualizarRemotamente()
    '    Dim listaLocal = d.ObtenerMovilesEnvio(_idVehiculo).ToList
    '    Dim listaRemota(listaLocal.Count - 1) As WebService.ObtenerMovilesEnvioResult
    '    Dim INDEX As Integer = 0
    '    For Each reg In listaLocal
    '        Dim regRemoto As New WebService.ObtenerMovilesEnvioResult
    '        regRemoto.idVehiculo = reg.idVehiculo
    '        regRemoto.AdminTemp = reg.AdminTemp
    '        regRemoto.AdminTemp2 = reg.AdminTemp2
    '        regRemoto.cedula = reg.cedula
    '        regRemoto.cedula1 = reg.cedula1
    '        regRemoto.EstadoSoat = reg.EstadoSoat
    '        regRemoto.fecha = reg.FechaPago
    '        regRemoto.fechaAfiliacion = reg.fechaAfiliacion
    '        regRemoto.fechaFinSancion = reg.fechaFinSancion
    '        regRemoto.FechaInhabilitacion = reg.FechaInhabilitacion
    '        regRemoto.FechaPago = reg.FechaPago
    '        regRemoto.Habilitado = reg.Habilitado
    '        regRemoto.idPago = reg.idPago
    '        regRemoto.idUsuario = reg.idUsuario
    '        regRemoto.idUsuario1 = reg.idUsuario1
    '        regRemoto.idVehiculo1 = reg.idVehiculo1
    '        regRemoto.Importe = reg.Importe
    '        regRemoto.Modelo = reg.Modelo
    '        regRemoto.NroMovil = reg.NroMovil
    '        regRemoto.Placa = reg.Placa
    '        regRemoto.telefono = reg.telefono
    '        regRemoto.TipoVehiculo = reg.TipoVehiculo
    '        regRemoto.Usuario = reg.Usuario
    '        listaRemota(INDEX) = regRemoto
    '        INDEX += 1
    '    Next
    '    Dim w As New WebService.Service

    '    Dim S As String = w.ActualizarEstadoMoviles(listaRemota)

    'End Sub

    Private Function Validar() As Boolean
        If Not nudImporte.Value > 0 Then
            MsgBox("Debe Ingresar Importe", MsgBoxStyle.Critical, "Error")
            Me.nudImporte.Focus()
            Return False
        End If
        If cboMes.Text = "" Then
            MsgBox("Debe seleccionar un mes")
            cboMes.Focus()
            Return False
        End If
        Return True
    End Function

    Private Sub IniciarEdicion(ByVal EsInicio As Boolean)
        Me.GridEX1.Enabled = Not EsInicio
        Me.btnNuevo.Visible = Not EsInicio
        Me.btnModificar.Visible = Not EsInicio
        Me.dtpFechaDeInaHabilitacion.Enabled = EsInicio
        Me.dtpFechaPago.Enabled = EsInicio
        cboMes.Enabled = EsInicio
        NumericUpDown1.Enabled = EsInicio
        Me.nudImporte.Enabled = EsInicio
        Me.btnAceptar.Visible = EsInicio
        Me.btnCancelar.Visible = EsInicio
        Me.btnEliminar.Visible = Not EsInicio
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click

        If UCase(USER_GLOBAL) <> UCase("ivon") Then
            MsgBox("Usted no tiene permisos para eliminar pagos.")
            Exit Sub
        End If
        Dim f As New frmPassword
        If Not f.ConfirmarClave Then
            Exit Sub
        End If
       
        If MsgBox("¿Está seguro de eliminar el pago?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Confirmación de eliminación") = MsgBoxResult.Yes Then
            Try
                Dim eliminar = d.proc_PagosRFDelete(GridEX1.GetValue("idPago"))
                LlenarGrilla()
                '    ActualizarRemotamente()
            Catch ex As Exception

            End Try


        End If
    End Sub

    Private Sub GridEX1_FormattingRow(ByVal sender As System.Object, ByVal e As Janus.Windows.GridEX.RowLoadEventArgs) Handles GridEX1.FormattingRow

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        ImprimirTickete(GridEX1.GetValue("idPago"))
    End Sub


    Private Sub ImprimirTickete(ByVal ID As Integer)
        Dim ids As New clsImpresionDataSet
        Dim ps As New System.Drawing.Printing.PageSettings
        Dim size As New System.Drawing.Printing.PaperSize

        With ids
            .TipoImpresora = clsImpresionDataSet.TipoImpresora_t.Grafica

            size = New System.Drawing.Printing.PaperSize("76mm Roll Paper", 76, 297)

            ps.PaperSize = size

            .ConfiguracionPagina = ps
            .NombreDocumentoImpresion = "Servicio"
            .PathArchivoXML = My.Application.Info.DirectoryPath & "\Comprobante.xml"
            .DataSet = DataSetImpresion(ID)

            .Imprimir()

        End With

        'Hago esto porque si se quiere reimprimir, arroja un error al querer agregar nuevamente los DataTables al DataSet:
        ids.DataSet.Tables.Clear()
        ids = Nothing
    End Sub

    Private Function DataSetImpresion(ByVal ID As Integer) As DataSet
        Try
            Dim ret As New DataSet
            Dim dr As DataRow
            Dim pago = d.proc_PagosRFLoadByPrimaryKey(ID).ToList.First

            Dim movil = d.proc_VehiculosLoadByPrimaryKey(pago.idVehiculo).ToList.First
            _DataTableEncabezado = New DataTable
            _DataTableDetalle = New DataTable
            Me._DataTableEncabezado.Rows.Clear()
            Me._DataTableEncabezado.Columns.Add("Titulo")
            Me._DataTableEncabezado.Columns.Add("NIT")

            Me._DataTableEncabezado.Columns.Add("Movil")
            Me._DataTableEncabezado.Columns.Add("Monto")
            Me._DataTableEncabezado.Columns.Add("Saldo")
            Me._DataTableEncabezado.Columns.Add("FechaPago")
            Me._DataTableEncabezado.Columns.Add("FechaInhabilitacion")

            dr = Me._DataTableEncabezado.NewRow

            dr.Item("Titulo") = getSettings("Titulo")
            dr.Item("NIT") = getSettings("NIT") & vbCrLf & "Comprobante de Pago " & pago.idPago.ToString("0000000")

            dr.Item("Movil") = "Móvil: " & movil.NroMovil.ToString
            dr.Item("Monto") = "Tarifa: " & pago.Importe.ToString
            dr.Item("Saldo") = "Saldo: " & InputBox("Ingrese saldo pendiente. ", "Saldo", "0")
            dr.Item("FechaPago") = "Fecha Pago: " & pago.FechaPago.ToString
            dr.Item("FechaInhabilitacion") = "Fecha Inhab: " & CDate(pago.FechaInhabilitacion).Date.ToString


            Me._DataTableEncabezado.Rows.Add(dr)

            ret.Tables.Add(Me._DataTableEncabezado)
            ret.Tables.Add(Me._DataTableDetalle)

            Return ret
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Private Sub cmdActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActualizar.Click
        LlenarGrilla()
    End Sub

    Private Sub GridEX1_SizingCards(ByVal sender As Object, ByVal e As Janus.Windows.GridEX.SizingCardsEventArgs) Handles GridEX1.SizingCards

    End Sub

   
    Private Sub Grupbox1_Enter(sender As System.Object, e As System.EventArgs) Handles Grupbox1.Enter

    End Sub
End Class