﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal))
        Me.UiPanelManager1 = New Janus.Windows.UI.Dock.UIPanelManager(Me.components)
        Me.uiPanel0 = New Janus.Windows.UI.Dock.UIPanel()
        Me.uiPanel0Container = New Janus.Windows.UI.Dock.UIPanelInnerContainer()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GridEX1 = New Janus.Windows.GridEX.GridEX()
        Me.btnAgregarMovil = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPosicion = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.txtMovil1 = New Janus.Windows.GridEX.EditControls.NumericEditBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboOrigen = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.AdministraciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdministrarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BarriosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PersonasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TarifasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ValesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VehiculosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PicoYPlacaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransaccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CoonfiguracionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EnviarActualizaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecaudaciónDiariaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ValesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcercaDeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.mnuEditarMovil = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAsignarServicio = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMovil = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.statusStrip = New System.Windows.Forms.StatusStrip()
        Me.lblUpdate = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        CType(Me.UiPanelManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiPanel0, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.uiPanel0.SuspendLayout()
        Me.uiPanel0Container.SuspendLayout()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.mnuMovil.SuspendLayout()
        Me.statusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'UiPanelManager1
        '
        Me.UiPanelManager1.ContainerControl = Me
        Me.uiPanel0.Id = New System.Guid("88bce23c-fddd-43c3-8c21-3ef08410ab54")
        Me.UiPanelManager1.Panels.Add(Me.uiPanel0)
        '
        'Design Time Panel Info:
        '
        Me.UiPanelManager1.BeginPanelInfo()
        Me.UiPanelManager1.AddDockPanelInfo(New System.Guid("88bce23c-fddd-43c3-8c21-3ef08410ab54"), Janus.Windows.UI.Dock.PanelDockStyle.Left, New System.Drawing.Size(270, 379), True)
        Me.UiPanelManager1.EndPanelInfo()
        '
        'uiPanel0
        '
        Me.uiPanel0.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.[False]
        Me.uiPanel0.InnerContainer = Me.uiPanel0Container
        Me.uiPanel0.Location = New System.Drawing.Point(3, 27)
        Me.uiPanel0.Name = "uiPanel0"
        Me.uiPanel0.Size = New System.Drawing.Size(270, 379)
        Me.uiPanel0.TabIndex = 4
        Me.uiPanel0.Text = "Moviles"
        '
        'uiPanel0Container
        '
        Me.uiPanel0Container.Controls.Add(Me.Button2)
        Me.uiPanel0Container.Controls.Add(Me.Button1)
        Me.uiPanel0Container.Controls.Add(Me.GridEX1)
        Me.uiPanel0Container.Controls.Add(Me.btnAgregarMovil)
        Me.uiPanel0Container.Controls.Add(Me.Label1)
        Me.uiPanel0Container.Controls.Add(Me.txtPosicion)
        Me.uiPanel0Container.Controls.Add(Me.txtMovil1)
        Me.uiPanel0Container.Controls.Add(Me.Label3)
        Me.uiPanel0Container.Controls.Add(Me.cboOrigen)
        Me.uiPanel0Container.Controls.Add(Me.Label2)
        Me.uiPanel0Container.Location = New System.Drawing.Point(1, 23)
        Me.uiPanel0Container.Name = "uiPanel0Container"
        Me.uiPanel0Container.Size = New System.Drawing.Size(264, 355)
        Me.uiPanel0Container.TabIndex = 0
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(81, 7)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(25, 20)
        Me.Button2.TabIndex = 20
        Me.Button2.Text = "..."
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Location = New System.Drawing.Point(235, 63)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(24, 14)
        Me.Button1.TabIndex = 19
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GridEX1
        '
        Me.GridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.[True]
        Me.GridEX1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridEX1.Location = New System.Drawing.Point(2, 61)
        Me.GridEX1.Name = "GridEX1"
        Me.GridEX1.Size = New System.Drawing.Size(259, 314)
        Me.GridEX1.TabIndex = 7
        Me.GridEX1.TabKeyBehavior = Janus.Windows.GridEX.TabKeyBehavior.ControlNavigation
        '
        'btnAgregarMovil
        '
        Me.btnAgregarMovil.Location = New System.Drawing.Point(202, 32)
        Me.btnAgregarMovil.Name = "btnAgregarMovil"
        Me.btnAgregarMovil.Size = New System.Drawing.Size(59, 23)
        Me.btnAgregarMovil.TabIndex = 7
        Me.btnAgregarMovil.Text = "&Agregar"
        Me.btnAgregarMovil.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "&Móvil"
        '
        'txtPosicion
        '
        Me.txtPosicion.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtPosicion.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtPosicion.Location = New System.Drawing.Point(161, 7)
        Me.txtPosicion.Name = "txtPosicion"
        Me.txtPosicion.Size = New System.Drawing.Size(35, 20)
        Me.txtPosicion.TabIndex = 4
        Me.txtPosicion.Text = "0"
        Me.txtPosicion.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtPosicion.Value = CType(0, Short)
        Me.txtPosicion.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16
        '
        'txtMovil1
        '
        Me.txtMovil1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.txtMovil1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.txtMovil1.Location = New System.Drawing.Point(38, 7)
        Me.txtMovil1.Name = "txtMovil1"
        Me.txtMovil1.Size = New System.Drawing.Size(42, 20)
        Me.txtMovil1.TabIndex = 2
        Me.txtMovil1.Text = "0"
        Me.txtMovil1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        Me.txtMovil1.Value = CType(0, Short)
        Me.txtMovil1.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(108, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "&Posición"
        '
        'cboOrigen
        '
        Me.cboOrigen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboOrigen.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboOrigen.FormattingEnabled = True
        Me.cboOrigen.Location = New System.Drawing.Point(38, 34)
        Me.cboOrigen.Name = "cboOrigen"
        Me.cboOrigen.Size = New System.Drawing.Size(158, 21)
        Me.cboOrigen.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "&Zona"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdministraciónToolStripMenuItem, Me.ReportesToolStripMenuItem, Me.AyudaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(989, 24)
        Me.MenuStrip1.TabIndex = 6
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'AdministraciónToolStripMenuItem
        '
        Me.AdministraciónToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdministrarToolStripMenuItem, Me.ServiciosToolStripMenuItem, Me.CoonfiguracionesToolStripMenuItem, Me.SalirToolStripMenuItem, Me.EnviarActualizaciónToolStripMenuItem})
        Me.AdministraciónToolStripMenuItem.Name = "AdministraciónToolStripMenuItem"
        Me.AdministraciónToolStripMenuItem.Size = New System.Drawing.Size(100, 20)
        Me.AdministraciónToolStripMenuItem.Text = "A&dministración"
        '
        'AdministrarToolStripMenuItem
        '
        Me.AdministrarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BarriosToolStripMenuItem, Me.ClientesToolStripMenuItem, Me.PersonasToolStripMenuItem, Me.TarifasToolStripMenuItem, Me.UsuariosToolStripMenuItem, Me.ValesToolStripMenuItem, Me.VehiculosToolStripMenuItem, Me.PicoYPlacaToolStripMenuItem, Me.TransaccionesToolStripMenuItem})
        Me.AdministrarToolStripMenuItem.Name = "AdministrarToolStripMenuItem"
        Me.AdministrarToolStripMenuItem.Size = New System.Drawing.Size(249, 22)
        Me.AdministrarToolStripMenuItem.Text = "Ver"
        '
        'BarriosToolStripMenuItem
        '
        Me.BarriosToolStripMenuItem.Name = "BarriosToolStripMenuItem"
        Me.BarriosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.BarriosToolStripMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.BarriosToolStripMenuItem.Text = "&Barrios"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.ClientesToolStripMenuItem.Text = "&Clientes"
        '
        'PersonasToolStripMenuItem
        '
        Me.PersonasToolStripMenuItem.Name = "PersonasToolStripMenuItem"
        Me.PersonasToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.PersonasToolStripMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.PersonasToolStripMenuItem.Text = "Perso&nas"
        '
        'TarifasToolStripMenuItem
        '
        Me.TarifasToolStripMenuItem.Name = "TarifasToolStripMenuItem"
        Me.TarifasToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.T), System.Windows.Forms.Keys)
        Me.TarifasToolStripMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.TarifasToolStripMenuItem.Text = "&Tarifas"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.U), System.Windows.Forms.Keys)
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.UsuariosToolStripMenuItem.Text = "&Usuarios"
        '
        'ValesToolStripMenuItem
        '
        Me.ValesToolStripMenuItem.Name = "ValesToolStripMenuItem"
        Me.ValesToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.ValesToolStripMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.ValesToolStripMenuItem.Text = "&Vales"
        '
        'VehiculosToolStripMenuItem
        '
        Me.VehiculosToolStripMenuItem.Name = "VehiculosToolStripMenuItem"
        Me.VehiculosToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.VehiculosToolStripMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.VehiculosToolStripMenuItem.Text = "V&ehiculos"
        '
        'PicoYPlacaToolStripMenuItem
        '
        Me.PicoYPlacaToolStripMenuItem.Name = "PicoYPlacaToolStripMenuItem"
        Me.PicoYPlacaToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.Y), System.Windows.Forms.Keys)
        Me.PicoYPlacaToolStripMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.PicoYPlacaToolStripMenuItem.Text = "&Pico y placa - Fest"
        '
        'TransaccionesToolStripMenuItem
        '
        Me.TransaccionesToolStripMenuItem.Name = "TransaccionesToolStripMenuItem"
        Me.TransaccionesToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.TransaccionesToolStripMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.TransaccionesToolStripMenuItem.Text = "T&ransacciones"
        '
        'ServiciosToolStripMenuItem
        '
        Me.ServiciosToolStripMenuItem.Name = "ServiciosToolStripMenuItem"
        Me.ServiciosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.ServiciosToolStripMenuItem.Size = New System.Drawing.Size(249, 22)
        Me.ServiciosToolStripMenuItem.Text = "&Servicios"
        '
        'CoonfiguracionesToolStripMenuItem
        '
        Me.CoonfiguracionesToolStripMenuItem.Name = "CoonfiguracionesToolStripMenuItem"
        Me.CoonfiguracionesToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.CoonfiguracionesToolStripMenuItem.Size = New System.Drawing.Size(249, 22)
        Me.CoonfiguracionesToolStripMenuItem.Text = "C&onfiguraciones"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(249, 22)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'EnviarActualizaciónToolStripMenuItem
        '
        Me.EnviarActualizaciónToolStripMenuItem.Name = "EnviarActualizaciónToolStripMenuItem"
        Me.EnviarActualizaciónToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F5), System.Windows.Forms.Keys)
        Me.EnviarActualizaciónToolStripMenuItem.Size = New System.Drawing.Size(249, 22)
        Me.EnviarActualizaciónToolStripMenuItem.Text = "Actualizar"
        Me.EnviarActualizaciónToolStripMenuItem.Visible = False
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RecaudaciónDiariaToolStripMenuItem, Me.ValesToolStripMenuItem1})
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(65, 20)
        Me.ReportesToolStripMenuItem.Text = "Reportes"
        '
        'RecaudaciónDiariaToolStripMenuItem
        '
        Me.RecaudaciónDiariaToolStripMenuItem.Name = "RecaudaciónDiariaToolStripMenuItem"
        Me.RecaudaciónDiariaToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.RecaudaciónDiariaToolStripMenuItem.Text = "Re&caudación Diaria"
        '
        'ValesToolStripMenuItem1
        '
        Me.ValesToolStripMenuItem1.Name = "ValesToolStripMenuItem1"
        Me.ValesToolStripMenuItem1.Size = New System.Drawing.Size(175, 22)
        Me.ValesToolStripMenuItem1.Text = "Va&les"
        '
        'AyudaToolStripMenuItem
        '
        Me.AyudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AcercaDeToolStripMenuItem})
        Me.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem"
        Me.AyudaToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.AyudaToolStripMenuItem.Text = "&Ayuda"
        '
        'AcercaDeToolStripMenuItem
        '
        Me.AcercaDeToolStripMenuItem.Name = "AcercaDeToolStripMenuItem"
        Me.AcercaDeToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.AcercaDeToolStripMenuItem.Text = "A&cerca de"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'mnuEditarMovil
        '
        Me.mnuEditarMovil.Name = "mnuEditarMovil"
        Me.mnuEditarMovil.Size = New System.Drawing.Size(183, 22)
        Me.mnuEditarMovil.Text = "ToolStripMenuItem1"
        '
        'mnuAsignarServicio
        '
        Me.mnuAsignarServicio.Name = "mnuAsignarServicio"
        Me.mnuAsignarServicio.Size = New System.Drawing.Size(183, 22)
        Me.mnuAsignarServicio.Text = "ToolStripMenuItem1"
        '
        'mnuMovil
        '
        Me.mnuMovil.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEditarMovil, Me.mnuAsignarServicio})
        Me.mnuMovil.Name = "mnuMovil"
        Me.mnuMovil.Size = New System.Drawing.Size(184, 48)
        '
        'statusStrip
        '
        Me.statusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblUpdate, Me.ToolStripStatusLabel1})
        Me.statusStrip.Location = New System.Drawing.Point(0, 409)
        Me.statusStrip.Name = "statusStrip"
        Me.statusStrip.Size = New System.Drawing.Size(989, 22)
        Me.statusStrip.TabIndex = 10
        '
        'lblUpdate
        '
        Me.lblUpdate.Name = "lblUpdate"
        Me.lblUpdate.Size = New System.Drawing.Size(110, 17)
        Me.lblUpdate.Text = "Estado: Actualizado"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(0, 17)
        '
        'BackgroundWorker1
        '
        '
        'frmPrincipal
        '
        Me.AcceptButton = Me.btnAgregarMovil
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(989, 431)
        Me.Controls.Add(Me.uiPanel0)
        Me.Controls.Add(Me.statusStrip)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmPrincipal"
        Me.Text = "SGT"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.UiPanelManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiPanel0, System.ComponentModel.ISupportInitialize).EndInit()
        Me.uiPanel0.ResumeLayout(False)
        Me.uiPanel0Container.ResumeLayout(False)
        Me.uiPanel0Container.PerformLayout()
        CType(Me.GridEX1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.mnuMovil.ResumeLayout(False)
        Me.statusStrip.ResumeLayout(False)
        Me.statusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UiPanelManager1 As Janus.Windows.UI.Dock.UIPanelManager
    Friend WithEvents GridEX1 As Janus.Windows.GridEX.GridEX
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents AdministraciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiciosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtMovil1 As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents btnAgregarMovil As System.Windows.Forms.Button
    Friend WithEvents txtPosicion As Janus.Windows.GridEX.EditControls.NumericEditBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboOrigen As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CoonfiguracionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents uiPanel0 As Janus.Windows.UI.Dock.UIPanel
    Friend WithEvents uiPanel0Container As Janus.Windows.UI.Dock.UIPanelInnerContainer
    Friend WithEvents mnuEditarMovil As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAsignarServicio As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMovil As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents EnviarActualizaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents statusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents lblUpdate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents AdministrarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BarriosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PersonasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TarifasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ValesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VehiculosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PicoYPlacaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransaccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecaudaciónDiariaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ValesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AcercaDeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
