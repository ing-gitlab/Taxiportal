﻿Imports Common
Imports Telerik.Charting.Styles
Imports Telerik.Charting
Imports System.Drawing
Imports System.Data.Linq
Imports System.Data

'para crear 

Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Data.SqlClient

Partial Class ReporteRecaudacionVale
    Inherits System.Web.UI.Page

#Region "Eventos"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        lblFecha.Text = "Fecha de Emision: " & Now
        dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
        dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)
        CargarGrilla()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        CargarGrilla()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        GridView1.AllowPaging = False
        GridView1.Rebind()
        GridView1.ExportSettings.OpenInNewWindow = True
        GridView1.ExportSettings.ExportOnlyData = True
        GridView1.ExportSettings.FileName = "ReporteRecaudacionVale"
        GridView1.MasterTableView.ExportToExcel()
    End Sub
#End Region


#Region "Metodos"
    Private Sub CargarGrilla()
        'GridView1.DataSource = Funciones.proc_ObtenerRecaudacionDetalleVale(dtpDesde.SelectedDate, dtpHasta.SelectedDate).Tables(0)
        'GridView1.DataBind()
        Try
            Dim dtBarrios As DataTable = proc_ObtenerRecaudacionDetalleVale(dtpDesde.SelectedDate, dtpHasta.SelectedDate).Tables(0)
            GridView1.DataSource = dtBarrios
            GridView1.DataBind()

            If dtBarrios.Rows.Count > 0 Then
                Dim Importe As Decimal = 0
                Dim ImporteP As Decimal = 0
                For index As Integer = 0 To dtBarrios.Rows.Count - 1
                    Importe += dtBarrios.Rows(index).Item("Tarifa")
                    ImporteP += dtBarrios.Rows(index).Item("pago")
                Next
                lblImporte.Text = "Total Tarifas: " & FormatCurrency(Importe, 0)
                lblImporteP.Text = "Total Pagos: " & FormatCurrency(ImporteP, 0)

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

#End Region


    
End Class
