﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteRecaudacionVale.aspx.vb" Inherits="ReporteRecaudacionVale" %>

<%@ Register assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<%@ Register assembly="Telerik.Charting" namespace="Telerik.Charting" tagprefix="telerik" %>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reporte por Pago de Vales</title>
    <style type="text/css">


.radInput_Default
{
	font:12px aReporte por Barrios</title>
    <style type="text/css">


.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

        .style1
        {
            width: 74px;
        }
        .style2
        {
            width: 73px;
        }

        .style3
        {
            width: 22px;
        }

        .style4
        {
            width: 132px;
        }


        .style1
        {
            height: 58px;
        }
        .style3
        {
            height: 29px;
        }
        .style4
        {
            height: 29px;
            width: 123px;
        }
        
        }
        </style>
</head>
<body>
    <form id="form1" runat="server" >
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        
</telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        
</telerik:RadScriptManager>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            
 <AjaxSettings>
 <telerik:AjaxSetting AjaxControlID="Button1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadChart1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                         <telerik:AjaxUpdatedControl ControlID="GridView1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpHasta"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpDesde"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="lblCarreras" />
                        <telerik:AjaxUpdatedControl ControlID="lblImporte"/>
                        
                         <telerik:AjaxUpdatedControl ControlID="DropDownList1"/>
                        
                        
                    </UpdatedControls>
                </telerik:AjaxSetting>
            
            
 </AjaxSettings>
 </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" >
         <table style="height:100%; width:100%" ><tr><td><img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" /></td></tr></table></telerik:RadAjaxLoadingPanel>

    <div>
        <br />
&nbsp;
      <table>
            <tr>
                <td >

        <asp:Label ID="lblCarreras" runat="server" Font-Names="Calibri" 
            Text="Reporte Recaudaciòn de pago por Vales."></asp:Label>

                </td>
                <td >

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    &nbsp;</td>
                <td >

                    &nbsp;</td>
                <td>

        <asp:Label ID="lblFecha" runat="server" Font-Names="Calibri" 
            Text="Fecha Emision:"></asp:Label>
                </td>
                </td>
            </tr>
            </table>
        <br />
        <br />
      <table>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Desde:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpDesde" Runat="server" 
                        Culture="Spanish (Colombia)">
                        
                        <TimeView Culture="Spanish (Colombia)"></TimeView>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                        
</telerik:RadDateTimePicker>
                </td>
                <td>
                    &nbsp;&nbsp;</td>
                <td>
                    <asp:Button ID="Button1" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Procesar" />
                    </td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Hasta:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpHasta" Runat="server" 
                        Culture="Spanish (Colombia)">
                        
                        <TimeView Culture="Spanish (Colombia)"></TimeView>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                        
</telerik:RadDateTimePicker>
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;</td>
                <td>
                    <asp:Button ID="Button2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Exportar" />
                    </td>
            </tr>
            </table>
        <br />
        <br />
      <table>
            <tr>
                <td >

        <asp:Label ID="lblImporte" runat="server" Font-Names="Calibri" 
            Text="Importe Tarifas: "></asp:Label>

                </td>
                <td >

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    &nbsp;</td>
                <td >

        <asp:Label ID="lblImporteP" runat="server" Font-Names="Calibri" 
            Text="Importe Pagos: "></asp:Label>

                </td>
                <td>

                    &nbsp;</td>
                </td>
            </tr>
            </table>
        <br />
        <br />
    </div>
    
                   
<telerik:RadGrid ID="GridView1" runat="server" AllowSorting="True" 
        GridLines="None" ShowGroupPanel="True" Skin="Vista"
        Font-Size="Small">

<ClientSettings AllowDragToGroup="True">
<Selecting AllowRowSelect="True" />
 </ClientSettings>
 <GroupPanel Text="Arrastre una columna aqui para agrupar">
        
</GroupPanel>
<MasterTableView ShowGroupFooter="true" AllowMultiColumnSorting="true" AutoGenerateColumns="false" >

<Columns>


<telerik:GridBoundColumn  DataField="FechaVale" HeaderText="Fecha Vale"  > 
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="NroVale" HeaderText="Nro. Vale">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  Aggregate = "Sum" DataField="tarifa" HeaderText="Tarifas"    DataFormatString = "{0:C0}" FooterText="Total: ">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  Aggregate="Sum" DataField="pago" HeaderText="Pago"  DataFormatString = "{0:C0}">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="nombre" HeaderText="Usuario">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="NroMovil" HeaderText="Nro Movil">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="Placa" HeaderText="Placa">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="Nombre" HeaderText="Nombre">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="nombre" HeaderText="Cliente">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="CentroCosto" HeaderText="CentroCosto">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="pasajero" HeaderText="Pasajero">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn DataField="fechaCarga" HeaderText="Fecha Carga">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="peaje" HeaderText="Peaje">
</telerik:GridBoundColumn>




</Columns>


<GroupByExpressions>

 <telerik:GridGroupByExpression >
                    <GroupByFields>
                        <telerik:GridGroupByField FieldName="FechaVale"  SortOrder="Ascending" />
                    </GroupByFields>
                    <SelectFields>
                        <telerik:GridGroupByField  FieldName="FechaVale"  HeaderText="fecha tarifa Vales" />
                    </SelectFields>

 </telerik:GridGroupByExpression>
</GroupByExpressions>

        <RowIndicatorColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
        

</MasterTableView>

        <FilterMenu EnableTheming="True">
        <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
</telerik:RadGrid>
               
    <br />
    <p>
        &nbsp;</p>
               
    </form>
</body>
</html>
