﻿Imports Common
Partial Class ReporteRecaudacion
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ObtenerRecaudacion(True)
    End Sub

    Private Sub ObtenerRecaudacion(ByVal doDataBind As Boolean)
        Dim cedula As String = Nothing
        Dim nroMovil As String = Nothing
        If Not cboAdmin.Text = "Todos" Then
            cedula = cboAdmin.SelectedValue
        End If
        If Not cboMovil.Text = "Todos" Then
            nroMovil = cboMovil.SelectedValue
        End If
        Dim ds As System.Data.DataSet
        If CheckBox1.Checked Then
            ds = proc_ObtenerReporteRecaudo(dtpDesde.SelectedDate, dtpHasta.SelectedDate, cedula)

        Else
            ds = proc_ObtenerReporteRecaudoSinAgrupar(dtpDesde.SelectedDate, dtpHasta.SelectedDate, cedula)

        End If
        GridView1.DataSource = ds.Tables(0)
        If doDataBind Then
            GridView1.DataBind()

            lblTotal.Text = "Total: " & Format(ds.Tables(1).Rows(0)("Total"), "$ ###,###,###.##0")
            lblTitulo.Text = "Recaudación desde " & dtpDesde.SelectedDate & " hasta " & dtpHasta.SelectedDate
            Dim fecha As Date
            'repetir para cada mes
            '-----------
            Try
                ds = proc_ObtenerReporteRecaudo(New Date(dtpDesde.SelectedDate.Value.AddMonths(-3).Year, dtpDesde.SelectedDate.Value.AddMonths(-3).Month, 1), _
                                                         New Date(dtpDesde.SelectedDate.Value.AddMonths(-3).Year, dtpDesde.SelectedDate.Value.AddMonths(-3).Month, New Date(dtpDesde.SelectedDate.Value.AddMonths(-2).Year, dtpDesde.SelectedDate.Value.AddMonths(-2).Month, 1).AddDays(-1).Day), _
                                                                   cedula)
                fecha = New Date(dtpDesde.SelectedDate.Value.AddMonths(-3).Year, dtpDesde.SelectedDate.Value.AddMonths(-3).Month, 1)
                Me.lbl3.Text = "Mes: " & fecha.Month & " Año: " & fecha.Year
                Me.lblProm3.Text = Format(ds.Tables(1).Rows(0)("Total"), "$ ###,###,###.##0")
            Catch ex As Exception

            End Try
            Try
                '-----------

                ds = proc_ObtenerReporteRecaudo(New Date(dtpDesde.SelectedDate.Value.AddMonths(-2).Year, dtpDesde.SelectedDate.Value.AddMonths(-2).Month, 1), _
                                                New Date(dtpDesde.SelectedDate.Value.AddMonths(-2).Year, dtpDesde.SelectedDate.Value.AddMonths(-2).Month, New Date(dtpDesde.SelectedDate.Value.AddMonths(-1).Year, dtpDesde.SelectedDate.Value.AddMonths(-1).Month, 1).AddDays(-1).Day), _
                                                          cedula)
                fecha = New Date(dtpDesde.SelectedDate.Value.AddMonths(-2).Year, dtpDesde.SelectedDate.Value.AddMonths(-2).Month, 1)
                Me.lbl2.Text = "Mes: " & fecha.Month & " Año: " & fecha.Year
                Me.lblProm2.Text = Format(ds.Tables(1).Rows(0)("Total"), "$ ###,###,###.##0")
            Catch ex As Exception

            End Try

            Try
                ds = proc_ObtenerReporteRecaudo(New Date(dtpDesde.SelectedDate.Value.AddMonths(-1).Year, dtpDesde.SelectedDate.Value.AddMonths(-1).Month, 1), _
                                                           New Date(dtpDesde.SelectedDate.Value.AddMonths(-1).Year, dtpDesde.SelectedDate.Value.AddMonths(-1).Month, New Date(dtpDesde.SelectedDate.Value.Year, dtpDesde.SelectedDate.Value.Month, 1).AddDays(-1).Day), _
                                                                    cedula)
                fecha = New Date(dtpDesde.SelectedDate.Value.AddMonths(-1).Year, dtpDesde.SelectedDate.Value.AddMonths(-1).Month, 1)
                Me.lbl1.Text = "Mes: " & fecha.Month & " Año: " & fecha.Year
                Me.lblProm1.Text = Format(ds.Tables(1).Rows(0)("Total"), "$ ###,###,###.##0")
            Catch ex As Exception

            End Try
            '-----------
            Try
                ds = proc_ObtenerReporteRecaudo(New Date(dtpDesde.SelectedDate.Value.AddMonths(-3).Year, dtpDesde.SelectedDate.Value.AddMonths(-3).Month, 1), _
                                                          New Date(dtpDesde.SelectedDate.Value.AddMonths(-1).Year, dtpDesde.SelectedDate.Value.AddMonths(-1).Month, New Date(dtpDesde.SelectedDate.Value.Year, dtpDesde.SelectedDate.Value.Month, 1).AddDays(-1).Day), _
                                                                   cedula)

                Me.lblProm.Text = "Promedio Trimestre: " & Format(ds.Tables(1).Rows(0)("Total") / 3, "$ ###,###,###.##0")

            Catch ex As Exception

            End Try
        End If
       

    End Sub

    Private Sub ObtenerAdministradores()
        Using d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            'Dim lista = d.proc_AdministradoresLoadAll
            'cboAdmin.DataSource = lista
            'cboAdmin.DataValueField = "cedula"
            'cboAdmin.DataTextField = "nombre"
            'cboAdmin.DataBind()
            'Me.cboAdmin.Items.Insert(0, "Todos")
            'cboAdmin.SelectedIndex = 0
        End Using
        
    End Sub

    Private Sub ObtenerMoviles()
        Using d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            Dim listamoviles = d.proc_ObtenerNumerosMovil
            Me.cboMovil.DataSource = listamoviles
            Me.cboMovil.DataValueField = "idVehiculo"
            Me.cboMovil.DataTextField = "nroMovil"
            Me.cboMovil.DataBind()
            Me.cboMovil.Items.Insert(0, "Todos")
            cboMovil.SelectedIndex = 0
        End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            ObtenerAdministradores()
            ObtenerMoviles()
            dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
            dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)
            ObtenerRecaudacion(True)
        End If
        
    End Sub

    Protected Sub GridView1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles GridView1.NeedDataSource
        ObtenerRecaudacion(False)
    End Sub
End Class
