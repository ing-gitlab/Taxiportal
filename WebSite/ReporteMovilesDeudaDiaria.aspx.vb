﻿Imports Common
Imports Telerik.Charting.Styles
Imports Telerik.Charting
Imports System.Drawing
Imports System.Data.Linq
Imports System.Data
Partial Class ReporteMovilesDeudaDiaria
    Inherits System.Web.UI.Page

#Region "Eventos"
    Protected Sub ReporteMovilesDeudaDiaria_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblFecha.Text = "Fecha de Emision: " & Now
        CargarGrilla()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        GridView1.AllowPaging = False
        GridView1.Rebind()
        GridView1.ExportSettings.OpenInNewWindow = True
        GridView1.ExportSettings.ExportOnlyData = True
        GridView1.ExportSettings.FileName = "ReporteMovilesDeudaDiaria"
        GridView1.MasterTableView.ExportToExcel()
    End Sub
#End Region


#Region "Metodos"
    Private Sub CargarGrilla()
        Try
            Dim dtDiarios As DataTable = proc_ObtenerReporteMovilesDeudaDiaria().Tables(0)
            GridView1.DataSource = dtDiarios
            GridView1.DataBind()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

#End Region



End Class
