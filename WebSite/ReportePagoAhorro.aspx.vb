﻿Imports Common
Imports Telerik.Charting.Styles
Imports Telerik.Charting
Imports System.Drawing
Imports System.Data.Linq
Imports System.Data
Partial Class ReportePagoAhorro
    Inherits System.Web.UI.Page

#Region "Declaraciones"
    Dim ImporteAhorro As Decimal = 0
    Dim ImporteAhorroAdicional As Decimal = 0
    Dim lista As String
#End Region


#Region "Eventos"


    Protected Sub ReportePagoAhorro_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
        dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)
        ObtenerTipoMovimiento()
        CargarGrilla()
    End Sub



    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        CargarGrilla()
    End Sub

    Protected Sub DropDownList2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList2.SelectedIndexChanged
        CargarGrilla()
    End Sub



    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click

        Select Case DropDownList2.SelectedValue

            Case -1
                lista = "Todos"
            Case 3
                lista = "Egreso_Ahorros"
            Case 6
                lista = "Egreso_Ahorro_Adicional"
        End Select

        GridView1.AllowPaging = False
        GridView1.Rebind()
        GridView1.ExportSettings.OpenInNewWindow = True
        GridView1.ExportSettings.ExportOnlyData = True
        GridView1.ExportSettings.FileName = "ReportePagoAhorro__Movimiento_" & lista
        GridView1.MasterTableView.ExportToExcel()
    End Sub

#End Region



#Region "Metodos"
    Private Sub ObtenerTipoMovimiento()
        If Not IsPostBack Then
            Try
                Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
                Dim TipoMovimiento = d.proc_ObtenerTipoMovimientoPA
                Me.DropDownList2.DataSource = TipoMovimiento
                Me.DropDownList2.DataValueField = "idTipoMovimiento"
                Me.DropDownList2.DataTextField = "Descripcion"
                Me.DropDownList2.DataBind()

                Dim i As New Web.UI.WebControls.ListItem
                i.Value = -1
                i.Text = "Todos"
                Me.DropDownList2.Items.Insert(0, i)

                DropDownList2.SelectedIndex = 0

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If

    End Sub

    Private Sub CargarGrilla()
        Try
            Dim dtDiarios As DataTable = proc_ObtenerDetalleMCAhorro(dtpDesde.SelectedDate.Value, dtpHasta.SelectedDate.Value, DropDownList2.SelectedValue).Tables(0)
            GridView1.DataSource = dtDiarios
            GridView1.DataBind()
            If dtDiarios.Rows.Count > 0 Then

                For index As Integer = 0 To dtDiarios.Rows.Count - 1
                    If Not IsDBNull(dtDiarios.Rows(index).Item("importeAhorro")) Then
                        ImporteAhorro += dtDiarios.Rows(index).Item("importeAhorro")
                    End If
                    If Not IsDBNull(dtDiarios.Rows(index).Item("importeAhorroAdicional")) Then
                        ImporteAhorroAdicional += dtDiarios.Rows(index).Item("importeAhorroAdicional")
                    End If
                Next

                lblAhorro.Text = "Total Ahorro: " & FormatCurrency(ImporteAhorro, 0)
                lblAhorroAdicional.Text = "Total Ahorro Adicional: " & FormatCurrency(ImporteAhorroAdicional, 0)

              

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

#End Region


  

End Class
