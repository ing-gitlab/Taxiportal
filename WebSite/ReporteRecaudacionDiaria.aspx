﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteRecaudacionDiaria.aspx.vb" Inherits="ReporteRecaudacionDiaria" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

   

.radInput_Default
{
	font:12px aReporte por Barrios</title>
    <style type="text/css">


.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

        .style1
        {
            width: 74px;
        }
        .style2
        {
            width: 73px;
        }

        .style3
        {
            width: 22px;
        }

        .style4
        {
            width: 132px;
        }


        .style1
        {
            height: 58px;
        }
        .style3
        {
            height: 29px;
        }
        .style4
        {
            height: 29px;
            width: 123px;
        }
        
        
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
     <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        
</telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        
</telerik:RadScriptManager>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">

 <AjaxSettings>
 <telerik:AjaxSetting AjaxControlID="button1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadChart1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                         <telerik:AjaxUpdatedControl ControlID="GridView1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpHasta"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpDesde"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="lblRecaudacion"/>
                        <telerik:AjaxUpdatedControl ControlID="lblAhorro"/>
                        <telerik:AjaxUpdatedControl ControlID="lblAhorroAdicional"/>
                    </UpdatedControls>
 </telerik:AjaxSetting>
 

  </AjaxSettings>

   <AjaxSettings>
   <telerik:AjaxSetting AjaxControlID="DropDownList3">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadChart1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="GridView1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="lblRecaudacion"/>
                        <telerik:AjaxUpdatedControl ControlID="lblAhorro"/>
                        <telerik:AjaxUpdatedControl ControlID="lblAhorroAdicional"/>
                    </UpdatedControls>
 </telerik:AjaxSetting>
  </AjaxSettings>
 
   </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" >
         <table style="height:100%; width:100%" ><tr><td><img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" /></td></tr></table></telerik:RadAjaxLoadingPanel>

        <br />

        <asp:Label ID="lblTitulo" runat="server" Font-Names="Calibri" 
            Text="Reporte de Recaudaciòn Diaria."></asp:Label>
                <br />
        <asp:Panel ID="Panel1" runat="server">
        </asp:Panel>
        <br />

        <br />
      <table>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Desde:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpDesde" Runat="server" 
                        Culture="Spanish (Colombia)">
                        
                        <TimeView Culture="Spanish (Colombia)" runat="server"></TimeView>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                        
</telerik:RadDateTimePicker>
                </td>
                <td>
                    &nbsp;&nbsp;</td>
                <td>
                    &nbsp;
                    <asp:Label ID="Label4" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Recaudacion:"></asp:Label>
                    &nbsp;</td>
                <td>
                    &nbsp;&nbsp;<asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack = "true" >
                    </asp:DropDownList>
&nbsp;&nbsp;</td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                <td>
                    <asp:Button ID="Button1" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Procesar" />
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Hasta:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpHasta" Runat="server" 
                        Culture="Spanish (Colombia)">
                        
                        <TimeView Culture="Spanish (Colombia)" runat="server"></TimeView>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                        
</telerik:RadDateTimePicker>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;&nbsp;
                    <asp:Label ID="Label5" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Prestamo:" Visible="False"></asp:Label>
                </td>
                <td>
                    &nbsp;
                    <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack = "true" 
                        Visible="False">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="Button2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Exportar" />
                    </td>
            </tr>
            </table>
        <br />
      <table>
            <tr>
                <td >

                    <asp:Label ID="lblRecaudacion" runat="server" Font-Names="Calibri" 
            Text="Total Recaudacion: "></asp:Label>

                </td>
                <td >

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    &nbsp;</td>
                <td >

        <asp:Label ID="lblAhorro" runat="server" Font-Names="Calibri" 
            Text="Total Ahorro: "></asp:Label>

                </td>
                <td >

                    &nbsp;</td>
                <td>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    &nbsp;</td>
                
                <td>

                    <asp:Label ID="lblAhorroAdicional" runat="server" Font-Names="Calibri" 
            Text="Total Ahorro Adicional:"></asp:Label>
                    </td>
            </tr>
            </table>
                    <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
    
                   
<telerik:RadGrid ID="GridView1" runat="server" AllowSorting="True" 
        GridLines="None" ShowGroupPanel="True" Skin="Vista"
        Font-Size="Small">

<ClientSettings AllowDragToGroup="True">
<Selecting AllowRowSelect="True" />
 </ClientSettings>
 <GroupPanel Text="Arrastre una columna aqui para agrupar">
        
</GroupPanel>
<MasterTableView ShowGroupFooter="true" AllowMultiColumnSorting="true" AutoGenerateColumns = "false" >

<Columns>

<telerik:GridBoundColumn  DataField="Conductor" HeaderText="Conductor">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="CC_Conductor" HeaderText="CC Conductor">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="DirConductor" HeaderText="direccion Conductor">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="TelConductor" HeaderText="Tel Conductor">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="FechaRecaudacion" HeaderText="Fecha Recaudacion">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   DataField="fechaPagada" HeaderText="fecha Pagada">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   DataField="FechaInhabilitacion" HeaderText="Fecha Inhabilitacion" >
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   Aggregate="Sum" DataField="importeRecaudacion" HeaderText="Recaudacion" DataFormatString = "{0:C0}" FooterText="Total recaudado:">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   Aggregate="Sum" DataField="importeAhorro" HeaderText="importe Ahorro" DataFormatString = "{0:C0}" FooterText="Total Ahorro:">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   Aggregate="Sum" DataField="importeAhorroAdicional" HeaderText="importe Ahorro Adicional" DataFormatString = "{0:C0}" FooterText="Total Ahorro Adicional:">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="Observaciones" HeaderText="Observaciones">
</telerik:GridBoundColumn>

</Columns>

<%--
<GroupByExpressions>

 <telerik:GridGroupByExpression >
                    <GroupByFields>
                        <telerik:GridGroupByField FieldName="FechaRegistro"  SortOrder="Ascending" />
                    </GroupByFields>
                    <SelectFields>
                        <telerik:GridGroupByField  FieldName="FechaRegistro"  HeaderText="Fecha Registro Vales" />
                    </SelectFields>

 </telerik:GridGroupByExpression>
</GroupByExpressions>--%>

        <RowIndicatorColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
        

</MasterTableView>

        <FilterMenu EnableTheming="True">
        <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        </telerik:RadGrid>
               
        <br />
    
    </div>
    </form>
</body>
</html>
