﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteMovilesDeudaDiaria.aspx.vb" Inherits="ReporteMovilesDeudaDiaria" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        
</telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        
</telerik:RadScriptManager>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">

 
   </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" >
         <table style="height:100%; width:100%" ><tr><td><img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" /></td></tr></table></telerik:RadAjaxLoadingPanel>

        <br />
      <table>
            <tr>
                <td >

        <asp:Label ID="lblTitulo" runat="server" Font-Names="Calibri" 
            Text="Reporte Deuda Diaria Por Movil."></asp:Label>

                </td>
                <td >

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    &nbsp;</td>
                <td >

                    &nbsp;</td>
                <td >

                    &nbsp;</td>
                <td>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    &nbsp;</td>
                </td>
                <td>

        <asp:Label ID="lblFecha" runat="server" Font-Names="Calibri" 
            Text="Fecha Emision del Reporte:"></asp:Label>
                </td>
                <td>

        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button1" runat="server" Text="Exportar" />
                </td>
            </tr>
            </table>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <br />
        <br />
    
                   
<telerik:RadGrid ID="GridView1" runat="server" AllowSorting="True" 
        GridLines="None" ShowGroupPanel="True" Skin="Vista"
        Font-Size="Small">


<ClientSettings AllowDragToGroup="True">
<Selecting AllowRowSelect="True" />
 </ClientSettings>
 <GroupPanel Text="Arrastre una columna aqui para agrupar">
        
</GroupPanel>
<MasterTableView ShowGroupFooter="true" AllowMultiColumnSorting="true" AutoGenerateColumns="false">



<Columns>

    	
<telerik:GridBoundColumn  DataField="NroMovil" HeaderText="Nro. Movil">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   DataField="marca" HeaderText="Marca">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   DataField="Modelo" HeaderText="Modelo" >
</telerik:GridBoundColumn>

<telerik:GridBoundColumn    DataField="Placa" HeaderText="Placa">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   DataField="Dias" HeaderText="Cantidad de Dias en Mora" >
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   DataField="nombre" HeaderText="Conductor">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="telefono" HeaderText="Telefono">
</telerik:GridBoundColumn>


</Columns>


<%--<GroupByExpressions>

 <telerik:GridGroupByExpression >
                    <GroupByFields>
                        <telerik:GridGroupByField FieldName="FechaRegistro"  SortOrder="Ascending" />
                    </GroupByFields>
                    <SelectFields>
                        <telerik:GridGroupByField  FieldName="FechaRegistro"  HeaderText="Fecha Registro Vales" />
                    </SelectFields>

 </telerik:GridGroupByExpression>
</GroupByExpressions>--%>

        <RowIndicatorColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
        

</MasterTableView>

        <FilterMenu EnableTheming="True">
        <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        </telerik:RadGrid>
               
        <br />
    
    </div>
    </form>
</body>
</html>
