﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteMovilesHabilitados.aspx.vb" Inherits="ReporteMovilesHabilitados" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register assembly="Telerik.Charting, Version=2.0.5.0, Culture=neutral, PublicKeyToken=d14f3dcc8e3e8763" namespace="Telerik.Charting" tagprefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">


.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}


.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

        .style1
        {
            width: 74px;
        }
        .style2
        {
            width: 73px;
        }

        .style3
        {
            width: 22px;
        }

    </style>
</head>
<body>
    <form id="form2" runat="server" >
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
    </telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="Button1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="GridView1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                     
                         <telerik:AjaxUpdatedControl ControlID="DropDownList1"/>
                        
                        
                    </UpdatedControls>
                </telerik:AjaxSetting>
                               
                
 
            </AjaxSettings>
        </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" ><table style="height:100%; width:100%" ><tr><td><img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" /> </td></tr></table></telerik:RadAjaxLoadingPanel>

    <div>
    <asp:Label ID="lblTitulo" runat="server" Font-Names="calibri" 
                        Font-Size="X-Large" Text="Label"></asp:Label>
      <table>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td valign="middle" class="style1">
                    
                </td><td class="style2">
                    &nbsp;</td><td class="style3"></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td class="style1">
                    <asp:Label ID="Label4" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Mostrar"></asp:Label>
                </td><td>
                <asp:DropDownList ID="DropDownList1" runat="server" >
                    <asp:ListItem>Todos</asp:ListItem>
                    <asp:ListItem>Habilitados</asp:ListItem>
                    <asp:ListItem>Inhabilitados</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style3">
                    <asp:Button ID="Button1" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Buscar" />
                    </td>
            </tr>
            <tr id="trChart" >
                <td colspan="3">
                   
                    &nbsp;</td>
                
            </tr>
        </table>
    </div>
    
    <div>
    
    <telerik:RadGrid ID="GridView1" runat="server" AllowSorting="True" GridLines="None" 
            ShowGroupPanel="True" Skin="Vista" AutoGenerateColumns="False" 
            ShowFooter="True" AllowPaging="True" PageSize="50">
        <ClientSettings AllowDragToGroup="True">
        </ClientSettings>
        <GroupPanel Text="Arrastre una columna aqui para agrupar">
        </GroupPanel>
<MasterTableView>
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <Columns>
        <telerik:GridBoundColumn Aggregate="Count" DataField="NroMovil" 
            EmptyDataText="&amp;nbsp;" FooterText="Cantidad: " HeaderText="Num. Móvil" 
            UniqueName="numMovil">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Placa" EmptyDataText="&amp;nbsp;" 
            HeaderText="Placa" UniqueName="Placa">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Cedula" EmptyDataText="&amp;nbsp;" 
            HeaderText="Cédula" UniqueName="column1">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Nombre" EmptyDataText="&amp;nbsp;" 
            HeaderText="Nombre" UniqueName="column2">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Telefono" EmptyDataText="&amp;nbsp;" 
            HeaderText="Teléfono" UniqueName="column3">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Estado" EmptyDataText="&amp;nbsp;" 
            HeaderText="Estado" UniqueName="column4">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="FechaInhabilitacion" 
            EmptyDataText="&amp;nbsp;" HeaderText="Fecha de Inhabilitación" 
            UniqueName="column">
        </telerik:GridBoundColumn>
    </Columns>
</MasterTableView>

<FilterMenu EnableTheming="True">
<CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
</FilterMenu>
    </telerik:RadGrid>
               
    </div>
                   
               
    </form>
</body>
</html>
