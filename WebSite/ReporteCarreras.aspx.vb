﻿Imports Common

Partial Class ReporteCarreras
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ObtenerReporte(True)
    End Sub

    Private Sub ObtenerMoviles()
        Using d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            Dim listamoviles = d.proc_ObtenerNumerosMovil
            Me.cboMovil.DataSource = listamoviles
            Me.cboMovil.DataValueField = "nroMovil"
            Me.cboMovil.DataTextField = "nroMovil"
            Me.cboMovil.DataBind()

            cboMovil.SelectedIndex = 1
        End Using
    End Sub

    Private Sub ObtenerReporte(ByVal doDataBind As Boolean)
        Try
            Dim cedula As String = Nothing
            Dim nroMovil As String = Nothing

            nroMovil = cboMovil.SelectedValue
            Dim ds As System.Data.DataSet
            ds = proc_ObtenerReporteCarreras(dtpDesde.SelectedDate, dtpHasta.SelectedDate, nroMovil)

            GridView1.DataSource = ds.Tables(0)
            If doDataBind Then GridView1.DataBind()

            lblTitulo.Text = "Carreras móvil " & nroMovil & ":  " & ds.Tables(0).Rows.Count
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            ObtenerMoviles()
            dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
            dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)
            ObtenerReporte(True)
        End If
    End Sub

    Protected Sub GridView1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles GridView1.NeedDataSource
        ObtenerReporte(False)
    End Sub
End Class
