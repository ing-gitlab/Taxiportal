﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteVales.aspx.vb" Inherits="ReporteVales" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reporte de vales</title>
    <style type="text/css">


.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

    </style>
</head>
<body>

    <form id="form2" runat="server">
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
    </telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="Button1">
                    <UpdatedControls>
                         <telerik:AjaxUpdatedControl ControlID="GridView1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                          <telerik:AjaxUpdatedControl ControlID="GridView2"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpHasta"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpDesde"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                         <telerik:AjaxUpdatedControl ControlID="cboMovil"/>
                          <telerik:AjaxUpdatedControl ControlID="cboAdmin"/>
                          <telerik:AjaxUpdatedControl ControlID="lblTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="lblTitulo"/>
                        <telerik:AjaxUpdatedControl ControlID="chkPago"/>
                        <telerik:AjaxUpdatedControl ControlID="chkNumVale"/>
                        <telerik:AjaxUpdatedControl ControlID="chkFecha"/>
                        <telerik:AjaxUpdatedControl ControlID="chkMovil"/>
                        <telerik:AjaxUpdatedControl ControlID="chkTarifa"/>
                        <telerik:AjaxUpdatedControl ControlID="chkDependencia"/>
                        <telerik:AjaxUpdatedControl ControlID="chkPasajero"/>
                        <telerik:AjaxUpdatedControl ControlID="chkCliente"/>
                        
                    </UpdatedControls>
                </telerik:AjaxSetting>
                               
                
 
            </AjaxSettings>
        </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" >
        <table style="height:100%; width:100%" >
        <tr>
            <td>
                <img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" />
            </td>
        </tr>
        </table>
    </telerik:RadAjaxLoadingPanel>

    <div>
    <table>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Desde:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpDesde" Runat="server" 
                        Culture="Spanish (Colombia)">
<TimeView Culture="Spanish (Colombia)"></TimeView>

<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                    </telerik:RadDateTimePicker>
                </td>
                <td valign="middle" class="style1">
                    <asp:Label ID="Label4" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Cliente"></asp:Label>
                </td><td class="style2">
                <asp:DropDownList ID="cboAdmin" runat="server" >
                    </asp:DropDownList>
                </td>
                <td class="style4">
                    <asp:CheckBox ID="chkNumVale" Text="Num. Vale" Checked=true  runat="server" CssClass="radInput_Default"/>
                </td>
                <td class="style4">
                    <asp:CheckBox ID="chkFecha" Text="Fecha" Checked=true  runat="server" CssClass="radInput_Default"/>
                </td>
                <td class="style4">
                    <asp:CheckBox ID="chkMovil" Text="Movil" Checked=true  runat="server" CssClass="radInput_Default"/>
                </td>

                <td class="style4">
                    <asp:CheckBox ID="chkTarifa" Text="Tarifa" Checked=true  runat="server" CssClass="radInput_Default"/>
                </td>
                <td class="style4">
                    <asp:CheckBox ID="chkDependencia" Text="Dependencia" Checked=true  runat="server" CssClass="radInput_Default"/>
                </td>
                <td class="style4">
                    <asp:CheckBox ID="chkPasajero" Text="Pasajero" Checked=true  runat="server" CssClass="radInput_Default"/>
                </td>
                <td class="style4">
                    <asp:CheckBox ID="chkCliente" Text="Cliente" Checked=true  runat="server" CssClass="radInput_Default"/>
                </td>
                <td class="style4">
                    <asp:CheckBox ID="chkPago" Text="Pago" Checked=true  runat="server" CssClass="radInput_Default"/>
                </td>
                 <td class="style4">
                    <asp:CheckBox ID="chkZona" Text="Zona" Checked="true"  runat="server" CssClass="radInput_Default"/>
                </td>
                  <td class="style4">
                    <asp:CheckBox ID="chkPeaje" Text="Peaje" Checked="true"  runat="server" CssClass="radInput_Default"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Hasta:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpHasta" Runat="server" 
                        Culture="Spanish (Colombia)">
<TimeView Culture="Spanish (Colombia)"></TimeView>

<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                    </telerik:RadDateTimePicker>
                </td>
                <td class="style1">
                    &nbsp;</td><td>
                    <asp:Button ID="Button1" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Procesar" />
                </td><td class="style4">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
            </tr>
            </table>
    </div>
               

    <br />
    <asp:Label ID="lblTitulo" runat="server" Font-Names="calibri" 
                        Font-Size="X-Large" Text="Label"></asp:Label>
               
    <telerik:RadGrid ID="GridView2"  runat="server" 
        GridLines="None" ShowFooter="True" Skin="Vista" 
        Font-Size="Small" ShowGroupPanel="True">
        
        <ClientSettings AllowDragToGroup="True">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
        
        <GroupPanel Text="Arrastre una columna aqui para agrupar">
        </GroupPanel>
<MasterTableView ShowGroupFooter="true" AllowMultiColumnSorting="true" AutoGenerateColumns="false">

<Columns>
                <telerik:GridBoundColumn Aggregate="Count" DataField="nroVale" HeaderText="Num. Vale"
                    FooterText="Cantidad Vales: ">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Fecha" HeaderText="Fecha"  
                    UniqueName="Fecha"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NroMovil" HeaderText="Movil"  
                    UniqueName="NroMovil">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Tarifa" HeaderText="Tarifa"  
                    UniqueName="Tarifa" Aggregate="Sum" FooterText="Total Tarifa: "></telerik:GridBoundColumn>

                   
                    <telerik:GridBoundColumn DataField="Dependencia" HeaderText="Dependencia"  
                    UniqueName="Dependencia"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Pasajero" HeaderText="Pasajero"  
                    UniqueName="Pasajero"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente"  
                    UniqueName="Nombre"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ZonaDestino" HeaderText="ZonaDestino"  
                    UniqueName="ZonaDestino"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn Aggregate="Sum" DataField="Pago" HeaderText="Pago"
                    FooterText="Total pago: ">
                </telerik:GridBoundColumn>
               
                <telerik:GridBoundColumn Aggregate="Sum" DataField="Peaje" 
                    EmptyDataText="&amp;nbsp;" FooterText="Total Peajes: " HeaderText="Peaje" 
                    UniqueName="column">
                </telerik:GridBoundColumn>
               
            </Columns>
            
            <GroupByExpressions>
                <telerik:GridGroupByExpression>
                    <GroupByFields>
                        <telerik:GridGroupByField FieldName="ZonaDestino"  SortOrder="Ascending" />
                    </GroupByFields>
                    <SelectFields>
                        <telerik:GridGroupByField FieldName="ZonaDestino"  HeaderText="ZonaDestino" />
                    </SelectFields>
                </telerik:GridGroupByExpression>
            </GroupByExpressions>


<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>



</MasterTableView>

<FilterMenu EnableTheming="True">
<CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
</FilterMenu>
  
  
    </telerik:RadGrid>
                

    </form>
    
</body>
</html>
