﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteViajesPorBarrio.aspx.vb" Inherits="ReporteViajesPorBarrio" %>

<%@ Register assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>



<%@ Register assembly="Telerik.Charting" namespace="Telerik.Charting" tagprefix="telerik" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reporte por Barrios</title>
    <style type="text/css">


.radInput_Default
{
	font:12px aReporte por Barrios</title>
    <style type="text/css">


.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

        .style1
        {
            width: 74px;
        }
        .style2
        {
            width: 73px;
        }

        .style3
        {
            width: 22px;
        }

        .style4
        {
            width: 132px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server" >
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
    </telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="Button1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadChart1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                         <telerik:AjaxUpdatedControl ControlID="GridView1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpHasta"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpDesde"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="lblCarreras" />
                        <telerik:AjaxUpdatedControl ControlID="lblImporte"/>
                        
                         <telerik:AjaxUpdatedControl ControlID="DropDownList1"/>
                        
                        
                    </UpdatedControls>
                </telerik:AjaxSetting>
                               
                
 
            </AjaxSettings>
        </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" >
        <table style="height:100%; width:100%" >
        <tr>
            <td>
                <img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" />
            </td>
        </tr>
        </table>
    </telerik:RadAjaxLoadingPanel>

    <div>
      <table>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Desde:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpDesde" Runat="server" 
                        Culture="Spanish (Colombia)">
<TimeView Culture="Spanish (Colombia)"></TimeView>

<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                    </telerik:RadDateTimePicker>
                </td>
                <td valign="middle" class="style1">
                    <asp:Label ID="Label4" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Mostrar"></asp:Label>
                </td><td class="style2">
                    <telerik:RadNumericTextBox   ID="RadNumericTextBox1" Runat="server" 
                        Culture="Spanish (Colombia)"   Value="10" Width="45px">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="RadNumericTextBox1" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td><td class="style4">
                    <asp:CheckBox ID="chkChart" runat="server" Font-Names="calibri" 
                        Font-Size="Medium" Text="Gráfico" />
                <td></td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Hasta:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpHasta" Runat="server" 
                        Culture="Spanish (Colombia)">
<TimeView Culture="Spanish (Colombia)"></TimeView>

<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                    </telerik:RadDateTimePicker>
                </td>
                <td class="style1">
                    <asp:Label ID="Label5" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Movil"></asp:Label>
                </td><td>
                <asp:DropDownList ID="DropDownList1" runat="server" >
                    </asp:DropDownList>
                </td><td class="style4">
                    <asp:CheckBox ID="chkLista" runat="server" Font-Names="calibri" 
                        Font-Size="Medium" Text="Lista" Checked="True" />
                </td>
                <td class="style3">
                    <asp:Button ID="Button1" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Graficar" />
                    </td>
            </tr>
            </table>
        <br />
        <asp:Label ID="lblCarreras" runat="server" Font-Names="Calibri" 
            Text="Cantidad de Carreras:"></asp:Label>
        <br />
        <asp:Label ID="lblImporte" runat="server" Font-Names="Calibri" Text="Importe: "></asp:Label>
        <br />
    </div>
    
                   
                    <telerik:RadChart ID="RadChart1" runat="server" Height="401px" 
            Width="1100px" Skin="Desert" DefaultType="Pie">
                        <PlotArea>
                            <EmptySeriesMessage Visible="True">
                                <Appearance Visible="True">
                                </Appearance>
                            </EmptySeriesMessage>
                            <XAxis>
                                <Appearance Color="215, 214, 202" MajorTick-Color="226, 225, 207">
                                    <MajorGridLines Color="226, 225, 207" PenStyle="Solid" />
                                    <TextAppearance TextProperties-Color="87, 84, 65">
                                    </TextAppearance>
                                </Appearance>
                                <AxisLabel>
                                    <TextBlock>
                                        <Appearance TextProperties-Color="96, 93, 75">
                                        </Appearance>
                                    </TextBlock>
                                </AxisLabel>
                            </XAxis>
                            <YAxis>
                                <Appearance Color="212, 211, 199" MajorTick-Color="226, 225, 207" 
                                    MinorTick-Color="226, 225, 207" MinorTick-Width="0">
                                    <MajorGridLines Color="226, 225, 207" PenStyle="Solid" />
                                    <MinorGridLines Color="226, 225, 207" PenStyle="Solid" Width="0" />
                                    <TextAppearance TextProperties-Color="87, 84, 65">
                                    </TextAppearance>
                                </Appearance>
                                <AxisLabel>
                                    <TextBlock>
                                        <Appearance TextProperties-Color="96, 93, 75">
                                        </Appearance>
                                    </TextBlock>
                                </AxisLabel>
                            </YAxis>
                            <Appearance>
                                <FillStyle FillType="Solid" MainColor="243, 243, 229">
                                </FillStyle>
                                <Border Color="208, 207, 195" />
                            </Appearance>
                        </PlotArea>
                        <Appearance>
                            <Border Color="183, 181, 159" />
                        </Appearance>
                        <ChartTitle>
                            <Appearance Dimensions-Margins="4%, 0px, 0px, 8%">
                            </Appearance>
                            <TextBlock>
                                <Appearance TextProperties-Color="113, 87, 52" 
                                    TextProperties-Font="Verdana, 16pt">
                                </Appearance>
                            </TextBlock>
                        </ChartTitle>
                        <Legend>
                            <Appearance>
                                <ItemTextAppearance TextProperties-Color="87, 84, 65">
                                </ItemTextAppearance>
                                <FillStyle MainColor="243, 243, 229">
                                </FillStyle>
                                <Border Color="208, 207, 195" />
                            </Appearance>
                        </Legend>
                    </telerik:RadChart>
                    
    <telerik:RadGrid ID="GridView1" runat="server" AllowSorting="True" 
        GridLines="None" ShowGroupPanel="True" Skin="Vista">
        <ClientSettings AllowDragToGroup="True">
        </ClientSettings>
        <GroupPanel Text="Arrastre una columna aqui para agrupar">
        </GroupPanel>
<MasterTableView>
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
</MasterTableView>

<FilterMenu EnableTheming="True">
<CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
</FilterMenu>
    </telerik:RadGrid>
               
    </form>
</body>
</html>
