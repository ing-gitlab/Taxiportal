﻿Imports Common
Partial Class ReporteMovilesEnCola
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
        GridView1.DataSource = d.proc_ObtenerMovilesPorZona
        GridView1.DataBind()
        
    End Sub

End Class
