﻿Imports Common
Imports Telerik.Charting.Styles
Imports Telerik.Charting
Imports System.Drawing
Imports System.Data.Linq
Imports System.Data

Partial Class ReporteViajesPorBarrio
    Inherits System.Web.UI.Page
    Private Shared maxSales As Integer = 5000
    Private Shared profitRatio As Double = 0.1

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
        dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)

        ObtenerMoviles()
        If chkChart.Checked Then initChart()
        RadChart1.Visible = chkChart.Checked
        If chkLista.Checked Then InitGrid(True)
        GridView1.Visible = chkLista.Checked

    End Sub

    Private Sub ObtenerMoviles()
        Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
        Dim listamoviles = d.proc_ObtenerNumerosMovil
        Me.DropDownList1.DataSource = listamoviles
        Me.DropDownList1.DataValueField = "idVehiculo"
        Me.DropDownList1.DataTextField = "nroMovil"
        Me.DropDownList1.DataBind()
        Me.DropDownList1.Items.Insert(0, "Todos")
        DropDownList1.SelectedIndex = 0
    End Sub

    Private Sub initChart()
        RadChart1.Chart.Series.Clear()
        RadChart1.ChartTitle.TextBlock.Text = String.Format("Viajes por Barrio. Desde: {0} Hasta: {1}", dtpDesde.SelectedDate, dtpHasta.SelectedDate)

        RadChart1.PlotArea.XAxis.Appearance.TextAppearance.TextProperties.Font = New Font("Calibri", 10)
        RadChart1.PlotArea.XAxis.Appearance.TextAppearance.TextProperties.Color = Color.Black
        RadChart1.PlotArea.XAxis.Appearance.TextAppearance.AutoTextWrap = AutoTextWrap.True

        RadChart1.PlotArea.XAxis.AutoScale = False

        Dim dtBarrios As DataTable = proc_ObtenerReporteBarrios(dtpDesde.SelectedDate, dtpHasta.SelectedDate, IIf(DropDownList1.Text = "Todos", -1, DropDownList1.SelectedValue)).Tables(0)

        RadChart1.PlotArea.XAxis.Clear()
        Dim count As Integer
        If RadNumericTextBox1.Value < dtBarrios.Rows.Count Then
            count = RadNumericTextBox1.Value
        Else
            count = dtBarrios.Rows.Count
        End If
        For index As Integer = 0 To count - 1
            RadChart1.PlotArea.XAxis.AddItem(dtBarrios.Rows(index)("Barrio"))
        Next

        If dtBarrios.Rows.Count > 0 Then
            Dim cantCarreras As Integer = 0
            Dim Importe As Decimal = 0
            For index As Integer = 0 To dtBarrios.Rows.Count - 1
                Importe += dtBarrios.Rows(index).Item("Tarifa")
                cantCarreras = dtBarrios.Rows(index).Item("Cantidad")
            Next
            lblCarreras.Text = "Carreras: " & cantCarreras
            ' lblImporte.Text = "Importe: " & Importe
        End If


        RadChart1.PlotArea.XAxis.LayoutMode = ChartAxisLayoutMode.Between

        Dim BarriosSeries As New ChartSeries("Barrios", ChartSeriesType.Bar)

        RadChart1.AddChartSeries(BarriosSeries)

        BarriosSeries.Appearance.Border.Color = Color.Black
        BarriosSeries.Appearance.ShowLabels = True

        Dim BarriosDataSeries As ChartSeries = RadChart1.GetSeries("Barrios")

        If Not (BarriosDataSeries Is Nothing) Then
            BarriosDataSeries.Clear()
            Dim seriesItem As ChartSeriesItem

            For index As Integer = 0 To count - 1
                seriesItem = New ChartSeriesItem

                seriesItem.YValue = dtBarrios.Rows(index)("Cantidad")
                BarriosDataSeries.AddItem(seriesItem)
            Next

        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If chkChart.Checked Then initChart()
        RadChart1.Visible = chkChart.Checked
        If chkLista.Checked Then InitGrid(True)
        GridView1.Visible = chkLista.Checked
    End Sub


    Private Sub InitGrid(ByVal doDataBind As Boolean)
        Dim dtBarrios As DataTable = proc_ObtenerReporteBarrios(dtpDesde.SelectedDate, dtpHasta.SelectedDate, IIf(DropDownList1.Text = "Todos", -1, DropDownList1.SelectedValue)).Tables(0)
        GridView1.DataSource = dtBarrios
        If doDataBind Then GridView1.DataBind()
        If dtBarrios.Rows.Count > 0 Then
            Dim cantCarreras As Integer = 0
            Dim Importe As Decimal = 0
            For index As Integer = 0 To dtBarrios.Rows.Count - 1
                Importe += dtBarrios.Rows(index).Item("Tarifa")
                cantCarreras += dtBarrios.Rows(index).Item("Cantidad")
            Next
            lblCarreras.Text = "Carreras: " & cantCarreras
            lblImporte.Text = "Importe: " & Importe
        End If
    End Sub


    Protected Sub GridView1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles GridView1.NeedDataSource
        InitGrid(False)
    End Sub
End Class
