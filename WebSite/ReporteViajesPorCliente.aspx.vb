﻿Imports Common
Partial Class ReporteViajesPorCliente
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        cargarGrilla()
    End Sub

    Private Sub CargarGrilla()
        GridView1.DataSource = Funciones.proc_ObtenerViajesPorCliente(Me.RadNumericTextBox1.Value, dtpDesde.SelectedDate, dtpHasta.SelectedDate).Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
        dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)
        CargarGrilla()
    End Sub
End Class
