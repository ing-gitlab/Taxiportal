﻿Imports Common
Partial Class ReporteVales
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
            dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)
            ObtenerClientes()
            obtenerVales(1)
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        obtenerVales(1)
    End Sub

    Private Sub obtenerVales(ByVal doDataBind As Boolean)

        Using d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            Dim cliente As Integer = Nothing
            If cboAdmin.SelectedIndex > 0 Then cliente = cboAdmin.SelectedValue
            Dim lista = d.proc_ObtenerVales(dtpDesde.SelectedDate, dtpHasta.SelectedDate, cliente)

            Me.GridView2.DataSource = lista
            If doDataBind Then GridView2.DataBind()
            GridView2.Columns(0).Visible = (chkNumVale.Checked)
            GridView2.Columns(1).Visible = (chkFecha.Checked)
            GridView2.Columns(2).Visible = (chkMovil.Checked)
            GridView2.Columns(3).Visible = (chkTarifa.Checked)
            GridView2.Columns(4).Visible = (chkDependencia.Checked)
            GridView2.Columns(5).Visible = (chkPasajero.Checked)
            GridView2.Columns(6).Visible = (chkCliente.Checked)
            GridView2.Columns(7).Visible = (chkZona.Checked)
            GridView2.Columns(8).Visible = (chkPago.Checked)
            GridView2.Columns(9).Visible = (chkPeaje.Checked)

            lblTitulo.Text = "Reporte de Vales de " & cboAdmin.SelectedItem.Text



        End Using


    End Sub

    Private Sub ObtenerClientes()
        Using d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            Dim lista = d.proc_ObtenerClientesVale
            cboAdmin.DataSource = lista
            cboAdmin.DataValueField = "idCliente"
            cboAdmin.DataTextField = "nombre"

            cboAdmin.DataBind()
            cboAdmin.Items.Insert(0, "Todos")
            'Me.cboAdmin.Items.Insert(0, "Todos")
            cboAdmin.SelectedIndex = 0
        End Using

    End Sub

    Protected Sub GridView2_GroupsChanging(ByVal source As Object, ByVal e As Telerik.Web.UI.GridGroupsChangingEventArgs) Handles GridView2.GroupsChanging
        Console.Write("")
    End Sub

    Protected Sub GridView2_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles GridView2.NeedDataSource
        obtenerVales(0)
    End Sub
End Class
