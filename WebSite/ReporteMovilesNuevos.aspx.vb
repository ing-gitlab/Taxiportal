﻿Imports Common

Partial Class ReporteMovilesNuevos
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ObtenerReporte(True)
    End Sub

    Private Sub ObtenerReporte(ByVal doDataBind As Boolean)
        Try
            Dim cedula As String = Nothing
            Dim nroMovil As String = Nothing


            Dim ds As System.Data.DataSet
            ds = proc_ObtenerReporteMovilesNuevos(dtpDesde.SelectedDate, dtpHasta.SelectedDate)

            GridView1.DataSource = ds.Tables(0)
            If doDataBind Then GridView1.DataBind()
             

        Catch ex As Exception

        End Try

    End Sub



  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then


            dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
            dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)
            ObtenerReporte(True)
        End If

    End Sub

    Protected Sub GridView1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles GridView1.NeedDataSource
        ObtenerReporte(False)
    End Sub
End Class
