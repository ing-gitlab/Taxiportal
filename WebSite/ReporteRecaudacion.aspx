﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteRecaudacion.aspx.vb" Inherits="ReporteRecaudacion" %>
<%@ Register assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reporte de Reacaudación</title>
</head>
<body>

    <form id="form1" runat="server">
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
    </telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="Button1">
                    <UpdatedControls>
                         <telerik:AjaxUpdatedControl ControlID="GridView1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpHasta"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpDesde"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                         <telerik:AjaxUpdatedControl ControlID="cboMovil"/>
                          <telerik:AjaxUpdatedControl ControlID="cboAdmin"/>
                          <telerik:AjaxUpdatedControl ControlID="lblTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="lblTitulo"/>
                         <telerik:AjaxUpdatedControl ControlID="lblProm"/>
                          <telerik:AjaxUpdatedControl ControlID="lblProm3"/>
                           <telerik:AjaxUpdatedControl ControlID="lblProm2"/>
                            <telerik:AjaxUpdatedControl ControlID="lblProm1"/>
                             <telerik:AjaxUpdatedControl ControlID="lbl3"/>
                              <telerik:AjaxUpdatedControl ControlID="lbl2"/>
                               <telerik:AjaxUpdatedControl ControlID="lbl1"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                               
                
 
            </AjaxSettings>
        </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" >
        <table style="height:100%; width:100%" >
        <tr>
            <td>
                <img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" />
            </td>
        </tr>
        </table>
    </telerik:RadAjaxLoadingPanel>

    <div>
    <table>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Desde:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpDesde" Runat="server" 
                        Culture="Spanish (Colombia)">
<TimeView Culture="Spanish (Colombia)"></TimeView>

<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                    </telerik:RadDateTimePicker>
                </td>
                <td valign="middle" class="style1">
                    <asp:Label ID="Label4" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Administrador"></asp:Label>
                </td><td class="style2">
                <asp:DropDownList ID="cboAdmin" runat="server" >
                    </asp:DropDownList>
                    <asp:CheckBox ID="CheckBox1" runat="server" Font-Names="Calibri" 
                        Font-Size="Small" Text="Agrupar" />
                </td><td class="style4">
                    &nbsp;<td></td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Hasta:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpHasta" Runat="server" 
                        Culture="Spanish (Colombia)">
<TimeView Culture="Spanish (Colombia)"></TimeView>

<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                    </telerik:RadDateTimePicker>
                </td>
                <td class="style1">
                    <asp:Label ID="Label5" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Movil" Visible="False"></asp:Label>
                </td><td>
                <asp:DropDownList ID="cboMovil" runat="server" Visible="False" >
                    </asp:DropDownList>
                    <asp:Button ID="Button1" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Procesar" />
                </td><td class="style4">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
            </tr>
            </table>
    </div>
    <br />
    <asp:Label ID="lblTitulo" runat="server" Font-Names="calibri" 
                        Font-Size="X-Large" Text="Label"></asp:Label>
               
    <telerik:RadGrid ID="GridView1" runat="server" AllowSorting="True" 
        GridLines="None" ShowGroupPanel="True" Skin="Vista">
        <ClientSettings AllowDragToGroup="True">
        </ClientSettings>
        <GroupPanel Text="Arrastre una columna aqui para agrupar">
        </GroupPanel>
<MasterTableView>
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
</MasterTableView>

<FilterMenu EnableTheming="True">
<CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
</FilterMenu>
    </telerik:RadGrid>
               
    <br />
    <asp:Label ID="lblTotal" runat="server" Font-Names="calibri"></asp:Label>
    <br />
    <table style="border-width: thin; border-style: solid; width:100%; color: #000000; table-layout: auto; border-collapse: collapse; background-color: #FFFF99;">
        <tr>
            <td colspan=3 align=center 
                style="border-style: solid; background-color: #FFCC66;">
                <asp:Label ID="lblProm" runat="server" Font-Names="Calibri" Font-Size="Small" 
                     Text="Label"></asp:Label>
            </td>
            
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl3" runat="server" Font-Names="Calibri" Font-Size="Small" 
                    Text="Label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl2" runat="server" Font-Names="Calibri" Font-Size="Small" 
                    Text="Label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl1" runat="server" Font-Names="Calibri" Font-Size="Small" 
                    Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblProm3" runat="server" Font-Names="Calibri" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblProm2" runat="server" Font-Names="Calibri" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblProm1" runat="server" Font-Names="Calibri" Font-Size="Small"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
