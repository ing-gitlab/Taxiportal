﻿Imports Common

Partial Class ReporteRecaudacionPorMovil
    Inherits System.Web.UI.Page


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ObtenerRecaudacion(True)
    End Sub

    Private Sub ObtenerRecaudacion(ByVal doDataBind As Boolean)
        Try
            Dim cedula As String = Nothing
            Dim nroMovil As String = Nothing

            nroMovil = cboMovil.SelectedValue
            Dim ds As System.Data.DataSet
            ds = proc_ObtenerReporteRecaudoPorMovil(dtpDesde.SelectedDate, dtpHasta.SelectedDate, nroMovil)

            GridView1.DataSource = ds.Tables(0)
            If doDataBind Then GridView1.DataBind()

            lblTotal.Text = "Total: " & Format(ds.Tables(1).Rows(0)("Total"), "$ ###,###,###.##0")
            lblTitulo.Text = "Pagos del Móvil " & nroMovil & " desde " & dtpDesde.SelectedDate & " hasta " & dtpHasta.SelectedDate
        Catch ex As Exception

        End Try

    End Sub

  

    Private Sub ObtenerMoviles()
        Using d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            Dim listamoviles = d.proc_ObtenerNumerosMovil
            Me.cboMovil.DataSource = listamoviles
            Me.cboMovil.DataValueField = "nroMovil"
            Me.cboMovil.DataTextField = "nroMovil"
            Me.cboMovil.DataBind()

            cboMovil.SelectedIndex = 0
        End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            ObtenerMoviles()
            dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
            dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)
            ObtenerRecaudacion(True)
        End If

    End Sub

    Protected Sub GridView1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles GridView1.NeedDataSource
        ObtenerRecaudacion(False)
    End Sub
End Class
