﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Main.aspx.vb" Inherits="Main" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
    </telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div>
    
    </div>
    <telerik:RadPanelBar ID="RadPanelBar1" Runat="server" Skin="Office2007">
        <Items>
            <telerik:RadPanelItem runat="server" Owner="RadPanelBar1" Text="Caribe Plaza">
                <Items>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://rtc-pc01.no-ip.org/website/reporteviajesporbarrio.aspx" 
                        Owner="" Target="_parent" Text="Reporte de viajes por barrio">
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://rtc-pc01.no-ip.org/website/reportemovilesencola.aspx" 
                        Text="Reporte móviles en cola">
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://rtc-pc01.no-ip.org/website/reporterecaudacion.aspx" 
                        Text="Reporte de Recaudación">
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://rtc-pc01.no-ip.org/website/reportetarifas.aspx" 
                        Text="Reporte de tarifas">
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://rtc-pc01.no-ip.org/website/reportevales.aspx" 
                        Text="Reporte de vales">
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelItem>
            <telerik:RadPanelItem runat="server" Owner="RadPanelBar1" Text="Central">
                <Items>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://rtc-pc02.no-ip.org/website/reporteviajesporbarrio.aspx" 
                        Owner="" Target="_parent" Text="Reporte de viajes por barrio">
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://rtc-pc02.no-ip.org/website/reportemovilesencola.aspx" 
                        Text="Reporte móviles en cola">
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://rtc-pc02.no-ip.org/website/reporterecaudacion.aspx" 
                        Text="Reporte de Recaudación">
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://rtc-pc02.no-ip.org/website/reportetarifas.aspx" 
                        Text="Reporte de tarifas">
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://rtc-pc02.no-ip.org/website/reportevales.aspx" 
                        Text="Reporte de vales">
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem runat="server" 
                        NavigateUrl="http://localhost:59542/WebSite/ReporteRecaudacionVale.aspx" 
                        Text="Reporte Recaudacion por vales">
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelItem>
        </Items>
<ExpandAnimation Type="None" Duration="100"></ExpandAnimation>

<CollapseAnimation Type="None" Duration="100"></CollapseAnimation>
    </telerik:RadPanelBar>
    <telerik:RadWindow ID="RadWindow1" runat="server" Skin="Telerik" 
        Behavior="Default" InitialBehavior="None" Left="" NavigateUrl="" 
        style="display: none;" Top="">
    </telerik:RadWindow>
    </form>
</body>
</html>
