﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteDeudores.aspx.vb" Inherits="ReporteDeudores" %>

<%@ Register assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Reporte de Deudores por Mes</title>
</head>
<body>

    <form id="form1" runat="server">
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
    </telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="Button1">
                    <UpdatedControls>
                         <telerik:AjaxUpdatedControl ControlID="RadGrid1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                    
                         <telerik:AjaxUpdatedControl ControlID="cboMovil"/>
                          <telerik:AjaxUpdatedControl ControlID="RadCombobox1"/>
                          <telerik:AjaxUpdatedControl ControlID="lblTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="lblTitulo"/>
                       
                        
                    </UpdatedControls>
                </telerik:AjaxSetting>
                               

                  <telerik:AjaxSetting AjaxControlID="RadGrid1">
                    <UpdatedControls>
                         <telerik:AjaxUpdatedControl ControlID="RadGrid1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                    
                    </UpdatedControls>
                </telerik:AjaxSetting>
 
            </AjaxSettings>
        </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" >
        <table style="height:100%; width:100%" >
        <tr>
            <td>
                <img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" />
            </td>
        </tr>
        </table>
    </telerik:RadAjaxLoadingPanel>

    <div>
    <table>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Mes:"></asp:Label>
                </td>
                <td style="width:153px">
                    <telerik:RadComboBox ID="RadComboBox1" Runat="server" Skin="Vista" 
                        Height="23px" Width="157px">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Enero" Value="1" />
                             <telerik:RadComboBoxItem runat="server" Text="Febrero" Value="2" />
                              <telerik:RadComboBoxItem runat="server" Text="Marzo" Value="3" />
                               <telerik:RadComboBoxItem runat="server" Text="Abril" Value="4" />
                                <telerik:RadComboBoxItem runat="server" Text="Mayo" Value="5" />
                                 <telerik:RadComboBoxItem runat="server" Text="Junio" Value="6" />
                                  <telerik:RadComboBoxItem runat="server" Text="Julio" Value="7" />
                                   <telerik:RadComboBoxItem runat="server" Text="Agosto" Value="8" />
                                    <telerik:RadComboBoxItem runat="server" Text="Septiembre" Value="9" />
                                     <telerik:RadComboBoxItem runat="server" Text="Octubre" Value="10" />
                                      <telerik:RadComboBoxItem runat="server" Text="Noviembre" Value="11" />
                                       <telerik:RadComboBoxItem runat="server" Text="Diciembre" Value="12" />
                        </Items>
                        

                       
<CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                    </telerik:RadComboBox>
                </td>
                <td valign="middle" class="style1">
                    <asp:Label ID="Label5" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Movil" Visible="True"></asp:Label>
                </td><td class="style2">
                <asp:DropDownList ID="cboMovil" runat="server" Visible="True" >
                    </asp:DropDownList>
                </td><td class="style4">
                    &nbsp;<td>&nbsp;</td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Año:"></asp:Label>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBox1" Runat="server" 
                        MaxValue="2050" MinValue="2009">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
                <td class="style1">
                    &nbsp;</td><td>
                    <asp:Button ID="Button1" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Procesar" />
                </td><td class="style4">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
            </tr>
            </table>
    </div>
    <br />
    <asp:Label ID="lblTitulo" runat="server" Font-Names="calibri" 
                        Font-Size="X-Large" Text=""></asp:Label>
               
    <telerik:RadGrid ID="RadGrid1" runat="server" AllowSorting="True" 
        GridLines="None" Skin="Vista" ShowGroupPanel="True">
        <ClientSettings AllowDragToGroup="True">
        </ClientSettings>
        <GroupPanel Text="Arrastre una columna aqui para agrupar">
        </GroupPanel>
<MasterTableView>
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
</MasterTableView>

<FilterMenu EnableTheming="True">
<CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
</FilterMenu>
    </telerik:RadGrid>
               
    <br />
    <asp:Label ID="lblTotal" runat="server" Font-Names="calibri"></asp:Label>
    <br />
    <table style="border-width: thin; border-style: solid; width:100%; color: #000000; table-layout: auto; border-collapse: collapse; background-color: #FFFF99;">
        <tr>
            <td colspan=3 align=center 
                style="border-style: solid; background-color: #FFCC66;">
                <asp:Label ID="lblProm" runat="server" Font-Names="Calibri" Font-Size="Small" 
                     Text="Label"></asp:Label>
            </td>
            
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl3" runat="server" Font-Names="Calibri" Font-Size="Small" 
                    Text="Label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl2" runat="server" Font-Names="Calibri" Font-Size="Small" 
                    Text="Label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl1" runat="server" Font-Names="Calibri" Font-Size="Small" 
                    Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblProm3" runat="server" Font-Names="Calibri" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblProm2" runat="server" Font-Names="Calibri" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblProm1" runat="server" Font-Names="Calibri" Font-Size="Small"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
