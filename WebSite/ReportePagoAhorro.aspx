﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportePagoAhorro.aspx.vb" Inherits="ReportePagoAhorro" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">




.radInput_Default
{
	font:12px aReporte por Barrios</title>
    <style type="text/css">


.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

        .style1
        {
            width: 74px;
        }
        .style2
        {
            width: 73px;
        }

        .style3
        {
            width: 22px;
        }

        .style4
        {
            width: 132px;
        }


        .style1
        {
            height: 58px;
        }
        .style3
        {
            height: 29px;
        }
        .style4
        {
            height: 29px;
            width: 123px;
        }
        
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        
</telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        
</telerik:RadScriptManager>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            
 <AjaxSettings>
 <telerik:AjaxSetting AjaxControlID="DropDownList2">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadChart1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                         <telerik:AjaxUpdatedControl ControlID="GridView1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpHasta"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpDesde"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="lblAhorro" />
                        <telerik:AjaxUpdatedControl ControlID="lblAhorroAdicional" />
                        <telerik:AjaxUpdatedControl ControlID="lblDeuda" />
                        <telerik:AjaxUpdatedControl ControlID="lblBAhorro" />
                        <telerik:AjaxUpdatedControl ControlID="lblBAhorroA" />
                        <telerik:AjaxUpdatedControl ControlID="lblBDeuda" />
                        
                    </UpdatedControls>
 </telerik:AjaxSetting>

  <telerik:AjaxSetting AjaxControlID="button1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadChart1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                         <telerik:AjaxUpdatedControl ControlID="GridView1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpHasta"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpDesde"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="lblAhorro" />
                        <telerik:AjaxUpdatedControl ControlID="lblAhorroAdicional" />
                        <telerik:AjaxUpdatedControl ControlID="lblDeuda" />
                        <telerik:AjaxUpdatedControl ControlID="lblBAhorro" />
                        <telerik:AjaxUpdatedControl ControlID="lblBAhorroA" />
                        <telerik:AjaxUpdatedControl ControlID="lblBDeuda" />
                        
                    </UpdatedControls>
 </telerik:AjaxSetting>

 </AjaxSettings>


   </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" >
         <table style="height:100%; width:100%" ><tr><td><img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" /></td></tr></table></telerik:RadAjaxLoadingPanel>

        <br />

        <asp:Label ID="lblTitulo" runat="server" Font-Names="Calibri" 
            Text="Reporte Pago Ahorro."></asp:Label>

        <br />
        <br />
      <table>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Desde:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpDesde" Runat="server" 
                        Culture="Spanish (Colombia)">
                        
                        <TimeView Culture="Spanish (Colombia)"></TimeView>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                        
</telerik:RadDateTimePicker>
                </td>
                <td>
                    &nbsp;&nbsp;</td>
                <td>
                    &nbsp;
                    <asp:Label ID="Label4" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Movimiento:"></asp:Label>
                    &nbsp;</td>
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack = "true" >
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                <td>
                    <asp:Button ID="Button1" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Procesar" />
                 </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Hasta:"></asp:Label>
                </td>
                <td >
                    <telerik:RadDateTimePicker ID="dtpHasta" Runat="server" 
                        Culture="Spanish (Colombia)">
                        
                        <TimeView Culture="Spanish (Colombia)"></TimeView>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                        
</telerik:RadDateTimePicker>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;&nbsp;
                    </td>
                <td>
                    &nbsp;
                    </td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="Button2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Exportar" />
                 </td>
            </tr>
            </table>
        <br />
      <table>
            <tr>
                <td >

        <asp:Label ID="lblAhorro" runat="server" Font-Names="Calibri" 
            Text="Total Ahorro:"></asp:Label>

                </td>
                <td >

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    &nbsp;</td>
                <td >

        <asp:Label ID="lblAhorroAdicional" runat="server" Font-Names="Calibri" 
            Text="Total Ahorro Adicional:"></asp:Label>

                </td>
                <td>

                    &nbsp;</td>
                </td>
            </tr>
            </table>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
    
                   
<telerik:RadGrid ID="GridView1" runat="server" AllowSorting="True" 
        GridLines="None" ShowGroupPanel="True" Skin="Vista"
        Font-Size="Small">

<ClientSettings AllowDragToGroup="True">
<Selecting AllowRowSelect="True" />
 </ClientSettings>
 <GroupPanel Text="Arrastre una columna aqui para agrupar">
        
</GroupPanel>
<MasterTableView ShowGroupFooter="true" AllowMultiColumnSorting="true" AutoGenerateColumns="false" ShowFooter="true">

<Columns>


<telerik:GridBoundColumn  DataField="Persona" HeaderText="Nombre">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   DataField="CC_Persona" HeaderText="Cedula">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   DataField="Usuario" HeaderText="Usuario" >
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   DataField="fechaMovimiento" HeaderText="Fecha Movimiento">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   Aggregate="Sum" DataField="importeAhorro" HeaderText="Ahorro"  DataFormatString = "{0:C0}" FooterText="Total Ahorro: ">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   Aggregate="Sum" DataField="importeAhorroAdicional" HeaderText="Ahorro Adicional"  DataFormatString = "{0:C0}" FooterText="Total Ahorro Adicional: ">
</telerik:GridBoundColumn>


</Columns>


<GroupByExpressions>

 <%--<telerik:GridGroupByExpression >
                    <GroupByFields>
                        <telerik:GridGroupByField FieldName="fechaMovimiento"  SortOrder="Ascending" />
                    </GroupByFields>
                    <SelectFields>
                        <telerik:GridGroupByField  FieldName="fechaMovimiento"  HeaderText="fecha Movimiento" />
                    </SelectFields>

 </telerik:GridGroupByExpression>--%>
</GroupByExpressions>

        <RowIndicatorColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
        

</MasterTableView>

        <FilterMenu EnableTheming="True">
        <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        </telerik:RadGrid>
               
        <br />
    
    </div>
    </form>
</body>
</html>
