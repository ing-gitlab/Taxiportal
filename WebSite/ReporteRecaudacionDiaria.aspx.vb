﻿Imports Common
Imports Telerik.Charting.Styles
Imports Telerik.Charting
Imports System.Drawing
Imports System.Data.Linq
Imports System.Data
Partial Class ReporteRecaudacionDiaria
    Inherits System.Web.UI.Page

#Region "Declaraciones"
    Dim ImporteRecaudacion As Decimal = 0
    Dim ImporteAhorro As Decimal = 0
    Dim ImporteAhorroAdicional As Decimal = 0
    Dim lista As String
#End Region

    
#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
        dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)
        ObtenerTipoRecaudacion()
        ObtenerTipoPrestamo()
        CargarGrilla()

    End Sub


    Protected Sub DropDownList2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList2.SelectedIndexChanged
        lblRecaudacion.Text = "Total Recaudaciòn: " & 0
        lblAhorro.Text = "Total Ahorro: " & 0
        lblAhorroAdicional.Text = "Total Ahorro Adicional: " & 0
        CargarGrilla()
        If DropDownList2.SelectedValue = 2 Then
            ObtenerTipoPrestamo()
            Label5.Visible = True
            DropDownList3.Visible = True
        Else
            Label5.Visible = False
            DropDownList3.Visible = False

        End If
        DropDownList3.SelectedIndex = 0

    End Sub

    Protected Sub DropDownList3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList3.SelectedIndexChanged
        lblRecaudacion.Text = "Total Recaudaciòn: " & 0
        lblAhorro.Text = "Total Ahorro: " & 0
        lblAhorroAdicional.Text = "Total Ahorro Adicional: " & 0

        CargarGrilla()

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        CargarGrilla()

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click


        Select Case DropDownList2.SelectedValue
            Case 1
                lista = "Recaudacion_PagoDiario"
            Case 2
                If DropDownList3.SelectedValue = 1 Then
                    lista = "Recaudacion_Prestamo_Propio"
                Else
                    lista = "Recaudacion_Prestamo_de_Terceros"
                End If

        End Select

        GridView1.AllowPaging = False
        GridView1.Rebind()
        GridView1.ExportSettings.OpenInNewWindow = True
        GridView1.ExportSettings.ExportOnlyData = True
        GridView1.ExportSettings.FileName = "ReporteRecaudacionDiaria___" & lista
        GridView1.MasterTableView.ExportToExcel()
    End Sub
   

#End Region

#Region "Metodos"

    Private Sub ObtenerTipoRecaudacion()
        If Not IsPostBack Then
            Try
                Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
                Dim TiposRecaudacion = d.proc_TipoRecaudacionLoadAll
                Me.DropDownList2.DataSource = TiposRecaudacion
                Me.DropDownList2.DataValueField = "idTipoRecaudacion"
                Me.DropDownList2.DataTextField = "Descripcion"
                Me.DropDownList2.DataBind()
                DropDownList2.SelectedIndex = 0
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If

    End Sub

    Private Sub ObtenerTipoPrestamo()
        Try
            Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            Dim TiposPrestamo = d.proc_TipoPrestamoLoadAll
            Me.DropDownList3.DataSource = TiposPrestamo
            Me.DropDownList3.DataValueField = "idTipoPrestamo"
            Me.DropDownList3.DataTextField = "Descripcion"
            Me.DropDownList3.DataBind()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

  

    Private Sub CargarGrilla()
        Dim dtDiarios As DataTable
        Try
            If DropDownList2.SelectedValue = 1 Then
                dtDiarios = proc_ObtenerRecaudacionDetalles(dtpDesde.SelectedDate.Value, dtpHasta.SelectedDate.Value, DropDownList2.SelectedValue).Tables(0)
                GridView1.DataSource = dtDiarios
                GridView1.DataBind()
            ElseIf DropDownList2.SelectedValue = 2 Then
                dtDiarios = proc_ObtenerRecaudacionDetallesPrestamo(dtpDesde.SelectedDate.Value, dtpHasta.SelectedDate.Value, DropDownList2.SelectedValue, DropDownList3.SelectedValue).Tables(0)
                GridView1.DataSource = dtDiarios
                GridView1.DataBind()
            End If


            If dtDiarios.Rows.Count > 0 Then

                For index As Integer = 0 To dtDiarios.Rows.Count - 1
                    If Not IsDBNull(dtDiarios.Rows(index).Item("importeRecaudacion")) Then
                        ImporteRecaudacion += dtDiarios.Rows(index).Item("importeRecaudacion")
                    End If
                    If Not IsDBNull(dtDiarios.Rows(index).Item("importeAhorro")) Then
                        ImporteAhorro += dtDiarios.Rows(index).Item("importeAhorro")
                    End If
                    If Not IsDBNull(dtDiarios.Rows(index).Item("importeAhorroAdicional")) Then
                        ImporteAhorroAdicional += dtDiarios.Rows(index).Item("importeAhorroAdicional")
                    End If
                Next

                lblRecaudacion.Text = "Total Recaudaciòn: " & FormatCurrency(ImporteRecaudacion, 0)
                lblAhorro.Text = "Total Ahorro: " & FormatCurrency(ImporteAhorro, 0)
                lblAhorroAdicional.Text = "Total Ahorro Adicional: " & FormatCurrency(ImporteAhorroAdicional, 0)

            Else
                'MsgBox("No hay registros")

                lblRecaudacion.Text = "Total Recaudaciòn: " & 0
                lblAhorro.Text = "Total Ahorro: " & 0
                lblAhorroAdicional.Text = "Total Ahorro Adicional: " & 0

            End If

        Catch ex As Exception
            MsgBox("Here!!!" & ex.Message)
        End Try

    End Sub


#End Region

    
  
End Class
