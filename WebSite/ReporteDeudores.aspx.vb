﻿Imports Common
Partial Class ReporteDeudores
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            ObtenerMoviles()
            Me.RadNumericTextBox1.Value = Now.Year
            Me.RadComboBox1.SelectedValue = Now.Month
            procesarDeudores()
        End If


    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        procesarDeudores()

    End Sub

    Private Sub ObtenerMoviles()
        Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
        Dim listamoviles = d.proc_ObtenerNumerosMovil
        Me.cboMovil.DataSource = listamoviles
        Me.cboMovil.DataValueField = "idVehiculo"
        Me.cboMovil.DataTextField = "nroMovil"
        Me.cboMovil.DataBind()
        Me.cboMovil.Items.Insert(0, "Todos")
        cboMovil.SelectedIndex = 0
    End Sub



    Private Sub procesarDeudores()

        Dim fechaInicial As New Date(RadNumericTextBox1.Value, RadComboBox1.SelectedValue, 1)

        Dim dt = proc_ObtenerReporteDeudores(fechaInicial.Month, fechaInicial.Year, IIf(cboMovil.Text = "Todos", -1, cboMovil.SelectedItem.Text)).Tables(0)

        Dim total As Decimal = 0
        For Each r As Data.DataRow In dt.Rows
            total += r.Item("Deuda")
        Next
        lbl1.Text = "Mes Actual"
        lblProm1.Text = Format(total, "$ ###,###,###.##0")
        Me.RadGrid1.DataSource = dt
        Me.RadGrid1.DataBind()

        total = 0
        Dim unMesAntes As Date = fechaInicial.AddMonths(-1)

        dt = proc_ObtenerReporteDeudores(unMesAntes.Month, unMesAntes.Year, IIf(cboMovil.Text = "Todos", -1, cboMovil.SelectedItem.Text)).Tables(0)

        For Each r As Data.DataRow In dt.Rows
            total += r.Item("Deuda")
        Next
        lbl2.Text = unMesAntes.Month & "-" & unMesAntes.Year
        lblProm2.Text = Format(total, "$ ###,###,###.##0")

        total = 0
        Dim dosMesesAntes As Date = unMesAntes.AddMonths(-1)
        dt = proc_ObtenerReporteDeudores(dosMesesAntes.Month, dosMesesAntes.Year, IIf(cboMovil.Text = "Todos", -1, cboMovil.SelectedItem.Text)).Tables(0)

        For Each r As Data.DataRow In dt.Rows
            total += r.Item("Deuda")
        Next
        lbl3.Text = dosMesesAntes.Month & "-" & dosMesesAntes.Year
        lblProm3.Text = Format(total, "$ ###,###,###.##0")

        lblProm.Text = "Promedio dos últimos meses: " & Format((CDbl(lblProm3.Text) + CDbl(lblProm2.Text)) / 2, "$ ###,###,###.##0")
    End Sub

    Protected Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource
        Dim fechaInicial As New Date(RadNumericTextBox1.Value, RadComboBox1.SelectedValue, 1)

        Dim dt = proc_ObtenerReporteDeudores(fechaInicial.Month, fechaInicial.Year, IIf(cboMovil.Text = "Todos", -1, cboMovil.SelectedItem.Text)).Tables(0)



        Me.RadGrid1.DataSource = dt
    End Sub
End Class
