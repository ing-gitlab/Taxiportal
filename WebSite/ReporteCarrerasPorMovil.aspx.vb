﻿Imports Common
Imports Telerik.Charting.Styles
Imports Telerik.Charting
Imports System.Drawing
Imports System.Data.Linq
Imports System.Data
Partial Class ReporteCarrerasPorMovil
    Inherits System.Web.UI.Page

#Region "Eventos"
    Protected Sub ReporteCarrerasPorMovil_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblFecha.Text = "Fecha de Emision: " & Now
        If IsPostBack Then Exit Sub
        dtpDesde.SelectedDate = New Date(Now.Year, Now.Month, 1)
        dtpHasta.SelectedDate = New Date(Now.Year, Now.Month, 1).AddMonths(1).AddDays(-1)
 
        CargarGrilla()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        GridView1.AllowPaging = False
        GridView1.Rebind()
        GridView1.ExportSettings.OpenInNewWindow = True
        GridView1.ExportSettings.ExportOnlyData = True
        GridView1.ExportSettings.FileName = "ReporteCarrerasPorMovil"
        GridView1.MasterTableView.ExportToExcel()
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        CargarGrilla()
    End Sub

#End Region


#Region "Metodos"
    Private Sub CargarGrilla()
        Try
            Dim dtDiarios As DataTable = proc_ObtenerReporteCarrerasPorMovil(dtpDesde.SelectedDate.Value, dtpHasta.SelectedDate.Value).Tables(0)
            GridView1.DataSource = dtDiarios
            GridView1.DataBind()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region




End Class
