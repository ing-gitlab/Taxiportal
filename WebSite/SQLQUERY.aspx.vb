﻿Imports System.Data.SqlClient
Imports System.Data
Imports Common
Partial Class SQLQUERY
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Label1.Text = ""
            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = TextBox1.Text

            c.CommandType = CommandType.Text
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            GridView1.DataSource = ds.Tables(0)
            GridView1.DataBind()

        Catch ex As Exception
            Label1.Text = ex.Message
        End Try
    End Sub

    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class
