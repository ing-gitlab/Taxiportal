﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteMovilesPorCreditos.aspx.vb" Inherits="ReporteMovilesPorCreditos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        
</telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        
</telerik:RadScriptManager>



     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">

 
   </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" >
         <table style="height:100%; width:100%" ><tr><td><img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" /></td></tr></table></telerik:RadAjaxLoadingPanel>

        <br />

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <br />
      <table>
            <tr>
                <td >

        <asp:Label ID="lblTitulo" runat="server" Font-Names="Calibri" 
            Text="Reporte De Moviles por Creditos."></asp:Label>

                </td>
                <td >

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    &nbsp;</td>
                <td >

                    &nbsp;</td>
                <td >

                    &nbsp;</td>
                <td>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    &nbsp;</td>
                </td>
                <td>

        <asp:Label ID="lblFecha" runat="server" Font-Names="Calibri" 
            Text="Fecha Emision del Reporte:"></asp:Label>
                </td>
                <td>

        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button1" runat="server" Text="Exportar" />
                </td>
            </tr>
            </table>
        <br />
                <br />
    
                   
<telerik:RadGrid ID="GridView1" runat="server" AllowSorting="True" 
        GridLines="None" ShowGroupPanel="True" Skin="Vista"
        Font-Size="Small">

<ClientSettings AllowDragToGroup="True">
<Selecting AllowRowSelect="True" />
 </ClientSettings>
 <GroupPanel Text="Arrastre una columna aqui para agrupar">
        
</GroupPanel>
<MasterTableView ShowGroupFooter="true" AllowMultiColumnSorting="true" AutoGenerateColumns="false" >



<Columns>


<telerik:GridBoundColumn  DataField="Dias" HeaderText="Dias en Mora">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="Prestamo" HeaderText="Prestamo">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="Tipo_Prestamo" HeaderText="Tipo de Prestamo">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="FechaInicio" HeaderText="Fecha Inicio del Prestamo">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="NroCuotas" HeaderText="Nro. Cuotas">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="CuotasSaldadas" HeaderText="Cuotas Saldadas">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn   Aggregate="Sum" DataField="ImporteCuotas" HeaderText="Importe Cuotas"  DataFormatString = "{0:C0}" FooterText="Total Importe Cuotas: " >
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  Aggregate="Sum" DataField="ImportePrestado" HeaderText="Importe Prestado" DataFormatString = "{0:C0}" FooterText="Total Importe Prestado: ">
</telerik:GridBoundColumn>


<telerik:GridBoundColumn Aggregate="Sum" DataField="ImporteSaldado" HeaderText="Importe Saldado" DataFormatString = "{0:C0}" FooterText="Total Importe Saldado: ">
</telerik:GridBoundColumn>

<telerik:GridBoundColumn  DataField="Observaciones" HeaderText="Observaciones">
</telerik:GridBoundColumn>

</Columns>


<GroupByExpressions>

 <telerik:GridGroupByExpression >
                    <GroupByFields>
                        <telerik:GridGroupByField FieldName="NroMovil"  SortOrder="Ascending" />
                    </GroupByFields>
                    <SelectFields>
                        <telerik:GridGroupByField  FieldName="NroMovil"  HeaderText="Nro Movil" /> 
                        <telerik:GridGroupByField  FieldName="marca"  HeaderText="Marca" />
                        <telerik:GridGroupByField  FieldName="Modelo"  HeaderText="Modelo" />
                        <telerik:GridGroupByField  FieldName="Placa"  HeaderText="Placa" />
                        <telerik:GridGroupByField  FieldName="nombre"  HeaderText="Conductor" />
                        <telerik:GridGroupByField  FieldName="telefono"  HeaderText="Telefono" />
                    </SelectFields>

 </telerik:GridGroupByExpression>
</GroupByExpressions>

        <RowIndicatorColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
        

</MasterTableView>

        <FilterMenu EnableTheming="True">
        <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        </telerik:RadGrid>
               
        <br />
    
    </div>
    </form>
</body>
</html>
