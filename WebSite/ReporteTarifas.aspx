﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteTarifas.aspx.vb" Inherits="ReporteTarifas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <asp:Label ID="lblTitulo" runat="server" Font-Names="calibri" 
                        Font-Size="X-Large" Text="Tarifas"></asp:Label>
               <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
        <telerik:RadGrid ID="GridView1" runat="server" AllowFilteringByColumn="True" 
            AllowSorting="True" GridLines="None" ShowGroupPanel="True" Skin="Vista">
            <ClientSettings AllowDragToGroup="True">
            </ClientSettings>
<MasterTableView>
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
</MasterTableView>

<FilterMenu EnableTheming="True">
<CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
</FilterMenu>
        </telerik:RadGrid>
               
    </div>
    </form>
</body>
</html>
