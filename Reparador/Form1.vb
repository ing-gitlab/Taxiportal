﻿Imports System.Data
Imports System.Data.SqlClient
Imports Ionic.Zip

Public Class Form1

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Dim z As New ZipFile("c:\compartida\mizip.zip")
            z.AddFile("c:\compartida\dibujo.jpg")
            z.Save()
        Catch ex As Exception

        End Try


        'Dim dtPagosCentral As DataTable = ObtenerPagosCentral()

        'For Each DRcentral As DataRow In dtPagosCentral.Rows
        '    Dim movilplaza As DataTable = ObtenerMovilPlaza(DRcentral("nroMovil"))
        '    If movilplaza.Rows.Count > 0 Then
        '        Dim idVehiculoPlaza As Integer = movilplaza.Rows(0)("idVehiculo")
        '        InsertarPagoPlaza(DRcentral, idVehiculoPlaza)
        '    Else
        '        Console.WriteLine("Este movil no existe en c.plaza " & DRcentral("nroMovil"))
        '    End If

        'Next

    End Sub

    Private Function ObtenerPagosCentral() As DataTable

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "SELECT PAGOSRF.*,0 as mesPago, 0 as anopago, VEHICULOS.NroMovil FROM PAGOSRF INNER JOIN VEHICULOS ON PAGOSRF.IDVEHICULO = VEHICULOS.IDVEHICULO"


            c.CommandType = CommandType.Text
            Dim dbConnectionAutomation As New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings.Get("dbConnectionCentral"))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds.Tables(0)

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerMovilPlaza(nroMovil As Integer) As DataTable

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "SELECT top 1 idVehiculo from Vehiculos where nromovil=@nroMovil"

            c.Parameters.AddWithValue("@nroMovil", nroMovil)

            c.CommandType = CommandType.Text
            Dim dbConnectionAutomation As New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings.Get("dbConnectionPlaza"))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds.Tables(0)

        Catch ex As Exception

        End Try

    End Function

    Private Sub InsertarPagoPlaza(dr As DataRow, idVehiculoPlaza As Integer)
        Dim c As New SqlCommand
        Try


            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "INSERT INTO PAGOSRF(idVehiculo, FechaPago,FechaInhabilitacion," & _
                "Importe, Usuario, Observaciones, mespago, anopago) VALUES(@idVehiculo,@FechaPago,@FechaInhabilitacion," & _
                " @Importe,@Usuario,'',@mesPago,@anoPago)"

            c.Parameters.AddWithValue("@idVehiculo", idVehiculoPlaza)
            c.Parameters.AddWithValue("@FechaPago", dr("FechaPago"))
            c.Parameters.AddWithValue("@FechaInhabilitacion", dr("FechaInhabilitacion"))
            c.Parameters.AddWithValue("@Importe", dr("Importe"))
            c.Parameters.AddWithValue("@Usuario", dr("Usuario"))

            c.Parameters.AddWithValue("@mesPago", dr("mesPago"))
            c.Parameters.AddWithValue("@anoPago", dr("anoPago"))



            c.CommandType = CommandType.Text
            Dim dbConnectionAutomation As New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings.Get("dbConnectionPlaza"))

            c.Connection = dbConnectionAutomation
            c.Connection.Open()
            c.ExecuteNonQuery()
            c.Connection.Close()

        Catch ex As Exception

        Finally
            c.Connection.Close()
        End Try

    End Sub


End Class
