﻿<%@ page language="VB" autoeventwireup="false" inherits="ReporteVales, App_Web_-oarchzm" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reporte de vales</title>
    <style type="text/css">


.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

.radInput_Default
{
	font:12px arial,tahoma,sans-serif;
	vertical-align:middle;
}

    </style>
</head>
<body>

    <form id="form2" runat="server">
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
    </telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="Button1">
                    <UpdatedControls>
                         <telerik:AjaxUpdatedControl ControlID="GridView1"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpHasta"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                        <telerik:AjaxUpdatedControl ControlID="dtpDesde"  LoadingPanelID="RadAjaxLoadingPanel1"/>
                         <telerik:AjaxUpdatedControl ControlID="cboMovil"/>
                          <telerik:AjaxUpdatedControl ControlID="cboAdmin"/>
                          <telerik:AjaxUpdatedControl ControlID="lblTotal"/>
                        <telerik:AjaxUpdatedControl ControlID="lblTitulo"/>
                        <telerik:AjaxUpdatedControl ControlID="chkPago"/>
                        
                    </UpdatedControls>
                </telerik:AjaxSetting>
                               
                
 
            </AjaxSettings>
        </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  BackColor="LightGray"  runat="server" Height="100%"
        Width="100%" Transparency="15" >
        <table style="height:100%; width:100%" >
        <tr>
            <td>
                <img alt="Loading..."  src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>' style="border: 0;" />
            </td>
        </tr>
        </table>
    </telerik:RadAjaxLoadingPanel>

    <div>
    <table>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Desde:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpDesde" Runat="server" 
                        Culture="Spanish (Colombia)">
<TimeView Culture="Spanish (Colombia)"></TimeView>

<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                    </telerik:RadDateTimePicker>
                </td>
                <td valign="middle" class="style1">
                    <asp:Label ID="Label4" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Cliente"></asp:Label>
                </td><td class="style2">
                <asp:DropDownList ID="cboAdmin" runat="server" >
                    </asp:DropDownList>
                </td><td class="style4">
                    &nbsp;<td>
                    <asp:CheckBox ID="chkPago" runat="server" Font-Names="calibri" 
                        Font-Size="Small" Text="Mostrar Pagos" />
                </td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Hasta:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="dtpHasta" Runat="server" 
                        Culture="Spanish (Colombia)">
<TimeView Culture="Spanish (Colombia)"></TimeView>

<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                    </telerik:RadDateTimePicker>
                </td>
                <td class="style1">
                    &nbsp;</td><td>
                    <asp:Button ID="Button1" runat="server" Font-Names="calibri" Font-Size="Small" 
                        Text="Procesar" />
                </td><td class="style4">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
            </tr>
            </table>
    </div>
    <br />
    <asp:Label ID="lblTitulo" runat="server" Font-Names="calibri" 
                        Font-Size="X-Large" Text="Label"></asp:Label>
               <asp:GridView ID="GridView1" runat="server" BackColor="LightGoldenrodYellow" 
        BorderColor="Tan" BorderWidth="1px" CellPadding="6" Font-Names="verdana" 
        Font-Overline="False" Font-Size="X-Small" ForeColor="Black" GridLines="None" 
        CellSpacing="1" AutoGenerateColumns="False">
        <FooterStyle BackColor="Tan" />
                   <Columns>
                       <asp:BoundField DataField="Fecha" HeaderText="Fecha" />
                       <asp:BoundField DataField="nroVale" HeaderText="Num. Vale" />
                       <asp:BoundField DataField="nroMovil" HeaderText="Num. Móvil" />
                       <asp:BoundField DataField="Tarifa" HeaderText="Tarifa" />
                       <asp:BoundField DataField="Pago" HeaderText="Pago" />
                       <asp:BoundField DataField="zonaDestino" HeaderText="Zona" />
                       <asp:BoundField DataField="Dependencia" HeaderText="Dependencia" />
                       <asp:BoundField DataField="Pasajero" HeaderText="Pasajero" />
                       <asp:BoundField DataField="Nombre" HeaderText="Cliente" />
                   </Columns>
        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" 
            HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
        <HeaderStyle BackColor="Tan" Font-Bold="True" />
        <AlternatingRowStyle BackColor="PaleGoldenrod" />
    </asp:GridView>    
               
    <asp:Label ID="lblTotal" runat="server" Font-Names="calibri" 
                        Font-Size="Medium" Text="Total: 0"></asp:Label>
               
    </form>
    
</body>
</html>
