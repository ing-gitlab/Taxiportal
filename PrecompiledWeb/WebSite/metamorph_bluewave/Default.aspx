﻿<%@ page language="VB" autoeventwireup="false" inherits="metamorph_bluewave_Default, App_Web_cyznsb32" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SGT</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="styles.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
    
<div id="bg">
	<div id="pol_left"></div>
    <div id="main">
    <div id="main_g">
    	<!-- header begins -->
        <div id="header">
            <div id="buttons">
                <ul>
                  <li><a href="#"  title="">Home</a></li>
                  <li><a href="#" title="">Blog</a></li>
                  <li><a href="#" title="">Gallery</a></li>
                   <li><a href="#" title="">About Us</a></li>
                  <li><a href="#" title="">Contact Us</a></li>
                </ul>
            </div>
            <div id="logo"><a href="#">metamorph_bluewave</a>
                <h2><a href="http://www.metamorphozis.com/" id="metamorph">Design by Metamorphosis Design</a></h2>
            </div>
        </div>
        <!-- header ends -->
            <!-- content begins -->
            <div id="content">
                <div id="razd">
                    <div id="left">
                        <h1>Metamorphosis Design</h1>
                        <div class="text">
                            <br /><br /> 
                       	</div>
                        <div class="text">
                            <img src="images/img1.jpg" class="img" alt="" />
                            <span>Ut quis magna gravida nibh varius malesuada vitae ac leo.</span> <br />
                            Proin malesuada, nisi at pretium sollicitudin, purus purus sodales purus, sed scelerisque velit ipsum non mauris. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin turpis lorem, vehicula ut rutrum sit amet, laoreet consequat turpis. Sed non imperdiet mauris. Aenean vitae lectus ipsum. Cras luctus molestie nibh id lobortis. Phasellus egestas risus at ligula
                            quis magna gravida nibh varius malesuada vitae ac leo. Proin malesuada, nisi at pretium sollicitudin, purus purus sodales purus, sed scelerisque velit ipsum non mauris. Cum sociis natoque penatibus et magnis dis pa mauris. Cum sociis natoque penatibus et magnis dis parturient... <br />
                      </div>
                        <div class="read"><a href="#">read more</a></div><br />
                         <h1>Free Website Templates</h1>
                        <div class="text">
                            <img src="images/img2.jpg" class="img" alt="" />
                            <span>Suspendisse sit amet dictum justo. </span> <br />
                            In eu viverra velit. In hac habitasse platea dictumst. Quisque ac nisl felis, ac egestas justo. Pellentesque eu odio et nisi auctor vehicula. Etiam porttitor, ligula vel pharetra blandit, ligula neque imperdiet lectus, vel laoreet ipsum eros eget enim. Sed id dignissim nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer in nisi ante, sed egestas est. Morbi ultrices sagittis odio at porttitor. Ut tincidunt ultricies ante eget porttitor. Donec et diam non diam pellentesque
                            dapibus. Vestibulum adipiscing varius arcu, interdum facilisis mauris lobortis in. Proin pellentesque quam vitae mi laoreet vestibulum. Suspendisse iaculis, sapien ut egestas tristique, nibh odio ultrices sapien, sit amet congue massa dolor sit...
                        </div>
                        <div class="read"><a href="#">read more</a></div>
                    </div>
                    <div id="right">
                        <h1>Categories</h1>
                        <ul class="munth">
                        	<li>January  2009</li>
                            <li>February  2009</li>
                            <li>March  2009</li>
                            <li>April  2009</li>
                            <li>May  2009</li>
                            <li>June  2009</li>
                        </ul><br />
                        <h1>Company News</h1>
                        <ul>
                            <li><span class="dat">05-11-2009</span> <br /><span>Lorem ipsum dolor sit amet. </span><br />
                            Consectetur adipiscing elit. Integer ac bibendum lorem. Nunc bibendum scelerisque porta. Curabitur egestas lorem eu sapien lacinia accumsan vulputate dui...
                            <div class="read"><a href="#">read more</a></div>
                            <h1 id="padding"></h1>
                            </li>
                            <li ><span class="dat">05-11-2009</span> <br /><span>Lorem ipsum dolor sit amet. </span><br />
                            Consectetur adipiscing elit. Integer ac bibendum lorem. Nunc bibendum scelerisque porta. Curabitur egestas lorem eu sapien lacinia accumsan vulputate dui...
                            <div class="read"><a href="#">read more</a></div>
                            </li>
                        </ul>
                    </div>
                    <div style="clear: both"><img src="images/spaser.gif" alt="" width="1" height="1" /></div>
                </div>
            </div>
            <!-- content ends -->
            <!-- footer begins -->
            <div id="footer">
          Copyright  2009. Designed by <a href="http://www.metamorphozis.com/" title="Flash Website Templates">Flash Website Templates</a><br />
                <a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a> | <a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional"><abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a> | <a href="http://jigsaw.w3.org/css-validator/check/referer" title="This page validates as CSS"><abbr title="Cascading Style Sheets">CSS</abbr></a></div>
        <!-- footer ends -->
    
    </div>
    </div>
    <div id="pol_right"></div>
	<div style="clear: both"></div>
</div>
<div style="text-align: center; font-size: 0.75em;">Design downloaded from <a href="http://www.freewebtemplates.com/">free website templates</a>.</div></body>

    </form>
</body>
</html>
