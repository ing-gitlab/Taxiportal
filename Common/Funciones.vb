﻿Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Data.SqlClient

Public Module Funciones

    Public USER_GLOBAL As proc_UsuariosLoadByPrimaryKeyResult
    Public CONFIG_GLOBAL As proc_ConfiguracionesLoadByPrimaryKeyResult
    Public CNNSTR_GLOBAL As String

    Public Function NOW_GLOBAL() As DateTime
        Dim D As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
        Return D.proc_GetDate().ToList(0).Now

    End Function


    Public Function Salir() As Boolean
        If MsgBox("Está seguro que desea salir del sistema?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, getSettings("Titulo")) = MsgBoxResult.Yes Then
            Try
                Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
                If USER_GLOBAL Is Nothing Then USER_GLOBAL.idUsuario = "NoUser"

                Return True
            Catch ex As Exception
                Return True
            End Try

        End If
        Return False
    End Function



    Public Function getSettings(ByVal Key As String) As String
        Return System.Configuration.ConfigurationManager.AppSettings.Get(Key)
    End Function

    Public Enum EstadosServicios
        Solicitado = 1
        MovilEnCamino
        EnCurso
        Terminado
        Cancelado

    End Enum

    Public Enum TipoServicios
        Normal = 1
        Rapido
        Programado
    End Enum

    Public Enum TipoMovimientos

        Apertura = 1
        AporteAhorros = 2
        EgresoAhorros = 3
        IncrementoDeuda = 4
        PagoDeuda = 5
        EgresoAhorroAdicional = 6
    End Enum



    Public Function proc_ObtenerPagosVehiculo(ByVal idVehiculo As Integer, ByVal desde As Date, ByVal hasta As Date) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerPagosVehiculo"

            c.Parameters.AddWithValue("@idVehiculo", idVehiculo)
            c.Parameters.AddWithValue("@desde", desde)
            c.Parameters.AddWithValue("@hasta", hasta)

            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Function proc_ObtenerViajesPorCliente(ByVal base As Integer, desde As Date, hasta As Date) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerViajesPorCliente"

            c.Parameters.AddWithValue("@base", base)
            c.Parameters.AddWithValue("@desde", desde)
            c.Parameters.AddWithValue("@hasta", hasta)

            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Function proc_ObtenerReporteDeudores(ByVal mes As Integer, ByVal ano As Integer, ByVal nroMovil As String) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerReporteDeudores"

            c.Parameters.AddWithValue("@mes", mes)
            c.Parameters.AddWithValue("@ano", ano)
            c.Parameters.AddWithValue("@nroMovil", nroMovil)
            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Function proc_ObtenerVehiculoPorNroMovil(ByVal nroMovil As String) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerVehiculoPorNroMovil"

            c.Parameters.AddWithValue("@nroMovil", nroMovil)

            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Function proc_ObtenerPago(ByVal idVehiculo As String, ByVal FechaPago As Date) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerPago"

            c.Parameters.AddWithValue("@idVehiculo", idVehiculo)
            c.Parameters.AddWithValue("@fechaPago", FechaPago)

            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function




    Public Function proc_ObtenerReporteBarrios(ByVal desde As Date, ByVal hasta As Date, ByVal idVehiculo As Integer) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerReporteBarrios"

            c.Parameters.AddWithValue("@desde", desde)
            c.Parameters.AddWithValue("@hasta", hasta)
            c.Parameters.AddWithValue("@idVehiculo", idVehiculo)

            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Function proc_ObtenerReporteRecaudo(ByVal desde As Date, ByVal hasta As Date, ByVal cedula As String) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerReporteRecaudo"

            c.Parameters.AddWithValue("@desde", desde)
            c.Parameters.AddWithValue("@hasta", hasta)
            'If nroMovil IsNot Nothing Then
            '    c.Parameters.AddWithValue("@nroMovil", nroMovil)
            'End If
            If cedula IsNot Nothing Then
                c.Parameters.AddWithValue("@cedula", cedula)
            End If


            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Function proc_ObtenerReporteMovilesNuevos(ByVal desde As Date, ByVal hasta As Date) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerReporteMovilesNuevos"

            c.Parameters.AddWithValue("@desde", desde)
            c.Parameters.AddWithValue("@hasta", hasta)
            'If nroMovil IsNot Nothing Then
            '    c.Parameters.AddWithValue("@nroMovil", nroMovil)




            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Function proc_ObtenerReporteRecaudoPorMovil(ByVal desde As Date, ByVal hasta As Date, ByVal nroMovil As String) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerReporteRecaudoPorMovil"

            c.Parameters.AddWithValue("@desde", desde)
            c.Parameters.AddWithValue("@hasta", hasta)
            'If nroMovil IsNot Nothing Then
            '    c.Parameters.AddWithValue("@nroMovil", nroMovil)
            'End If

            c.Parameters.AddWithValue("@nroMovil", nroMovil)



            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Function proc_ObtenerReporteCarreras(ByVal desde As Date, ByVal hasta As Date, ByVal nroMovil As String) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerReporteCarreras"

            c.Parameters.AddWithValue("@desde", desde)
            c.Parameters.AddWithValue("@hasta", hasta)
            'If nroMovil IsNot Nothing Then
            '    c.Parameters.AddWithValue("@nroMovil", nroMovil)
            'End If

            c.Parameters.AddWithValue("@nroMovil", nroMovil)



            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Function proc_ObtenerReporteRecaudoSinAgrupar(ByVal desde As Date, ByVal hasta As Date, ByVal cedula As String) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerReporteRecaudoSinAgrupar"

            c.Parameters.AddWithValue("@desde", desde)
            c.Parameters.AddWithValue("@hasta", hasta)
            'If nroMovil IsNot Nothing Then
            '    c.Parameters.AddWithValue("@nroMovil", nroMovil)
            'End If
            If cedula IsNot Nothing Then
                c.Parameters.AddWithValue("@cedula", cedula)
            End If


            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function
    Public Function proc_ObtenerServicios(ByVal Fecha As System.Nullable(Of Date), _
                                          ByVal Horas As System.Nullable(Of Integer), _
                                          ByVal enCamino As System.Nullable(Of Boolean), _
                                          ByVal enCurso As System.Nullable(Of Boolean), _
                                          ByVal cancelado As System.Nullable(Of Boolean), _
                                          ByVal terminado As System.Nullable(Of Boolean), _
                                          ByVal radioFrecuencia As System.Nullable(Of Integer)) As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_ObtenerServicios"

            c.Parameters.AddWithValue("@FECHA", Fecha)
            c.Parameters.AddWithValue("@HORAS", Horas)
            c.Parameters.AddWithValue("@enCamino", enCamino)
            c.Parameters.AddWithValue("@enCurso", enCurso)
            c.Parameters.AddWithValue("@cancelado", cancelado)
            c.Parameters.AddWithValue("@terminado", terminado)
            c.Parameters.AddWithValue("@radioFrecuencia", radioFrecuencia)

            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Function proc_VehiculosLoadAll() As DataSet

        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "proc_VehiculosLoadAll"

            c.CommandType = CommandType.StoredProcedure
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnection" & CNNSTR_GLOBAL))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)

            Return ds

        Catch ex As Exception

        End Try


    End Function

    Public Sub StartBroadcast(ByVal Port As Integer, ByVal Message As String)
        Try
            Dim HostInfo As IPHostEntry
            Dim Addresses As IPAddress()
            Dim ip, nodos() As String
            Dim Broadcast As IPAddress = Nothing
            Dim sendbuf As Byte()
            Dim ep As IPEndPoint
            Dim s As Socket

            HostInfo = Dns.GetHostEntry(My.Computer.Name)
            Addresses = HostInfo.AddressList
            'Preparo el buffer con el mensaje a enviar:
            sendbuf = Encoding.ASCII.GetBytes(Message)


            'Recorro todas las IPs del equipo:
            For Each a As IPAddress In Addresses
                Try
                    If a.AddressFamily = AddressFamily.InterNetwork Then
                        'Si es tipo Internet armo la dirección de broadcast reemplazando el último nodo de la IP por 255:
                        nodos = Split(a.ToString(), ".")
                        ip = nodos(0) & "." & nodos(1) & "." & nodos(2) & ".255"
                        Broadcast = IPAddress.Parse(ip)
                        'Creo la dirección de destino del envío:
                        ep = New IPEndPoint(Broadcast, Port)

                        'Envío:
                        s = New Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
                        s.SendTo(sendbuf, ep)

                    End If
                Catch ex As Exception

                End Try

            Next
        Catch ex As Exception
            'Loggear error!
        End Try
    End Sub


End Module

