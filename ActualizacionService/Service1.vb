﻿Imports Common
Imports Common.Funciones
Imports System.IO

Public Class Service1
    Dim WithEvents tmr As New Timers.Timer

    Protected Overrides Sub OnStart(ByVal args() As String)
        tmr.Interval = getSettings("TmrInterval")
        Actualizar()
        tmr.Enabled = True
        EscribirLog("[" & Now & "] - " & " Servicio Iniciado")
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

    Protected Overrides Sub OnPause()
        'MyBase.OnPause()
    End Sub

    Protected Overrides Sub OnContinue()
        'MyBase.OnContinue()
    End Sub

    Private Sub tmr_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmr.Elapsed
        Actualizar()
    End Sub

    Private Sub Actualizar()

        Dim s As New Service.Service

        Dim ListaMoviles = s.ObternerMovilesActualizacion
        ''''''''''''''''''''''''''''

        Dim idVehiculo As Integer : Dim idPago As Integer
        Try
            Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            For Each movil In ListaMoviles
                'actualizar administradores
                Try
                    Dim ListaAdmin = d.proc_AdministradoresLoadByPrimaryKey(movil.cedula).ToList
                    If ListaAdmin.Count = 0 Then
                        d.proc_AdministradoresInsert(movil.nombre, movil.cedula, movil.telefono, (movil.fecha), movil.idUsuario)
                    Else
                        'update admin
                        d.proc_AdministradoresUpdate(movil.nombre, movil.cedula, movil.telefono, (movil.fecha), movil.idUsuario)
                    End If
                Catch ex As Exception
                    EscribirLog("[" & Now & "] - " & ex.Message)
                    Me.ManejoExcepciones(ex.Message)

                End Try

                Try
                    'actualizar vehiculos
                    Dim listaVehiculos As DataTable = Funciones.proc_ObtenerVehiculoPorNroMovil(movil.NroMovil).Tables(0)

                    If listaVehiculos.Rows.Count = 0 Then
                        'crear movil
                        d.proc_VehiculosInsert(idVehiculo, movil.NroMovil, movil.Habilitado, movil.Modelo, movil.Placa, Nothing, Nothing, movil.EstadoSoat, movil.idUsuario, Nothing, _
                                               movil.fechaFinSancion, movil.TipoVehiculo, movil.AdminTemp, movil.AdminTemp2, movil.cedula, movil.fechaAfiliacion, _
                                                Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                    Else
                        'update movil
                        d.proc_VehiculosUpdate(listaVehiculos.Rows(0)("idVehiculo"), movil.NroMovil, movil.Habilitado, movil.Modelo, movil.Placa, Nothing, Nothing, movil.EstadoSoat, movil.idUsuario, Nothing, _
                                               movil.fechaFinSancion, movil.TipoVehiculo, movil.AdminTemp, movil.AdminTemp2, movil.cedula, movil.fechaAfiliacion, _
                                                Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                        idVehiculo = listaVehiculos.Rows(0)("idVehiculo")
                    End If
                Catch ex As Exception
                    EscribirLog("[" & Now & "] - " & ex.Message)
                    Me.ManejoExcepciones(ex.Message)

                End Try


                Try
                    'actualizar pagosRF
                    If (movil.FechaPago) IsNot Nothing Then
                        Dim ListaPagos As DataTable = Funciones.proc_ObtenerPago(idVehiculo, movil.FechaPago).Tables(0)

                        If ListaPagos.Rows.Count = 0 Then
                            d.proc_PagosRFInsert(idPago, idVehiculo, movil.FechaPago, movil.FechaInhabilitacion, movil.Importe, movil.Usuario, Nothing, Nothing, Nothing)
                        Else
                            d.proc_PagosRFUpdate(ListaPagos.Rows(0)("idPago"), idVehiculo, movil.FechaPago, movil.FechaInhabilitacion, movil.Importe, movil.Usuario, Nothing, Nothing, Nothing)
                            idPago = ListaPagos.Rows(0)("idPago")
                        End If
                    End If
                Catch ex As Exception

                    EscribirLog("[" & Now & "] - " & ex.Message)
                    Me.ManejoExcepciones(ex.Message)

                End Try



            Next

        Catch ex As Exception


            EscribirLog("[" & Now & "] - " & ex.Message)
            Me.ManejoExcepciones(ex.Message)


        Finally
            Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")
            StartBroadcast(PORT_Principal, "RefrescarMoviles")

        End Try

    End Sub

    Private Sub EscribirLog(ByVal texto As String)
        Dim filelog As FileStream = Nothing
        Dim escritor As StreamWriter = Nothing
        Try
            filelog = New FileStream(System.AppDomain.CurrentDomain.BaseDirectory & "\log.txt", FileMode.Append, FileAccess.Write)
            escritor = New StreamWriter(filelog)
            escritor.WriteLine(DateTime.Now + " " & texto)
            escritor.Close()
        Finally
            escritor.Dispose()
        End Try
    End Sub


    Private Sub ManejoExcepciones(ByVal Mensaje As String)
        Dim filelog As FileStream = Nothing
        Dim escritor As StreamWriter = Nothing

        Try
            filelog = New FileStream(System.AppDomain.CurrentDomain.BaseDirectory & "\log.txt", FileMode.Append, FileAccess.Write)
            escritor = New StreamWriter(filelog)
            escritor.WriteLine(DateTime.Now + " " & Mensaje)
            escritor.Close()
        Finally
            escritor.Dispose()
        End Try

    End Sub

End Class
