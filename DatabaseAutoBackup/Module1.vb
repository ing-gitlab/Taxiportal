﻿Imports System.Configuration.ConfigurationSettings
Imports System.IO
Imports System.Net.Mail
Imports ICSharpCode
Imports Ionic.Zip
Imports System.Data.SqlClient
Imports Common

Module Module1

    Sub Main()
        Dim filename As String = AppSettings.Get("FileName")
        Dim folderFrom As String = AppSettings.Get("FolderFrom")
        Dim folderTo As String = AppSettings.Get("FolderTo")
        Dim replace As Boolean = AppSettings.Get("Replace")
        Try
            Try
                Shell("net use \\" & AppSettings("PC") & " /user:" & AppSettings("user") & " " & AppSettings("pwd"))

            Catch ex As Exception

            End Try
            Console.WriteLine("iniciando proceso de backup " & Now)
            Dim d As New Common.DataClasses1DataContext(AppSettings.Get("dbConnection"))
            d.BackupDataBase("RTC_255", True)

            System.Threading.Thread.Sleep(5000)
            System.IO.File.Copy(folderFrom & filename, folderTo & Now.Year.ToString & Now.Month.ToString & Now.Day.ToString & filename, replace)

            RestaurarBackup(folderTo & Now.Year.ToString & Now.Month.ToString & Now.Day.ToString & filename)

            Dim z As New ZipFile(folderTo & Now.Year.ToString & Now.Month.ToString & Now.Day.ToString & filename & ".zip")
            z.AddFile(folderTo & Now.Year.ToString & Now.Month.ToString & Now.Day.ToString & filename)
            z.Save()
            System.IO.File.Delete(folderTo & Now.Year.ToString & Now.Month.ToString & Now.Day.ToString & filename)
            Console.WriteLine("Archivo de backup copiado.")
            escribirLog("Archivo de backup copiado.")
            EnviarMail("Backup de Base de Datos de central realizado exitosamente." & vbCrLf & "Fecha/Hora: " & DateTime.Now, "Backup Realizado exitosamente en C. Plaza")

        Catch ex As Exception
            escribirLog(ex.Message)
            Console.WriteLine("Error en backup en central. " & DateTime.Now & vbCrLf & ex.Message, "Error en backup RTC Central")
            EnviarMail("Error en backup en central. " & DateTime.Now & vbCrLf & ex.Message, "Error en backup RTC Central")
        End Try



    End Sub

    Private Sub RestaurarBackup(Path As String)
        Try

            Dim c As New SqlCommand
            Dim a As SqlDataAdapter
            Dim ds As New DataSet

            c.CommandText = "RESTORE DATABASE RTC_255  FROM DISK = '" & Path & "' WITH REPLACE"

            
            c.CommandType = CommandType.Text
            Dim dbConnectionAutomation As New SqlConnection(getSettings("dbConnectionLocalMaster"))

            c.Connection = dbConnectionAutomation

            a = New SqlDataAdapter(c)
            a.Fill(ds)


        Catch ex As Exception

        End Try
    End Sub

    Private Sub EnviarMail(Texto As String, asunto As String)
        Try
            Dim mail As New MailMessage("sistema@rtc.com", AppSettings.Get("Destinatarios"), asunto, Texto)
            Dim smtp As New SmtpClient
            mail.IsBodyHtml = True
            smtp.Host = AppSettings.Get("SMTP")
            smtp.Port = AppSettings.Get("SMTPport")
            smtp.Send(mail)
        Catch ex As Exception

        End Try

    End Sub

    Sub escribirLog(texto As String)
        Dim filelog As FileStream = Nothing
        Dim escritor As StreamWriter = Nothing
        Try
            filelog = New FileStream(System.AppDomain.CurrentDomain.BaseDirectory & "\log.txt", FileMode.Append, FileAccess.Write)
            escritor = New StreamWriter(filelog)
            escritor.WriteLine(DateTime.Now + " " & texto)
            escritor.Close()
        Finally
            escritor.Dispose()
        End Try
    End Sub

End Module
