﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Common
Imports System.Data

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Service
    Inherits System.Web.Services.WebService

    <WebMethod()> _
      Public Function ActualizarEstadoMoviles(ByVal ListaMoviles As List(Of ObtenerMovilesEnvioResult)) As String
        Dim idVehiculo As Integer : Dim idPago As Integer
        Try
            Dim d As New DataClasses1DataContext(getSettings("dbConnection"))
            For Each movil In ListaMoviles
                'actualizar administradores
                Try
                    Dim ListaAdmin As DataTable = Funciones.proc_AdministradoresLoadByPrimaryKey(movil.cedula).Tables(0)
                    If ListaAdmin.Rows.Count = 0 Then
                        d.proc_AdministradoresInsert(movil.nombre, movil.cedula, movil.telefono, Date.Parse(movil.fecha), movil.idUsuario)
                    Else
                        'update admin
                        d.proc_AdministradoresUpdate(movil.nombre, movil.cedula, movil.telefono, Date.Parse(movil.fecha), movil.idUsuario)
                    End If
                Catch ex As Exception

                End Try

                Try
                    'actualizar vehiculos
                    Dim listaVehiculos As DataTable = Funciones.proc_ObtenerVehiculoPorNroMovil(movil.NroMovil).Tables(0)

                    If listaVehiculos.Rows.Count = 0 Then
                        'crear movil
                        d.proc_VehiculosInsert(idVehiculo, movil.NroMovil, movil.Habilitado, movil.Modelo, movil.Placa, Nothing, Nothing, movil.EstadoSoat, movil.idUsuario, Nothing, _
                                               movil.fechaFinSancion, movil.TipoVehiculo, movil.AdminTemp, movil.AdminTemp2, movil.cedula, movil.fechaAfiliacion, _
                                                Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                    Else
                        'update movil
                        d.proc_VehiculosUpdate(listaVehiculos.Rows(0)("idVehiculo"), movil.NroMovil, movil.Habilitado, movil.Modelo, movil.Placa, Nothing, Nothing, movil.EstadoSoat, movil.idUsuario, Nothing, _
                                               movil.fechaFinSancion, movil.TipoVehiculo, movil.AdminTemp, movil.AdminTemp2, movil.cedula, movil.fechaAfiliacion, _
                                                Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                        idVehiculo = listaVehiculos.Rows(0)("idVehiculo")
                    End If
                Catch ex As Exception

                End Try


                Try
                    'actualizar pagosRF
                    If (movil.FechaPago) IsNot Nothing Then
                        Dim ListaPagos As DataTable = Funciones.proc_ObtenerPago(idVehiculo, movil.FechaPago).Tables(0)

                        If ListaPagos.Rows.Count = 0 Then
                            d.proc_PagosRFInsert(idPago, idVehiculo, movil.FechaPago, movil.FechaInhabilitacion, movil.Importe, movil.Usuario, Nothing, Nothing, Nothing)
                        Else
                            d.proc_PagosRFUpdate(ListaPagos.Rows(0)("idPago"), idVehiculo, movil.FechaPago, movil.FechaInhabilitacion, movil.Importe, movil.Usuario, Nothing, Nothing, Nothing)
                            idPago = ListaPagos.Rows(0)("idPago")
                        End If
                    End If
                Catch ex As Exception

                End Try



            Next
            Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")
            StartBroadcast(PORT_Principal, "RefrescarMoviles")
            d.proc_LogInsert("ws", "Actualizacion Exitosa: ", NOW_GLOBAL, "WebService")
            Return "Envio exitoso."
        Catch ex As Exception
            Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")
            StartBroadcast(PORT_Principal, "RefrescarMoviles")

            Return ex.Message
        End Try

    End Function

    <WebMethod()> _
    Public Function ObternerMovilesActualizacion() As List(Of ObtenerMovilesEnvioResult)
        Dim d As New Common.DataClasses1DataContext(getSettings("dbConnection"))

        Try
            StartBroadcast(getSettings("frmPrincipal_PORT"), "InicioActualizacion")
            Dim listaLocal = d.ObtenerMovilesEnvio(Nothing).ToList
            Dim listaRemota As New List(Of ObtenerMovilesEnvioResult)

            For Each reg In listaLocal
                Dim regRemoto As New ObtenerMovilesEnvioResult
                regRemoto.idVehiculo = reg.idVehiculo
                regRemoto.AdminTemp = reg.AdminTemp
                regRemoto.AdminTemp2 = reg.AdminTemp2
                regRemoto.cedula = reg.cedula
                regRemoto.cedula1 = reg.cedula1
                regRemoto.EstadoSoat = reg.EstadoSoat
                regRemoto.fecha = reg.FechaPago
                regRemoto.fechaAfiliacion = reg.fechaAfiliacion
                regRemoto.fechaFinSancion = reg.fechaFinSancion
                regRemoto.FechaInhabilitacion = reg.FechaInhabilitacion
                regRemoto.FechaPago = reg.FechaPago
                regRemoto.Habilitado = reg.Habilitado
                regRemoto.idPago = reg.idPago
                regRemoto.idUsuario = reg.idUsuario
                regRemoto.idUsuario1 = reg.idUsuario1

                regRemoto.Importe = reg.Importe|
                regRemoto.Modelo = reg.Modelo
                regRemoto.NroMovil = reg.NroMovil
                regRemoto.Placa = reg.Placa
                regRemoto.telefono = reg.telefono
                regRemoto.TipoVehiculo = reg.TipoVehiculo
                regRemoto.Usuario = reg.Usuario
                listaRemota.Add(regRemoto)
            Next

            Return listaRemota
        Catch ex As Exception
            MsgBox("Falló el envío. Intente nuevamente mas tarde.")
            d.proc_LogInsert(USER_GLOBAL, ex.Message, NOW_GLOBAL, "Envio actualizacion")
        Finally
            Dim PORT_Principal As Integer = getSettings("frmPrincipal_PORT")
            StartBroadcast(PORT_Principal, "FinActualizacion")
        End Try
    End Function



End Class
