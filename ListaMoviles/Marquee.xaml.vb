﻿Imports System.Windows.Media.Animation
Imports Common

Partial Public Class Marquee

    Dim DC As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
    Dim WithEvents doubleAnimation As New DoubleAnimation()


    Private Sub AnimarPrompter()

        IniciarTexto()
        doubleAnimation.From = Forms.Screen.AllScreens(getSettings("monitor")).WorkingArea.Width
        doubleAnimation.To = tbmarquee.ActualWidth * -1
        'doubleAnimation.RepeatBehavior = RepeatBehavior.Forever
        doubleAnimation.Duration = New Duration(New TimeSpan(0, 0, (tbmarquee.ActualWidth + Forms.Screen.AllScreens(getSettings("monitor")).WorkingArea.Width) / (Forms.Screen.AllScreens(getSettings("monitor")).WorkingArea.Width / 10)))
        tbmarquee.BeginAnimation(Canvas.LeftProperty, doubleAnimation)
    End Sub

    Private Sub UserControl_Loaded(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded

        AnimarPrompter()

    End Sub

    Private Sub doubleAnimation_Completed(ByVal sender As Object, ByVal e As System.EventArgs) Handles doubleAnimation.Completed

        AnimarPrompter()
    End Sub

    Private Sub IniciarTexto()

        Try
            tbmarquee.Text = DC.proc_ConfigGlobalLoadAll().First.TextoPrompter

        Catch ex As Exception

        End Try

       


    End Sub
End Class
