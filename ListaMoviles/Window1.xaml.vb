﻿Imports Common
Imports System.Net.Sockets
Imports System.Net
Imports System.Text
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Collections.ObjectModel




Class Window1
    Dim WithEvents tmr As New Timers.Timer
    Private Delegate Sub SetearReloj()
    Private Delegate Sub SetearMovilRampa(ByVal idVehiculo As String)
    Private Delegate Sub RefrescarMoviles()
    Private Delegate Sub SetearMovilServicio(ByVal idServicio As String)
    Private _RecordCollection As New ObservableCollection(Of Moviles)()


    Dim RAMPA_PORT As Integer = CInt(getSettings("Rampa_PORT"))
    Dim udp As New UdpClient(RAMPA_PORT)
    Dim d As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))

    Private Sub MovilARampa(ByVal nroMovil As String)
        Rampa.SetearMovil("Móvil " & nroMovil & " a Rampa")
        ' obtenerMovilesEnCola()
    End Sub

    Private Sub MovilAServicio(ByVal idServicio As String)
        Try
            Dim DataClassContext As New DataClasses1DataContext(getSettings("dbConnection" & CNNSTR_GLOBAL))
            Dim ListaServicios = DataClassContext.proc_ServiciosLoadByPrimaryKey(idServicio).ToList()
            Dim ListaMovil = DataClassContext.proc_VehiculosLoadByPrimaryKey(ListaServicios(0).idVehiculo1).ToList()
            Dim ListaBarrios = DataClassContext.proc_BarriosLoadByPrimaryKey(ListaServicios(0).idBarrioDestino).ToList()
            Rampa.SetearMovil("Móvil " & ListaMovil(0).NroMovil & " a " & ListaBarrios(0).descripcion)
            ' obtenerMovilesEnCola()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Delegate_Data(ByVal Data As IAsyncResult)

        Dim groupEP As New IPEndPoint(IPAddress.Any, RAMPA_PORT)
        Dim bytes As Byte() = udp.EndReceive(Data, groupEP)
        Dim STR As String = Encoding.ASCII.GetString(bytes, 0, bytes.Length)

        If STR.Contains("Refresca") Then
            Dispatcher.Invoke(New RefrescarMoviles(AddressOf obtenerMovilesEnCola))
        Else
            If STR.Contains("Movil") Then
                Dispatcher.Invoke(New SetearMovilRampa(AddressOf MovilARampa), STR.Replace("Movil", String.Empty))
            ElseIf STR.Contains("Servicio") Then
                Dispatcher.Invoke(New SetearMovilServicio(AddressOf MovilAServicio), STR.Replace("Servicio", String.Empty))

            End If
        End If

        udp.BeginReceive(AddressOf Delegate_Data, Nothing)
    End Sub

    Private Sub RefrescarGrid()
        obtenerMovilesEnCola()
    End Sub

    Private Sub Window1_Loaded(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
        tmr.Interval = 1000
        tmr.Enabled = True
        Me.WindowState = Windows.WindowState.Normal


        Try
            Me.WindowStartupLocation = WindowStartupLocation.Manual
            Dim workingArea As System.Drawing.Rectangle = System.Windows.Forms.Screen.AllScreens(getSettings("Monitor")).WorkingArea
            Me.Left = workingArea.Left
            Me.Top = workingArea.Top
            'Me.Width = workingArea.Width
            'Me.Height = workingArea.Height
            Me.WindowState = WindowState.Maximized
            Me.WindowStyle = WindowStyle.None

        Catch ex As Exception

        End Try

        Me.Topmost = False
        Me.Show()

        Me.DataGridControl1.ItemsSource = Me._RecordCollection
        Me.DataGridControl1.Columns(0).Title = "Posición"
        Me.DataGridControl1.Columns(1).Title = "Móvil"

        obtenerMovilesEnCola()
        udp.BeginReceive(AddressOf Delegate_Data, Nothing)


    End Sub

    

    Sub setearHora()
        lblHora.Content = Now
    End Sub

    Private Sub tmr_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmr.Elapsed
        Dispatcher.Invoke(New SetearReloj(AddressOf setearHora))
    End Sub

    Private Sub obtenerMovilesEnCola()
        Try

            Dim lista = d.proc_ObtenerMovilesEnCola().ToList()
            Me._RecordCollection.Clear()
            For Each elemento As proc_ObtenerMovilesEnColaResult In lista
                Dim movil As New Moviles(elemento.Posicion, elemento.nroMovil)
                Me._RecordCollection.Add(movil)

            Next




            'Me.DataGridControl1.ItemsSource = lista
            'Me.DataGridControl1.Columns("Zona").Visible = False
            'Me.DataGridControl1.Columns("placa").Visible = False
            'Me.DataGridControl1.Columns("nroMovil").Title = "Nro. Móvil"
            'Me.DataGridControl1.Columns("Hora_Llegada").Visible = False
            'Me.DataGridControl1.Columns("Posicion").Title = "Posición"

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button1.Click

        obtenerMovilesEnCola()
    End Sub


    Private Sub Rampa_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.DragEventArgs)

    End Sub


    Private Sub Window1_StateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.StateChanged
        If Me.WindowState = Windows.WindowState.Maximized Then
            Me.WindowStyle = Windows.WindowStyle.None
        Else
            Me.WindowStyle = Windows.WindowStyle.SingleBorderWindow
        End If

    End Sub
End Class
