﻿Public Class Moviles
    Private _Posicion As New TextBlock()
    Private _Movil As New TextBlock()

    Public ReadOnly Property Posicion() As TextBlock
        Get
            Return Me._Posicion
        End Get
    End Property
    Public ReadOnly Property Movil() As TextBlock
        Get
            Return Me._Movil
        End Get
    End Property

    Public Sub New(ByVal Posicion As Int32, ByVal Movil As Int32)
        Me._Posicion.Text = Posicion.ToString()
        Me._Movil.Text = Movil.ToString()
    End Sub

    Public Function GetPosicion() As Int32
        Return Int32.Parse(Me._Posicion.Text)
    End Function

    Public Function GetMovil() As Int32
        Return Int32.Parse(Me._Movil.Text)
    End Function

End Class
