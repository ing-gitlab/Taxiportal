﻿Imports Common
Partial Public Class Rampa
    Dim WithEvents tmr As New Timers.Timer
    Private Delegate Sub EsconderBanner()

    Public Sub SetearMovil(ByVal Texto As String)
        txtMovil.Text = Texto
        Me.Visibility = Windows.Visibility.Visible
        Me.tmr.Interval = getSettings("Interval")
        Me.tmr.Enabled = True

    End Sub

    Private Sub OcultarBanner()
        Me.Visibility = Windows.Visibility.Hidden
        Me.tmr.Enabled = False
    End Sub

    Private Sub tmr_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmr.Elapsed
        Me.Dispatcher.Invoke(New EsconderBanner(AddressOf OcultarBanner))

    End Sub



End Class
